<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Ahlan Guide',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >
               <input type="file" name="img" class="propic" id="file" onchange="readURL(this);">
                <label for="file" id="showimage"></label></form>',
                
    'logo_mini' => '',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        [
            'text' => 'Blog',
            'url'  => 'admin/blog',
            'can'  => 'manage-blog',
        ],
        [
            'text' => 'Dashboard',
            'url'  => '/home',
            'icon' => 'home',
            'class' => 'hidden-xs',
            'can' => 'super_admin'
        ],
        [
            'text' => 'Dashboard',
            'url'  => '/home',
            'icon' => 'home',
            'class' => 'hidden-xs',
            'can' => 'agent'
        ],
        [
            'text' => 'Dashboard',
            'url'  => '/home',
            'icon' => 'home',
            'class' => 'hidden-xs',
            'can' => 'staff'
        ],
        [
            'text' => 'Travel Agencies',
            'url'  => 'admin/travel_agency',
            'icon' => 'home',
            'can' => 'super_admin'
        ],
        [
            'text' => 'Packages',
            'url'  => 'admin/packages',
            'icon' => 'cubes',
            'can' => 'super_admin'
        ],
        [
            'text' => 'Task Management',
            'icon' => 'plane',
            'can' => 'agent',
            'url' => '/admin/trip/task/index'
        ],
        [
            'text' => 'Trip Management ',
            'icon' => 'plane',
            'can' => 'agent',
            'url' => 'admin/trips',
        ],
        [
            'text' => 'Client Management',
            'icon' => 'users',
            'can' => 'agent',
            'url'  => 'admin/clients',
            //'submenu' => [
                //[
                    //'text' => 'Categories',
                  //  'url'  => 'admin/client_categories',
                //],
                //[
                    //'text' => 'Sub Categories',
                  //  'url'  => 'admin/client_sub_categories',
                //],
                //[
                    
              //  ],
            //]
        ],
        [
            'text' => 'Alerts',
            'icon' => 'user',
            'can' => 'agent',
            'url'  => 'admin/emergency_alert/all',
        ],
        // [
        //     'text' => 'Step Pack',
        //     'icon' => 'cubes',
        //     'url' => 'admin/step_pack',
        //     'can' => 'agent'
        // ],
        //[
          //  'text'  => 'Airlines',
            //'icon'  => 'plane',
            //'url'   => 'admin/airlines',
            //'can'   => 'agent'
        //],
        //[
          //  'text'  => 'Airports',
            //'icon'  => 'plane',
            //'url'   => 'admin/airports',
            //'can'   => 'agent'
        //],

        [
            'text'  => 'Staff Management',
            'icon'  => 'users',
            'url'   => 'admin/staffs',
            'can'   => 'agent'
        ],
        [
            'text'  => 'Client Message',
            'icon'  => 'users',
            'url'   => 'admin/trip/client/messages',
            'can'   => 'agent'
        ],
        [
            'text'  => 'Feedback',
            'icon'  => 'random',
            'url'   => 'admin/feedbacks',
            'can'   => 'agent'
        ],
        [
            'text'  => 'Tasks',
            'icon'  => 'tasks',
            'url'   => 'staff/tasks',
            'can'   => 'staff'
        ],

        [
            'text' => 'Trip Management ',
            'icon' => 'plane',
            'url' => 'admin/trips',
            'can'   => 'mobile_agent',
        ],

        [
            'text'  => 'Client Message',
            'icon'  => 'users',
            'url'   => 'admin/trip/client/messages',
            'can'   => 'mobile_agent'
        ],
        [
            'text' => 'Alerts',
            'icon' => 'user',
            'can'  => 'mobile_agent',
            'url'  => 'admin/emergency_alert/all',
        ],
        
        [
            'text' => 'Master Setting',
            'icon' => 'random',
            'can'  => 'agent',
            'submenu' => [
                [
                    'text' => 'Countries',
                    'url'  => 'admin/countries',
                    'icon' => 'lock',
                ],
                [
                    'text' => 'Client Categories',
                    'url'  => 'admin/client_categories',
                    'icon' => 'lock',
                ],
                [
                    'text' => 'Client Sub Categories',
                    'url'  => 'admin/client_sub_categories',
                    'icon' => 'lock',
                ],
                [
                    'text' => 'Airlines',
                    'url'  => 'admin/airlines',
                    'icon' => 'lock',
                ],
                [
                    'text' => 'Airports',
                    'url'  => 'admin/airports',
                    'icon' => 'lock',
                ],
                [
                    'text' => 'Hotels',
                    'url'  => 'admin/hotels',
                    'icon' => 'lock',
                ],
                [
                    'text' => 'Feedback Categories',
                    'url'  => 'admin/feedback/categories',
                    'icon' => 'lock',
                ],
                [
                    'text' => 'Change Password',
                    'url'  => 'admin/change_password/changepassword',
                    'icon' => 'lock',
                ],

            ],
        ],
        [
            'text' => 'Profile',
            'url'  => 'admin/change_password',
            'icon' => 'lock',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];
