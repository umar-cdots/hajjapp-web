<?php

use Illuminate\Database\Seeder;

class AirportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$airports 		= array();
    	$csvFile 		= storage_path("airports.csv");
        $file_handle 	= fopen($csvFile, 'r');
	    while (!feof($file_handle)) 
	    {
	        $airports[] = fgetcsv($file_handle, 1024);
	    }
	    fclose($file_handle);

	    $data 			= [];
	    foreach($airports as $airport)
	    {
	    	if(empty($airport[0]))
	    		break;
	    	
	    	$data[]		= ['name' => $airport[0], 'code' => $airport[3], 'city_id' => \App\City::where('name', $airport[1])->first()->id ?? null, 'country_id' => \App\Country::where('name', $airport[2])->first()->id ?? null];
	    }

	    \App\Airport::insert($data);
    }
}
