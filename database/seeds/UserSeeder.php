<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 5)->create()->each(function($user){
    		$faker 	= \Faker\Factory::create();
        	if($user->user_type == 'client')
        	{
        		$client = new App\Client;

        		$client->user_id 		= $user->id;
        		$client->first_name 	= $user->name;
        		$client->last_name 		= $faker->lastName;
        		$client->dob 			= $faker->dateTimeThisCentury->format('Y-m-d');
        		$client->gender 		= array('male', 'female')[rand(0,1)];
        		$client->save();
        	}
        	elseif($user->user_type == 'agent' || $user->user_type == 'staff')
        	{
        		$travel_agency 	= new App\TravelAgency;

        		$travel_agency->agency_name = $faker->company ;
        		$travel_agency->package_id 	= 1;
        		$travel_agency->status 		= 'active';
        		$travel_agency->save();

        		$agent 			= new App\Agent;

        		$agent->travel_agency_id = $travel_agency->id;
        		$agent->user_id 		 = $user->id;
        		$agent->first_name 		 = $user->name;
        		$agent->last_name 		 = $faker->lastName;
        		$agent->save();
        	}
        });
    }
}
