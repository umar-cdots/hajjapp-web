<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropStepSectionInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('step_section_information');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        require_once database_path('migrations/2019_07_19_133725_create_step_section_information_table.php');

        $obj = new CreateStepSectionInformationTable();
        $obj->up();
    }
}
