<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('travel_agency_id');
            $table->integer('step_pack_id')->nullable();
            $table->string('title');
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->string('path');
            $table->boolean('end_trip')->nullable();
            $table->boolean('is_qibla_widget_enable');
            $table->bigInteger('capacity')->nullable();
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
