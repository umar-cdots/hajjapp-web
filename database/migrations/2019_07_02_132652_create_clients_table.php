<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->integer('client_category_id')->nullable();
            $table->integer('sub_client_category_id')->nullable();
            $table->string('primary_phone_number')->nullable();
            $table->string('secondary_phone_number')->nullable();
            $table->string('emergency_phone_number')->nullable();
            $table->string('occupation')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('decease')->nullable();
            $table->string('special_needs')->nullable();
            $table->integer('nationality')->nullable();
            $table->text('postal_address')->nullable();
            $table->date('dob');
            $table->enum('gender', ['male', 'female', 'not_specified']);
            $table->string('age')->nullable();
            $table->string('qualification')->nullable();
            $table->text('personality_comments')->nullable();
            $table->string('picture_path')->nullable();
            $table->integer('created_by_agent_id')->nullable();
            $table->integer('updated_by_agent_id')->nullable();
            $table->string('total_paid_amount')->nullable();
            $table->boolean('status')->nullable();
            $table->string('description')->nullable();
            $table->enum('paid_amount', ['yes', 'no'])->nullable();
            $table->integer('travel_agency_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
