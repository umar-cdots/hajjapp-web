<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientTripStepsColumnChanged extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_trip_steps', function (Blueprint $table) {
            $table->dropColumn('trip_step_id');
            $table->integer('step_id')->after('client_trip_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_trip_steps', function (Blueprint $table) {
            $table->integer('trip_step_id');
            $table->dropColumn('step_id');
        });
    }
}
