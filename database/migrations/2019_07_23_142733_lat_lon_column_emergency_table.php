<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LatLonColumnEmergencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emergency_alerts', function (Blueprint $table) {
            $table->double('lat')->after('reply');
            $table->double('lon')->after('lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emergency_alerts', function (Blueprint $table) {
            $table->dropColumn(['lat', 'lon']);
        });
    }
}
