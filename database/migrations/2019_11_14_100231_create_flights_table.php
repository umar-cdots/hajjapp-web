<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('trip_id');
            $table->integer('airline_id');
            $table->datetime('departure_datetime');
            $table->datetime('arrival_datetime');
            $table->integer('from_airport_id');
            $table->integer('to_airport_id');
            $table->string('pnr')->nullable();
            $table->string('terminal');
            $table->string('gate');
            $table->string('note');
            $table->integer('travel_agency_id')->nullable();
            $table->softDeletes()->nullable();
            $table->timestamps()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
