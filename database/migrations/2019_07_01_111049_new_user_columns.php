<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewUserColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('fcm_token')->nullable();
            $table->enum('user_type', ['super_admin', 'staff', 'agent', 'client']);
            $table->text('fcm_token')->nullable();
            $table->enum('status', ['active', 'suspended', 'deactive', 'pending', 'banned'])->default('pending');
            $table->boolean('login_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['fcm_token', 'user_type', 'status']);
        });
    }
}
