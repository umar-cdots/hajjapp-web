<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmergencyAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergency_alerts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_trip_id');
            $table->text('message')->nullable();
            $table->text('reply')->nullable();
            $table->enum('status', ['sent', 'seen', 'replied', 'solved'])->default('sent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergency_alerts');
    }
}
