<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TripTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('staff_id')->unsigned()->nullable();
            $table->integer('trip_id')->unsigned()->nullable();
            $table->string('task_title');
            $table->enum('status', ['Inprogress','Upcoming','Complete'])->nullable();
            $table->date('due_date')->nullable();
            $table->time('due_time')->nullable();
            $table->date('actual_date')->nullable();
            $table->time('actual_time')->nullable();
            $table->tinyInteger('delay');
            $table->text('description')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
            $table->date('start_date');
            $table->integer('travel_agency_id')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_tasks');
    }
}
