<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTripAirlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_trip_airlines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('flight_id');
            $table->integer('client_trip_id');
            $table->text('feedback')->nullable();
            $table->enum('status', ['upcoming_flight', 'on_flight', 'flight_over'])->default('upcoming_flight');
            $table->softDeletes();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_trip_airlines');
    }
}
