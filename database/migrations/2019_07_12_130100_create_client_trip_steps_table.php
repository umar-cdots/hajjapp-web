<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTripStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_trip_steps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_trip_id');
            $table->integer('trip_step_id');
            $table->enum('status', ['current', 'completed'])->default('current');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_trip_steps');
    }
}
