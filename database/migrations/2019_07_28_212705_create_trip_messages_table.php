<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('travel_agency_id');
            $table->integer('trip_id');
            $table->integer('client_id');
            $table->integer('agent_id');
            $table->text('message');
            $table->boolean('admin_seen_status')->nullable();
            $table->boolean('client_seen_status')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_messages');
    }
}
