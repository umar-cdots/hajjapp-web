<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTripStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename("trip_steps", "trip_step_packs");
        Schema::table('trip_step_packs', function (Blueprint $table) {
            $table->dropColumn(['step_id', 'sort']);
            $table->integer('step_pack_id')->after('trip_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename("trip_step_packs", "trip_steps");
        Schema::table('trip_steps', function (Blueprint $table) {
            $table->dropColumn('step_pack_id');
            $table->integer('step_id');
            $table->integer('sort');
        });
    }
}
