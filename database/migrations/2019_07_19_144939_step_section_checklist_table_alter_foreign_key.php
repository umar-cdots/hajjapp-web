<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StepSectionChecklistTableAlterForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('step_section_check_lists', function (Blueprint $table) {
            $table->integer('step_section_id')->after('id');
            $table->dropColumn('step_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('step_section_check_lists', function (Blueprint $table) {
            $table->integer('step_id')->after('id');
            $table->dropColumn('step_section_id');
        });
    }
}
