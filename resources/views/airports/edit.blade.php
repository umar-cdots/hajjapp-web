@extends('adminlte::page')
@section('title', 'Edit New Airport')
@section('content_header')
<h1>Edit Airport</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection
@section('content')
	<div class="box">
		<div class="box-header">
			{{-- <h3 class="box-title">Edit Airport</h3> --}}
		</div>
		<form action="{{ route('airports.update' , $airports->id) }}" method="POST">
			@csrf
			@method('PUT')
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>Name <span>*</span></label>
						<input type="text" name="name" class="form-control" value="{{ $airports->name }}">
						@if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">
						<label>Code <span>*</span></label>
						<input type="text" name="code" class="form-control" value="{{ $airports->code }}">
						@if($errors->has('code'))
                                <span class="help-block text-danger">{{ $errors->first('code') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
						<label>Country <span>*</span></label>
						<select class="form-control country" name="country" id="country" multiple="multiple">
							@foreach($countries as $country)
								<option value="{{ $country->id }}" {{ ($airports->country_id == $country->id) ? 'selected' : 'none' }}>{{ $country->name }}</option>
							@endforeach
						</select>
						@if($errors->has('country'))
                                <span class="help-block text-danger">{{ $errors->first('country') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
						<label>City <span>*</span></label>
						<select class="form-control city" name="city" id="city" multiple>
							<option value="{{$airports->city_id}}" selected>{{($airports->city?$airports->city->name:"")}}</option>
						</select>
						@if($errors->has('city'))
                                <span class="help-block text-danger">{{ $errors->first('city') }}</span>
                         @endif
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer text-center">
			<button type="submit" class="btn btn-primary btnSubmit">Update</button>
		</div>
		</form>
	</div>
@endsection
@push('js')
<script type="text/javascript">
	$(".country").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Country"//placeholder
   });
   $(".city").select2({
        maximumSelectionLength: 1,
        placeholder: "Select City"//placeholder
   });

	$('#country').change(function () {
		var country_id = $(this).val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			}
		});
		$.ajax({
			url : "{{ route('ajax.load.cities') }}",
			type: "POST",
			dataType: 'JSON',
			data: { country_id: country_id},
			success: function(response){
				$(".city").empty();
				var cities = '<option value="">Select City</option>';
				if(response.status == 'success'){
					$(response.cities).each( function(i,d){
						cities += '<option value="'+ d.id +'">'+ d.name +'</option>';
					});

					$(".city").html(cities);
					
				}else{
					toastr['error']('Something went wrong.');
				}
			},
			error: function(error){
				toastr['error']('Something went wrong.');
			}
		});
	});
</script>
@endpush
