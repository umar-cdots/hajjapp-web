@extends('adminlte::page')
@section('title', 'Airports')

@section('content_header')
<h1>Airports</h1>
{{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection

@section('content')
	<div class="box">
		<div class="box-body travelAgencyTableBtn">
       <div class="col-md-12 text-right">
        <a href="{{ route('airports.create') }}" class="btn btn-primary btnSubmit">Add Airport</a>
      </div>
      <div class="clearfix"></div>
			<table id="airlines" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th>Sr#</th>
          	<th>Name</th>
            <th>Code</th>
          	<th>Country</th>
           <th>City</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($airports as $airport)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $airport->name }}</td>
          <td>{{ $airport->code }}</td>
          <td>{{ $airport->country->name??"" }}</td>
          <td>{{ $airport->city->name??"" }}</td>
          <td>
          	<a href="{{ route('airports.edit', $airport->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
          	{{-- <a href="{{ route('airports.show', $airport->id) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Step"><i class="fa fa-eye"></i></button></a> --}}
          	<form action="{{ route('airports.destroy', $airport->id) }}" method="POST" class="delete_item">
          		@csrf
          		@method('DELETE')
          		<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
          	</form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
		</div>
	</div>
@endsection
@push('js')
  <script type="text/javascript">
    $(function () {
      $('#airlines').DataTable({
      	'lengthChange': true,
        'searching': false,
        "columnDefs": [
                 { orderable: false, targets: 3}
                 ],
      });
    });
  </script>
@endpush
