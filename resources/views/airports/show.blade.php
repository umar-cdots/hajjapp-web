@extends('adminlte::page')
@section('title', 'Airport Details')

@section('content_header')
<h1>Airport Details</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
  <div class="box">
            <div class="box-header">
              <a href="{{ route('airports.create') }}" class="btn btn-primary">Add Airport</a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
               <tbody>
               
                <tr>
                  <th>Name</th>
                  <td>{{ $airport->name }}</td>
                </tr>
                 <tr>
                   <th>Code</th>
                   <td>{{ $airport->code }}</td>
                 </tr>
                 <tr>
                   <th>Country</th>
                   <td>{{ $airport->country->name }}</td>
                 </tr>
                 <tr>
                   <th>City</th>
                   <td>{{ $airport->city->name }}</td>
                 </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection