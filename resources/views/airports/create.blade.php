@extends('adminlte::page')
@section('title', 'Create New Airport')
@section('content_header')
<h1>Create New Airport</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::previous() }}">Back</a></li>
</ol>
<div class="clearfix"></div>
@endsection
@section('content')
	<div class="box">
		<form action="{{ route('airports.store') }}" method="POST">
			@csrf
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>Name <span>*</span></label>
						<input type="text" name="name" class="form-control" value="{{ old('name') }}">
						@if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
						<div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">
						<label>Code <span>*</span></label>
						<input type="text" name="code" class="form-control" value="{{ old('code') }}">
						@if($errors->has('code'))
                                <span class="help-block text-danger">{{ $errors->first('code') }}</span>
                         @endif
					</div>
				</div>
{{--				<div class="col-md-3">--}}
{{--					<div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">--}}
{{--						<label>Code <span>*</span></label>--}}
{{--						<input type="text" name="code" class="form-control" value="{{ old('code') }}">--}}
{{--						@if($errors->has('code'))--}}
{{--                                <span class="help-block text-danger">{{ $errors->first('code') }}</span>--}}
{{--                         @endif--}}
{{--					</div>--}}
{{--				</div>--}}
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
						<label>Country <span>*</span></label>
						<select class="form-control country" name="country" id="country" multiple="multiple">
							@foreach($countries as $country)
								<option value="{{ $country->id }}">{{ $country->name }}</option>
							@endforeach
						</select>
						@if($errors->has('country'))
                                <span class="help-block text-danger">{{ $errors->first('country') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
						<label>City <span>*</span></label>
						<select class="form-control city" name="city" id="city" multiple>
						</select>
						@if($errors->has('city'))
                                <span class="help-block text-danger">{{ $errors->first('city') }}</span>
                         @endif
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer text-center">
			<button type="submit" class="btn btn-primary btnSubmit">Create</button>
		</div>
		</form>
	</div>
@endsection

@push('js')
<script type="text/javascript">
	$(".country").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Country"//placeholder
   });
   $(".city").select2({
        maximumSelectionLength: 1,
        placeholder: "Select City"//placeholder
   });

	$('#country').change(function () {
		var country_id = $(this).val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			}
		});
		$.ajax({
			url : "{{ route('ajax.load.cities') }}",
			type: "POST",
			dataType: 'JSON',
			data: { country_id: country_id},
			success: function(response){
				$(".city").empty();
				var cities = '<option value="">Select City</option>';
				if(response.status == 'success'){
					$(response.cities).each( function(i,d){
						cities += '<option value="'+ d.id +'">'+ d.name +'</option>';
					});

					$(".city").html(cities);
					
				}else{
					toastr['error']('Something went wrong.');
				}
			},
			error: function(error){
				toastr['error']('Something went wrong.');
			}
		});
	});
</script>
@endpush
