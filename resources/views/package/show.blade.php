@extends('adminlte::page')
@section('title', 'Package Detail')

@section('content_header')
<h1>Package Detail</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection

@section('content')
	<div class="box">
          @if(\Auth::user()->user_type == "super_admin")
            <div class="box-header">
              <a href="{{ route('packages.create') }}" class="btn btn-primary">Add New</a>
            </div>
            @endif
             <div class="slideanim" id="packageSelect"
                 style="display: {{ empty(old('package_id')) ? 'unset' : 'none' }};">
                
                    <div class="col-md-4">
                        <div class="packagesList packagesList1">
                            <div class="pkgHead pkgHead1">{{ $package->title }}</div>
                            <div class="pkgDet">
                                <h2>{{ $package->title }}</h2>
                                {{--  <h2>0.00</h2>
                                 <h3>per 30 Days</h3> --}}
                                <ul>
                                    <li>No. of Trips <b>{{ $package->no_of_trips }}</b></li>
                                    <li>No. of Customers <b>{{ $package->no_of_customers }}</b></li>
                                    <li>No. of Guides <b>{{ $package->no_of_guides }}</b></li>
                                    <li>No. of Steps <b>{{ $package->no_of_steps }}</b></li>
                                    <li>Start Date <b>{{ $package->start_date }}</b></li>
                                    <li>Expiry Date <b>{{ $package->expiry_date }}</b></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <div class="clearfix"></div>
            </div>
            
            <!-- /.box-body -->
          </div>
@endsection