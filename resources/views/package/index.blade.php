@extends('adminlte::page')
@section('title', 'All Packages')

@section('content_header')
<h1>All Packages</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
	<div class="box">
    <div class="box-body travelAgencyTableBtn">
      <div class="col-md-6">
        {{-- <h3 class="box-title">All Packages</h3> --}}
      </div>
      @if(\Auth::user()->user_type == "super_admin")
      
      @endif
      <div class="text-right">
        <a href="{{ route('packages.create') }}" class="btn btn-primary btnSubmit">Add Package</a>
      </div>
      <table id="travelAgency" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th>Sr#</th>
          	<th>Package Name</th>
          	<th>No. of Trips</th>
            <th>No. of Clients</th>
            <th>No. of Steps</th>
            <th>No. of Section</th>
{{--            <th>Start Date</th>--}}
{{--            <th>Expiry Date</th>--}}
          	<th>Action</th>
             @if(\Auth::user()->user_type == "agent")
              <th>Expire Package</th>
            @endif
        </tr>
        </thead>
        <tbody>
        	@foreach($packages as $package)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $package->title }}</td>
          <td>{{ $package->no_of_trips }}</td>
          <td>{{ $package->no_of_customers }}</td>
          <td>{{ $package->no_of_steps }}</td>
          <td>{{ $package->no_of_section }}</td>
{{--          <td>{{ $package->start_date ?? '--' }}</td>--}}
{{--          <td>{{ $package->expiry_date ?? '--' }}</td>--}}
          <td>
          	@if(\Auth::user()->user_type == "super_admin")
            <a href="{{ route('packages.edit', $package->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
            @endif
          	{{-- <a href="{{ route('packages.show', $package->id) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Package"><i class="fa fa-eye"></i></button></a> --}}
            @if(\Auth::user()->user_type == "super_admin")
          	<form action="{{ route('packages.destroy', $package->id) }}" method="POST" class="delete_item">
          		@csrf
          		@method('DELETE')
          		<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
          	</form>
            @endif
          </td>
          @if(\Auth::user()->user_type == "agent")
          <td><a href="#" class="btn btn-danger btn-sm">Expire Package</a></td>
          @endif
        </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@endsection

@push('js')
  <script type="text/javascript">
    $(function () {
      $('#travelAgency').DataTable({
      	'bFilter': false,
        "columnDefs": [
        { orderable: false, targets: 6 }
                 ],
      });
    });
  </script>
@endpush
