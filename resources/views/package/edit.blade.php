@extends('adminlte::page')
@section('title', 'Edit Package')

@section('content_header')
<h1>Edit Package</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
<div class="box">
  <div class="box-body">
    	<form action="{{ route('packages.update', $package->id) }}" method="post" role="form">
    		@csrf
        @method('put')
              <div class="box-body">
                 <div class="row">
                  <div class="col-md-2">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                      <label>Package Name <span>*</span></label>
                      <input type="text" name="title" class="form-control" placeholder="Enter Package Name" value="{{ $package->title }}">
                      @if($errors->has('title'))
                    <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                  @endif
                    </div>
                  </div>
                   <div class="col-md-2">
                 <div class="form-group {{ $errors->has('no_of_trips') ? 'has-error' : '' }}">
                  <label>Number of Trips <span>*</span></label>
                  <input type="number" class="form-control" name="no_of_trips" placeholder="Enter Number of Trips" value="{{ $package->no_of_trips }}" min="1">
                  @if($errors->has('no_of_trips'))
                    <span class="help-block text-danger">{{ $errors->first('no_of_trips') }}</span>
                  @endif
                </div>
               </div>
               <div class="col-md-2">
                 <div class="form-group {{ $errors->has('no_of_customers') ? 'has-error' : '' }}">
                  <label>Number of Clients <span>*</span></label>
                  <input type="number" class="form-control" name="no_of_customers" placeholder="Enter Number of Clients" value="{{ $package->no_of_customers }}" min="1">
                  @if($errors->has('no_of_customers'))
                    <span class="help-block text-danger">{{ $errors->first('no_of_customers') }}</span>
                  @endif
                </div>
               </div>
               <div class="col-md-2">
                 <div class="form-group {{ $errors->has('no_of_steps') ? 'has-error' : '' }}">
                  <label>Number of Steps <span>*</span></label>
                  <input type="number" class="form-control" name="no_of_steps" placeholder="Enter Number of Steps" value="{{ $package->no_of_steps }}" min="1">
                  @if($errors->has('no_of_steps'))
                    <span class="help-block text-danger">{{ $errors->first('no_of_steps') }}</span>
                  @endif
                </div>
               </div>
               <div class="col-md-2">
                 <div class="form-group {{ $errors->has('no_of_section') ? 'has-error' : '' }}">
                  <label>Number of Section per Step <span>*</span></label>
                  <input type="number" class="form-control" name="no_of_section" placeholder="Enter Number of Sections" value="{{ $package->no_of_section }}" min="1">
                  @if($errors->has('no_of_section'))
                    <span class="help-block text-danger">{{ $errors->first('no_of_section') }}</span>
                  @endif
                </div>
               </div>
{{--               <div class="col-md-3">--}}
{{--                    <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">--}}
{{--                        <label>Start Date <span>*</span></label>--}}
{{--                        <input type="text" name="start_date" class="form-control datepicker" value="{{ $package->start_date }}">--}}
{{--                        @if($errors->has('start_date'))--}}
{{--                            <span class="help-block text-danger">{{ $errors->first('start_date') }}</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--               <div class="col-md-3">--}}
{{--                    <div class="form-group {{ $errors->has('expiry_date') ? 'has-error' : '' }}">--}}
{{--                        <label>Expiry Date <span>*</span></label>--}}
{{--                        <input type="text" name="expiry_date" class="form-control datepicker" value="{{ $package->expiry_date }}">--}}
{{--                        @if($errors->has('expiry_date'))--}}
{{--                            <span class="help-block text-danger">{{ $errors->first('expiry_date') }}</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}

                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update</button>
              </div>
            </form>

  </div>
</div>
@endsection
@push('js')
<script type="text/javascript">
	$(document).ready(function(){
		$('.subscribeBtn').click(function(){
			var data = $(this).data('product-id');
			$('#package_id').val(data);
			$('#packageSelect').hide(500);
			$('.formArea').show(500);
		});
     $('.datepicker').datepicker({
        autoclose: true,
         orientation: 'auto bottom',
         format : 'yyyy/mm/dd'
      });
	});
</script>
@endpush
