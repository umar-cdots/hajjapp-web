@extends('adminlte::page')
@section('title', 'Add Sections')

@section('content_header')
<h1>Add Sections</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
	<div class="box">
		<div class="box-header">
			{{-- <h3 class="box-title">Add Sections</h3> --}}
		</div>
		<form action="" method="post" role="form">
		<div class="box-body">
			<div class="row">
               <div class="field_wrapper">
                 <div class="col-md-2">
                 <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                  <label>Step</label>
                  <select name="step_pack_id" class="form-control">
                    <option value=""></option>
                  </select>
                  @if($errors->has('title'))
                    <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                  @endif
                </div>
               </div>
               <div class="col-md-2">
                 <div class="form-group {{ $errors->has('section_name') ? 'has-error' : '' }}">
                  <label>Section Name</label>
                  <input type="text" name="section_name" class="form-control">
                  @if($errors->has('section_name'))
                    <span class="help-block text-danger">{{ $errors->first('section_name') }}</span>
                  @endif
                </div>
               </div>
               <div class="col-md-2">
                 <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                  <label>Description</label>
                  <input type="text" name="description" class="form-control">
                  @if($errors->has('description'))
                    <span class="help-block text-danger">{{ $errors->first('description') }}</span>
                  @endif
                </div>
               </div>
               <div class="col-md-2">
                 <div class="form-group {{ $errors->has('path') ? 'has-error' : '' }}">
                  <label>Image</label>
                  <input type="file" name="path" class="form-control">
                  @if($errors->has('path'))
                    <span class="help-block text-danger">{{ $errors->first('path') }}</span>
                  @endif
                </div>
               </div>
               <div class="col-md-2">
                 <div class="form-group {{ $errors->has('information') ? 'has-error' : '' }}">
                  <label>Information</label>
                  <input type="text" name="information" class="form-control">
                  @if($errors->has('information'))
                    <span class="help-block text-danger">{{ $errors->first('information') }}</span>
                  @endif
                </div>
               </div>
               </div>
               <div class="col-md-2">
                  <div>
                      {{-- <input type="text" class="form-control" name="field_name[]" value=""/> --}}
                      <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa  fa-plus-square"></i></a>
                  </div>
              </div>
			</div>	
        </div>
		<div class="box-footer">
			<button type="submit" class="btn btn-primary btnSubmit">Submit</button>
		</div>
		<form>
		</div>
@endsection

@push('css')
<style type="text/css">
  .add_button {
    display: inline;
    font-size: 38px;
    margin-top: 15px;
  }
</style>
@endpush
@push('js')
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div> <input type="text" name="field_name[]" class="form-control" value=""/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus-square"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
@endpush