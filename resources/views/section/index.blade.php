@extends('adminlte::page')
@section('title', 'All Sections')

@section('content_header')
<h1>All Sections</h1>
{{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
@endsection


@section('content')
	<div class="box">
    <div class="box-header">
      <div class="col-md-6">
      	{{-- <h3 class="box-title">All Sections</h3> --}}
      </div>
      <div class="col-md-6 text-right">
      	<a href="{{ url('admin/section/create') }}" class="btn btn-primary">Add Section</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="travelAgency" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th>Sr#</th>
          	<th>Step Name</th>
          	<th>Section Name</th>
          	<th>Description</th>
            <th>Image</th>
            <th>Information</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        	{{-- @foreach($travelAgency as $data) --}}
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>
          	<a href="{{ url('admin/step/edit') }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
          	{{-- <a href="{{ url('admin/step/show') }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Step"><i class="fa fa-eye"></i></button></a> --}}
          	<form action="" method="POST" class="delete_item">
          		@csrf
          		@method('DELETE')
          		<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
          	</form>
          </td>

        </tr>
        {{-- @endforeach --}}
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@endsection

@push('js')
  <script type="text/javascript">
    $(function () {
      $('#travelAgency').DataTable({
      	'lengthChange': false,
      });
    });
  </script>
@endpush
