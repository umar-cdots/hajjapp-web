<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    </button>
    @foreach($errors->all() as $error)
        <strong>Warning!</strong> {{$error}}.<br />
    @endforeach
</div>