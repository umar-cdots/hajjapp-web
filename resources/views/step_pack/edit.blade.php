@extends('adminlte::page')
@section('title', 'Edit Step Pack')

@section('content_header')
    <h1>Edit Step Pack</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
    <div class="box">
        <div class="box-header">
            {{-- <h3 class="box-title">Edit Step Pack</h3> --}}
        </div>
        <form action="{{ route('step_pack.update', $step_pack->id) }}" method="post" role="form" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Step Pack Name <span>*</span></label>
                            <input type="text" class="form-control" name="title" placeholder="Enter Step Pack Name" value="{{ $step_pack->title }}" required="">
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Travel Agency <span>*</span></label>
                            <select name="travel_agency_id" class="form-control">
                                <option value="">Select Travel Agency</option>
                                @foreach($travel_agencies as $travel_agency)
                                    <option value="{{ $travel_agency->id }}" {{ ($step_pack->travel_agency_id == $travel_agency->id) ? 'selected' : 'none' }}>{{ $travel_agency->agency_name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
            </div>
            <form>
    </div>
@endsection