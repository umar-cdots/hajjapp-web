@extends('adminlte::page')
@section('title', 'Trip Steps Pack')

@section('content_header')
	<h1>Trip Steps Pack</h1>
	{{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
@endsection


@section('content')
	<div class="box">
    <div class="box-header">
      <div class="col-md-6">
      	{{-- <h3 class="box-title">Trip Steps Pack</h3> --}}
      </div>
      <div class="col-md-6 text-right">
      	<a href="{{ route('step_pack.create') }}" class="btn btn-primary">Add Step Pack</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="travelAgency" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th>Sr#</th>
			@if(\Auth::user()->user_type == "super_admin")
				<th>Travel Agency</th>
			@endif
          	<th>Title</th>
          	<th>No. of Trips</th>
          	<th>Action</th>
        </tr>
        </thead>
        <tbody>
    	@foreach($step_packs as $step_pack)
	        <tr>
	          	<td>{{ $loop->iteration }}</td>
	          	@if(\Auth::user()->user_type == "super_admin")
					<td>
						<a href="{{ route('travel_agency.show', ['id' => $step_pack->travel_agency_id]) }}">{{ $step_pack->travelAgency->agency_name }}</a>
					</td>
				@endif
				<td>{{ $step_pack->title }}</td>
				<td>{{ $step_pack->trips->count() }}</td>
				<td>
					<a href="{{ route('step_pack.edit', ['id' => $step_pack->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
					{{-- <a href="{{ route('step_pack.show', ['id' => $step_pack->id]) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button></a> --}}
					<form action="{{ route('step_pack.destroy', ['id' => $step_pack->id]) }}" method="POST" class="delete_item">
						@csrf
						@method('DELETE')
						<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
					</form>
				</td>
	        </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@endsection

@push('js')
	<script type="text/javascript">
		 $(function () {
	    $('#stepPacks').DataTable({
	    	'lengthChange': false,
	    })

	  });
	</script>
@endpush
