@extends('adminlte::page')
@section('title', 'Step Pack Details')

@section('content_header')
<h1>Step Pack Details</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
  <div class="box">
            <div class="box-header">
              <a href="{{ route('step_pack.create', $step_pack->id) }}" class="btn btn-primary">Add Step</a>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered">
                   <tbody>
                    <tr>
                      <th>Title</th>
                      <td>{{ $step_pack->title }}</td>
                    </tr>
                    <tr>
                      <th>No of Trips</th>
                      <td>{{  $step_pack->trips->count() }}</td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <!-- /.box-body -->
  </div>
@endsection