@extends('adminlte::page')
@section('title', 'Tasks')

@section('content_header')
<h1>Tasks</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
 <div class="box">
        <div class="box-body">
            <table id="client_categories" class="table table-bordered table-striped data-table">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Trip Name</th>
                    <th>Task Name</th>
                    <th>Due Date</th>
                    <th>Description</th>
                    <th>Comment</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($trip_tasks))
                 @foreach($trip_tasks as $trip_task)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $trip_task->trip->title ?? '--' }}</td>
                        <td>{{ $trip_task->task_title }}</td>
                        <td>{{ $trip_task->due_date }}</td>
                        <td>{{ $trip_task->description }}</td>
                        <td>{{ $trip_task->comment }}</td>
                        <td>{{ $trip_task->status }}</td>
                        @if($trip_task->status != 'complete')
                            <td>
                                <button type="button" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Mark as Complete" onclick="showMessage('{{ $trip_task->id }}')"><i class="fa fa-check"></i></button>
                            {{--<form action="{{ route('staff.task.complete') }}" method="post">
                                @csrf
                                <input type="hidden" name="trip_task_id" value="{{ $trip_task->id }}">
                                <button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Mark as Complete"><i class="fa fa-check"></i></button>
                            </form>--}}
                        </td>
                        @endif
                    </tr>
                 @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
<div class="modal fade in" id="showmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Add Comment</h4>
      </div> 
      <form method="post">
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="task_id" id="task_id">
                <div class="form-group">
                 <label>Comments</label>
                  <textarea class="form-control" name="comment" id="comment" rows="5" placeholder="Task Comment"></textarea>
                  </div>
            </div> 
        </div>
      </div>
  </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="BtnUpdateStatus">Update Status</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection
@push('js')
  <script type="text/javascript">
    $(function () {
      $('#client_categories').DataTable({
        "bFilter":false
      });
    });
  function showMessage(id){
    $("#showmodal").modal('toggle');
    $("#task_id").val(id);
  }
  $("#BtnUpdateStatus").click(function(){
    $.ajax({
            url: "{{route('staff.task.complete')}}",
            data:{_token: $("#token").val(),task_id:$("#task_id").val(),comment:$("#comment").val()},
            type: "POST",
            success: function (json) {
                if(json.success==true)
                {
                toastr['success']("Task has been marked completely"); 
                 location.reload(); 
                }
                else
                {
                    toastr['error']("Task can not marked completely");
                }
               }
        });
  });
  </script>
@endpush
