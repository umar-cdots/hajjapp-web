@extends('adminlte::page')
@section('title', 'Add Step')

@section('content_header')
<h1>Add Step</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
	<div class="box">
		<div class="box-header">
			{{-- <h3 class="box-title">Add Step</h3> --}}
		</div>
		<form action="{{ route('steps.store') }}" method="post" role="form" enctype="multipart/form-data">
			@csrf
			<div class="box-body">
				<div class="row">
					<div class="col-md-3">
					 <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
					  <label>Step Name</label>
					  <input type="text" class="form-control" name="title" placeholder="Enter Step Name" value="{{ old('title') }}" required="">
					  @if($errors->has('title'))
						<span class="help-block text-danger">{{ $errors->first('title') }}</span>
					  @endif
					</div>
				   </div>

				   <div class="col-md-3">
					 <div class="form-group {{ $errors->has('last_video_path') ? 'has-error' : '' }}">
					  <label>Step Video</label>
					  <input type="file" name="last_video_path" class="form-control">
					  @if($errors->has('last_video_path'))
						<span class="help-block text-danger">{{ $errors->first('last_video_path') }}</span>
					  @endif
					</div>
				   </div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary btnSubmit">Submit</button>
			</div>
		<form>
	</div>
@endsection