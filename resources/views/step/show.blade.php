@extends('adminlte::page')
@section('title', 'Step Details')

@section('content_header')
<h1>Step Details</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
  <div class="box">
            <div class="box-header">
              <a href="{{ url('admin/section/index') }}" class="btn btn-primary">Add Sections</a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
               <tbody>
                <tr>
                  <th>Step Name</th>
                  <td></td>
                </tr>
                <tr>
                  <th>Step Pack</th>
                  <td></td>
                </tr>
                <tr>
                  <th>Video</th>
                  <td></td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection