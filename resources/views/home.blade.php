@extends('adminlte::page')

@section('title', 'AdminLTE')


@section('content')
  @if(Auth::user()->user_type == "super_admin")
    <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $total_travelAgency ?? '--' }}</h3>
              <p>Total Travel Agencies</p>
            </div>
            <div class="icon">
              <i class="fa fa-plane"></i>
            </div>
            <a href="{{ route('travel_agency.index') }}" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $total_packages ?? '--' }}</h3>
              <p>Total Packages</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-cubes"></i>
            </div>
            <a href="{{ route('packages.index') }}" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        {{-- <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $total_trips_for_superadmin }}</h3>
              <p>Total Trips</p>
            </div>
            <div class="icon">
              <i class="fa fa-cubes"></i>
            </div>
            <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $total_agents }}</h3>
              <p>Total Agents</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> --}}
        @elseif(Auth::user()->user_type == "staff")
        <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $total_tasks }}</h3>
              <p>Total Tasks</p>
            </div>
            <div class="icon">
              <i class="fa fa-tasks"></i>
            </div>
            <a href="{{route('staff.tasks.index')}}" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $completed_tasks }}</h3>
              <p>Completed Tasks</p>
            </div>
            <div class="icon">
              <i class="fa fa-tasks"></i>
            </div>
            <a href="#" class="small-box-footer" onclick="opennewtask()">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        </div>
        <div class="row">
      <div class="col-lg-6 col-xs-12">
     <h3>Notice Board
     </h3>
     <div class="box">
         <div class="box-body">
            <div class="scrollContentDashboard scrollContentDashboardNotice">
              @if(isset($StaffNotificationData))
              @foreach($StaffNotificationData as $StaffNotification)
              <h5>{{ $StaffNotification->message }} on {{$StaffNotification->created_at}}</h5>
              @endforeach
              @endif
            </div>
         </div>
     </div>
 </div>
 </div>
        @elseif(Auth::user()->user_type == "mobile_agent")
        <div></div>
  @else
 <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $new_complaint }}</h3>
              <p>New Complaints</p>
            </div>
            <div class="icon">
              <i class="fa fa-exclamation-triangle"></i>
            </div>
            <a href="#" class="small-box-footer" onclick="Complaint()">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
 </div>
         <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $total_trips }}</h3>
              <p>Active Trips</p>
            </div>
            <div class="icon">
              <i class="fa fa-plane"></i>
            </div>
            <a href="#" class="small-box-footer" onclick="ActiveTrip()">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
         {{-- <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $total_messages }}</h3>
              <p>Total Messages</p>
            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="#" class="small-box-footer">&nbsp;</a>
          </div>
        </div> --}}
         <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $overduetask }}</h3>
              <p>Tasks Overdue</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="#" class="small-box-footer" onclick="OverdueTask()">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-8">
          <h3>Tasks Due Today
          </h3>
          <div class="box">
            <div class="box-body">
              <div class="scrollContentDashboard">
                <table class="table table-bordered table-striped dashboardTables">
                <thead>
                <tr>
                  <th>Task Name</th>
                  <th>Trip</th>
                  <th>Assigned To</th>
                  <th>Due Date</th>
                  <th>Status</th>
                  <th>Actual Date</th>
                </tr>
                </thead>
                <tbody>
                    @if(isset($taskduetodays))
                        @foreach ($taskduetodays as $taskduetoday)
                            <tr>
                                <td>{{ $taskduetoday->task_title}}</td>
                                <td>{{ $taskduetoday->trip?$taskduetoday->trip->title:"--"}}</td>
                                <td>{{ $taskduetoday->staff?$taskduetoday->staff->name:"--"}}</td>
                                <td>{{ $taskduetoday->due_date}}</td>
                                <td>{{ $taskduetoday->status}}</td>
                                <td>{{ $taskduetoday->actual_date}}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
              </table>
             
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <a href="{{route('trip.task.index')}}" class="btn btn-primary btnSubmit pull-right">View All Tasks <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
 <div class="col-md-4">
     <h3>Notice Board
     </h3>
     <div class="box">
         <div class="box-body">
            <div class="scrollContentDashboard scrollContentDashboardNotice">
              @if(isset($getFlightDepature))
              @foreach($getFlightDepature as $flightdata)
              <h5>{{$flightdata['message']}}</h5>
              @endforeach
              @endif
              @if(isset($getFeedback))
              @foreach($getFeedback as $feedback)
              <h5>{{$feedback->clientgrouptrip->client->name}} for trip {{$feedback->clientgrouptrip->clienttrip->trip->title}} has provided feedback on {{$feedback->created_at->format('Y-m-d h:i:s a')}}</h5>
              @endforeach
              @endif
              @if(isset($getemergencyalerts))
              @foreach($getemergencyalerts as $emergency_alert)
              <h5>{{ Utils::clientByClientTrip($emergency_alert->client_trip_id)->getFullName() }} for trip {{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }} has raised Alert on {{$emergency_alert->created_at->format('Y-m-d h:i:s a')}}</h5>
              @endforeach
              @endif
              @if(isset($getTripMessage))
              @foreach($getTripMessage as $tripmessage)
              @if($tripmessage->agent_id==null)
              <h5>{{$tripmessage->client->name}} for trip {{$tripmessage->trip->title}} has sent message on {{$tripmessage->created_at->format('Y-m-d h:i:s a')}}</h5>
              @endif
              @endforeach
              @endif
              <h5>{{$clientqota}}</h5>
              <h5>{{$tripqota}}</h5>
              @if(isset($tripcapacity))
              @foreach($tripcapacity as $capacity)
              <h5>{{$capacity['message']}}</h5>
              @endforeach
              @endif
              @if(isset($tripqouta))
              @foreach($tripqouta as $qouta)
              <h5>{{$qouta['message']}}</h5>
              @endforeach
              @endif
              @if(isset($getTask))
              @foreach($getTask as $task)
              <h5>{{$task->task_title}} task is created for you</h5>
              @endforeach
              @endif
              <h5>{{$checklicense}}</h5>

            </div>
         </div>
     </div>
 </div>
 </div>
 <div class="row">
 <div class="col-md-12">
     <h3>New Alerts</h3>
     <div class="box">
         <div class="box-body">
          <div class="scrollContentDashboard">
             <table id="emergencyAlerts" class="table table-bordered">
                <thead>
                <tr>
                    <th>Sr#</th>
                    @if(\Auth::user()->user_type == "super_admin")
                        <th>Travel Agency</th>
                    @endif
                    <th>Client</th>
                    <th>Trip</th>
                    <th>Catagory</th>
                    <th>Group</th>
                    <th>Phone Main</th>
                    <th>Phone Backup</th>
                    <th>Phone Emergency</th>
                    <th>Status</th>
                    <th>Sent Time</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($emergency_alerts as $emergency_alert)
                @if($emergency_alert->status == 'solved')
                    <tr style="background-color: #90EE90">
                        <td>{{ $loop->iteration }}</td>
                        @if(\Auth::user()->user_type == "super_admin")
                            <td>
                                <a href="{{ route('travel_agency.show', ['id' => Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->agency_name }}</a>
                            </td>
                        @endif
                        <td>
                            <a href="{{ route('clients.showprofile', ['id' => Utils::clientByClientTrip($emergency_alert->client_trip_id)->user_id]) }}" target="_blank">{{ Utils::clientByClientTrip($emergency_alert->client_trip_id)->getFullName() }}</a>
                        </td>
                        <td>{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</td>
                       {{-- <td>
                            <a href="{{ route('trips.show', ['id' => Utils::tripByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</a>
                        </td>--}}
                        <td>{{$emergency_alert->clientgtouptrip->client->client_category->title ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->clienttrip->group->group_name ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->primary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->secondary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->emergency_phone_number ?? '--'}}</td>
                        <td>
                            <button class="btn btn-xs btn-{{ $emergency_alert->status == 'sent' ? 'danger' : ($emergency_alert->status == 'seen' ? 'warning' : 'success') }}">{{ $emergency_alert->status == 'sent' ? 'New' : ($emergency_alert->status == 'seen' ? 'Seen' : 'Resolved') }}</button>
                        </td>
                        <td>{{ $emergency_alert->created_at->format('Y-m-d h:i:s a') }}</td>
                        <td>
                            {{-- <a href="{{ route('clients.edit', ['id' => $client->user_id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a> --}}
                            <a target="_blank" href="https://www.google.com/maps?q={{ $emergency_alert->lat }},{{ $emergency_alert->lon }}">
                                <button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Location"><i class="fa fa-map-pin"></i></button>
                            </a>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="seen">
                                <button type="submit" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Mark As Seen"><i class="fa fa-eye"></i></button>
                            </form>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="solved">
                                <button type="submit" class="btn btn-xs btn-success" data-toggle="tooltip" title="Mark As Solved"><i class="fa fa-check"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif
                    @if($emergency_alert->status == 'seen')
                    <tr style="background-color: #F5F5F5">
                        <td>{{ $loop->iteration }}</td>
                        @if(\Auth::user()->user_type == "super_admin")
                            <td>
                                <a href="{{ route('travel_agency.show', ['id' => Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->agency_name }}</a>
                            </td>
                        @endif
                        <td>
                            <a href="{{ route('clients.showprofile', ['id' => Utils::clientByClientTrip($emergency_alert->client_trip_id)->user_id]) }}" target="_blank">{{ Utils::clientByClientTrip($emergency_alert->client_trip_id)->getFullName() }}</a>
                        </td>
                        <td>{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</td>
                       {{-- <td>
                            <a href="{{ route('trips.show', ['id' => Utils::tripByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</a>
                        </td>--}}
                        <td>{{$emergency_alert->clientgtouptrip->client->client_category->title ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->clienttrip->group->group_name ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->primary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->secondary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->emergency_phone_number ?? '--'}}</td>
                        <td>
                            <button class="btn btn-xs btn-{{ $emergency_alert->status == 'sent' ? 'danger' : ($emergency_alert->status == 'seen' ? 'warning' : 'success') }}">{{ $emergency_alert->status == 'sent' ? 'New' : ($emergency_alert->status == 'seen' ? 'Seen' : 'Resolved') }}</button>
                        </td>
                        <td>{{ $emergency_alert->created_at->format('Y-m-d h:i:s a') }}</td>
                        <td>
                            {{-- <a href="{{ route('clients.edit', ['id' => $client->user_id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a> --}}
                            <a target="_blank" href="https://www.google.com/maps?q={{ $emergency_alert->lat }},{{ $emergency_alert->lon }}">
                                <button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Location"><i class="fa fa-map-pin"></i></button>
                            </a>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="seen">
                                <button type="submit" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Mark As Seen"><i class="fa fa-eye"></i></button>
                            </form>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="solved">
                                <button type="submit" class="btn btn-xs btn-success" data-toggle="tooltip" title="Mark As Solved"><i class="fa fa-check"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif
                    @if($emergency_alert->status == 'sent')
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        @if(\Auth::user()->user_type == "super_admin")
                            <td>
                                <a href="{{ route('travel_agency.show', ['id' => Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->agency_name }}</a>
                            </td>
                        @endif
                        <td>
                            <a href="{{ route('clients.showprofile', ['id' => Utils::clientByClientTrip($emergency_alert->client_trip_id)->user_id]) }}" target="_blank">{{ Utils::clientByClientTrip($emergency_alert->client_trip_id)->getFullName() }}</a>
                        </td>
                        <td>{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</td>
                       {{-- <td>
                            <a href="{{ route('trips.show', ['id' => Utils::tripByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</a>
                        </td>--}}
                        <td>{{$emergency_alert->clientgtouptrip->client->client_category->title ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->clienttrip->group->group_name ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->primary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->secondary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->emergency_phone_number ?? '--'}}</td>
                        <td>
                            <button class="btn btn-xs btn-{{ $emergency_alert->status == 'sent' ? 'danger' : ($emergency_alert->status == 'seen' ? 'warning' : 'success') }}">{{ $emergency_alert->status == 'sent' ? 'New' : ($emergency_alert->status == 'seen' ? 'Seen' : 'Resolved') }}</button>
                        </td>
                        <td>{{ $emergency_alert->created_at->format('Y-m-d h:i:s a') }}</td>
                        <td>
                            <a target="_blank" href="https://www.google.com/maps?q={{ $emergency_alert->lat }},{{ $emergency_alert->lon }}">
                                <button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Location"><i class="fa fa-map-pin"></i></button>
                            </a>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="seen">
                                <button type="submit" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Mark As Seen"><i class="fa fa-eye"></i></button>
                            </form>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="solved">
                                <button type="submit" class="btn btn-xs btn-success" data-toggle="tooltip" title="Mark As Solved"><i class="fa fa-check"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif

                @endforeach
                </tbody>
            </table>
             </div>
         </div>
         <div class="box-footer">
           <a href="{{route('emergency_alert.index')}}" class="btn btn-primary btnSubmit pull-right">View All Alerts <i class="fa fa-arrow-circle-right"></i></a>
         </div>
         <!-- /.box-body -->
     </div>
 </div>
 </div>
        <div class="clearfix"></div>
  @endif
@stop
@push('js')
    <script>
      $(document).ready(function(){
        onloadData();
      });

      $(function(){
        $('form.delete_item').unbind('submit');
      })
      function onloadData(){
        $.ajax({
         type: "POST",
            url: "{{route('usefortimezone')}}",
            data: {_token: $("#token").val(),timezone:Intl.DateTimeFormat().resolvedOptions().timeZone},
            success: function (json) {
              if(json.status=="true")
              {
              }
              else
              {

              }
            }
        });
      }

      function opennewtask(){
        $.ajax({
         type: "POST",
            url: "{{route('useForSession')}}",
            data: {_token: $("#token").val(),type:"completed_tasks"},
            success: function (json) {
              if(json.status==true)
              {
                window.location.href = "{{route('staff.tasks.index')}}";
              }
              else
              {

              }
            }
        });
      }

      function Complaint(){
        $.ajax({
         type: "POST",
            url: "{{route('useForSession')}}",
            data: {_token: $("#token").val(),type:"complaint"},
            success: function (json) {
              if(json.status==true)
              {
                window.location.href = "{{route('feedback.index')}}";
              }
              else
              {

              }
            }
        });
      }

      function OverdueTask(){
        $.ajax({
         type: "POST",
            url: "{{route('useForSession')}}",
            data: {_token: $("#token").val(),type:"over_due_task"},
            success: function (json) {
              if(json.status==true)
              {
                window.location.href = "{{route('trip.task.index')}}";
              }
              else
              {

              }
            }
        });
      }

      function ActiveTrip(){
        $.ajax({
         type: "POST",
            url: "{{route('useForSession')}}",
            data: {_token: $("#token").val(),type:"trip"},
            success: function (json) {
              if(json.status==true)
              {
                window.location.href = "{{route('trips.index')}}";
              }
              else
              {

              }
            }
        });
      }
    </script>
@endpush
