@extends('adminlte::page')
@section('title', 'Create Travel Agency')

@section('content_header')
    <h1>Create Travel Agency</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
    <div class="box">
        <div class="box-body">
            {{-- <div class="slideanim" id="packageSelect"
                 style="display: {{ empty(old('package_id')) ? 'unset' : 'none' }};">
                @foreach($packages as $package)
                    <div class="col-md-4">
                        <div class="packagesList packagesList1">
                            <div class="pkgHead pkgHead1">{{ $package->title }}</div>
                            <div class="pkgDet">
                                <h2>{{ $package->title }}</h2>
                                {{--  <h2>0.00</h2>
                                 <h3>per 30 Days</h3> --}}
                                {{-- <ul> --}}
                                    {{-- <li>No. of Trips <b>{{ $package->no_of_trips }}</b></li> --}}
                                    {{-- <li>No. of Customers <b>{{ $package->no_of_customers }}</b></li> --}}
                                    {{-- <li>No. of Guides <b>{{ $package->no_of_guides }}</b></li> --}}
                                    {{-- <li>No. of Steps <b>{{ $package->no_of_steps }}</b></li> --}}
{{--                                    <li>Start Date <b>{{ $package->start_date }}</b></li>--}}
{{--                                    <li>End Date <b>{{ $package->expiry_date }}</b></li>--}}
                                {{-- </ul> --}}
                                {{-- <a href="#" class="pkgBtn packageLink subscribeBtn" --}}
                                   {{-- data-product-id="{{ $package->id }}">Subscribe --}}
                                {{-- </a> --}}
                            {{-- </div> --}}
                        {{-- </div> --}}
                    {{-- </div> --}}
                {{-- @endforeach --}}
                {{-- <div class="clearfix"></div> --}}
            {{-- </div> --}}
            {{-- style="display: {{ !empty(old('package_id')) ? 'unset' : 'none' }};" --}}
            <div class="formArea" >
                <form action="{{ route('travel_agency.store') }}" method="POST" role="form"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('agency_name') ? 'has-error' : '' }}">
                                        <label>Agency Name <span>*</span></label>
                                        <input type="text" class="form-control" name="agency_name"
                                               placeholder="Enter Agency Name" value="{{ old('agency_name') }}">
                                        @if($errors->has('agency_name'))
                                            <span class="help-block text-danger">{{ $errors->first('agency_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <label>Agent Name <span>*</span></label>
                                        <input type="text" class="form-control" name="name" placeholder="Enter Name"
                                               value="{{ old('name') }}">
                                        @if($errors->has('name'))
                                            <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
                                        <label>Primary Phone Number <span>*</span></label>
                                        <input type="text" class="form-control" name="phone_no"
                                               placeholder="Enter Phone Number" value="{{ old('phone_no') }}" minlength="8">
                                        @if($errors->has('phone_no'))
                                            <span class="help-block text-danger">{{ $errors->first('phone_no') }}</span>
                                        @endif
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('phone_no_sec') ? 'has-error' : '' }}">
                                        <label>Secondary Phone Number <span>*</span></label>
                                        <input type="text" class="form-control" name="phone_no_sec"
                                               placeholder="Enter Secondary Phone Number" value="{{ old('phone_no_sec') }}" minlength="7">
                                        @if($errors->has('phone_no_sec'))
                                            <span class="help-block text-danger">{{ $errors->first('phone_no_sec') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                        <label>Username (Email) <span>*</span></label>
                                        <input type="email" class="form-control" name="email" placeholder="Enter Email" value="{{ old('email') }}">
                                        @if($errors->has('email'))
                                            <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                        <label>Password <span>*</span></label>
                                        <input type="password" class="form-control" name="password"
                                               placeholder="Enter Password" value="{{ old('password') }}" minlength="8">
                                        <span style="font-size:11px;">Password must contain at least one uper character, number and special character(@,%)</span>

                                        @if($errors->has('password'))
                                            <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                        <label>Confirm Password <span>*</span></label>
                                        <input type="password" class="form-control" name="password_confirmation"
                                               placeholder="Enter Confirm Password"
                                               value="{{ old('password_confirmation') }}" minlength="8">
                                        @if($errors->has('password_confirmation'))
                                            <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('path') ? 'has-error' : '' }}">
                                        <label>Logo <span>*</span></label>
                                        <input type="file" name="path" class="form-control" value="{{ old('path') }}">
                                        @if($errors->has('path'))
                                            <span class="help-block text-danger">{{ $errors->first('path') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('package') ? 'has-error' : '' }}">
                                        <label style="display: block;">Package <span>*</span> <span class="viewPackage"><a id="package_view" href="javascript:viod(0)" data-toggle="modal" data-target="#viewPackage">View</a></span></label>
                                        <select id="package" class="form-control" name="package">
                                            <option value="">Select</option>
                                            @if(!empty($packages))
                                                @foreach($packages as $package)
                                                    <option value="{{ $package->id }}">{{ $package->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('package'))
                                            <span class="help-block text-danger">{{ $errors->first('package') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3 packageDates" style="display: none;">
                                    <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                                        <label>Package Start Date <span>*</span></label>
                                        <input type="text" class="form-control datepicker" name="start_date"
                                               placeholder="Enter Package Start Date" id="start_date" value="{{ old('start_date') }}" autocomplete="off" required="required">
                                        @if($errors->has('start_date'))
                                            <span class="help-block text-danger">{{ $errors->first('start_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3 packageDates" style="display: none;">
                                    <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                                        <label>Package End Date <span>*</span></label>
                                        <input type="text" class="form-control datepicker" name="end_date"
                                               placeholder="Enter Package End Date" id="end_date" value="{{ old('end_date') }}" autocomplete="off" required="required" onchange="Checkdate()">
                                        @if($errors->has('end_date'))
                                            <span class="help-block text-danger">{{ $errors->first('end_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(!empty($packages))
        @foreach($packages as $package)
            <div id="packageView_{{ $package->id}}" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {{-- <h4 class="modal-title">{{ $package->title }}</h4> --}}
              </div>
              <div class="modal-body">
               <div class="col-md-12">
                        <div class="packagesList packagesList1">
                            <div class="pkgHead pkgHead1">{{ $package->title }}</div>
                            <div class="pkgDet">
                                <h2>{{ $package->title }}</h2>
                                 {{-- <h2>0.00</h2> --}}
                                 {{-- <h3>per 30 Days</h3> --}}
                                <ul class="packageList">
                                    <li>No. of Trips <b>{{ $package->no_of_trips }}</b></li>
                                    <li>No. of Customers <b>{{ $package->no_of_customers }}</b></li>
                                    <li>No. of Steps <b>{{ $package->no_of_steps }}</b></li>
                                    <li>No. of Sections <b>{{ $package->no_of_section }}</b></li>
                                    
                                </ul>
                               {{--  <a href="#" class="pkgBtn packageLink subscribeBtn"
                                   data-product-id="{{ $package->id }}">Subscribe
                                </a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
              </div>

            </div>

          </div>
        </div>
    @endforeach
@endif
@endsection
@push('js')
    <script type="text/javascript">
        // $(document).ready(function () {
        //     $('.subscribeBtn').click(function () {
        //         var data = $(this).data('product-id');
        //         $('#package_id').val(data);
        //         $('#packageSelect').hide(500);
        //         $('.formArea').show(500);
        //     });
        // });

        $("#package").change( function(){
            var value = $(this).val();
            $('#package_view').attr('data-target', "#packageView_"+value);
            $('.packageDates').fadeIn();
        });
        $('.datepicker').datepicker({
            autoclose: true,
            orientation: 'auto bottom',
            format : 'yyyy/mm/dd'
        });
    function Checkdate(){
    if(Date.parse($("#end_date").val())>Date.parse($("#start_date").val())){
    }
    else{
        toastr['error']("Please check date range");
        $("#end_date").val('');
    }
}
    </script>
@endpush
