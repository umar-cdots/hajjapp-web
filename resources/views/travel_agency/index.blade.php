@extends('adminlte::page')
@section('title', 'Travel Agencies')

@section('content_header')
<h1>Travel Agencies</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
	<div class="box">
    <div class="box-body travelAgencyTableBtn">
     <div class="row">
         <div class="col-md-12 text-right">
        <a href="{{ route('travel_agency.create') }}" class="btn btn-primary btnSubmit">Add Travel Agency</a>
      </div>
     </div>
      <div class="clearfix"></div>
      <table id="travelAgency" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th>Sr#</th>
          	<th>Agency Name</th>
            <th>Agent Name</th>
            <th>Email</th>
          	<th>Package Name</th>
          	<th>Action</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($travelAgencies as $travelAgency)

        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $travelAgency->agency_name }}</td>
          <td>{{ $travelAgency->agent->first_name }} {{ $travelAgency->agent->last_name }}</td>
          <td> {{ $travelAgency->agent->user->email ?? '--' }}</td>
          <td> {{ $travelAgency->package->title ?? '--' }}</td>
          <td>
          	<a href="{{ route('travel_agency.edit', ['id' => $travelAgency->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
          	{{-- <a href="{{ route('travel_agency.show', ['id' => $travelAgency->id]) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Agents & Staff"><i class="fa fa-eye"></i></button></a> --}}
          	<form action="{{ route('travel_agency.destroy', ['id' => $travelAgency->id]) }}" method="POST" class="delete_item">
          		@csrf
          		@method('DELETE')
          		<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
          	</form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
@endsection

@push('js')
  <script type="text/javascript">
    $(function () {
      $('#travelAgency').DataTable({
      	'bFilter': false,
        "columnDefs": [
        { orderable: false, targets: 5 }
                 ],
      });
    });

  </script>
@endpush
