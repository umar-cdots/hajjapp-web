@extends('adminlte::page')
@section('title', 'Edit Travel Agency')

@section('content_header')
<h1>Edit Travel Agency</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::previous() }}">Back</a></li>
</ol>
@endsection


@section('content')
    <div class="box">
        <div class="box-body">
            <div class="formArea">
                <form action="{{ route('travel_agency.update', ['id' => $travel_agency->id]) }}" method="post"
                      role="form" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <input type="hidden" name="user_id" value="{{$agent->user->id}}">
                    <input type="hidden" name="agent_id" value="{{$agent->id}}">
 <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                <div class="form-group {{ $errors->has('agency_name') ? 'has-error' : '' }}">
                                    <label>Agency Name <span>*</span></label>
                                    <input type="text" class="form-control" name="agency_name"
                                           placeholder="Enter Agency Name"
                                           value="{{ old('agency_name', $travel_agency->agency_name) }}" required=""
                                           disabled="">
                                    @if($errors->has('agency_name'))
                                        <span class="help-block text-danger">{{ $errors->first('agency_name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label>Agent Name <span>*</span></label>
                                    <input type="text" name="name" class="form-control" value="{{ $agent->user->name }}">
                                    @if($errors->has('name'))
                                        <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
                                    <label>Primary Phone Number <span>*</span></label>
                                    <input type="text" name="phone_no" class="form-control"
                                           value="{{ $agent->phone_no ?? '' }}" minlength="8">
                                    @if($errors->has('phone_no'))
                                        <span class="help-block text-danger">{{ $errors->first('phone_no') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('phone_no_sec') ? 'has-error' : '' }}">
                                    <label>Secondary Phone Number <span>*</span></label>
                                    <input type="text" name="phone_no_sec" class="form-control"
                                           value="{{ $agent->phone_no_sec ?? '' }}" minlength="8">
                                    @if($errors->has('phone_no_sec'))
                                        <span class="help-block text-danger">{{ $errors->first('phone_no_sec') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" value="{{ $agent->user?$agent->user->email:"" }}">
                                        @if($errors->has('email'))
                                            <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                <div class="form-group {{ $errors->has('package') ? 'has-error' : '' }}">
                                    <label style="display: block;">Package <span>*</span> <span class="viewPackage"><a id="package_view" href="javascript:viod(0)" data-toggle="modal" data-target="#packageView_{{ $travel_agency->package_id }}">View</a></span></label>
                                    <select id="package" class="form-control" name="package">
                                        <option value="">Select</option>
                                        @if(!empty($packages))
                                            @foreach($packages as $package)
                                                <option value="{{ $package->id }}" {{ ($travel_agency->package_id != null && $travel_agency->package_id == $package->id) ? 'selected' : '' }}>{{ $package->title }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('package'))
                                        <span class="help-block text-danger">{{ $errors->first('package') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3 packageDates" style="display: {{ ($travel_agency->package_id) ? 'block' : 'none' }};">
                                <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                                    <label>Package Start Date <span>*</span></label>
                                    <input type="text" class="form-control datepicker" name="start_date" id="start_date"
                                           placeholder="Enter Package Start Date" value="{{ $travel_agency->package_start_date }}" autocomplete="off" required="required">
                                    @if($errors->has('start_date'))
                                        <span class="help-block text-danger">{{ $errors->first('start_date') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3 packageDates" style="display: {{ ($travel_agency->package_id) ? 'block' : 'none' }};">
                                <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                                    <label>Package End Date <span>*</span></label>
                                    <input type="text" class="form-control datepicker"  id="end_date" name="end_date"
                                           placeholder="Enter Package End Date" value="{{ $travel_agency->package_end_date }}" autocomplete="off" required="required" onchange="Checkdate()">
                                    @if($errors->has('end_date'))
                                        <span class="help-block text-danger">{{ $errors->first('end_date') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('path') ? 'has-error' : '' }}">
                                    <label>Logo <span>*</span></label>
                                    <input type="file" name="path" class="form-control" value="{{ old('path') }}">
                                    @if($agent->picture_path!=null)
                                    <img src="{{ asset('storage/profile_images/'. $agent->picture_path)}}" width="200">
                                    @else
                                    <img id="viewImage" src="http://placehold.it/100x100" alt=""  width="100px" />
                                    @endif
                                @if($errors->has('path'))
                                    <span class="help-block text-danger">{{ $errors->first('path') }}</span>
                                @endif
                                </div>
                            </div>
                                </div>
                            </div>
                        </div>

                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary btnSubmit">Update</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@if(!empty($packages))
        @foreach($packages as $package)
            <div id="packageView_{{ $package->id}}" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {{-- <h4 class="modal-title">{{ $package->title }}</h4> --}}
              </div>
              <div class="modal-body">
               <div class="col-md-12">
                        <div class="packagesList packagesList1">
                            <div class="pkgHead pkgHead1">{{ $package->title }}</div>
                            <div class="pkgDet">
                                <h2>{{ $package->title }}</h2>
                                 {{-- <h2>0.00</h2> --}}
                                 {{-- <h3>per 30 Days</h3> --}}
                                <ul class="packageList">
                                    <li>No. of Trips <b>{{ $package->no_of_trips }}</b></li>
                                    <li>No. of Customers <b>{{ $package->no_of_customers }}</b></li>
                                    <li>No. of Steps <b>{{ $package->no_of_steps }}</b></li>
                                    <li>No. of Sections <b>{{ $package->no_of_section }}</b></li>
                                </ul>
                                {{-- <a href="#" class="pkgBtn packageLink subscribeBtn"
                                   data-product-id="{{ $package->id }}">Subscribe
                                </a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
              </div>

            </div>

          </div>
        </div>
    @endforeach
@endif
@push('js')
    <script type="text/javascript">
        // $(document).ready(function () {
        //     $('.subscribeBtn').click(function () {
        //         var data = $(this).data('product-id');
        //         $('#package_id').val(data);
        //         $('#packageSelect').hide(500);
        //         $('.formArea').show(500);
        //     });
        // });

        $("#package").change( function(){
            var value = $(this).val();
            $('#package_view').attr('data-target', "#packageView_"+value);
            $('.packageDates').fadeIn();
        });
        $('.datepicker').datepicker({
            autoclose: true,
            orientation: 'auto bottom',
            format : 'yyyy/mm/dd'
        });
        function Checkdate(){
    if(Date.parse($("#end_date").val())>Date.parse($("#start_date").val())){
    }
    else{
        toastr['error']("Please check date range");
        $("#end_date").val('');
    }
}
    </script>
@endpush
