@extends('adminlte::page')
@section('title', 'Show Travel Agency')

@section('content_header')
<h1>Show Travel Agencies</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
  <div class="box">
            <div class="box-header">
              <a href="{{ route('travel_agency.create') }}" class="btn btn-primary">Add New</a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
               <tbody>
                <tr>
                  <th>Agency Name</th>
                  <td>{{ $travel_agency->agency_name }}</td>
                </tr>
                <tr>
                  <th>Package Name</th>
                  <td>{{ $travel_agency->package->title ?? '-' }}</td>
                </tr>
                <tr>
                    <th>Package Start Date</th>
                    <td>{{ $travel_agency->package_start_date ?? '-' }}</td>
                </tr>
                <tr>
                    <th>Package End Date</th>
                    <td>{{ $travel_agency->package_end_date ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Contact Person Name</th>
                  <td>{{ $agent->first_name. ' ' .$agent->last_name }}</td>
                </tr>
                <tr>
                  <th>Primary Phone Number</th>
                  <td>{{ $agent->phone_no }}</td>
                </tr>
                <tr>
                  <th>Secondary Phone Number</th>
                  <td>{{ $agent->phone_no_sec }}</td>
                </tr>
                <tr>
                  <th>Username (Email)</th>
                  <td>{{ $agent->user->email }}</td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection