@extends('adminlte::master')
@section('title', "License Expired")
@section('body_class', 'login-page')
@section('body')
<div class="pageCustom">
<div class="text-center">
<div class="login-box">
	 <div class="login-box-body">
	<h3>Your license has expired. Please contact Ahlan Guide for further action</h3>
	<a href="{{ route('homepage') }}" class="btn btn-primary btnSubmit">Ok</a>
</div>
</div>
</div>
</div>
@endsection
