@extends('adminlte::page')
@section('title', 'Change Profile')

@section('content_header')
<h1>Change Profile</h1>
{{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection

@section('content')
	<div class="box">
    <form action="{{ route('clientpassword') }}" id="cleint" method="POST" role="form">
			@csrf
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">
						<div class="form-group {{ $errors->has('client_id') ? 'has-error' : '' }}">
                        <label>Select Client<span>*</span></label>
                        <select name="client_id" class="form-control client_id" multiple="multiple" autocomplete="off">
                            @foreach($clients as $client)
                                <option value="{{ $client->user_id }}">{{ $client->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('client_id'))
                            <span class="help-block text-danger">{{ $errors->first('client_id') }}</span>
                        @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('client_password') ? 'has-error' : '' }}">
						<label>New Password <span>*</span></label>
						<input type="password" name="client_password" class="form-control">
                        <span style="font-size:11px;">Password must contain at least one upper character, number and special character(@,%)</span>

                    @if($errors->has('client_password'))
							<span class="help-block text-danger">{{ $errors->first('client_password') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('client_password_confirmation') ? 'has-error' : '' }}">
						<label>Confirm Password <span>*</span></label>
						<input type="password" name="client_password_confirmation" class="form-control">
						@if($errors->has('client_password_confirmation'))
							<span class="help-block text-danger">{{ $errors->first('client_password_confirmation') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<button type="submit" class="btn btn-primary form-control btnSubmit">Reset Client Password</button>
					</div>
				</div>
			</div>
		</div>
		</form>
		<form action="{{ route('staffpassword') }}" id="staff" method="POST" role="form">
			@csrf
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">
						<div class="form-group {{ $errors->has('staff_id') ? 'has-error' : '' }}">
                        <label>Select Staff<span>*</span></label>
                        <select name="staff_id" class="form-control staff_id" multiple="multiple" autocomplete="off">
                            @foreach($staffs as $staff)
                                <option value="{{ $staff->user_id }}">{{ $staff->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('staff_id'))
                            <span class="help-block text-danger">{{ $errors->first('staff_id') }}</span>
                        @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('staff_password') ? 'has-error' : '' }}">
						<label>New Password <span>*</span></label>
						<input type="password" name="staff_password" class="form-control">
                        <span style="font-size:11px;">Password must contain at least one upper character, number and special character(@,%)</span>

                    @if($errors->has('staff_password'))
							<span class="help-block text-danger">{{ $errors->first('staff_password') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('staff_password_confirmation') ? 'has-error' : '' }}">
						<label>Confirm Password <span>*</span></label>
						<input type="password" name="staff_password_confirmation" class="form-control">
						@if($errors->has('staff_password_confirmation'))
							<span class="help-block text-danger">{{ $errors->first('staff_password_confirmation') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<button type="submit" class="btn btn-primary form-control btnSubmit">Reset Staff Password</button>
					</div>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection
@push('js')
<script type="text/javascript">
    $(".client_id").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Client"//placeholder
   });
    $(".staff_id").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Staff"//placeholder
   });
</script>
@endpush