@extends('adminlte::page')
@section('title', 'Edit Hotel')
@section('content_header')
<h1>Edit Hotel</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::previous() }}">Back</a></li>
</ol>
<div class="clearfix"></div>
@endsection
@section('content')
	<div class="box">
		<form action="{{ route('hotels.update',$hotel->id) }}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('PUT')
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>Name <span>*</span></label>
						<input type="text" name="name" class="form-control" value="{{ $hotel->name }}">
						@if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
						<label>Country </label>
						<select class="form-control country" name="country" id="country" multiple="multiple">
							@foreach($countries as $country)
								<option value="{{ $country->id }}" {{$country->id==$hotel->country_id?'selected="selected"' : ''}}>{{ $country->name }}</option>
							@endforeach
						</select>
						@if($errors->has('country'))
                                <span class="help-block text-danger">{{ $errors->first('country') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('states') ? 'has-error' : '' }}">
						<label>States</label>
						<select class="form-control state" name="state" id="state" multiple="multiple">
							<option value="{{$hotel->state_id??''}}" selected="selected">{{$hotel->state->name??""}}</option>
						</select>
						@if($errors->has('states'))
                               <span class="help-block text-danger">{{ $errors->first('states') }}</span>
                        @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
					<label>City </label>
						<select class="form-control city" name="city" id="city" multiple="multiple">
							<option value="{{$hotel->city_id??''}}" selected="selected">{{$hotel->city->name??""}}</option>
						</select>
						@if($errors->has('city'))
                                <span class="help-block text-danger">{{ $errors->first('city') }}</span>
                         @endif
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
					<label>Phone </label>
						<input type="text" name="phone_number" class="form-control" value="{{$hotel->phone_number??''}}">
						@if($errors->has('phone_number'))
                                <span class="help-block text-danger">{{ $errors->first('phone_number') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
					<label>Address </label>
						<input type="text" name="address" class="form-control" value="{{$hotel->address??''}}">
						@if($errors->has('address'))
                                <span class="help-block text-danger">{{ $errors->first('address') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('map_url') ? 'has-error' : '' }}">
					<label>Map_Url </label>
						<input type="text" name="map_url" class="form-control" value="{{$hotel->map_url??''}}">
						@if($errors->has('map_url'))
                                <span class="help-block text-danger">{{ $errors->first('map_url') }}</span>
                         @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('path') ? 'has-error' : '' }}">
                        <label>Image</label>
                        <input type="file" name="path" class="form-control" id="imgpreview">
                        @if($hotel->path!=null)
                        <img id="viewImage" src="{{ asset('storage/hotel_images/'. $hotel->path)}}" alt=""  width="100px" />
                        @else
                        <img id="viewImage" src="{{ asset('images/userImg.png') }}" alt=""  width="100px" />
                        @endif
                        @if($errors->has('path'))
                            <span class="help-block text-danger">{{ $errors->first('image') }}</span>
                        @endif
                        @if($errors->has('path'))
                            <span class="help-block text-danger">{{ $errors->first('path') }}</span>
                        @endif
                    </div>
				</div>
			</div>
		</div>
		<div class="box-footer text-center">
			<button type="submit" class="btn btn-primary btnSubmit">Update</button>
		</div>
		</form>
	</div>
@endsection

@push('js')
<script type="text/javascript">
	$('#country').change(function () {
            var country_id = $(this).val();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
			});
			$.ajax({
				url : "{{ route('ajax.load.states') }}",
				type: "POST",
				dataType: 'JSON',
				data: { country_id: country_id},
				success: function(response){
					var states = '';
					if(response.status == 'success'){
						console.log(response);
						$(response.states).each( function(i,d){
							states += '<option value="'+ d.id +'" {{"'+ d.id +'"==$hotel->country_id?'selected="selected"' : ''}}>'+ d.name +'</option>';
						});

						$("#state").html(states);
					}else{
						toastr['error']('Something went wrong.');
					}
				},
				error: function(error){
					toastr['error']('Something went wrong.');
				}
			});
        });

	$('#state').change(function () {
		var state_id = $(this).val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			}
		});
		$.ajax({
			url : "{{ route('ajax.load.cities') }}",
			type: "POST",
			dataType: 'JSON',
			data: { state_id: state_id},
			success: function(response){
				console.log(response);
				var cities = '';
				if(response.status == 'success'){
					$(response.cities).each( function(i,d){
						cities += '<option value="'+ d.id +'">'+ d.name +'</option>';
					});

					$("#city").html(cities);
				}else{
					toastr['error']('Something went wrong.');
				}
			},
			error: function(error){
				toastr['error']('Something went wrong.');
			}
		});
	});
	$(".country").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Country"//placeholder
   });
	$(".state").select2({
        maximumSelectionLength: 1,
        placeholder: "Select State"//placeholder
   });
	$(".city").select2({
        maximumSelectionLength: 1,
        placeholder: "Select City"//placeholder
   });
</script>
@endpush
