@extends('adminlte::page')
@section('title', 'Hotels')

@section('content_header')
<h1>Hotels</h1>
{{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection

@section('content')
	<div class="box">
		<div class="box-body travelAgencyTableBtn">
       <div class="col-md-12 text-right">
        <a href="{{ route('hotels.create') }}" class="btn btn-primary btnSubmit">Add Hotel</a>
      </div>
      <div class="clearfix"></div>
			<table id="hotels" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th>Sr#</th>
          	<th>Name</th>
          	<th>Country</th>
            <th>State</th>
            <th>City</th>
            <th>Address</th>
            <th>Phone#</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($hotels as $hotel)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $hotel->name }}</td>
          <td>{{ $hotel->country->name??"" }}</td>
          <td>{{ $hotel->state->name??"" }}</td>
          <td>{{ $hotel->city->name??"" }}</td>
          <td>{{ $hotel->address??"" }}</td>
          <td>{{ $hotel->phone_number??"" }}</td>
          <td>
          	<a href="{{ route('hotels.edit', $hotel->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
          	{{-- <a href="{{ route('hotels.show', $hotel->id) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Step"><i class="fa fa-eye"></i></button></a> --}}
          	<form action="{{ route('hotels.destroy', $hotel->id) }}" method="POST" class="delete_item">
          		@csrf
          		@method('DELETE')
          		<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
          	</form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
		</div>
	</div>
@endsection
@push('js')
  <script type="text/javascript">
    $(function () {
      $('#hotels').DataTable({
      	'lengthChange': true,
        'searching': false,
        "columnDefs": [
                 { orderable: false, targets: 7}
                 ],
      });
    });
  </script>
@endpush
