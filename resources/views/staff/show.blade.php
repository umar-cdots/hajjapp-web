@extends('adminlte::page')
@section('title', 'Staff Detail')

@section('content_header')
<h1>Staff Detail</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection

@section('content')
	<div class="box">
            <div class="box-header">
              <a href="{{ route('staffs.create') }}" class="btn btn-primary">Add New</a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
               <tbody>
                <tr>
                  <th>Name</th>
                  <td>{{ $staff->name ?? '-'  }}</td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td>{{ $staff->user->email ?? '-' }}</td>
                </tr>
                <tr>
                	<th>Primary Phone Number</th>
                	<td>{{ $staff->primary_phone_number ?? '-'  }}</td>
                </tr>
                <tr>
                	<th>Secondary Phone Number</th>
                	<td>{{ $staff->secondary_phone_number ?? '-'  }}</td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection