@extends('adminlte::tasks')
@section('title', 'Add Staff')

@section('content_header')
    {{-- <a href="{{route('staffs.index')}}" class="btn btn-primary">Back</a> --}}
    <ol class="breadcrumb">
        {{-- <li><a href="{{route('staffs.index')}}">Staff Management</a></li>
        <li class="active">Add Staff</li> --}}
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
    {{-- <h1>Add Staff</h1> --}}
@endsection

@section('content')
    <div class="box">
        <form action="{{ route('staffs.store') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Staff Name <span>*</span></label>
                            <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                                   placeholder="Staff Name">
                            @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Type</label>
                            <select name="type" class="form-control select2 SelectType" multiple="multiple">
                                <option value="Permanent" {{ ( old('type')== 'Permanent' ) ? 'selected' : '' }}>
                                    Permanent
                                </option>
                                <option value="Part-time" {{ ( old('type')== 'Part-time' ) ? 'selected' : '' }}>
                                    Part-time
                                </option>
                                <option value="Supplier" {{ ( old('type')== 'Supplier' ) ? 'selected' : '' }}>Supplier
                                </option>
                                <option value="Other" {{ ( old('type')== 'Other' ) ? 'selected' : '' }}>Other</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Role <span>*</span></label>
                            <select name="role" class="form-control select2 SelectRole" multiple="multiple"
                                    required="required">
                                <option value="Admin" {{ ( old('role')== 'Admin' ) ? 'selected' : '' }}>Admin</option>
                                <option value="Staff" {{ ( old('role')== 'Staff' ) ? 'selected' : '' }}>Staff</option>
                                <option value="mobile_agent" {{ ( old('role')== 'mobile_agent' ) ? 'selected' : '' }}>
                                    Mobile Agent
                                </option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('primary_phone_number') ? 'has-error' : '' }}">
                            <label>Phone Number <span>*</span></label>
                            <input type="text" name="primary_phone_number" class="form-control"
                                   value="{{ old('primary_phone_number') }}" placeholder="Phone Number">
                            @if($errors->has('primary_phone_number'))
                                <span class="help-block text-danger">{{ $errors->first('primary_phone_number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('secondary_phone_number') ? 'has-error' : '' }}">
                            <label>Backup Phone Number <span>*</span></label>
                            <input type="text" name="secondary_phone_number" class="form-control"
                                   value="{{ old('secondary_phone_number') }}" placeholder="Backup Phone Number">
                            @if($errors->has('secondary_phone_number'))
                                <span
                                    class="help-block text-danger">{{ $errors->first('secondary_phone_number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('emergency_phone_number') ? 'has-error' : '' }}">
                            <label>Emergency Phone Number <span>*</span></label>
                            <input type="text" name="emergency_phone_number" class="form-control"
                                   value="{{ old('emergency_phone_number') }}" placeholder="Emergency Phone Number">
                            @if($errors->has('emergency_phone_number'))
                                <span
                                    class="help-block text-danger">{{ $errors->first('emergency_phone_number') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label>Email(UserName) <span>*</span></label>
                            <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                                   placeholder="Email">
                            @if($errors->has('email'))
                                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label>Profile Image</label>
                            <input type="file" name="image" class="form-control" id="imgpreview">
                            @if($errors->has('image'))
                                <span class="help-block text-danger">{{ $errors->first('image') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" name="description" rows="2"
                                      placeholder="Note About Staff">{{ old('description') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create Staff</button>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        function staffImageUpload(input) {

            if ($("#imgpreview").prop("files")[0]) {
                switch ($("#imgpreview").val().substring($("#imgpreview").val().lastIndexOf('.') + 1).toLowerCase()) {
                    case 'gif':
                    case 'jpg':
                    case 'png':

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#viewImage').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                        break;
                    default:
                        $("#imgpreview").val('');
                        toastr['error']("File format not correct!");
                        break;
                }
            }

            // if (input.files && input.files[0]) {
            //     var reader = new FileReader();
            //     reader.onload = function (e) {
            //         $('#viewImage').attr('src', e.target.result);
            //     }
            //     reader.readAsDataURL(input.files[0]);
            // }
        }

        $("#imgpreview").change(function () {
            staffImageUpload(this);
        });
        $(".SelectRole").select2({
            maximumSelectionLength: 1,
            placeholder: "Select Role"//placeholder
        });
        $(".SelectType").select2({
            maximumSelectionLength: 1,
            placeholder: "Select Type"//placeholder
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    </script>
@endpush
