@extends('adminlte::tasks')
@section('title', 'Edit Staff')

@section('content_header')
{{-- <a href="{{route('staffs.index')}}" class="btn btn-primary">Back</a> --}}
        <ol class="breadcrumb">
        {{-- <li><a href="{{route('staffs.index')}}">Staff Management</a></li>
        <li class="active">Edit Staff</li> --}}
        <li><a href="{{ URL::previous() }}">Back</a></li>
      </ol>
      <div class="clearfix"></div>
@endsection

@section('content')
	<div class="box">
		<form action="{{ route('staffs.update', $staff->id) }}" method="POST" role="form" enctype="multipart/form-data">
			@csrf
			@method('PUT')
		<div class="box-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>Staff Name <span>*</span></label>
						<input type="text" name="name" class="form-control" value="{{ $staff->name }}">
						@if($errors->has('name'))
						<span class="help-block text-danger">{{ $errors->first('name') }}</span>
					  @endif
					</div>
				</div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Type</label>
                        <select name="type" class="form-control select2 SelectType" multiple="multiple">
                            @if($staff->type=="Permanent")
                            <option value="">Select</option>
                            <option value="Permanent" selected="selected">Permanent</option>
                            <option value="Part-time">Part-time</option>
                            <option value="Supplier">Supplier</option>
                            <option value="Other">Other</option>
                            @elseif($staff->type=="Part-time")
                            <option value="">Select</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Part-time" selected="selected">Part-time</option>
                            <option value="Supplier">Supplier</option>
                            <option value="Other">Other</option>
                            @endelseif
                            @elseif($staff->type=="Supplier")
                            <option value="">Select</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Part-time">Part-time</option>
                            <option value="Supplier" selected="selected">Supplier</option>
                            <option value="Other">Other</option>
                            @endelseif
                            @elseif($staff->type=="Other")
                            <option value="">Select</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Part-time">Part-time</option>
                            <option value="Supplier">Supplier</option>
                            <option value="Other" selected="selected">Other</option>
                            @endelseif
                            @else
                            <option value="">Select</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Part-time">Part-time</option>
                            <option value="Supplier">Supplier</option>
                            <option value="Other">Other</option>
                            @endif
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Role <span>*</span></label>
                        <select name="role" class="form-control select2 SelectRole" multiple="multiple" required="required">
                            @if($staff->role=="Admin")
                            <option value="">Select</option>
                            <option value="Admin" selected="selected">Admin</option>
                            <option value="Staff">Staff</option>
                            <option value="mobile_agent">Mobile Agent</option>
                            @elseif($staff->role=="Staff")
                            <option value="">Select</option>
                            <option value="Admin">Admin</option>
                            <option value="Staff" selected="selected">Staff</option>
                            <option value="mobile_agent">Mobile Agent</option>
                            @endelseif
                            @elseif($staff->role=="mobile_agent")
                            <option value="">Select</option>
                            <option value="Admin">Admin</option>
                            <option value="Staff">Staff</option>
                            <option value="mobile_agent" selected="selected">Mobile Agent</option>
                            @endelseif
                            @else
                            <option value="">Select</option>
                            <option value="Admin">Admin</option>
                            <option value="Staff">Staff</option>
                            @endif
                        </select>
                    </div>
                </div>

			</div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('primary_phone_number') ? 'has-error' : '' }}">
                        <label>Phone Number <span>*</span></label>
                        <input type="text" name="primary_phone_number" class="form-control" value="{{ $staff->primary_phone_number }}">
                        @if($errors->has('primary_phone_number'))
                            <span class="help-block text-danger">{{ $errors->first('primary_phone_number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('secondary_phone_number') ? 'has-error' : '' }}">
                        <label>Backup Phone Number <span>*</span></label>
                        <input type="text" name="secondary_phone_number" class="form-control" value="{{ $staff->secondary_phone_number }}">
                        @if($errors->has('secondary_phone_number'))
                            <span class="help-block text-danger">{{ $errors->first('secondary_phone_number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('emergency_phone_number') ? 'has-error' : '' }}">
                        <label>Emergency Phone Number <span>*</span></label>
                        <input type="text" name="emergency_phone_number" class="form-control" value="{{ $staff->emergency_phone_number }}">
                        @if($errors->has('emergency_phone_number'))
                            <span class="help-block text-danger">{{ $errors->first('emergency_phone_number') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" value="{{ $staff->user?$staff->user->email:"" }}" disabled>
                        @if($errors->has('email'))
                            <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description" placeholder="Note About Staff" rows="5" >{{ $staff->description }} </textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label>Profile Image</label>
                        <input type="file" name="image" class="form-control" id="imgpreview">
                        @if($staff->img)
                        <img id="viewImage" src="{{ asset('storage/profile_images/'. $staff->img) }}" alt=""  width="100px" />
                        @else
                        <img id="viewImage" src="{{ asset('images/userImg.png') }}" alt=""  width="100px" />
                        @endif
                        @if($errors->has('image'))
                            <span class="help-block text-danger">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                </div>
            </div>
		</div>
		<div class="box-footer text-center">
			<button type="submit" class="btn btn-primary btnSubmit">Update Staff</button>
		</div>
		</form>
	</div>
@endsection
@push('js')
    <script type="text/javascript">
        function staffImageUpload(input) {
            if ($("#imgpreview").prop("files")[0]) {
                switch ($("#imgpreview").val().substring($("#imgpreview").val().lastIndexOf('.') + 1).toLowerCase()) {
                    case 'gif':
                    case 'jpg':
                    case 'png':

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#viewImage').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                        break;
                    default:
                        $("#imgpreview").val('');
                        toastr['error']("File format not correct!");
                        break;
                }
            }

            //
            // if (input.files && input.files[0]) {
            //     var reader = new FileReader();
            //     reader.onload = function(e) {
            //         $('#viewImage').attr('src', e.target.result);
            //     }
            //     reader.readAsDataURL(input.files[0]);
            // }
        }
        $("#imgpreview").change(function() {
            staffImageUpload(this);
        });
        $(".SelectRole").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Role"//placeholder
        });
        $(".SelectType").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Type"//placeholder
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    </script>
@endpush
