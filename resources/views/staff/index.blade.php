@extends('adminlte::page')
@section('title', 'All Staffs')

@section('content_header')
<h1>Staff Management</h1>
{{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection

@section('content')
	 <div class="box">
        <div class="box-body">
            <form action="{{ route('staffs.search') }}" method="POST">
                @csrf
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                @method('POST')
            <div class="row">
                    <div class="col-md-7 col-xs-10">
                        <div class="form-group" >
                            <select name="staff_id" id="StaffSelect2" class="form-control StaffSelect2" multiple="true">
                                <option value="">Search Staff</option>
                                @if(!empty($staffforselect2))
                                    @foreach($staffforselect2 as $staffset)
                                        <option value="{{ $staffset->id }}">{{ $staffset->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1" style="padding: 0;">
                        <div class="form-group">
                           <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="col-md-4 form-group clientBtnAdd text-right">
                        <a href="{{ route('staffs.create') }}" class="btn btn-primary btnSubmit">Add Staff</a>
                        <a href="#staffuploadModal" class="btn btn-primary btnSubmit" data-toggle="modal" id="BtnstaffuploadModal">Upload Staff List</a>
                        <a href="{{route('load.excel.downloadstaff')}}" class="btn btn-primary btnSubmit">Download Staff List</a>
                    </div>

            </div>
            </form>
        <!-- /.box-header -->
            <table id="staffs" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Role</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($staffs as $staff)
                    <tr>
                        <td>{{ $staff->name }}</td>
                        <td>{{ $staff->id }}</td>
                    	<td>{{ $staff->type }}</td>
                        <td>{{ $staff->role }}</td>
                        <td>
                            <input type="checkbox" id="changestaffstatus" value="{{$staff->status}}" {{ $staff->status==1 ? "checked" : "" }} data-toggle="toggle" onchange="changestaffstatus({{$staff->id}})">
                        </td>
                        <td>
                            <a href="{{ route('staffs.edit', $staff->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            {{-- <a href="{{ route('staffs.show', $staff->id) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button></a> --}}
                            <form action="{{ route('staffs.destroy', $staff->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                            <a href="{{ route('staffs.viewprofile', $staff->id) }}"><button class="btn btn-xs btn-primary btnSubmit" data-toggle="tooltip" title="Profile">View Profile</button></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

<div class="modal fade in" id="staffuploadModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Upload Staff From Excel Sheet</h4>
      </div>
      <form action="{{ route('load.excel.storestaff') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>File</label>
                        <input type="file" name="Stafffile" class="form-control">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Upload</button>
       </div>
        </form> 
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@endsection
{{-- @push('css')
<style type="text/css">
    .dataTables_length
    {
        float: right;
    }
    .dataTables_paginate 
    {
        float: right;
    }
</style>
@endpush --}}
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#staffs').DataTable({
                'lengthChange': true,
                'bFilter': false,
                "columnDefs": [
                 { orderable: false, targets: [4,5] }
                 ],
                // "dom": '<"top"lp>rt<"bottom"i><"clear">'
            });
            // $('.dataTables_wrapper .top').wrapAll('<div class="row"></div>');
            // $('.dataTables_wrapper .top').prepend('<div class="col-md-6 addClientBtn"></div>');
            // $("body").find('.clientBtnAdd').appendTo(".addClientBtn");
            // $('.dataTables_length').wrapAll('<div class="col-md-2"></div>');
            // $('.dataTables_paginate ').wrapAll('<div class="col-md-4"></div>');

        });
        function changestaffstatus(id) {
        $.ajax({
            url: "{{route('staffs.ChangeStaffStatus')}}",
            data:{_token: $("#token").val(),id:id},
            type: "POST",
            success: function (json) {
                        html = '<option>Select</option>';
                        $(json.staffforselect2).each( function (i,d){
                            html += '<option value="'+ d.id +'">'+ d.name +'</option>';
                        });
                        $("#StaffSelect2").html(html);
               }
        });    
    }
    $(".StaffSelect2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Staff"//placeholder
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    </script>
@endpush
