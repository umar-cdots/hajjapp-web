@extends('adminlte::tasks')
@section('title', 'View Staff Profile')

@section('content_header')
{{-- <a href="{{route('staffs.index')}}" class="btn btn-primary">Back</a> --}}
        <ol class="breadcrumb">
        {{-- <li><a href="{{route('staffs.index')}}">Staff Management</a></li>
        <li class="active">Staff Profile</li> --}}
        <li><a href="{{ URL::previous() }}">Back</a></li>
      </ol>
      <div class="clearfix"></div>
@endsection

@section('content')
	<div class="box">

		<form method="POST" role="form" enctype="multipart/form-data">
			@csrf
		<div class="box-body">
            <div class="row">
         <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $total_trips }}</h3>
              <p>Active Trips</p>
            </div>
            <div class="icon">
              <i class="fa fa-plane"></i>
            </div>
            <a href="{{ route('trips.staffdetail',$staff->id) }}" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
         {{-- <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $total_messages }}</h3>
              <p>Total Messages</p>
            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="#" class="small-box-footer">&nbsp;</a>
          </div>
        </div> --}}
         <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $overduetask }}</h3>
              <p>Tasks Overdue</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="{{ route('trip.task.stafftaskoverduedetail',$staff->id) }}" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
             @if($staff->img)
                <img id="viewImage" src="{{ asset('storage/profile_images/'. $staff->img) }}" alt=""  width="100px" />
            @else
               <img id="viewImage" src="{{ asset('images/userImg.png') }}" alt=""  width="100px" />
            @endif
        </div>
        </div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>Staff Name <span>*</span></label>
						<input type="text" name="name" class="form-control" value="{{ $staff->name }}" readonly="readonly">
						@if($errors->has('name'))
						<span class="help-block text-danger">{{ $errors->first('name') }}</span>
					  @endif
					</div>
				</div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Type</label>
                        <input type="text" name="name" class="form-control" value="{{ $staff->type }}" readonly="readonly">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Role</label>
                        <input type="text" name="role" class="form-control" value="{{ $staff->role }}" readonly="readonly">
                    </div>
                </div>



			</div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('primary_phone_number') ? 'has-error' : '' }}">
                        <label>Phone Number <span>*</span></label>
                        <input type="text" name="primary_phone_number" class="form-control" value="{{ $staff->primary_phone_number }}" readonly="readonly">
                        @if($errors->has('primary_phone_number'))
                            <span class="help-block text-danger">{{ $errors->first('primary_phone_number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('secondary_phone_number') ? 'has-error' : '' }}">
                        <label>Secondary Phone Number <span>*</span></label>
                        <input type="text" name="secondary_phone_number" class="form-control" value="{{ $staff->secondary_phone_number }}" readonly="readonly">
                        @if($errors->has('secondary_phone_number'))
                            <span class="help-block text-danger">{{ $errors->first('secondary_phone_number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('emergency_phone_number') ? 'has-error' : '' }}">
                        <label>Emergency Phone Number <span>*</span></label>
                        <input type="text" name="emergency_phone_number" class="form-control" value="{{ $staff->emergency_phone_number }}" readonly="readonly">
                        @if($errors->has('emergency_phone_number'))
                            <span class="help-block text-danger">{{ $errors->first('emergency_phone_number') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>Email <span>*</span></label>
                        <input type="email" name="email" class="form-control" value="{{ $staff->user->email }}" readonly="readonly">
                        @if($errors->has('email'))
                        <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date Created</label>
                        <input type="text" name="name" class="form-control" value="{{ $staff->created_at }}" readonly="readonly">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description" rows="2" readonly="readonly">{{ $staff->description }}</textarea>
                    </div>
                </div>
            </div>
		</div>
		<div class="box-footer">
		</div>
		</form>
	</div>
  </div>
@endsection
@push('js')
    <script type="text/javascript">
        function readURL(input) {
            if ($("#imgpreview").prop("files")[0]) {
                switch ($("#imgpreview").val().substring($("#imgpreview").val().lastIndexOf('.') + 1).toLowerCase()) {
                    case 'gif':
                    case 'jpg':
                    case 'png':

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#viewImage').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                        break;
                    default:
                        $("#imgpreview").val('');
                        toastr['error']("File format not correct!");
                        break;
                }
            }
            // if (input.files && input.files[0]) {
            //     var reader = new FileReader();
            //     reader.onload = function(e) {
            //         $('#viewImage').attr('src', e.target.result);
            //     }
            //     reader.readAsDataURL(input.files[0]);
            // }
        }
        $("#imgpreview").change(function() {
            readURL(this);
        });
    </script>
@endpush
