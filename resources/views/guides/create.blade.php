@extends('adminlte::page')
@section('title', 'Guides')

@section('section_header')
    <h1>Guides</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            {{-- <h3 class="box-title">Guides</h3> --}}
        </div>
        <div class="box-body">
            <form action="{{ route('guide.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group {{ $errors->has('guides') ? 'has-error' : '' }}">
                            <label>Upload Multiple Guides</label>
                            <input type="file" name="guides[]" class="form-control" multiple>
                            @if($errors->has('guides'))
                                <span class="help-block text-danger">{{ $errors->first('guides') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary btnSubmit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')

@endpush