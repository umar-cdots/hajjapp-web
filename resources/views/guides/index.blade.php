@extends('adminlte::page')
@section('title', 'Guides')

@section('content_header')
    <h1>Guides</h1>
   <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                {{-- <h3 class="box-title">Guides</h3> --}}
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('guide.create') }}" class="btn btn-primary">Add Guide</a>
            </div>
        </div>
        <div class="box-body">
            <table id="airlines" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>File name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($guides as $guide)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $guide->title }}</td>
                        <td>
                            <form action="{{ route('guide.destroy', $guide->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#airlines').DataTable({
                'lengthChange': false,
            });
        });
    </script>
@endpush
