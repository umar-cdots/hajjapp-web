<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ahlan Guide</title>
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/media.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/intlTelInput.min.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->

    <style>
        .country-name {
            display: none !important;
        }

        .intl-tel-input .country-list {
            width: 235px !important;
        }
    </style>

</head>


<body>

<div class="banner">


    <div class="container-fluid bannerInner">
        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="4000">
            <div class="bannerTop ">
                <div class="row">
                    <div class="col-md-7 col-sm-6 col-xs-12">
                        <div class="logo">
                            <a href="{{ route('welcome.index') }}"><img src="{{ asset('frontend/images/logo.png') }}"
                                                                        alt="Ahlan Guide"><span>Your trip
                                        management
                                        partner</span> </a>
                        </div>

                    </div>

                    <div class="col-md-5 col-sm-6 col-xs-12">

                        <div class="topBtn">

                            <a href="{{ route('login') }}" target="_blank" class="btn effect01"><span>Sign In</span></a>

                            <a href="javascript:void(0)" class="btn effect01" data-toggle="modal"
                               data-target="#myModal"><span>contact us</span></a>

                        </div>

                    </div>

                </div>

            </div>

            <!-- Indicators -->

            <ol class="carousel-indicators">

                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

                <li data-target="#myCarousel" data-slide-to="1"></li>

                <li data-target="#myCarousel" data-slide-to="2"></li>

                <li data-target="#myCarousel" data-slide-to="3"></li>

            </ol>


            <!-- Wrapper for slides -->

            <div class="container-fluid bannerInner">

                <div class="carousel-inner">

                    <!-- 1st screen -->


                    <div class="item item1 active"
                         style="background: url('{{ asset('frontend/images/banner-bg-2.png') }}') no-repeat; background-size: cover;">

                        <div class="carousel-caption">

                            <div class="row">

                                <div class="col-md-6 col-sm-6 col-xs-6 contentLeft contentPara">

                                    <h1>World's First Trip <span>Management Trip</span></h1>

                                    <p>Whether you're a travel agency or a tour guide,

                                        Ahlan Guide is made for you! </p>

                                    <div class="downloadApp">

                                        <a href="https://play.google.com/store/apps/details?id=com.ahlanguide.user"
                                           target="_blank"><img src="{{ asset('frontend/images/download-1.png') }}"
                                                                alt=""></a>

                                        <a href="#"><img src="{{ asset('frontend/images/download-2.png') }}" alt=""></a>

                                    </div>

                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-6 contentRight">

                                    <img src="{{ asset('frontend/images/alhan-app-2.png') }}" alt="">

                                </div>

                            </div>

                        </div>

                    </div>


                    <!-- 2nd screen -->


                    <div class="item item2"
                         style="background: url('{{ asset('frontend/images/banner-bg-3.png') }}') no-repeat; background-size: cover;">

                        <div class="carousel-caption">

                            <div class="row">

                                <div class="col-md-6 col-sm-6 col-xs-6 contentLeft contentHeader">

                                    <h1>Be the personal virtual<span>guide for your client!</span></h1>

                                    <div class="bannerList">

                                        <ul>

                                            <li>Plan daily activities</li>

                                            <li>Make packing checklists</li>

                                            <li>Share do's and don'ts</li>

                                            <li>Travel Hacks</li>

                                            <li>Promote offers</li>

                                            <li>Notify about events</li>

                                            <li>List places to visit</li>

                                            <li>Show videos</li>

                                        </ul>

                                    </div>

                                    <div class="downloadApp">

                                        <a href="https://play.google.com/store/apps/details?id=com.ahlanguide.user"
                                           target="_blank"><img src="{{ asset('frontend/images/download-1.png') }}"
                                                                alt=""></a>

                                        <a href="#"><img src="{{ asset('frontend/images/download-2.png') }}" alt=""></a>

                                    </div>

                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-6 contentRight">

                                    <img src="{{ asset('frontend/images/alhan-app-3.png') }}" alt="">

                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- 3rd screen -->


                    <div class="item item3"
                         style="background: url('{{ asset('frontend/images/banner-bg.png') }}') no-repeat; background-size: cover;">

                        <div class="carousel-caption">

                            <div class="row">

                                <div class="col-md-6 col-sm-6 col-xs-6 contentLeft">

                                    <h1>What you get with <span>Ahlan Guide</span></h1>

                                    <div class="bannerList">

                                        <ul>

                                            <li>Option to assign multiple trips to your clients</li>

                                            <li>Define virtual journeys</li>

                                            <li>24/7 chat</li>

                                            <li>Emergency alert and location tracking</li>

                                            <li>Customer feedback and complaint management</li>

                                            <li>Increased customer loyalty</li>

                                            <li>Google Maps integration</li>

                                            <li>Unlimited travel guides</li>

                                            <li>Flight information and updates</li>

                                            <li>Notify your client instantly</li>

                                            <li>Task and employee management</li>

                                        </ul>

                                    </div>

                                    <div class="downloadApp">

                                        <a href="https://play.google.com/store/apps/details?id=com.ahlanguide.user"
                                           target="_blank"><img src="{{ asset('frontend/images/download-1.png') }}"
                                                                alt=""></a>

                                        <a href="#"><img src="{{ asset('frontend/images/download-2.png') }}" alt=""></a>

                                    </div>

                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-6 contentRight">

                                    <img src="{{ asset('frontend/images/alhan-app-1.png') }}" alt="">

                                </div>

                            </div>

                        </div>

                    </div>


                    <!-- 4th screen -->

                    <div class="item item4"
                         style="background: url('{{ asset('frontend/images/banner-bg-4.png') }}') no-repeat; background-size: cover;">

                        <div class="carousel-caption">

                            <div class="row">

                                <div class="col-md-4 col-sm-4 col-xs-6 contentLeft">

                                    <h2>Use our interactive trip<span>management platform</span><span> for your
                                                clients</span></h2>

                                    <p>enhancing customer experience throughout

                                        the journey using an attractive mobile application

                                        which can be managed by the travel agent through

                                        our extensive trip management platform without

                                        writing a single line of code</p>

                                    <div class="downloadApp">

                                        <a href="https://play.google.com/store/apps/details?id=com.ahlanguide.user"
                                           target="_blank"><img src="{{ asset('frontend/images/download-1.png') }}"
                                                                alt=""></a>

                                        <a href="#"><img src="{{ asset('frontend/images/download-2.png') }}" alt=""></a>

                                    </div>

                                </div>

                                <div class="col-md-8 col-sm-8 col-xs-6 layerRight">

                                    <img src="{{ asset('frontend/images/Layer-right.png') }}" alt="">

                                </div>

                            </div>

                        </div>

                    </div>


                </div>


                <footer class="">

                    <p>Copyright 2019 by Ahlan Guide</p>

                    <ul>

                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>

                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>

                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>

                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>

                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>

                    </ul>

                </footer>


                <div class="modal fade" id="myModal" role="dialog">

                    <div class="modal-dialog">


                        <!-- Modal content-->

                        <div class="modal-content">

                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                            <div class="modal-header">


                                <h4 class="modal-title">Contact Us</h4>

                                <p>Want to use this unique platform and increase your customer loyalty? Get in touch

                                    with us, and we will reach out to you soon. </p>

                            </div>

                            <div class="modal-body">


                                <form action="{{ route('contact.us') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="contactForm">

                                        <div class="row">

                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <div class="form-group">

                                                    <div class="input-group">

                                                        <span class="input-group-addon btnIcon">

                                                            <img src="{{ asset('frontend/images/shape1.png') }}" alt="">

                                                        </span>

                                                        <input type="text" class="form-control formHeight"
                                                               name="first_name" required
                                                               placeholder="First Name">

                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <div class="form-group">

                                                    <div class="input-group">

                                                        <span class="input-group-addon btnIcon">

                                                            <img src="{{ asset('frontend/images/shape1.png') }}" alt="">

                                                        </span>

                                                        <input type="text" class="form-control formHeight"
                                                               placeholder="Last Name" name="last_name" required>

                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <div class="form-group">

                                                    <div class="input-group">

                                                        <span class="input-group-addon btnIcon">

                                                            <img src="{{ asset('frontend/images/shape2.png') }}" alt="">

                                                        </span>

                                                        <input type="text" class="form-control formHeight"
                                                               placeholder="Company" name="company_name" required>

                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <div class="form-group">

                                                    <div class="input-group">

                                                        <span class="input-group-addon btnIcon">

                                                            <img src="{{ asset('frontend/images/shape5.png') }}" alt="">

                                                        </span>

                                                        <input type="text" class="form-control formHeight"
                                                               placeholder="Country" name="country" required>

                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <div class="form-group">

                                                    <div class="input-group">

                                                        <span class="input-group-addon btnIcon">

                                                            <img src="{{ asset('frontend/images/shape4.png') }}" alt="">

                                                        </span>

                                                        <input type="text" class="form-control formHeight"
                                                               placeholder="Email" name="email" required>

                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <div class="form-group">

                                                    <input type="tel" class="form-control" id="telField"
                                                           name="phone_number" required>

                                                </div>

                                            </div>


                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <button type="submit" class="btn btn-default contactBtn">Submit
                                    </button>
                                </form>


                            </div>

                        </div>


                    </div>

                </div>


            </div>

        </div>

    </div>


    <!-- jQuery -->

    <script src="{{ asset('frontend/js/jquery-3.5.1.min.js') }}"></script>

    <!-- custom JavaScript -->

    <script src="{{ asset('frontend/js/custom.js') }}"></script>

    <!-- Bootstrap JavaScript -->

    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>


    <script src="{{ asset('frontend/js/intlTelInput.min.js') }}"></script>

    <script src="{{ asset('frontend/js/utils.js') }}"></script>


    <script>


        $('#telField').intlTelInput({

            initialCountry: "ae",

            geoIpLookup: function (success, failure) {
                $('.country-name').text('');
                $.get("https://ipinfo.io", function () {
                }, "jsonp").always(function (resp) {


                    var countryCode = (resp && resp.country) ? resp.country : "";

                    success(countryCode);

                });

            },

        });


    </script>

</div>
</body>


</html>


<!-- var formattedNumber = intlTelInputUtils.formatNumber("07733123456", "gb", intlTelInputUtils.numberFormat.INTERNATIONAL); -->
