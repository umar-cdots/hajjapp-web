@extends('adminlte::page')
@section('title', 'Feedback')

@section('content_header')
    <h1>Feedback</h1>
    <div class="clearfix"></div>
    {{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
@endsection


@section('content')
    <div class="box">

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="active" id="feedbackurl"><a data-toggle="tab" href="#feedback" id="btnfeedback"
                                                               aria-expanded="false" class="activeBtnTab">Feedback</a>
                        </li>
                        <li id="complainturl"><a href="#complaint" data-toggle="tab" id="btncomplaint"
                                                 aria-expanded="false" class="">Complaints</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">

            </div>
            <div class="tab-content">
                <div class="tab-pane active fade in" id="feedback">
                    <table id="FeedbackTable" class="table table-bordered table-striped dataTable no-footer">
                        <thead>
                        <tr>
                            <th>Client Name</th>
                            <th>Trip</th>
                            <th>Category</th>
                            <th>Group</th>
                            <th>Feedback Category</th>
                            <th>Feedback</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($allfeedbacks)>0)
                            @foreach($allfeedbacks as $feedback)
                                <tr>
                                    <td>{{ $feedback->clientgrouptrip->client->name ?? '--' }}</td>
                                    <td>{{ $feedback->clientgrouptrip->clienttrip->trip->title ?? '--' }}</td>
                                    <td>{{ $feedback->clientgrouptrip->client->client_category->title ?? '--' }}</td>
                                    <td>{{ $feedback->clientgrouptrip->clienttrip->group->group_name ?? '--' }}</td>
                                    <td>{{ $feedback->feedbackcategory->title ?? '--' }}</td>
                                    <td>{{ $feedback->feedback ?? '--' }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="complaint">
                    <table id="ComplaintTable" class="table table-bordered table-striped dataTable no-footer">
                        <thead>
                        <tr>
                            <th>Client Name</th>
                            <th>Trip</th>
                            <th>Category</th>
                            <th>Group</th>
                            <th>Feedback Category</th>
                            <th>Feedback</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($compliantfeedback)>0)
                            @foreach($compliantfeedback as $feedback)
                                <tr>
                                    <td>{{ $feedback->clientgrouptrip->client->name ?? '--' }}</td>
                                    <td>{{ $feedback->clientgrouptrip->clienttrip->trip->title ?? '--' }}</td>
                                    <td>{{ $feedback->clientgrouptrip->client->client_category->title ?? '--' }}</td>
                                    <td>{{ $feedback->clientgrouptrip->clienttrip->group->group_name ?? '--' }}</td>
                                    <td>{{ $feedback->feedbackcategory->title ?? '--' }}</td>
                                    <td>{{ $feedback->feedback ?? '--' }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script type="text/javascript">

        $(function () {
            var SessionID = "{{$valuecheck}}";
            if (SessionID == 1) {
                $("#complaint").addClass('active');
                $("#complaint").removeClass('fade');

                $("#feedback").removeClass('active');
                $("#feedback").addClass('fade');

                $("#complainturl").addClass('active');

                $("#feedbackurl").removeClass('active');

                $("#btncomplaint").addClass('activeBtnTab');
                $("#btnfeedback").removeClass('activeBtnTab');
            }
        });

        $(function () {
            $('#FeedbackTable').DataTable({
                'bFilter': false,
            });
            $('#ComplaintTable').DataTable({
                'bFilter': false
            });
        });
        $("#btncomplaint").click(function () {
            $("#btncomplaint").addClass('activeBtnTab');
            $("#btnfeedback").removeClass('activeBtnTab');
        });
        $("#btnfeedback").click(function () {
            $("#btnfeedback").addClass('activeBtnTab');
            $("#btncomplaint").removeClass('activeBtnTab');
        });
    </script>
@endpush
