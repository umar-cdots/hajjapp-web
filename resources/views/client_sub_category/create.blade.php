@extends('adminlte::page')

@section('title', 'Add Client Sub Category')

@section('content_header')
<h1>Add Client Sub Category</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <form action="{{ route('client_sub_categories.store') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('client_category_id') ? 'has-error' : '' }}">
                        <label>Select Client Category<span>*</span></label>
                        <select name="client_category_id" class="form-control client_category_id" multiple="multiple">
                            @foreach($client_categories as $client_category)
                                <option value="{{ $client_category->id }}">{{ $client_category->title }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('client_category_id'))
                            <span class="help-block text-danger">{{ $errors->first('client_category_id') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label>Sub Category Name <span>*</span></label>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                          @if($errors->has('name'))
                            <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create</button>
            </div>
        </form>
    </div>

@endsection



@push('js')
<script type="text/javascript">
    $(".client_category_id").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Client Catagory"//placeholder
   });
</script>
@endpush