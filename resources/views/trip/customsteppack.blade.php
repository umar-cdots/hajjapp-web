@extends('adminlte::trip')
@section('title', 'Add Step')

@section('content_header')
    <h1>Manage your Trip steps</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection


@section('content')

    <div class="box">
        {{-- <form id="step_pack_form" method="post" role="form" enctype="multipart/form-data">
            <input type="hidden" name="trip_id" value="{{ $trip->id ?? '' }}">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Step Pack Name</label>
                            <input type="text" class="form-control" name="title" placeholder="Enter Step Pack Name" value="{{ old('title') }}" required="">
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnSubmit" id="step_pack_submit_btn">Submit</button>
            </div>
        </form> --}}
        <div class="box-body">
            <div class="row">
                <div class="col-md-9 col-sm-7 col-xs-7">
                    <div class="form-group">
                        <select name="steppack_id" id="StepPackSelect2" class="form-control StepPackSelect2"
                                multiple="true">
                            @foreach($trips as $getSteppack)
                                <option
                                    value="{{ $getSteppack->steppack->id }}">{{ $getSteppack->steppack->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-1 col-sm-4 col-xs-4">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btnSubmit" id="BtnSearchStepPack">Save as Trip
                            Template
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-unstyled list-inline text-right assignCustomerBtns">
                        <li><a href="#" class="btn btn-primary btnSubmit" id="BtnAddStep">Add Step</a></li>
                    </ul>
                </div>
            </div>
            <div id="multiSteps"></div>
            <div class="col-md-12 text-center">
                <button type="button" class="btn btn-primary btnSubmit" id="BtnSave">Save</button>
            </div>
        </div>
    </div>



    <div class="modal fade in" id="stepModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add Step</h4>
                </div>
                <form method="POST" role="form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Step Name</label>
                                    <input type="text" name="group_name" id="step_name" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="AppendCheckListModal">

    </div>
    <div class="AppendDetailListModal"></div>
    <div class="AppenedVideoModal"></div>

    {{-- <div class="modal fade in" id="clientModal">
    <div class="modal-dialog">
      <div class="modal-content">
       <div class="modal-header">
        <button type="button" class="close" aria-label="Close">
         <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Add Clients</h4>
            </div> <form method="POST" role="form">
            <div class="modal-body">
            <div class="row">
            <div class="col-md-12">
            <ul class="checkboxesMain checkboxclient">
            </ul>
         </div>
      </div>
    </div>
  <div class="modal-footer">
  </div>
 </form>
</div> --}}
    <!-- /.modal-content -->

    {{-- <div id="box-container">

        </div>
        <div class="box" id="step_container" style="display: none;">
            <div class="box-header">
                <form id="step_form" method="post" enctype="multipart/form-data">
                    <h3 class="box-title">Steps</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>

                    <div class="box-body">
                        <div id="step-container">
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Step Title</label>
                                        <input type="text" name="step_title" class="form-control" id="txtStepName" value="{{ old('step_title') }}">
                                    </div>
                                </div>
                            </div>
                            <div id="section-container">
                                <div class="row" id="section-1">
                                    <div class="col-md-11">
                                        <h3 class="box-title">Section 1</h3>
                                    </div>
                                    <div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Section Title</label>
                                                <input type="text" name="section_title[]" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Image</label>
                                                <input type="file" name="path[]" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Checklist</label>
                                            <button class="btn btn-primary btn-sm modalbtn" type="button" id="0" onclick="openModal(this)"><i class="fa fa-plus"></i></button>
                                            <div class="form-group" id="check-list-0">
                                                <ul></ul>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <label>&nbsp;</label>
                                            <div class="form-group">
                                                <label>&nbsp</label>
                                                <button class="btn btn-danger btn-sm" id="add_section">Add</button>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Description</label>
                                            <textarea class="content_area" name="description[]"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Information</label>
                                            <textarea class="content_area" name="information[]"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary btnSubmit">Save</button>
                    </div>
                </form>
            </div>
        </div> --}}
@endsection
@push('css')
    <style type="text/css">
        .btnPlus {
            margin-top: 23px;
        }

        .displayBox {
            display: block !important;
        }

        .StepNameField {
            color: black;
        }
    </style>
@endpush

@push('js')
    <script>
        var SteploopCheck = [];
        var CheckList = [];
        var Detail = [];
        var OuterStep = [];
        var StepSection = [];
        var id = 0;
        var x = 0;
        var DetailID = 0;
        var SectionID = 0;
        var AutoStep = 0;
        var SectionStep = '';
        var SectionValue = 0;


        $(document).ready(function () {
            var max_fields = 15;
            var wrapper = $("#checkListFields");
            var add_button = $("#add_checkList");


            $(".StepPackSelect2").select2({
                placeholder: "Existing Trip Templates"
            });

            $("#BtnSearchStepPack").click(function () {
                var StepCount = '{{Session::get('TotalStep')}}';
                if (id >= StepCount) {
                    toastr['error']('you can add only  ' + StepCount + ' Steps!');
                } else {
                    $.ajax({
                        url: "{{ route('SearchAllSteps') }}",
                        type: 'POST',
                        data: {
                            _token: $("#token").val(),
                            StepPackId: $(".StepPackSelect2").val() == null ? 0 : $(".StepPackSelect2").val()
                        },
                        success: function (response) {
                            $(".removedata").empty();
                            id = AutoStep;
                            var newStepCount = response.Step.length + id;
                            if (newStepCount >= StepCount) {
                                toastr['error']('you can add only  ' + StepCount + ' Steps!');
                            } else {
                                toastr['success']('Trip Template Add Successfully!');
                                $(".removedata").empty();
                                id = AutoStep;
                                for (var checksteps = 0; checksteps < response.Step.length; checksteps++) {

                                    if (response.Step[checksteps] != null) {
                                        for (var i = 0; i < response.Step[checksteps].length; i++) {
                                            var innerstep = [];
                                            innerstep.push(id);
                                            innerstep.push(response.Step[checksteps][i].title);
                                            innerstep.push('Active');
                                            SteploopCheck.push(innerstep);
                                            $('#multiSteps').append('<div class="removedata"><div class="panel-group" id="steps" role="tablist" aria-multiselectable="true">' +
                                                '<div class="panel panel-default">' +
                                                '  <div class="panel-heading">' +
                                                '     <h4 class="panel-title"> <input type="hidden" name="" id=txtStepID' + id + ' value="0">  <input type="checkbox" name="" id="Stepchecked' + id + '" checked="checked" onclick="StepStatusCheck(' + id + ')"> ' +
                                                '        <a data-toggle="collapse" data-parent="#steps" href="#step' + id + '" aria-expanded="true" aria-controls="step">' +
                                                '         <i class="accordion_icon fa fa-angle-down"></i> ' +
                                                '        </a><input type="text" id="txtstepname' + id + '" class="StepNameField" value="' + response.Step[checksteps][i].title + '" maxlength="25" onfocusout="stepvalueget(' + id + ')">' +
                                                '        <span class="pull-right">' +
                                                '             Section Sort <input type="checkbox" id="sort' + id + '"> ' +
                                                '            <input type="button" id="addVideo' + id + '"  class="btn-sm btnTabHead" value="Add Video" onclick="addVideo(' + id + ')"> ' +
                                                '            <input type="button" id="addSectionBtn' + id + '" onclick="addSection(' + id + ')" class="btn-sm btnTabHead addClientBtn" value="Add Section">' +
                                                '        </span>' +
                                                '    </h4>' +
                                                '  </div>' +
                                                '<div id="step' + id + '" class="panel-collapse collapse">' +
                                                '    <div class="panel-body">' +
                                                '        <div class="row">' +
                                                '            <div class="col-md-3 sectionNameArea" id="sectionNameArea' + id + '"></div>' +
                                                '            <div id=HalfSection' + id + '></div>' +
                                                '            </div>' +
                                                '        </div>' +
                                                '    </div>' +
                                                '</div></div>');
                                            if (response.Step[checksteps][i].sort == "desc") {
                                                $("#sort" + id + "").attr('checked', 'checked');
                                            }
                                            if (isUnicode(response.Step[checksteps][i].title) == true) {
                                                $("#txtstepname" + id + "").css('direction', 'rtl');
                                            } else {
                                                $("#txtstepname" + id + "").css('direction', 'ltr');
                                            }

                                            $('.panel-title > a').click(function () {
                                                $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
                                                    .closest('panel').siblings('panel')
                                                    .find('i')
                                                    .removeClass('fa-angle-down').addClass('fa-angle-up');
                                            });
                                            $(".AppenedVideoModal").append('<div class="modal fade in" id="videoModal' + id + '">' +
                                                '<div class="modal-dialog">' +
                                                '<div class="modal-content">' +
                                                '<div class="modal-header">' +
                                                '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                                                '<h4 class="modal-title">Add Video</h4></div>' +
                                                '<form method="POST" role="form">' +
                                                '<div class="modal-body">' +
                                                '<div class="row">' +
                                                '<div class="col-md-12">' +
                                                '<div class="form-group">' +
                                                '<label>Add Video</label>' +
                                                '<label class="float-right" onclick="removeVideo(' + id + ')" id="removeVideo' + id + '" style="float:right;display: none">Remove</label>' +
                                                '<input type="file" name="step_video" id="StepVideo' + id + '" onchange="ShowVideo(' + id + ')">' +
                                                '</div>' +
                                                '</div>' +
                                                '</div>' +
                                                '<span id="videoName'+id+'"></span>' +
                                                '<video width="400" controls id="stepviodeshow' + id + '"></video>' +
                                                '</div>' +
                                                '<div class="modal-footer">' +
                                                '<button type="button" class="btn btn-primary" onclick="VideoModalClosed(' + id + ')">Save</button></div>' +
                                                '</form></div></div></div>');
                                            if (response.Step[checksteps][i].last_video_path != null) {
                                                $('#videoName'+ id).text(response.Step[i]['last_video_path']);
                                                $('#removeVideo' + id).css('display', 'block');
                                                $("#stepviodeshow" + id + "").append('<source  src="{{ asset("storage/StepViodes/")}}/' + response.Step[checksteps][i].last_video_path + '" type="video/mp4">');
                                            } else {
                                                $('#removeVideo' + id + '').css('display', 'none');
                                            }
                                            if (response.Step[checksteps][i].stepsection != null) {
                                                for (var secid = 0; secid < response.Step[checksteps][i].stepsection.length; secid++) {
                                                    var a = id + '' + SectionID;
                                                    $('#sectionNameArea' + id + '').append('<div id="sectionAreacheck' + id + '' + SectionID + '"><div class="form-group">\n' +
                                                        '<input type="hidden" id=txtSectionID' + id + '' + SectionID + ' value="0"/><input type="text" name="section_name" id="sectionField' + id + '' + SectionID + '" class="form-control" placeholder="Section Name" value="' + response.Step[checksteps][i].stepsection[secid].title + '" onclick="VisibleNewSection(\'' + a + '\')" maxlength="30" onfocusout="getSectionValue(' + SectionID + ',' + id + ')">\n' +
                                                        '</div></div>');

                                                    if (isUnicode(response.Step[checksteps][i].stepsection[secid].title) == true) {
                                                        $('#txtSectionID' + id + '' + SectionID + '').css('direction', 'rtl');
                                                    } else {
                                                        $('#txtSectionID' + id + '' + SectionID + '').css('direction', 'ltr');
                                                    }

                                                    $("#HalfSection" + id + "").append('<div id="sectionContentArea' + id + '' + SectionID + '" hidden="hidden"><div class="col-md-1">' +
                                                        '                    <input type="file" name="SectionImage" class="propic" id="SectionImage' + id + '' + SectionID + '" onchange="SectionImage(\'' + a + '\')">' +
                                                        '                    <label for="SectionImage' + id + '' + SectionID + '"><img id="showSectionImage' + id + '' + SectionID + '" src="{{ asset('images/sectionImage1.png') }}" alt="Upload Image"></label>' +
                                                        '                </div>' +
                                                        '                <div class="col-md-7">' +
                                                        '                    <div class="col-md-12">' +
                                                        '                        <div class="form-group">' +
                                                        '                            <textarea class="form-control" rows="4" id="SectionTextArea' + id + '' + SectionID + '">' + response.Step[checksteps][i].stepsection[secid].description + '</textarea>' +
                                                        '                        </div>' +
                                                        '                    </div>' +
                                                        '                    <div class="col-md-12">' +
                                                        '                        <a  class="btn btn-primary btnSubmit" data-toggle="modal" onclick="SectionCheckList(\'' + a + '\')">Check List</a>' +
                                                        '                        <a  class="btn btn-primary btnSubmit" data-toggle="modal" onclick="SectionDetail(\'' + a + '\')">Details</a>' +
                                                        '                        <a  class="btn btn-danger" onclick="deleteSectionFromdb(\'' + a + '\')">Delete</a>' +
                                                        '                    </div>' +
                                                        '                </div>' +
                                                        '                <div class="clearfix"></div></div>');
                                                    if (isUnicode(response.Step[checksteps][i].stepsection[secid].description) == true) {
                                                        $('#SectionTextArea' + id + '' + SectionID + '').css('direction', 'rtl');
                                                    } else {
                                                        $('#SectionTextArea' + id + '' + SectionID + '').css('direction', 'ltr');
                                                    }
                                                    $(".AppendCheckListModal").append('<div class="modal fade in" id="checkList' + id + '' + SectionID + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Add your items below</h4></div><div class="modal-body"><div><div class="col-md-12"><div class="form-group text-right"><a href="#" class="btn btn-primary" id="add_checkList" onclick=AddCheckList(\'' + a + '\')><i class="fa fa-plus"></i></a></div></div><div class="AppendTextBoxHeight AppendTextBox' + id + '' + SectionID + '"><div class="row"> <input type="hidden" id="txtCheckListID' + id + '' + SectionID + '' + x + '" value="0"><div class="col-md-10"><div class="form-group"><input type="text" class="form-control" name="list[]" id="txtlistname' + id + '' + SectionID + '' + x + '"></div></div></div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-primary" id="saveCheckList' + id + '' + SectionID + '' + x + '" onclick=saveCheckList(\'' + a + '\')> Save</button></div></div></div></div></div>');

                                                    $(".AppendDetailListModal").append('<div class="modal fade in" id="detailsModal' + id + '' + SectionID + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Details</h4></div><form method="POST" role="form"><div class="modal-body"><input type="hidden" id="txtSectiondetailId"><input type="hidden" id="txtDetialID' + id + '' + SectionID + '" value="0"><div class="row"><div class="col-md-12"><div class="form-group"><label>Add Details</label><textarea  id="textArea' + id + '' + SectionID + '">' + response.Step[checksteps][i].stepsection[secid].information + '</textarea></div></div></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="btnSaveDetail" onclick=SaveDetail(\'' + a + '\')>Save</button></div></form></div></div></div>');

                                                    $("#textArea" + id + "" + SectionID + "").wysihtml5({
                                                        style:   { remove: 1 },
                                                    });

                                                    if (response.Step[checksteps][i].stepsection[secid].path != null) {
                                                        $('#showSectionImage' + id + '' + SectionID + '').attr('src', '{{ asset("storage/StepImages/")}}/' + response.Step[checksteps][i].stepsection[secid].path + '');
                                                    }
                                                    for (var checklistdb = 0; checklistdb < response.Step[checksteps][i].stepsection[secid].step_check_lists.length; checklistdb++) {
                                                        if (checklistdb == 0) {
                                                            $("#txtCheckListID" + id + "" + SectionID + "" + x + "").val(0);
                                                            $("#txtlistname" + id + "" + SectionID + "" + x + "").val(response.Step[checksteps][i].stepsection[secid].step_check_lists[checklistdb].description);
                                                            if (isUnicode(response.Step[checksteps][i].stepsection[secid].step_check_lists[checklistdb].description) == true) {
                                                                $("#txtlistname" + id + "" + SectionID + "" + x + "").css('direction', 'rtl');
                                                            } else {
                                                                $("#txtlistname" + id + "" + SectionID + "" + x + "").css('direction', 'ltr');
                                                            }
                                                        } else {
                                                            $(".AppendTextBox" + id + "" + SectionID + "").append('<div class="row removeclass' + id + '' + SectionID + '' + x + '" ><input type="hidden" id="txtCheckListID' + id + '' + SectionID + '' + x + '" value="0"><div class="col-md-10 col-sm-9 col-xs-9"><div class="form-group"><input type="text" class="form-control" name="list[]" id="txtlistname' + id + '' + SectionID + '' + x + '" value="' + response.Step[checksteps][i].stepsection[secid].step_check_lists[checklistdb].description + '"></div></div><div class="col-md-2 col-sm-3 col-xs-3"><div class="form-group"><a href="#" class="btn btn-danger remove_check_field" onclick=RemoveFields("' + id + '' + SectionID + '' + x + '")><i class="fa fa-minus"></i></a></div></div></div>');
                                                            if (isUnicode(response.Step[checksteps][i].stepsection[secid].step_check_lists[checklistdb].description) == true) {
                                                                $("#txtlistname" + id + "" + SectionID + "" + x + "").css('direction', 'rtl');
                                                            } else {
                                                                $("#txtlistname" + id + "" + SectionID + "" + x + "").css('direction', 'ltr');
                                                            }
                                                        }
                                                        x++;
                                                    }
                                                    SectionID++;
                                                    SectionValue++;
                                                    $('textarea,input[type=text]').keyup(function (e) {
                                                        if (isUnicode($(this).val())) {
                                                            $(this).css('direction', 'rtl');
                                                        } else {
                                                            $(this).css('direction', 'ltr');
                                                        }
                                                    });

                                                    function VisibleNewSection(SID) {
                                                        $('#sectionContentArea' + SID + '').removeAttr("hidden");
                                                        for (var k = 0; k < id; k++) {
                                                            for (var l = 0; l < SectionID; l++) {
                                                                var j = k + '' + l;
                                                                if (SID == j) {

                                                                } else {
                                                                    $('#sectionContentArea' + j + '').attr("hidden", "hidden");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            var SectionArray = [];
                                            SectionArray.push(id);
                                            SectionArray.push(SectionValue);
                                            StepSection.push(SectionArray);
                                            SectionValue = 0;
                                            id++;
                                        }
                                    }
                                }
                            }

                        }

                    });
                }
            });
            GetAllSteps();


        });
        $('#stepModal').on('hidden.bs.modal', function () {
            $("#step_name").val('');
        });
        //On Add Step Button Click
        $('#BtnAddStep').on('click', function () {
            var StepCount = '{{Session::get('TotalStep')}}';
            if (id >= StepCount) {
                toastr['error']('you can add only  ' + StepCount + ' Steps!');
            } else {
                if (SteploopCheck.length > 0) {
                    var check = 0;
                    //for(var i=0;i<SteploopCheck.length;i++){
                    //  if(SteploopCheck[i][1]==$("#step_name").val()){
                    //    check=1;
                    //  break;
                    //}
                    //else{
                    // check=0;
                    //}
                    //}
                    if (check == 1) {
                        toastr['error']('Step Already Add!');
                        $("#step_name").val('');
                    } else {
                        toastr['success']('Step Added Successfully!');
                        var innerstep = [];
                        innerstep.push(id);
                        innerstep.push("New Step");
                        innerstep.push('Active');
                        SteploopCheck.push(innerstep);
                        $('#multiSteps').append('<div class="panel-group" id="steps" role="tablist" aria-multiselectable="true">' +
                            '<div class="panel panel-default">' +
                            '  <div class="panel-heading">' +
                            '     <h4 class="panel-title"><input type="hidden" id=txtStepID' + id + ' value=0> <input type="checkbox" name="" id="Stepchecked' + id + '" onclick="StepStatusCheck(' + id + ')" checked="checked"> ' +
                            '        <a data-toggle="collapse" data-parent="#steps" href="#step' + id + '" aria-expanded="true" aria-controls="step">' +
                            '         <i class="accordion_icon fa fa-angle-down"></i></a><input type="text" class="StepNameField" value="New Step" id="txtstepname' + id + '" maxlength="25" onfocusout="stepvalueget(' + id + ')">' +
                            '        <span class="pull-right">' +
                            '             Section Sort <input type="checkbox" id="sort' + id + '"> ' +
                            '            <input type="button" id="addVideo' + id + '"  class="btn-sm btnTabHead" value="Add Video" onclick="addVideo(' + id + ')"> ' +
                            '            <input type="button" id="addSectionBtn' + id + '" onclick="addSection(' + id + ')" class="btn-sm btnTabHead addClientBtn" value="Add Section">' +
                            '        </span>' +
                            '    </h4>' +
                            '  </div>' +
                            '<div id="step' + id + '" class="panel-collapse collapse in">' +
                            '    <div class="panel-body">' +
                            '        <div class="row">' +
                            '            <div class="col-md-3 sectionNameArea" id="sectionNameArea' + id + '"></div>' +
                            '            <div id=HalfSection' + id + '></div>' +
                            '           ' +
                            '        </div>' +
                            '    </div>' +
                            '</div>');
                        $('.panel-title > a').click(function () {
                            $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
                                .closest('panel').siblings('panel')
                                .find('i')
                                .removeClass('fa-angle-down').addClass('fa-angle-up');
                        });
                        $(".AppenedVideoModal").append('<div class="modal fade in" id="videoModal' + id + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Add Video</h4></div><form method="POST" role="form"><div class="modal-body"><div class="row"><div class="col-md-12"><div class="form-group"><label>Add Video</label><label class="float-right" onclick="removeVideo(' + id + ')" id="removeVideo' + id + '" style="float:right;display: none">Remove</label><input type="file" name="step_video" id="StepVideo' + id + '" onchange="ShowVideo(' + id + ')"></div></div></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" onclick="VideoModalClosed(' + id + ')">Save</button></div></form></div></div></div>');
                        var a = id + '' + SectionID;
                        $('#sectionNameArea' + id + '').append('<div id="sectionAreacheck' + id + '' + SectionID + '"><div class="form-group">\n' +
                            '<input type="hidden" id=txtSectionID' + id + '' + SectionID + ' value=0><input type="text" name="section_name" id="sectionField' + id + '' + SectionID + '" class="form-control" placeholder="Section Name" onclick="VisibleNewSection(\'' + a + '\')" maxlength="30" onfocusout="getSectionValue(' + SectionID + ',' + id + ')">\n' +
                            '</div></div>');
                        $("#HalfSection" + id + "").append('<div id="sectionContentArea' + id + '' + SectionID + '" hidden="hidden"><div class="col-md-1">' +
                            '                    <input type="file" name="SectionImage" class="propic" id="SectionImage' + id + '' + SectionID + '" onchange="SectionImage(\'' + a + '\')">' +
                            '                    <label for="SectionImage' + id + '' + SectionID + '"><img id="showSectionImage' + id + '' + SectionID + '" src="{{ asset('images/sectionImage1.png') }}" alt="Upload Image"></label>' +
                            '                </div>' +
                            '                <div class="col-md-7">' +
                            '                    <div class="col-md-12">' +
                            '                        <div class="form-group">' +
                            '                            <textarea class="form-control" rows="4" id="SectionTextArea' + id + '' + SectionID + '"></textarea>' +
                            '                        </div>' +
                            '                    </div>' +
                            '                    <div class="col-md-12">' +
                            '                        <a  class="btn btn-primary btnSubmit" data-toggle="modal" onclick="SectionCheckList(\'' + a + '\')">Check List</a>' +
                            '                        <a  class="btn btn-primary btnSubmit" data-toggle="modal" onclick="SectionDetail(\'' + a + '\')">Details</a>' +
                            '                        <a  class="btn btn-danger" onclick="removeSection(' + id + '' + SectionID + ')">Delete</a>' +
                            '                    </div>' +
                            '                </div>' +
                            '                <div class="clearfix"></div></div>');
                        $(".AppendCheckListModal").append('<div class="modal fade in" id="checkList' + id + '' + SectionID + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Add your items below</h4></div><div class="modal-body"><div><div class="col-md-12"><div class="form-group text-right"><a href="#" class="btn btn-primary" id="add_checkList" onclick=AddCheckList(\'' + a + '\')><i class="fa fa-plus"></i></a></div></div><div class="AppendTextBoxHeight AppendTextBox' + id + '' + SectionID + '"><div class="row"> <input type="hidden" id="txtCheckListID' + id + '' + SectionID + '' + x + '" value=0><div class="col-md-10"><div class="form-group"><input type="text" class="form-control" name="list[]" id="txtlistname' + id + '' + SectionID + '' + x + '"></div></div></div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-primary" id="saveCheckList' + id + '' + SectionID + '' + x + '" onclick=saveCheckList(\'' + a + '\')> Save</button></div></div></div></div></div>');
                        x++;

                        $(".AppendDetailListModal").append('<div class="modal fade in" id="detailsModal' + id + '' + SectionID + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Details</h4></div><form method="POST" role="form"><div class="modal-body"><input type="hidden" id="txtSectiondetailId"><input type="hidden" id="txtDetialID' + id + '' + SectionID + '" value=0><div class="row"><div class="col-md-12"><div class="form-group"><label>Add Details</label><textarea  id="textArea' + id + '' + SectionID + '"></textarea></div></div></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="btnSaveDetail" onclick=SaveDetail(\'' + a + '\')>Save</button></div></form></div></div></div>');
                        $("#textArea" + id + "" + SectionID + "").wysihtml5({
                            style:   { remove: 1 },
                        });
                        $('textarea,input[type=text]').keyup(function (e) {
                            if (isUnicode($(this).val())) {
                                $(this).css('direction', 'rtl');
                            } else {
                                $(this).css('direction', 'ltr');
                            }
                        });
                        SectionValue = 0;
                        toastr['success']('Section Added Successfully!');
                        SectionValue++;
                        var SectionArray = [];
                        SectionArray.push(id);
                        SectionArray.push(SectionValue);
                        StepSection.push(SectionArray);

                        SectionID++;
                        id++;
                        AutoStep++;
                    }
                } else {
                    toastr['success']('Step Added Successfully!');
                    var innerstep = [];
                    innerstep.push(id);
                    innerstep.push("New Step");
                    innerstep.push('Active');
                    SteploopCheck.push(innerstep);
                    $('#multiSteps').append('<div class="panel-group" id="steps" role="tablist" aria-multiselectable="true">' +
                        '<div class="panel panel-default">' +
                        '  <div class="panel-heading">' +
                        '     <h4 class="panel-title"> <input type="hidden" id=txtStepID' + id + ' value=0><input type="checkbox" name="" id="Stepchecked' + id + '" onclick="StepStatusCheck(' + id + ')" checked="checked"> ' +
                        '        <a data-toggle="collapse" data-parent="#steps" href="#step' + id + '" aria-expanded="true" aria-controls="step">' +
                        '         <i class="accordion_icon fa  fa-angle-down"></i></a><input class="StepNameField" type="text" id="txtstepname' + id + '" value="New Step" maxlength="25" onfocusout="stepvalueget(' + id + ')">' +
                        '        <span class="pull-right">' +
                        '             Section Sort <input type="checkbox" id="sort' + id + '"> ' +
                        '            <input type="button" id="addVideo' + id + '"  class="btn-sm btnTabHead" value="Add Video" onclick="addVideo(' + id + ')"> ' +
                        '            <input type="button" id="addSectionBtn' + id + '"  onclick="addSection(' + id + ')" class="btn-sm btnTabHead addClientBtn" value="Add Section">' +
                        '        </span>' +
                        '    </h4>' +
                        '  </div>' +
                        '<div id="step' + id + '" class="panel-collapse collapse in">' +
                        '    <div class="panel-body">' +
                        '        <div class="row">' +
                        '            <div class="col-md-3 sectionNameArea" id="sectionNameArea' + id + '"></div>' +
                        '            <div id=HalfSection' + id + '></div>' +
                        '        </div>' +
                        '    </div>' +
                        '</div>');
                    $('.panel-title > a').click(function () {
                        $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
                            .closest('panel').siblings('panel')
                            .find('i')
                            .removeClass('fa-angle-down').addClass('fa-angle-up');
                    });
                    $(".AppenedVideoModal").append('<div class="modal fade in" id="videoModal' + id + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Add Video</h4></div><form method="POST" role="form"><div class="modal-body"><div class="row"><div class="col-md-12"><div class="form-group"><label>Add Video</label><label class="float-right" onclick="removeVideo(' + id + ')" id="removeVideo' + id + '" style="float:right;display: none">Remove</label><input type="file" name="step_video" id="StepVideo' + id + '" onchange="ShowVideo(' + id + ')"></div></div></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" onclick="VideoModalClosed(' + id + ')">Save</button></div></form></div></div></div>');
                    SectionValue = 0;
                    $('textarea,input[type=text]').keyup(function (e) {
                        if (isUnicode($(this).val())) {
                            $(this).css('direction', 'rtl');
                        } else {
                            $(this).css('direction', 'ltr');
                        }
                    });
                    //SectionStep=''+id+''+','+''+SectionValue+'';
                    id++;
                    AutoStep++;
                }
            }


        });

        function GetAllSteps() {
            $.ajax({
                url: "{{ route('GetAllSteps') }}",
                type: 'POST',
                data: {_token: $("#token").val(), TripId: '{{Session::get('TripID')}}'},
                success: function (response) {
                    if (response.Step != null) {
                        for (var i = 0; i < response.Step.length; i++) {
                            var innerstep = [];
                            innerstep.push(id);
                            innerstep.push(response.Step[i].title);
                            innerstep.push('Active');
                            SteploopCheck.push(innerstep);
                            $('#multiSteps').append('<div class="panel-group" id="steps" role="tablist" aria-multiselectable="true">' +
                                '<div class="panel panel-default">' +
                                '  <div class="panel-heading">' +
                                '     <h4 class="panel-title"> <input type="hidden" name="" id=txtStepID' + id + ' value="' + response.Step[i]['id'] + '">  <input type="checkbox" name="" id="Stepchecked' + id + '" checked="checked" onclick="StepStatusCheck(' + id + ')"> ' +
                                '        <a data-toggle="collapse" data-parent="#steps" href="#step' + id + '" aria-expanded="true" aria-controls="step">' +
                                '         <i class="accordion_icon fa  fa-angle-down"></i> ' +
                                '        </a><input type="text" id="txtstepname' + id + '" class="StepNameField" value="' + response.Step[i]['title'] + '" maxlength="25" onfocusout="stepvalueget(' + id + ')">' +
                                '        <span class="pull-right">' +
                                '          Section Sort <input type="checkbox" id="sort' + id + '" > ' +
                                '            <input type="button" id="addVideo' + id + '" class="btn-sm btnTabHead" value="Add Video" onclick="addVideo(' + id + ')"> ' +
                                '            <input type="button" id="addSectionBtn' + id + '" onclick="addSection(' + id + ')" class="btn-sm btnTabHead addClientBtn" value="Add Section">' +
                                '        </span>' +
                                '    </h4>' +
                                '  </div>' +
                                '<div id="step' + id + '" class="panel-collapse collapse">' +
                                '    <div class="panel-body">' +
                                '        <div class="row">' +
                                '            <div class="col-md-3 sectionNameArea" id="sectionNameArea' + id + '"></div>' +
                                '            <div id=HalfSection' + id + '></div>' +
                                '            </div>' +
                                '        </div>' +
                                '    </div>' +
                                '</div>');
                            if (response.Step[i]['sort'] == 'desc') {
                                $("#sort" + id + "").attr('checked', 'checked');
                            }
                            if (isUnicode(response.Step[i]['title']) == true) {
                                $("#txtstepname" + id + "").css('direction', 'rtl');
                            } else {
                                $("#txtstepname" + id + "").css('direction', 'ltr');
                            }

                            $('.panel-title > a').click(function () {
                                $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
                                    .closest('panel').siblings('panel')
                                    .find('i')
                                    .removeClass('fa-angle-down').addClass('fa-angle-up');
                            });
                            $(".AppenedVideoModal").append('<div class="modal fade in" id="videoModal' + id + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Add Video</h4></div><form method="POST" role="form"><div class="modal-body"><div class="row"><div class="col-md-12"><div class="form-group"><label>Add Video</label><label class="float-right" onclick="removeVideo(' + id + ')" id="removeVideo' + id + '" style="float:right;display: none">Remove</label><input type="file" name="step_video" id="StepVideo' + id + '" onchange="ShowVideo(' + id + ')"></div></div></div><span id="videoName'+id+'"></span><video width="400" id="stepviodeshow' + id + '" controls></video></div><div class="modal-footer"><button type="button" class="btn btn-primary" onclick="VideoModalClosed(' + id + ')">Save</button></div></form></div></div></div>');
                            if (response.Step[i]['last_video_path'] != null) {
                                $('#removeVideo' + id).css('display', 'block');
                                $('#videoName'+ id).text(response.Step[i]['last_video_path']);
                                $("#stepviodeshow" + id + "").append('<source src="{{ asset("storage/StepViodes/")}}/' + response.Step[i]['last_video_path'] + '" type="video/mp4">');
                            } else {
                                $('#removeVideo' + id).css('display', 'none');
                            }

                            for (var secid = 0; secid < response.Step[i].stepsection.length; secid++) {
                                var a = id + '' + SectionID;
                                $('#sectionNameArea' + id + '').append('<div id="sectionAreacheck' + id + '' + SectionID + '"><div class="form-group">\n' +
                                    '<input type="hidden" id=txtSectionID' + id + '' + SectionID + ' value="' + response.Step[i].stepsection[secid].id + '"/><input type="text" name="section_name" id="sectionField' + id + '' + SectionID + '" class="form-control" placeholder="Section Name" value="' + response.Step[i].stepsection[secid].title + '" onclick="VisibleNewSection(\'' + a + '\')" maxlength="30" onfocusout="getSectionValue(' + SectionID + ',' + id + ')">\n' +
                                    '</div></div>');
                                if (isUnicode(response.Step[i].stepsection[secid].title) == true) {
                                    $("#sectionField" + id + "" + SectionID + "").css('direction', 'rtl');
                                } else {
                                    $("#sectionField" + id + "" + SectionID + "").css('direction', 'ltr');
                                }
                                $("#HalfSection" + id + "").append('<div id="sectionContentArea' + id + '' + SectionID + '" hidden="hidden"><div class="col-md-1">' +
                                    '                    <input type="file" name="SectionImage" class="propic" id="SectionImage' + id + '' + SectionID + '" onchange="SectionImage(\'' + a + '\')">' +
                                    '                    <label for="SectionImage' + id + '' + SectionID + '"><img id="showSectionImage' + id + '' + SectionID + '" src="{{ asset('images/sectionImage1.png') }}" alt="Upload Image"></label>' +
                                    '                </div>' +
                                    '                <div class="col-md-7">' +
                                    '                    <div class="col-md-12">' +
                                    '                        <div class="form-group">' +
                                    '                            <textarea class="form-control" rows="4" id="SectionTextArea' + id + '' + SectionID + '">' + response.Step[i].stepsection[secid].description + '</textarea>' +
                                    '                        </div>' +
                                    '                    </div>' +
                                    '                    <div class="col-md-12">' +
                                    '                        <a  class="btn btn-primary btnSubmit" data-toggle="modal" onclick="SectionCheckList(\'' + a + '\')">Check List</a>' +
                                    '                        <a  class="btn btn-primary btnSubmit" data-toggle="modal" onclick="SectionDetail(\'' + a + '\')">Details</a>' +
                                    '                        <a  class="btn btn-danger" onclick="deleteSectionFromdb(\'' + a + '\')">Delete</a>' +

                                    '                    </div>' +
                                    '                </div>' +
                                    '                <div class="clearfix"></div></div>');
                                if (isUnicode(response.Step[i].stepsection[secid].description) == true) {
                                    $("#SectionTextArea" + id + "" + SectionID + "").css('direction', 'rtl');
                                } else {
                                    $("#SectionTextArea" + id + "" + SectionID + "").css('direction', 'ltr');
                                }
                                $(".AppendCheckListModal").append('<div class="modal fade in" id="checkList' + id + '' + SectionID + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Add your items below</h4></div><div class="modal-body"><div><div class="col-md-12"><div class="form-group text-right"><a href="#" class="btn btn-primary" id="add_checkList" onclick=AddCheckList(\'' + a + '\')><i class="fa fa-plus"></i></a></div></div><div class="AppendTextBoxHeight AppendTextBox' + id + '' + SectionID + '"><div class="row"> <input type="hidden" id="txtCheckListID' + id + '' + SectionID + '' + x + '" value=0><div class="col-md-10"><div class="form-group"><input type="text" class="form-control" name="list[]" id="txtlistname' + id + '' + SectionID + '' + x + '"></div></div></div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-primary" id="saveCheckList' + id + '' + SectionID + '' + x + '" onclick=saveCheckList(\'' + a + '\')> Save</button></div></div></div></div></div>');

                                $(".AppendDetailListModal").append('<div class="modal fade in" id="detailsModal' + id + '' + SectionID + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Details</h4></div><form method="POST" role="form"><div class="modal-body"><input type="hidden" id="txtSectiondetailId"><input type="hidden" id="txtDetialID' + id + '' + SectionID + '" value="0"><div class="row"><div class="col-md-12"><div class="form-group"><label>Add Details</label><textarea  id="textArea' + id + '' + SectionID + '">' + response.Step[i].stepsection[secid].information + '</textarea></div></div></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="btnSaveDetail" onclick=SaveDetail(\'' + a + '\')>Save</button></div></form></div></div></div>');
                                $("#textArea" + id + "" + SectionID + "").wysihtml5({
                                    style:   { remove: 1 },
                                });
                                if (response.Step[i].stepsection[secid].path != null) {
                                    $('#showSectionImage' + id + '' + SectionID + '').attr('src', '{{ asset("storage/StepImages/")}}/' + response.Step[i].stepsection[secid].path + '');
                                }
                                for (var checklistdb = 0; checklistdb < response.Step[i].stepsection[secid].step_check_lists.length; checklistdb++) {
                                    if (checklistdb == 0) {
                                        $("#txtCheckListID" + id + "" + SectionID + "" + x + "").val(response.Step[i].stepsection[secid].step_check_lists[checklistdb].id);
                                        $("#txtlistname" + id + "" + SectionID + "" + x + "").val(response.Step[i].stepsection[secid].step_check_lists[checklistdb].description);
                                        if (isUnicode(response.Step[i].stepsection[secid].step_check_lists[checklistdb].description) == true) {
                                            $("#txtlistname" + id + "" + SectionID + "" + x + "").css('direction', 'rtl');
                                        } else {
                                            $("#txtlistname" + id + "" + SectionID + "" + x + "").css('direction', 'ltr');
                                        }
                                    } else {
                                        $(".AppendTextBox" + id + "" + SectionID + "").append('<div class="row removeclass' + id + '' + SectionID + '' + x + '" ><input type="hidden" id="txtCheckListID' + id + '' + SectionID + '' + x + '" value="' + response.Step[i].stepsection[secid].step_check_lists[checklistdb].id + '"><div class="col-md-10 col-sm-9 col-xs-9"><div class="form-group"><input type="text" class="form-control" name="list[]" id="txtlistname' + id + '' + SectionID + '' + x + '" value="' + response.Step[i].stepsection[secid].step_check_lists[checklistdb].description + '"></div></div><div class="col-md-2 col-sm-3 col-xs-3"><div class="form-group"><a href="#" class="btn btn-danger remove_check_field" onclick=RemoveFields("' + id + '' + SectionID + '' + x + '")><i class="fa fa-minus"></i></a></div></div></div>');
                                        if (isUnicode(response.Step[i].stepsection[secid].step_check_lists[checklistdb].description) == true) {
                                            $("#txtlistname" + id + "" + SectionID + "" + x + "").css('direction', 'rtl');
                                        } else {
                                            $("#txtlistname" + id + "" + SectionID + "" + x + "").css('direction', 'ltr');
                                        }
                                    }
                                    x++;
                                }
                                SectionValue++;
                                SectionID++;
                                $('textarea,input[type=text]').keyup(function (e) {
                                    if (isUnicode($(this).val())) {
                                        $(this).css('direction', 'rtl');
                                    } else {
                                        $(this).css('direction', 'ltr');
                                    }
                                });

                                function VisibleNewSection(SID) {
                                    $('#sectionContentArea' + SID + '').removeAttr("hidden");
                                    for (var k = 0; k < id; k++) {
                                        for (var i = 0; i < SectionID; i++) {
                                            var j = k + '' + i;
                                            if (SID == j) {

                                            } else {
                                                $('#sectionContentArea' + j + '').attr("hidden", "hidden");
                                            }
                                        }
                                    }
                                }

                            }
                            var SectionArray = [];
                            SectionArray.push(id);
                            SectionArray.push(SectionValue);
                            StepSection.push(SectionArray);
                            SectionValue = 0;
                            id++;
                            AutoStep++;
                        }
                        $("#step" + window.localStorage.getItem('Stepid') + "").removeClass('collapse');
                        $("#step" + window.localStorage.getItem('Stepid') + "").addClass('collapse in');
                        var sID = window.localStorage.getItem('Stepid');
                        var sectID = window.localStorage.getItem('SectionID');
                        var newid = "" + sID + "" + sectID + "";
                        $('#sectionContentArea' + newid + '').removeAttr("hidden");
                        $('#sectionField' + sID + '' + sectID + '').select().focus();
                        if ($('#sectionField' + sID + '' + sectID + '').val() == undefined) {

                        } else {
                            $('.sectionNameArea').animate({
                                scrollTop: $('#sectionField' + sID + '' + sectID + '').offset().top
                            }, 2000);
                        }

                    }
                }

            });
        }

        function StepStatusCheck(StepId) {
            if ($("#Stepchecked" + StepId + "").prop("checked") == true) {
                for (var i = 0; i < SteploopCheck.length; i++) {
                    a = $("#Stepchecked" + StepId + "").val();
                    if (SteploopCheck[i][0] == StepId) {
                        SteploopCheck[i][2] = 'Active';
                    }
                }
            } else if ($("#Stepchecked" + StepId + "").prop("checked") == false) {
                for (var i = 0; i < SteploopCheck.length; i++) {
                    a = $("#Stepchecked" + StepId + "").val();
                    if (SteploopCheck[i][0] == StepId) {
                        SteploopCheck[i][2] = 'Deactive';
                        RemoveSteps(StepId);
                    }
                }
            }
        }

        function addSection(stepid) {
            var count;
            SectionValue++;
            for (var i = 0; i < StepSection.length; i++) {
                if (stepid == StepSection[i][0]) {
                    count = StepSection[i][1];
                    SectionValue = count;
                    StepSection.splice(i, 1);
                }
            }

            var SectionCount = '{{Session::get('TotalSection')}}';
            if (count >= SectionCount) {
                toastr['error']('you can add only  ' + SectionCount + ' Section!');
                var SectionArray = [];
                SectionArray.push(stepid);
                SectionArray.push(SectionValue);
                StepSection.push(SectionArray);
                SectionValue = 0;
            } else {
                var a = stepid + '' + SectionID;
                $('#sectionNameArea' + stepid + '').append('<div id="sectionAreacheck' + stepid + '' + SectionID + '"><div class="form-group">\n' +
                    '<input type="hidden" id=txtSectionID' + stepid + '' + SectionID + ' value=0><input type="text" name="section_name" id="sectionField' + stepid + '' + SectionID + '" class="form-control" placeholder="Section Name" onclick="VisibleNewSection(\'' + a + '\')" maxlength="30" onfocusout="getSectionValue(' + SectionID + ',' + stepid + ')">\n' +
                    '</div></div>');
                $("#HalfSection" + stepid + "").append('<div id="sectionContentArea' + stepid + '' + SectionID + '" hidden="hidden"><div class="col-md-1">' +
                    '                    <input type="file" name="SectionImage" class="propic" id="SectionImage' + stepid + '' + SectionID + '" onchange="SectionImage(\'' + a + '\')">' +
                    '                    <label for="SectionImage' + stepid + '' + SectionID + '"><img id="showSectionImage' + stepid + '' + SectionID + '" src="{{ asset('images/sectionImage1.png') }}" alt="Upload Image"></label>' +
                    '                </div>' +
                    '                <div class="col-md-7">' +
                    '                    <div class="col-md-12">' +
                    '                        <div class="form-group">' +
                    '                            <textarea class="form-control" rows="4" id="SectionTextArea' + stepid + '' + SectionID + '"></textarea>' +
                    '                        </div>' +
                    '                    </div>' +
                    '                    <div class="col-md-12">' +
                    '                        <a  class="btn btn-primary btnSubmit" data-toggle="modal" onclick="SectionCheckList(\'' + a + '\')">Check List</a>' +
                    '                        <a  class="btn btn-primary btnSubmit" data-toggle="modal" onclick="SectionDetail(\'' + a + '\')">Details</a>' +
                    '                        <a  class="btn btn-danger" onclick="removeSection(\'' + a + '\')">Delete</a>' +
                    '                    </div>' +
                    '                </div>' +
                    '                <div class="clearfix"></div></div>');
                $(".AppendCheckListModal").append('<div class="modal fade in" id="checkList' + stepid + '' + SectionID + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Add your items below</h4></div><div class="modal-body"><div><div class="col-md-12"><div class="form-group text-right"><a href="#" class="btn btn-primary" id="add_checkList" onclick=AddCheckList(\'' + a + '\')><i class="fa fa-plus"></i></a></div></div><div class="AppendTextBoxHeight AppendTextBox' + stepid + '' + SectionID + '"><div class="row"> <input type="hidden" id="txtCheckListID' + stepid + '' + SectionID + '' + x + '" value=0><div class="col-md-10"><div class="form-group"><input type="text" class="form-control" name="list[]" id="txtlistname' + stepid + '' + SectionID + '' + x + '"></div></div></div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-primary" id="saveCheckList' + stepid + '' + SectionID + '' + x + '" onclick=saveCheckList(\'' + a + '\')> Save</button></div></div></div></div></div>');
                x++;

                $(".AppendDetailListModal").append('<div class="modal fade in" id="detailsModal' + stepid + '' + SectionID + '"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Details</h4></div><form method="POST" role="form"><div class="modal-body"><input type="hidden" id="txtSectiondetailId"><input type="hidden" id="txtDetialID' + stepid + '' + SectionID + '" value=0><div class="row"><div class="col-md-12"><div class="form-group"><label>Add Details</label><textarea  id="textArea' + stepid + '' + SectionID + '"></textarea></div></div></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="btnSaveDetail" onclick=SaveDetail(\'' + a + '\')>Save</button></div></form></div></div></div>');
                $("#textArea" + stepid + "" + SectionID + "").wysihtml5({
                    style:   { remove: 1 },
                });

                $('textarea,input[type=text]').keyup(function (e) {
                    if (isUnicode($(this).val())) {
                        $(this).css('direction', 'rtl');
                    } else {
                        $(this).css('direction', 'ltr');
                    }
                });

                toastr['success']('Section Added Successfully!');
                SectionID++;
                SectionValue++;
                var SectionArray = [];
                SectionArray.push(stepid);
                SectionArray.push(SectionValue);
                StepSection.push(SectionArray);

            }

        }

        function VisibleNewSection(SID) {
            $('#sectionContentArea' + SID + '').removeAttr("hidden");
            for (var k = 0; k < id; k++) {
                for (var i = 0; i < SectionID; i++) {
                    var j = k + '' + i;
                    if (SID == j) {

                    } else {
                        $('#sectionContentArea' + j + '').attr("hidden", "hidden");
                    }
                }
            }
        }

        function SectionImage(SID) {
            Sectionprofile(SID);
        }

        function AddCheckList(SID) {
            $(".AppendTextBox" + SID + "").append('<div class="row removeclass' + SID + '' + x + '" ><input type="hidden" id="txtCheckListID' + SID + '' + x + '" value=0><div class="col-md-10 col-sm-9 col-xs-9"><div class="form-group"><input type="text" class="form-control" name="list[]" id="txtlistname' + SID + '' + x + '" autofocus></div></div><div class="col-md-2 col-sm-3 col-xs-3"><div class="form-group"><a href="#" class="btn btn-danger remove_check_field" onclick=RemoveFields("' + SID + '' + x + '")><i class="fa fa-minus"></i></a></div></div></div>');
            x++;
            $('textarea,input[type=text]').keyup(function (e) {
                if (isUnicode($(this).val())) {
                    $(this).css('direction', 'rtl');
                } else {
                    $(this).css('direction', 'ltr');
                }
            });


        }

        function RemoveFields(abc) {
            RemoveCheckList(abc);
            $('.removeclass' + abc + '').remove();
        }

        function RemoveSteps(data) {
            $.ajax({
                url: "{{route('steppack.step.deletestep')}}",
                data: {_token: $("#token").val(), deletedata: $('#txtStepID' + data + '').val()},
                type: "POST",
                success: function (json) {
                    if (json.status == 1) {
                        toastr['success']("Step Delete Successfully");
                    } else if (json.status == -1) {
                        for (var i = 0; i < SteploopCheck.length; i++) {
                            if (SteploopCheck[i][0] == data) {
                                SteploopCheck[i][2] = 'Active';
                                $("#Stepchecked" + data + "").prop('checked', true);
                                toastr['error']("Step Cannot Delete");
                            }
                        }
                    } else {
                    }
                }
            });
        }

        function RemoveCheckList(data) {
            $.ajax({
                url: "{{route('steppack.step.deletechecklist')}}",
                data: {_token: $("#token").val(), deletedata: $('#txtCheckListID' + data + '').val()},
                type: "POST",
                success: function (json) {
                    if (json.status == true) {
                    } else {
                    }
                }
            });
        }

        function Sectionprofile(SID) {
            if ($("#SectionImage" + SID + "").prop("files")[0]) {
                switch ($("#SectionImage" + SID + "").val().substring($("#SectionImage" + SID + "").val().lastIndexOf('.') + 1).toLowerCase()) {
                    case 'gif':
                    case 'jpg':
                    case 'png':
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#showSectionImage' + SID + '').attr('src', e.target.result);
                        }
                        reader.readAsDataURL($("#SectionImage" + SID + "").prop("files")[0]);
                        break;
                    default:
                        $("#SectionImage" + SID + "").val('');
                        toastr['error']("File format not correct!");
                        break;
                }
            }
        }

        function removeVideo(StepId) {

            var textStepId = $('#txtStepID' + StepId + '').val();

            $("#StepVideo" + StepId + "").val('');
            $("#videoName" + StepId + "").text('');
            $('#stepviodeshow' + StepId).remove();


            if (textStepId != 0 && textStepId != '') {
                $.ajax({
                    url: "{{route('steppack.step.deletestepvideo')}}",
                    data: {_token: $("#token").val(), deletedata: textStepId},
                    type: "POST",
                    success: function (json) {
                        if (json.status == 1) {
                            $('#removeVideo'+ StepId).css('display','none');
                            toastr['success']("Step Video Deleted Successfully");
                        }
                    }
                });
            }


        }

        function ShowVideoCheck(StepID) {

            if ($("#StepVideo" + StepID + "").prop("files")[0]) {

                var value = $("#StepVideo" + StepID + "").val().substring($("#StepVideo" + StepID + "").val().lastIndexOf('.') + 1).toLowerCase();
                switch (value) {
                    case 'mp4':
                    case 'mov':
                    case 'mp3':
                    case 'mkv':
                    case 'webm':
                    case 'flv':
                    case 'wmv':
                    case '3gp':
                    case 'm4v':
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            //$('#showSectionImage'+StepID+'').attr('src', e.target.result);
                        }
                        if ($("#StepVideo" + StepID + "").prop("files")[0].size > 2097152) {
                            $("#StepVideo" + StepID + "").val('');
                            toastr['error']("Video size less then 2MB!");
                            break;
                        } else {
                            $('#removeVideo' + StepID).css('display', 'block');
                            reader.readAsDataURL($("#StepVideo" + StepID + "").prop("files")[0]);
                            break;
                        }

                    default:
                        $("#StepVideo" + StepID + "").val('');
                        toastr['error']("Video format " + value + " not correct!");
                        break;
                }
            }
        }

        function SectionCheckList(SID) {
            $("#checkList" + SID + "").modal('toggle');
        }

        function SectionDetail(SID) {
            $("#detailsModal" + SID + "").modal('toggle');
            $("#txtSectiondetailId").val(SID);
        }

        function addVideo(StepID) {

            $('#videoModal' + StepID + '').modal('toggle');
        }

        function VideoModalClosed(StepID) {
            $('#videoModal' + StepID + '').modal('toggle');
        }


        function saveCheckList(SID) {

            $("#checkList" + SID + "").modal('toggle');
        }

        function ShowVideo(StepID) {
            ShowVideoCheck(StepID);
        }

        function SaveDetail(SID) {

            $("#detailsModal" + SID + "").modal('toggle');
        }

        function removeSection(SID) {
            var arr = SID.toString().split("");
            for (var i = 0; i < StepSection.length; i++) {
                if (StepSection[i][0] == arr[0]) {
                    count = StepSection[i][1];
                    StepSection.splice(i, 1);
                    count = count - 1;
                    var SectionArray = [];
                    SectionArray.push(arr[0]);
                    SectionArray.push(count);
                    StepSection.push(SectionArray);
                }
            }
            $("#sectionContentArea" + SID + "").remove();
            $("#sectionAreacheck" + SID + "").remove();
        }

        function deleteSectionFromdb(SID) {
            deleteSectiondata(SID);


        }

        function deleteSectiondata(data) {
            $.ajax({
                url: "{{route('steppack.step.deletesection')}}",
                data: {_token: $("#token").val(), deletedata: $('#txtSectionID' + data + '').val()},
                type: "POST",
                success: function (json) {
                    if (json.status == true) {
                        var arr = data.toString().split("");
                        for (var i = 0; i < StepSection.length; i++) {
                            if (StepSection[i][0] == arr[0]) {
                                count = StepSection[i][1];
                                StepSection.splice(i, 1);
                                count = count - 1;
                                var SectionArray = [];
                                SectionArray.push(arr[0]);
                                SectionArray.push(count);
                                StepSection.push(SectionArray);
                            }
                        }
                        $("#sectionContentArea" + data + "").remove();
                        $("#sectionAreacheck" + data + "").remove();
                    } else {
                    }
                }
            });

        }

        $("#BtnSave").click(function () {
            for (var steploop = 0; steploop < SteploopCheck.length; steploop++) {
                if (SteploopCheck[steploop][2] == "Active") {
                    var SectionData = [];
                    for (var sectionloop = 0; sectionloop <= SectionID; sectionloop++) {
                        var checklistarray = [];
                        for (var checklist = 0; checklist <= x; checklist++) {
                            if ($("#txtlistname" + steploop + "" + sectionloop + "" + checklist + "").val() != undefined && $("#txtlistname" + steploop + "" + sectionloop + "" + checklist + "").val() != "") {
                                var checklistdata = [];
                                checklistdata.push($("#txtCheckListID" + steploop + "" + sectionloop + "" + checklist + "").val() == undefined ? 0 : $("#txtCheckListID" + steploop + "" + sectionloop + "" + checklist + "").val());
                                checklistdata.push($("#txtlistname" + steploop + "" + sectionloop + "" + checklist + "").val());
                                checklistarray.push(checklistdata);
                            }
                        }
                        if ($("#sectionField" + steploop + "" + sectionloop + "").val() != undefined && $("#sectionField" + steploop + "" + sectionloop + "").val() != "") {
                            var Section = [];
                            Section.push($("#txtSectionID" + steploop + "" + sectionloop + "").val() == undefined ? 0 : $("#txtSectionID" + steploop + "" + sectionloop + "").val());
                            Section.push($("#sectionField" + steploop + "" + sectionloop + "").val());
                            Section.push($("#SectionTextArea" + steploop + "" + sectionloop + "").val());
                            Section.push($("#textArea" + steploop + "" + sectionloop + "").val() == undefined ? "" : $("#textArea" + steploop + "" + sectionloop + "").val());
                            Section.push($("#txtDetialID" + steploop + "" + sectionloop + "").val() == undefined ? 0 : $("#txtDetialID" + steploop + "" + sectionloop + "").val());
                            Section.push(checklistarray);
                            SectionData.push(Section);
                        }
                    }
                    if ($("#txtstepname" + steploop + "").val() != undefined && $("#txtstepname" + steploop + "").val() != "") {
                        var steps = [];
                        steps.push($("#txtStepID" + steploop + "").val());
                        steps.push($("#txtstepname" + steploop + "").val());
                        if ($("#sort" + steploop + "").is(":checked")) {
                            steps.push('desc');
                        } else {
                            steps.push('asc');
                        }
                        steps.push(SectionData);
                        OuterStep.push(steps);
                    }

                }
            }
            var formdata = new FormData();
            var outerID = 0;
            for (var OurtLoop = 0; OurtLoop < id; OurtLoop++) {
                var InnerID = 0;
                for (var InnerLoop = 0; InnerLoop < SectionID; InnerLoop++) {
                    if ($("#SectionImage" + OurtLoop + "" + InnerLoop + "").prop("files") != undefined) {
                        if ($("#SectionImage" + OurtLoop + "" + InnerLoop + "").prop("files")[0] != undefined) {
                            formdata.append('image' + outerID + '' + InnerID + '', $("#SectionImage" + OurtLoop + "" + InnerLoop + "").prop("files")[0]);
                        }
                        InnerID++;
                    }

                }
                if ($("#StepVideo" + OurtLoop + "").prop("files") != undefined) {
                    if ($("#StepVideo" + OurtLoop + "").prop("files")[0] != undefined) {
                        formdata.append('StepVideo' + outerID + '', $("#StepVideo" + OurtLoop + "").prop("files")[0]);
                    }
                }
                outerID++;
            }

            formdata.append('value', JSON.stringify(OuterStep));
            formdata.append('_token', $("#token").val());

            formdata.append('TripID', '{{Session::get('TripID')}}');
            $("#BtnSave").attr('disabled', 'disabled');
            $.ajax({
                url: "{{route('steppack.step.add')}}",
                data: formdata,
                type: "POST",
                contentType: false,
                processData: false,
                success: function (json) {
                    if (json.status == 1) {
                        toastr['success']("" + json.errMsg + "");
                        $("#BtnSave").removeAttr('disabled');
                        location.reload(true);
                    } else {
                        toastr['error']("" + json.errMsg + "");
                    }
                }
            });
        });


        document.getElementsByTagName('input')[0].addEventListener('change', function (event) {
            var file = event.target.files[0];
            var fileReader = new FileReader();

            fileReader.onload = function () {
                var blob = new Blob([fileReader.result], {type: file.type});
                var url = URL.createObjectURL(blob);
                var video = document.createElement('video');
                var timeupdate = function () {
                    if (snapImage()) {
                        video.removeEventListener('timeupdate', timeupdate);
                        video.pause();
                    }
                };
                video.addEventListener('loadeddata', function () {
                    if (snapImage()) {
                        video.removeEventListener('timeupdate', timeupdate);
                    }
                });
                var snapImage = function () {
                    var canvas = document.createElement('canvas');
                    canvas.width = video.videoWidth;
                    canvas.height = video.videoHeight;
                    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                    var image = canvas.toDataURL();
                    var success = image.length > 100000;
                    if (success) {
                        var img = document.createElement('img');
                        img.src = image;
                        document.getElementsByTagName('div')[0].appendChild(img);
                        URL.revokeObjectURL(url);
                    }
                    return success;
                };
                video.addEventListener('timeupdate', timeupdate);
                video.preload = 'metadata';
                video.src = url;
                // Load video in Safari / IE11
                video.muted = true;
                video.playsInline = true;
                video.play();
            };
            fileReader.readAsArrayBuffer(file);
        });

        function stepvalueget(Stepid) {
            window.localStorage.setItem('Stepid', Stepid);
        }

        function getSectionValue(SectionID, Stepid) {
            window.localStorage.setItem('SectionID', SectionID);
            window.localStorage.setItem('Stepid', Stepid);
        }

        function isUnicode(str) {
            var letters = [];
            for (var i = 0; i <= str.length; i++) {
                letters[i] = str.substring((i - 1), i);
                if (letters[i].charCodeAt() > 255) {
                    return true;
                }
            }
            return false;
        }


    </script>
@endpush
