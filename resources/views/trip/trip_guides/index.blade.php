@extends('adminlte::trip')
@section('title', 'Guides')

@section('content_header')
<h1>Guides</h1>
 <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')

<div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                <form action="{{ route('trip.guide.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="Tripid" value="{{$trip->id}}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('guide_name') ? 'has-error' : '' }}">
                            <input type="text" name="guide_name" class="form-control" placeholder="Guide Name">
                            @if($errors->has('guide_name'))
                                <span class="help-block text-danger">{{ $errors->first('guide_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('guides') ? 'has-error' : '' }}">
                            <label>Upload Guides</label>
                            <input type="file" name="guides" class="form-control" multiple>
                            @if($errors->has('guides'))
                                <span class="help-block text-danger">{{ $errors->first('guides') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('youtube_link') ? 'has-error' : '' }}">
                            <label>Youtube Link</label>
                            <input type="text" name="youtube_link" class="form-control">
                            @if($errors->has('youtube_link'))
                                <span class="help-block text-danger">{{ $errors->first('youtube_link') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="form-group">
                            <button class="btn btn-primary btnSubmit">Add Guide</button>
                        </div>
                    </div>
                </div>
            </form>
                </div>
                <div class="col-md-8">
                <table id="airlines" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Guide Title</th>
                    <th>File Name</th>
                    <th>YouTube Link</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($guides as $guide)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $guide->title }}</td>
                        <td>{{ $guide->path?$guide->path:"--" }}</td>
                        <td>{{ $guide->youtube_link?$guide->youtube_link:"--" }}</td>
                        <td>
                            <form action="{{ route('trip.guide.destroy', $guide->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script type="text/javascript">
        $(function () {
            $('#airlines').DataTable({
                'lengthChange': false,
                "columnDefs": [
                 { orderable: false, targets:4 },
                 ],
            });
        });
    </script>
@endpush