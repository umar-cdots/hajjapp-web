@extends(\Auth::user()->user_type == "mobile_agent"?'adminlte::page':'adminlte::trip')

@section('title', 'Trip Updates')

@section('content_header')
<h1>Push Notification</h1>
 <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            {{-- <h3 class="box-title">Assign Customers to Trip</h3> --}}
        </div>
        <form action="{{ route('trip.notification.store') }}" method="POST" role="form" enctype="multipart/form-data" id="NotificationForm" onsubmit="return validate()">
            @csrf
            <input type="hidden" name="trip_id" value="{{ $trip->id }}">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 text-right">

                        <a href="#AssignClientModal" class="btn btn-primary btnSubmit"  data-toggle="modal" >Assign Client</a>

                      <a href="#AssignStaffModal" class="btn btn-primary btnSubmit"  data-toggle="modal" >Assign Staff</a>
                      
                          <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <textarea name="push_notification" class="form-control" style="margin-top: 21px;" required="required"></textarea>
                            @if($errors->has('push_notification'))
                                <span class="help-block text-danger">{{ $errors->first('push_notification') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
<div class="modal fade in" id="AssignClientModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Assign Client</h4>
      </div> 
      <div class="modal-body">
        <div class="row">
                    <div class="col-md-12">
                        @foreach($getTripGroup as $group)
                            <div class="panel-group" id="group" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title"> <input type="checkbox" name="Group[]" class="groupcheck" id="groupCheck{{$group->group->id}}" onclick="GroupCheck('{{$group->group->id}}')"> 
                                    <a data-toggle="collapse" data-parent="#group" href="#group-{{ $loop->iteration }}" aria-expanded="true" aria-controls="group-{{ $loop->iteration }}">
                                     <i class="accordion_icon fa fa-plus"></i> {{$group->group->group_name}} 
                                    </a>
                                  </h4>
                                </div>
                                <div id="group-{{ $loop->iteration }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    <ul class="checkboxesMain checkboxesMainclient">
                                    @foreach($group->clients as $client)
                                    <li><input type="checkbox"  name="mainclientcheckboxes[]" class="mainclientcheckboxes clientcheckbox{{$group->group->id}}" id="cleintbox{{$client->pivot['id']}}" value="{{$client->pivot['id']}}" onclick="Clinetcheckbox('{{$group->group->id}}','{{$client->pivot['id']}}')">{{$client->name}}</li>
                                    @endforeach
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                        @endforeach
                    </div> 
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="BtnAddGroup">Assign Client</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

                          <div class="modal fade in" id="AssignStaffModal">
                            <div class="modal-dialog">
                              <div class="modal-content">
                               <div class="modal-header">
                                <button type="button" class="close" aria-label="Close" id="btnclosestaff">
                                 <span aria-hidden="true">×</span></button>
                                  <h4 class="modal-title">Assign Staff</h4>
                                    </div> 
                                    <div class="modal-body">
                                    <div class="row">
                                    <div class="col-md-12">
                                    <ul class="checkboxesMain checkboxstaff">
                                      @foreach($allStaff as $staff)
                                      <li><input type="checkbox" class="staffs" name="staff[]" value="{{$staff->id}}">{{$staff->name}}</li>
                                      @endforeach
                                    </ul>
                                    </div>
                                   </div>
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btnCloseStaffModal">Assign Staff</button>
                          </div>
                        </div>
                         <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                        </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Push Notifications</button>
            </div>
        </form>
    </div>

@endsection
@push('js')
<script type="text/javascript">
$("#BtnAddGroup").click(function(){
    $("#AssignClientModal").modal('toggle');
});
function GroupCheck(id){
  if($("#groupCheck"+id+"").prop("checked") == true){
    $(".clientcheckbox"+id+"").prop("checked",true);
  }
  else{
  $(".clientcheckbox"+id+"").prop("checked",false); 
  }
}

$("#btnCloseStaffModal").click(function(){
  $("#AssignStaffModal").modal('toggle');
});
$("#btnclosestaff").click(function(){
  $("#AssignStaffModal").modal('toggle');
});

function Clinetcheckbox(id,clientid){
if($("#cleintbox"+clientid+"").prop("checked") == true){
    $("#groupCheck"+id+"").prop("checked",true);
  }
  else{
    var clientarray=[];
    $(".clientcheckbox"+id+"").each( function () {
       if($(this).prop("checked")==true) {
        clientarray.push($(this).val());
       }
   });
    if(clientarray.length > 0){

    }
    else{
    $("#groupCheck"+id+"").prop("checked",false);    
    }
   
  }
}

function validate() {
  var clientarray=[];
  var staffarray=[];
  $(".groupcheck").each( function () {
       if($(this).prop("checked")==true) {
        clientarray.push($(this).val());
       }
   });
  $(".staffs").each( function () {
       if($(this).prop("checked")==true) {
        staffarray.push($(this).val());
       }
   });
  if(clientarray.length<=0 && staffarray.length<=0){
         toastr['error']("Please select staff or client");
         return false;
         }
         else{
          return true;
         }
      }

</script>
@endpush