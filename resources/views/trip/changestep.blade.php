

@extends(\Auth::user()->user_type == "mobile_agent"?'adminlte::page':'adminlte::trip')

@section('title', 'Change Step')

@section('content_header')
    <h1>Change steps</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection


@section('content')
<div class="box"> 
   <div class="box-body">
    <form action="{{ route('changestep.storechangestep')}}" id="Form" method="POST">
        @csrf
    <div class="row">
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <label>Select Steps</label>
              </div>
              <div class="col-md-9 col-sm-9 col-xs-12 text-right">
                <div class="form-group">
                  <a href="#AssignClientModal" class="btn btn-primary btnSubmit"  data-toggle="modal" class="text-right" id="btnAssignClient">Assign Client</a>&nbsp;<a href="#" class="btn btn-primary btnSubmit" class="text-right" onclick="ShowAllClient()">View Assign Client</a>
                </div>
              </div>
              <div class="clearfix"></div>
          </div>
            <div class="form-group">
              
              <select class="form-control StepSelec2" name="Step" multiple="multiple" required="required">
               @if(!empty($getSteps))
               @foreach($getSteps as $step)
               <option value="{{ $step->id }}">{{ $step->title }}</option>
               {{--@if($flight->from_airport_id==$airport->id)
               <option value="{{ $airport->id }}" selected="selected">{{ $airport->name }}</option>
               @else
               <option value="{{ $airport->id }}">{{ $airport->name }}</option>
                @endif--}}
                @endforeach
                @endif
               </select>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="AssignClientModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Assign Client</h4>
      </div> 
      <div class="modal-body">
        <div class="row">
                    <div class="col-md-12">
                        @foreach($getTripGroup as $group)
                            <div class="panel-group" id="group" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title"> <input type="checkbox" name="Group[]" id="groupCheck{{$group->group->id}}" onclick="GroupCheck('{{$group->group->id}}')" checked="checked"> 
                                    <a data-toggle="collapse" data-parent="#group" href="#group-{{ $loop->iteration }}" aria-expanded="true" aria-controls="group-{{ $loop->iteration }}">
                                     <i class="accordion_icon fa fa-plus"></i> {{$group->group->group_name}} 
                                    </a>
                                  </h4>
                                </div>
                                <div id="group-{{ $loop->iteration }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    <ul class="checkboxesMain checkboxesMainclient">
                                    @foreach($group->clients as $client)
                                    <li><input type="checkbox"  name="mainclientcheckboxes[]" class="mainclientcheckboxes clientcheckbox{{$group->group->id}}" id="cleintbox{{$client->pivot['id']}}" value="{{$client->pivot['id']}}" onclick="Clinetcheckbox('{{$client->pivot['id']}}')" checked="checked">{{$client->name}}</li>
                                    @endforeach
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                        @endforeach
                    </div> 
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="BtnAddGroup">Assign Client</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
    <div class="text-center">
      <button type="submit" class="btn btn-primary btnSubmit">Save</button>
    </div>
    </form>
    </div>
</div>
 
@endsection

@push('js')
<script>
    $(".StepSelec2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Steps",
        });
    $("#BtnAddGroup").click(function (){
        $("#AssignClientModal").modal('toggle');
    });
    function GroupCheck(id){
    if($("#groupCheck"+id+"").prop("checked") == true){
        $(".clientcheckbox"+id+"").prop("checked",true);
    }
    else{
    $(".clientcheckbox"+id+"").prop("checked",false);   
    }
}
function ShowAllClient(){
    var a=$(".StepSelec2").val();
    if(a.length<=0){
        toastr['error']("please select first step");
    }
    else{
        var a=$(".StepSelec2").val();
        var url="{{route('changestep.viewassignclient',':id')}}";
        url = url.replace(':id', a);
        window.open(url,'_blank');
    }
}
</script>
@endpush