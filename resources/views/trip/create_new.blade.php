@extends('adminlte::page')
@section('title', 'Add Trip')

@section('content_header')
<h1>Add Trip</h1>
@endsection

@section('content')
	<div class="box">
		<div class="box-header"></div>
		<div class="box-body">
			<form action="#" method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-4">
					<div class="form-group">
						<label>Trip Name</label>
						<input type="text" name="trip_name" class="form-control">
						</div>
					</div>
				<div class="col-md-4">
					<div class="form-group">
						<label style="display: block;">Qibla Widget</label>
						<input type="checkbox" name="qibla_widget" class="minimal">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Trip Image</label>
						<input type="file" name="trip_image">
					</div>
				</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Assign Client</label> &nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#addClient">Add Client</a>
					</div>
					<div class="col-md-4"></div>
					<div class="col-md-2">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#viewCustomer">View Assigned Customer</button>
					</div>
					<div class="col-md-2">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addStep">Add Step</button>
					</div>
				</div>
			</form>
		</div>
		<div class="box-footer"></div>
	</div>

<div id="addClient" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Client</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-12">
        	<div class="form-group">
        		<label>Select Category</label>
	        	<select class="form-control" name="">
	        		<option value=""></option>
	        	</select>
        	</div>
        </div>
         <div class="col-md-12">
        	<div class="form-group">
        		<label>Select Category</label>
	        	<select class="form-control" name="">
	        		<option value=""></option>
	        	</select>
        	</div>
        </div>
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<label><input type="checkbox" name=""> &nbsp; Lorem Ipsum</label>
        	</div>
        	<div class="col-md-6">
        		<label><input type="checkbox" name=""> &nbsp; Lorem Ipsum</label>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection