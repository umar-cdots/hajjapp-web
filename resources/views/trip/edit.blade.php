@extends('adminlte::trip')

@section('title', 'Edit Trip')

@section('content_header')
    <h1>Edit Trip</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <form action="{{ route('trips.update', $trip->id) }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 tripImage">
                        <div class="input-group {{ $errors->has('profile_img') ? 'has-error' : '' }}">
                            <input type="file" name="profile_img" id="imgpreview">
                            <label for="imgpreview"><img id="viewImage"
                                                         src="{{ asset('storage/profile_images/'. $trip->path)}}"
                                                         alt="Upload Image" height="178"></label>
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('profile_img') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                    <label>Trip Name <span>*</span></label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter Trip Name"
                                           value="{{ $trip->title }}" maxlength="30">
                                    @if($errors->has('title'))
                                        <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('capacity') ? 'has-error' : '' }}">
                                    <label>Capacity <span>*</span></label>
                                    <input type="number" name="capacity" class="form-control"
                                           placeholder="Enter Capacity" value="{{$trip->capacity}}">
                                    @if($errors->has('capacity'))
                                        <span class="help-block text-danger">{{ $errors->first('capacity') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('guide_qouta') ? 'has-error' : '' }}">
                                    <label>Guide Quota <span>*</span></label>
                                    <input type="number" name="guide_qouta" class="form-control"
                                           placeholder="Enter Guide Quota" value="{{$trip->guide_qouta}}">
                                    @if($errors->has('capacity'))
                                        <span class="help-block text-danger">{{ $errors->first('guide_qouta') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                                    <label>Start Date <span>*</span></label>
                                    <input type="text" name="start_date" class="form-control datepicker"
                                           placeholder="Enter Start Date" value="{{ $trip->start_date}}"
                                           autocomplete="off">
                                    @if($errors->has('start_date'))
                                        <span class="help-block text-danger">{{ $errors->first('start_date') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                                    <label>End Date <span>*</span></label>
                                    <input type="text" name="end_date" class="form-control datepicker"
                                           placeholder="Enter End Date" value="{{$trip->end_date}}" autocomplete="off">
                                    @if($errors->has('end_date'))
                                        <span class="help-block text-danger">{{ $errors->first('end_date') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('app_menu') ? 'has-error' : '' }}">
                                    <label>App Menu Hide</label>
                                    <input type="checkbox" name="app_menu" {{$trip->app_menu == true ? 'checked':''}}>
                                    @if($errors->has('app_menu'))
                                        <span class="help-block text-danger">{{ $errors->first('app_menu') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('active_trip') ? 'has-error' : '' }}">
                                    <label>Active Trip Display</label>
                                    <input type="checkbox"
                                           name="active_trip" {{$trip->active_trip == true ? 'checked':''}}>
                                    @if($errors->has('active_trip'))
                                        <span class="help-block text-danger">{{ $errors->first('active_trip') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('dates_check') ? 'has-error' : '' }}">
                                    <label>App Dates Hide</label>
                                    <input type="checkbox"
                                           name="dates_check" {{$trip->dates_check == true ? 'checked':''}}>
                                    @if($errors->has('dates_check'))
                                        <span class="help-block text-danger">{{ $errors->first('dates_check') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="{{route('customer.trip.index',$trip->id)}}"
                               class="btn btn-primary btn-block btnSubmit">Assign Client</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="{{route('view.trip.customer',$trip->id)}}"
                               class="btn btn-primary btn-block btnSubmit">View Assign Client</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block btnSubmit" type="button" id="btncreategroup">Create
                                Group
                            </button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="{{route('customer.trip.index',$trip->id)}}"
                               class="btn btn-primary btn-block btnSubmit " type="button">View Groups</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="btn-block">Trip Description</label>
                        <textarea class="form-control" name="description" rows="5"
                                  maxlength="75">{{$trip->description}}</textarea>
                    </div>
                </div>


            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update Trip</button>
            </div>
        </form>
    </div>


    <div class="modal fade in" id="groupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add Group</h4>
                </div>
                <form method="POST" role="form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Group Name</label>
                                    <input type="text" name="group_name" id="group_name" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="BtnAddGroup">Create</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection



@push('js')
    <script type="text/javascript">

        function TripImageUpload(input) {

            if ($("#imgpreview").prop("files")[0]) {
                switch ($("#imgpreview").val().substring($("#imgpreview").val().lastIndexOf('.') + 1).toLowerCase()) {
                    case 'gif':
                    case 'jpg':
                    case 'png':

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#viewImage').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                        break;
                    default:
                        $("#imgpreview").val('');
                        toastr['error']("File format not correct!");
                        break;
                }
            }


            // if (input.files && input.files[0]) {
            //     var reader = new FileReader();
            //     reader.onload = function (e) {
            //         $('#viewImage').attr('src', e.target.result);
            //     }
            //     reader.readAsDataURL(input.files[0]);
            // }
        }

        $("#imgpreview").change(function () {
            TripImageUpload(this);
        });

        $('select#travel_agency_id').change(function () {
            var travel_agency_id = $(this).val();
            $.ajax({
                url: "{{ route('ajax.loadStepPacks', ['id' => "__TRAVEL_AGENCY_ID__"]) }}".replace("__TRAVEL_AGENCY_ID__", travel_agency_id),
                data: "travel_agency_id=" + travel_agency_id,
                cache: false,
                dataType: "json",
                success: function (response) {
                    var html = "<option>Select Steps Pack</option>";
                    $(response).each(function (index, step_pack) {
                        html += "<option value='" + step_pack.id + "'>" + step_pack.title + "</option>";
                    });
                    $('select#step_pack_id').html(html);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            })
        });
        $('.datepicker').datepicker({
            autoclose: true,
            orientation: 'auto bottom',
            format: 'yyyy/mm/dd'
        });

        $("#btncreategroup").click(function () {
            $("#groupModal").modal('show');
        });

        $("#BtnAddGroup").click(function () {
            $.ajax({
                url: "{{ route('addnewgroup') }}",
                type: 'POST',
                data: {_token: $("#token").val(), group_name: $("#group_name").val()},
                success: function (response) {
                    if (response.status == true) {
                        toastr['success']("" + response.errMsg + "");
                        location.reload();

                    } else {
                        toastr['error']("" + response.errMsg + "");
                    }
                }

            });
        });
    </script>
@endpush
