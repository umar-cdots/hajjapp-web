@extends('adminlte::page')

@section('title', 'Active Trips')

@section('content_header')
    <h1>Active Trips</h1>
    <div class="clearfix"></div>
    {{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
            </div>
            <form action="{{route('trips.search')}}" method="POST">
                @csrf
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                @method('POST')
                <div class="row">

                    <div class="col-md-9">
                        <div class="form-group" >
                            <select name="trip_id" class="form-control TripSelect" multiple="true">
                                <option value="">Select Trip</option>
                                @if(!empty($tripforselect2))
                                    @foreach($tripforselect2 as $tripset)
                                    @if($tripset->trip!=null)
                                        <option value="{{ $tripset->trip->id }}">{{ $tripset->trip->title }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1" style="padding: 0;">
                        <div class="form-group">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    
                </div>
            </form>

        </div>
        <!-- /.box-header -->
        <div class="box-body dataTables_wrapper tripTable">

            <table class="table table-bordered table-striped dataTable no-footer " id="travelAgency" width="100%">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        @if(\Auth::user()->user_type == "super_admin")
                            <th>Travel Agency</th>
                        @endif
                        <th>Trip Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Qibla Widget</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($trips as $trip)
                    @if($trip->trip!=null)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            @if(\Auth::user()->user_type == "super_admin")
                                <td>
                                    <a href="{{ route('travel_agency.show', ['id' => $trip->travel_agency_id]) }}">{{ $trip->travelAgency->agency_name }}</a>
                                </td>
                            @endif
                            <td>{{ $trip->trip->title }}</td>
                            <td>{{ $trip->trip->start_date }}</td>
                            <td>{{ $trip->trip->end_date }}</td>
                            <td>
                                {{-- <button class="btn btn-xs btn-{{ $trip->trip->is_qibla_widget_enable ? 'success' : 'warning' }}"></button> --}}
                                <input type="checkbox" id="changetripstatus" {{ $trip->trip->is_qibla_widget_enable ? 'Checked' : '' }} data-toggle="toggle" onchange="changetripstatus({{$trip->trip->id}})">

                            </td>
                            <td>
                                <input type="checkbox"  id="changetripactive" {{ $trip->trip->end_trip ? 'Checked' : '' }} data-toggle="toggle" onchange="changetripactive({{$trip->trip->id}})">

                            </td>

                            @if(Auth::user()->user_type == "mobile_agent")
                            <td>
                                <a href="{{route('changestep',['id' => $trip->trip->id])}}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Change Steps">Change Steps</button></a>
                                <a href="{{ route('trip.notification.index', ['id' => $trip->trip->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Push Notification">Push Notification</button></a>
                          </td>
                            @else
                            <td>
                                <a href="{{route('changestep',['id' => $trip->trip->id])}}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Change Steps">Change Steps</button></a>
                                <a href="{{ route('customStepPack', ['id' => $trip->trip->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('trips.destroy', $trip->trip->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                          </td>
                            @endif
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('css')

@endpush

@push('js')
    <script type="text/javascript">
        $(function () {
          $('#travelAgency').DataTable({
            'lengthChange': false,
            "searching":false,
            "columnDefs": [
                 { orderable: false, targets:[4,5,6] },
                 ],
          });
        });
        $(".TripSelect").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Trip"//placeholder
        });
        function changetripstatus(id) {
        $.ajax({
            url: "{{route('trips.ChangeTripQibla')}}",
            data:{_token: $("#token").val(),id:id},
            type: "POST",
            success: function (json) {
               }
        });
        }
        function changetripactive(id) {
        $.ajax({
            url: "{{route('trips.statuscheck')}}",
            data:{_token: $("#token").val(),id:id},
            type: "POST",
            success: function (json) {
                html = '<option>Select Trip</option>';
                        $(json.tripforselect2).each( function (i,d){
                            html += '<option value="'+ d.id +'">'+ d.title +'</option>';
                        });
                        $("#SelectAllTripDetail").html(html);
            }
        });
        }
    </script>
@endpush
