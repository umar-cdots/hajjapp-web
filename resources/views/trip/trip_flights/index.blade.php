@extends('adminlte::trip')
@section('title', 'Airline Management')

@section('content_header')
<h1>Flight Management</h1>
 <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
	<div class="box">
		<div class="box-body">
			<form action="{{ route('trip.flight.store') }}" id="Form" method="POST" class="mrgnTp" id="FlightForm" onsubmit="return validate()">
				@csrf
				<input type="hidden" value="{{ $trip->id }}" name="trip_id">
				<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
				@includeWhen($errors->any(), 'errors.form_errors')
				<div id="flight_container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4">
								<label><input type="checkbox" name="" class="selectAllCheckboxes" checked="checked"> &nbsp; Select All</label>
							</div>
							<div class="col-md-8 text-right">
								<a href="#AssignClientModal" class="btn btn-primary btnSubmit"  data-toggle="modal" >Assign Client</a>
								<a href="#" class="btn btn-primary btnSubmit" id="btnAddFlight">Add Flight</a>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="mrgnTp AppenedArilineRows">
						@if(!$getFlight->isEmpty())
						@foreach($getFlight as $key => $flight)
						<div class="remove{{$flight->id}}">
						<input type="hidden" name="flight_id[]" value="{{$flight->id}}" id="#txtFlightid{{$flight->id}}">
						<div class="col-md-3">
							<input type="checkbox" name="SelectFlight[]" value="{{$key}}" id="selectflight{{$key}}" class="checkboxSelect" checked="checked">
						</div>
						<div class="col-md-9 text-right">
							<a href="{{route('trip.flight.viewassignclient',$flight->id)}}" class="btn btn-primary btnSubmit">View Assigned Clients</a>
							<a href="javascript:void(0)" class="btn btn-danger remove_field"><i class="fa fa-minus" onclick="RemoveRowFromDB('{{$flight->id}}')"></i></a>
						</div>
						<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label>From</label>
										<select class="form-control FromAirportSelec2" name="from[]" multiple="multiple" required="required">
									     @if(!empty($airports))
										  @foreach($airports as $airport)
										  @if($flight->from_airport_id==$airport->id)
										  <option value="{{ $airport->id }}" selected="selected">{{ $airport->name }}</option>
										  @else
										  <option value="{{ $airport->id }}">{{ $airport->name }}</option>
										  @endif
										 @endforeach
									     @endif
								         </select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>To</label>
										<select class="form-control ToAirportSelec2" name="to[]" multiple="multiple" required="required">
									      @if(!empty($airports))
										  @foreach($airports as $airport)
										  @if($flight->to_airport_id==$airport->id)
										  <option value="{{ $airport->id }}" selected="selected">{{ $airport->name }}</option>
										  @else
										  <option value="{{ $airport->id }}">{{ $airport->name }}</option>
										  @endif
											
										  @endforeach
									      @endif
								        </select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Departure Date - Time</label>
										<input type="text" name="depareture_date_time[]" class="form-control datepicker" value="{{$flight->departure_datetime}}" required="required" id="DeparetureDate{{$key}}" autocomplete="off" onfocusout="Checkfromdate('{{$key}}')">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Arrival Date - Time</label>
										<input type="text" name="arrival_date_time[]" class="form-control datepicker" value="{{$flight->arrival_datetime}}" required="required" onfocusout="Checkdate('{{$key}}')" id="ArrivalDate{{$key}}" autocomplete="off">
									</div>
									</div>
								</div>
									<div class="row">
									<div class="col-md-3">
									<div class="form-group">
										<label>Reference</label>
										<input type="text" name="reference[]" class="form-control" value="{{$flight->pnr}}" required="required">
									</div>
									</div>
									<div class="col-md-3">
									<div class="form-group">
										<label>Airline</label>
										<select class="form-control AirlineSelec2" name="airline[]" multiple="multiple" required="required">
									      @if(!empty($airlines))
										  @foreach($airlines as $airline)
										  @if($flight->airline_id==$airline->id)
										  <option value="{{ $airline->id }}" selected="selected">{{ $airline->name }}</option>
										  @else
										  <option value="{{ $airline->id }}">{{ $airline->name }}</option>
										  @endif
										  @endforeach
									      @endif
								        </select>
									</div>
									</div>
									<div class="col-md-2">
									<div class="form-group">
										<label>Terminal</label>
										<input type="text" name="terminal[]" class="form-control" value="{{$flight->terminal}}">
									</div>
								    </div>
									<div class="col-md-2">
									<div class="form-group">
										<label>Gate</label>
										<input type="text" name="gate[]" class="form-control" value="{{$flight->gate}}">
									</div>
								   </div>
									<div class="col-md-2">
									<div class="form-group">
										<label>Note</label>
										<input type="text" name="note[]" class="form-control" value="{{$flight->note}}"  maxlength="30">
									</div>
								</div>
						</div>
						@endforeach
						@else
						<input type="hidden" name="flight_id[]" value="0" id="#txtFlightid0">
						<div class="col-md-3">
							<input type="checkbox" name="SelectFlight[]" value="0" class="checkboxSelect" id="selectflight0" checked="checked">
						</div>
						<div class="col-md-9 text-right">
							<a href="#" class="btn btn-primary btnSubmit" onclick="ViewAssignClientMessage()">View Assigned Clients</a>
						</div>
						<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label>From</label>
										<select class="form-control FromAirportSelec2" name="from[]" multiple="multiple" required="required">
									     @if(!empty($airports))
										  @foreach($airports as $airport)
											<option value="{{ $airport->id }}">{{ $airport->name }}</option>
										 @endforeach
									     @endif
								         </select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>To</label>
										<select class="form-control ToAirportSelec2" name="to[]" multiple="multiple" required="required">
									      @if(!empty($airports))
										  @foreach($airports as $airport)
											<option value="{{ $airport->id }}">{{ $airport->name }}</option>
										  @endforeach
									      @endif
								        </select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Departure Date - Time</label>
										<input type="text" name="depareture_date_time[]" class="form-control datepicker" required="required" id="DeparetureDate0" autocomplete="off" onfocusout="Checkfromdate(0)">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Arrival Date - Time</label>
										<input type="text" name="arrival_date_time[]" class="form-control datepicker" required="required" onfocusout="Checkdate(0)" id="ArrivalDate0" autocomplete="off">
									</div>
									</div>
								
							   </div>
							   <div class="row">
									<div class="col-md-3">
									<div class="form-group">
										<label>Reference</label>
										<input type="text" name="reference[]" class="form-control" required="required">
									</div>
									</div>
									<div class="col-md-3">
									<div class="form-group">
										<label>Airline</label>
										<select class="form-control AirlineSelec2" name="airline[]" multiple="multiple" required="required">
									      @if(!empty($airlines))
										  @foreach($airlines as $airline)
											<option value="{{ $airline->id }}">{{ $airline->name }}</option>
										  @endforeach
									      @endif
								        </select>
									</div>
									</div>
									<div class="col-md-2">
									<div class="form-group">
										<label>Terminal</label>
										<input type="text" name="terminal[]" class="form-control">
									</div>
									</div>
									<div class="col-md-2">
									<div class="form-group">
										<label>Gate</label>
										<input type="text" name="gate[]" class="form-control" >
									</div>
									</div>
									<div class="col-md-2">
									<div class="form-group">
										<label>Note</label>
										<input type="text" name="note[]" class="form-control" maxlength="30">
									</div>
									</div>
								</div>
						@endif
						
						</div>
				</div>
			</div>
<div class="modal fade in" id="AssignClientModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Assign Client</h4>
      </div> 
      <div class="modal-body">
        <div class="row">
                    <div class="col-md-12">
                        @foreach($getTripGroup as $group)
                            <div class="panel-group" id="group" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title"> <input type="checkbox" name="Group[]" id="groupCheck{{$group->group->id}}" onclick="GroupCheck('{{$group->group->id}}')" checked="checked"> 
                                    <a data-toggle="collapse" data-parent="#group" href="#group-{{ $loop->iteration }}" aria-expanded="true" aria-controls="group-{{ $loop->iteration }}">
                                     <i class="accordion_icon fa fa-plus"></i> {{$group->group->group_name}} 
                                    </a>
                                  </h4>
                                </div>
                                <div id="group-{{ $loop->iteration }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    <ul class="checkboxesMain checkboxesMainclient">
                                    @foreach($group->clients as $client)
                                    <li><input type="checkbox"  name="mainclientcheckboxes[]" class="mainclientcheckboxes clientcheckbox{{$group->group->id}}" id="cleintbox{{$client->pivot['id']}}" value="{{$client->pivot['id']}}" onclick="Clinetcheckbox('{{$client->pivot['id']}}')" checked="checked">{{$client->name}}</li>
                                    @endforeach
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                        @endforeach
                    </div> 
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="BtnAddGroup">Assign Client</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
				<div class="row text-center">
					<button type="submit" class="btn btn-primary btnSubmit" id="datashow">Save</button>
				</div>
			</form>
	</div>
</div>

@endsection

@push('js')
<script type="text/javascript">
	var group=JSON.parse("{{$getFlight}}".replace(/&quot;/g,'"'));
	var x=0;
	var a=group.length;
	if(a>0){
        a=a-1;
    }
	x=a;
$('.datepicker').datetimepicker({
	format: 'YYYY-MM-DD hh:mm:ss',
    ignoreReadonly: true
});





$('.selectAllCheckboxes').click(function(){
        if($('.selectAllCheckboxes').prop("checked")==false){
          $('.checkboxSelect').prop("checked", false);
        }
        else{
            $('.checkboxSelect').prop("checked", true);
        }
        });
$(document).ready(function() {
	 $("#BtnAddGroup").click(function(){
	 	$("#AssignClientModal").modal('toggle');
	 });
	 $("#BtnAssignClientModalData").click(function(){
	 	$("#AssignClientModalData").modal('toggle');
	 });
});

$("#btnAddFlight").click(function(){
	x++;
  $(".AppenedArilineRows").append('<div class="remove'+x+'"> <input type="hidden" name="flight_id[]" value="0" id="#txtFlightid0"> <div class="col-md-3"> <input type="checkbox" name="SelectFlight[]" value="'+x+'" class="checkboxSelect" id="selectflight'+x+'" checked="checked"> </div><div class="col-md-9 text-right"><a href="#" class="btn btn-primary btnSubmit" onclick="ViewAssignClientMessage()">View Assigned Clients</a> <a href="javascript:void(0)" class="btn btn-danger remove_field"><i class="fa fa-minus" onclick="RemoveAppenedRow('+x+')"></i></a></div><div class="row"> <div class="col-md-3"> <div class="form-group"> <label>From</label> <select class="form-control FromAirportSelec2" name="from[]" multiple="multiple" required="required">"@if(!empty($airports))"@foreach($airports as $airport) <option value="{{$airport->id}}">{{$airport->name}}</option>@endforeach"@endif"</select> </div></div><div class="col-md-3"> <div class="form-group"> <label>To</label> <select class="form-control ToAirportSelec2" name="to[]" multiple="multiple" required="required">"@if(!empty($airports))"@foreach($airports as $airport) <option value="{{$airport->id}}">{{$airport->name}}</option>@endforeach"@endif"</select> </div></div><div class="col-md-3"> <div class="form-group"> <label>Departure Date - Time</label> <input type="text" name="depareture_date_time[]" class="form-control datepicker" required="required" id="DeparetureDate'+x+'" autocomplete="off" onfocusout="Checkfromdate('+x+')"> </div></div><div class="col-md-3"> <div class="form-group"> <label>Arrival Date - Time</label> <input type="text" name="arrival_date_time[]" class="form-control datepicker" required="required" onfocusout="Checkdate('+x+')" id="ArrivalDate'+x+'" autocomplete="off"> </div></div></div><div class="row"><div class="col-md-3"><div class="form-group"><label>Reference</label><input type="text" name="reference[]" class="form-control" required="required"></div></div><div class="col-md-3"><div class="form-group"><label>Airline</label><select class="form-control AirlineSelec2" name="airline[]" multiple="multiple" required="required"> @if(!empty($airlines)) @foreach($airlines as $airline)<option value="{{$airline->id}}">{{$airline->name}}</option> @endforeach @endif </select></div></div><div class="col-md-2"><div class="form-group"><label>Terminal</label><input type="text" name="terminal[]" class="form-control"></div></div><div class="col-md-2"><div class="form-group"><label>Gate</label><input type="text" name="gate[]" class="form-control"></div></div><div class="col-md-2"><div class="form-group"><label>Note</label><input type="text" name="note[]" class="form-control" maxlength="30"></div></div></div></div>');
  $('.datepicker').datetimepicker({
  	format: 'YYYY-MM-DD hh:mm:ss',
    ignoreReadonly: true
  });
  $(".FromAirportSelec2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select From Airport",
        tags:true
        });
$(".ToAirportSelec2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select To Airport",
        tags:true
        });
$(".AirlineSelec2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Airline",
        tags:true
        });
});

function GroupCheck(id){
	if($("#groupCheck"+id+"").prop("checked") == true){
		$(".clientcheckbox"+id+"").prop("checked",true);
	}
	else{
	$(".clientcheckbox"+id+"").prop("checked",false);	
	}
}

  function RemoveAppenedRow(id){
     $(".remove"+id+"").empty();
     var c=0;
     for(var i=0;i<=x;i++){
     	if($("#selectflight"+i+"").val()!=undefined){
     		$("#selectflight"+i+"").val(c);
     	     c++;
     	}
     } 
    }

    function RemoveRowFromDB(Flightid){
            $.ajax({
            url: "{{route('trip.flight.deleteflightbyid')}}",
            data:{_token: $("#token").val(),deletedata:Flightid},
            type: "POST",
            success: function (json) {
                if(json.status==1)
                {
                toastr['success']("Flight Delete Successfully"); 
                $(".remove"+Flightid+"").empty();
                  var c=0;
                  for(var i=0;i<=x;i++){
     	          if($("#selectflight"+i+"").val()!=undefined){
     		       $("#selectflight"+i+"").val(c);
     	           c++;
     	           }
                  }
                }
                else
                {
                    toastr['error']("Flidht Cannot Delete");
                }
               }
            });
            
         }


$(".FromAirportSelec2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select From Airport",
        tags:true
        });
$(".ToAirportSelec2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select To Airport",
        tags:true
        });
$(".AirlineSelec2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Airline",
        tags:true
        });

function Checkdate(id){
	var Tripdate=Date.parse("{{$trip->end_date}}");
        var b=Date.parse($("#ArrivalDate"+id+"").val());
        if(b>Tripdate){
            toastr['error']("Your trip end date"+" "+"{{$trip->end_date}}"+" "+ "please enter correct date");
           
        }
        else{
        if(Date.parse($("#ArrivalDate"+id+"").val())>=Date.parse($("#DeparetureDate"+id+"").val())){
	}
	else{
		toastr['error']("Please check date range");
		$("#ArrivalDate"+id+"").val('');
	}    
        }
}

function Checkfromdate(id){
        var Tripdate=Date.parse("{{$trip->end_date}}");
        var b=Date.parse($("#DeparetureDate"+id+"").val());
        if(b>Tripdate){
            toastr['error']("Your trip end date"+" "+"{{$trip->end_date}}"+" "+ "please enter correct date");
        }
        else{
        return false;    
        }
}

$('#datashow').submit(function(event) {
	event.preventDefault();
  alert("hello");
});
function ViewAssignClientMessage(){
	 
    toastr['error']("There is no client assigned to this flight");
}
 function validate() {
 	var array=[];
 	$(".checkboxSelect").each( function () {
       if($(this).prop("checked")==true) {
       	array.push($(this).val());
       }
   });
 	if(array.length>0){
         	return true;
         }
         else{
         	 toastr['error']("Please select at least one flight");
         return false;	
         }
      }
</script>
@endpush

