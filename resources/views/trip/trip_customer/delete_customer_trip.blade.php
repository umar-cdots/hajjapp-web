@extends('adminlte::page')
@section('title', 'Delete Trip Customer')

@section('content_header')
<h1>Delete Trip Customers</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
	<div class="box">
		<div class="box-header">
			{{-- <h3 class="box-title">Trip Customers</h3> --}}
		</div>

		<div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-responsive table-hover">
                            <tr>
                                <th>Client Name</th>
                                <th>Trip Title</th>
                                <th>Trip Start Date</th>
                                <th>Trip End Date</th>
                                <th>Action</th>
                            </tr>
                            @if(!empty($trip_clients))
                                @foreach($trip_clients as $trip_client)
                                    <tr>
                                        <td>{{ $trip_client->client->name }}</td>
                                        <td>{{ $trip_client->trip->title }}</td>
                                        <td>{{ $trip_client->trip->start_date }}</td>
                                        <td>{{ $trip_client->trip->end_date }}</td>
                                        <td>
                                            <form action="{{ route('remove.customer.remove') }}" method="post" id="delete_{{$trip_client->id}}">
                                                @csrf
                                                <input type="hidden" name="trip_client_id" value="{{ $trip_client->id }}">
                                                <button class="btn btn-danger btn-sm" type="submit" onclick="if(confirmDelete()){ document.getElementById('delete_{{$trip_client->id}}').submit();}">Remove</button>
                                            </form>
                                        </td>
                                    </tr>
                                 @endforeach
                            @endif
                        </table>
                    </div>
                </div>
		</div>
	</div>
@endsection

@push('js')
	<script>
		function changeStep(client_trip_id, select){
			var step_id = $(select).find(':selected').val();
			$("#customer_trip_"+client_trip_id).find('#step_id').val(step_id);
        };

        function confirmDelete() {
        var r = confirm("Are you sure you want to perform this action");
        if (r === true) {
            return true;
        }
        else {
            return false;
        }
    }
	</script>
@endpush
