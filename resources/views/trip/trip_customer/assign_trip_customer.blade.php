@extends('adminlte::trip')

@section('title', 'Assign Customers to Trip')

@section('content_header')
<h1>Assign Clients to Trip</h1>
 <ol class="breadcrumb">
        <li><a href="{{route('trips.edit',Session::get('TripID'))}}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <form  method="POST" role="form">
            @csrf
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="trip_id" id="txttripid" value="{{ $id }}">
            <div class="box-body assignCustomer">
                {{-- <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Customer Category</label>
                            <select name="customer_category" class="form-control" id="customer_category">
                                <option value="">--Select Category--</option>
                                @if(!empty($client_categories))
                                    @foreach($client_categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('client_category') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Client Sub Category</label>
                            <select name="customer_category" class="form-control" id="client_sub_category">
                                <option value="">--Select Sub Category--</option>
                            </select>
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('client_category') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Client</label>
                            <select name="customers[]" class="form-control select2" multiple="multiple" id="customer">

                            </select>
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled list-inline text-right assignCustomerBtns">
                            <li><a href="#" class="btn btn-primary btnSubmit" data-toggle="modal" data-target="#groupModal">Add Group </a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        @foreach($groupList as $group)
                            <div class="panel-group" id="group" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title"> <input type="checkbox" name="" id="groupCheck{{$group->id}}" onclick="checkgroup('{{$group->id}}')">
                                  <input type="hidden" id="{{$group->id}}"> 
                                    <a data-toggle="collapse" data-parent="#group" href="#group-{{ $loop->iteration }}" aria-expanded="true" aria-controls="group-{{ $loop->iteration }}">
                                     <i class="accordion_icon fa fa-angle-down"></i></a><input type="text" class="groupfields" style="color:black;" id="txtgroupdata{{$group->id}}" value="{{$group->group_name}}" onchange ="UpdateGroup('{{$group->id}}')">
                                    <span class="pull-right"><a href="#" id="BtnclientModal{{$group->id}}" class="addClientBtn" onclick="OpenModel('{{$group->id}}')">Add Clients </a></span>
                                  </h4>
                                </div>
                                <div id="group-{{ $loop->iteration }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    <ul class="checkboxesMain checkboxesMainclient{{$group->id}}">
                                    
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="modal fade in" id="clientModal{{$group->id}}">
                            <div class="modal-dialog">
                              <div class="modal-content">
                               <div class="modal-header">
                                <button type="button" class="close" aria-label="Close" onclick="CloseModal('{{$group->id}}')">
                                 <span aria-hidden="true">×</span></button>
                                  <h4 class="modal-title">Add Clients</h4>
                                    </div> <form method="POST" role="form">
                                    <div class="modal-body clientsModal">
                                    <div class="row">
                                    <div class="col-md-12">
                                    <ul class="checkboxesMain checkboxclient{{$group->id}}">
                                    </ul>
                                    </div>
                                   </div>
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="CloseModal('{{$group->id}}')">Save</button>
                          </div>
                         </form>
                        </div>
                         <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                        </div>

                        @endforeach
                    </div> 
                </div>
                    
            </div>
            <div class="box-footer text-center">
                <button type="button" class="btn btn-primary btnSubmit" id="btnAssignClient">Save</button>
            </div>
        </form>
    </div>
<div class="modal fade in" id="groupModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Add Group</h4>
      </div> 
      <form method="POST" role="form">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Group Name</label>
              <input type="text" name="group_name" id="group_name" class="form-control">  
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="BtnAddGroup">Create</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@endsection

@push('js')
    <script>

        var Arry=[];
        var OuterGroup=[];
      $(document).ready(function(){
        var a= '{{ Session::get('GroupList')}}';
      var group=JSON.parse(a.replace(/&quot;/g,'"'));
      for(var i=0; i<group.length;i++)
      {
        var grouplist=[];
        grouplist.push(group[i]['id']);
        grouplist.push('Deactive');
        OuterGroup.push(grouplist);
      }
      var GetTripWiseGroup= '{{ Session::get('GetTripWiseGroup')}}';
      var groupChecked=JSON.parse(GetTripWiseGroup.replace(/&quot;/g,'"'));
      for(var i=0;i<groupChecked.length;i++){
        $("#groupCheck"+groupChecked[i]['group_id']+"").prop("checked", true);
        for(var l=0;l<OuterGroup.length;l++)
          {
            if(OuterGroup[l][0]==groupChecked[i]['group_id'])
              {
                OuterGroup[l][1]='Active';
              }
          }
        for(var j=0;j<groupChecked[i]['clients'].length;j++)
         {
          var Arry1=[];
          Arry1.push(''+groupChecked[i]['clients'][j]['id'] +','+''+groupChecked[i]['clients'][j]['name']+'');
          Arry1.push(groupChecked[i]['group_id']);
          Arry.push(Arry1);
          }
          if(Arry.length>0)
         {
            html='<li></li>';
            for(var k=0;k<Arry.length;k++)
            {
                if(groupChecked[i]['group_id']==Arry[k][1]){
                var abc=Arry[k].join();
                var values= abc.split(',');
                html += '<li><input type="checkbox" name="mainclientcheckboxes'+groupChecked[i]['group_id']+'[]" class="mainclientcheckboxes'+groupChecked[i]['group_id']+'" value="'+Arry[k][0]+'/'+groupChecked[i]['group_id']+'" checked="checked">'+values[1]+'</li>';
            }
        }
            $(".checkboxesMainclient"+groupChecked[i]['group_id']+"").html(html);
            $('.mainclientcheckboxes'+groupChecked[i]['group_id']+'').click(function(){
                            if($(this).prop("checked") == true){
                                var Arry1=[];
                                var a=$(this).val();
                                var values= a.split('/');
                                Arry1.push(values[0]);
                                Arry1.push(values[1]);
                                Arry.push(Arry1);
                             }
                            else if($(this).prop("checked") == false){
                                if(Arry.length>0){
                                for(var i=0;i<Arry.length;i++)
                                {
                                   var a=$(this).val();
                                    var values= a.split('/');
                                    if(Arry[i][0]==values[0])
                                    {
                                        var a=Arry[i];
                                        $(this).empty();
                                        Arry.splice(i, 1);
                                        DeleteClient(a);
                                    }
                                }
                              }
                             }
                        });
         }
      }
      });
        $('.select2').select2();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        $("#customer_category").change( function(){
           var id = $(this).val();
           $.ajax({
                url: "{{ route('get.subcategories.category') }}",
               type: 'POST',
               data : { data: id},
               success: function(response){
                    if(response.status == 'success'){
                        html = '<option>--Select sub category</option>';
                        toastr['success']("Sub categories has been loaded.");
                        $(response.data).each( function (i,d){
                            html += '<option value="'+ d.id +'">'+ d.name +'</option>';
                        });
                        $("#client_sub_category").html(html);
                    }else{
                        toastr['error']("Something went wrong.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
        });

        $("#client_sub_category").change( function(){
            var id = $(this).val();
            $.ajax({
                url: "{{ route('get.client.subcategories') }}",
                type: 'POST',
                data : { data: id},
                success: function(response){
                    if(response.status == 'success'){
                        html = '<option>--Select customers--</option>';
                        toastr['success']("Client has been loaded.");
                        $(response.data).each( function (i,d){
                            html += '<option value="'+ d.id +'" selected>'+ d.name +'</option>';
                        });
                        $("#customer").html(html);
                    }else{
                        toastr['error']("Something went wrong.");
                    }
                },
                error: function(error){
                    toastr['error']("Something went wrong.");
                }

            });
        });
        function checkGroups(){
            var checkbox = $('.checkedCheckbox');
            checkbox.prop("checked", true);
        }
        function checkClients(){
            var checkbox = $('.checkedCheckbox2');
            checkbox.prop("checked", true);
        }



        $("#BtnAddGroup").click(function(){
            $.ajax({
                url: "{{ route('addnewgroup') }}",
                type: 'POST',
                data : { _token: $("#token").val(),group_name:$("#group_name").val()},
                success: function(response){
                    if(response.status == true){
                        toastr['success'](""+response.errMsg+"");
                        location.reload();
                    }else{
                        toastr['error'](""+response.errMsg+"");
                        location.reload();
                    }
                }

            });
        });

        function OpenModel(id)
        {
            $.ajax({
                url: "{{ route('getallclient') }}",
                type: 'POST',
                data : { _token: $("#token").val()},
                success: function(response){
                    if(response.status == true){
                        html='<li></li>';
                        $(response.data).each( function (i,d){
                          if(Arry.length>0){
                            var a=0;
                            for(var i=0;i<Arry.length;i++){
                              if(Arry[i][0]==""+ d.id +','+ d.name +""){
                                a=1;
                                break;
                              }
                              else{
                                a=0;
                              }
                            }
                            if(a==0){
                              html += '<li><input type="checkbox" name="clientcheckboxes" class="clientcheckboxes'+id+'" value="'+ d.id +','+ d.name +'"> '+d.name+'</li>';
                            }
                            else{
                              html += '<li><input type="checkbox" name="clientcheckboxes" class="clientcheckboxes'+id+'" value="'+ d.id +','+ d.name +'" checked="checked" disabled="disabled"> '+d.name+'</li>';
                            }
                          }
                          else{
                            html += '<li><input type="checkbox" name="clientcheckboxes" class="clientcheckboxes'+id+'" value="'+ d.id +','+ d.name +'"> '+d.name+'</li>';
                          }
                        });
                        $(".checkboxclient"+id+"").html(html);
                        $("#clientModal"+id+"").modal('show');
                        $('.clientcheckboxes'+id+'').click(function(){
                            if($(this).prop("checked") == true){
                              if(Arry.length>0){
                                var a=0;
                                for(var i=0;i<Arry.length;i++){
                                  if(Arry[i][0]==$(this).val() && Arry[i][1]==id){
                                    a=1;
                                    break;
                                  }
                                  else{
                                    a=0;
                                  }
                                }
                                if(a==0){
                                  var Arry1=[];
                                Arry1.push($(this).val());
                                Arry1.push(id);
                                Arry.push(Arry1);
                                }
                              }
                              else{
                                var Arry1=[];
                                Arry1.push($(this).val());
                                Arry1.push(id);
                                Arry.push(Arry1);
                              }
                                
                             }
                            else if($(this).prop("checked") == false){
                                if(Arry.length>0){
                                for(var i=0;i<Arry.length;i++)
                                {
                                    a=$(this).val();
                                    if(Arry[i][0]==$(this).val())
                                    {
                                        Arry.splice(i, 1);
                                    }
                                }
                              }
                             }
                        });
                    }else{
                        toastr['error']("Please Add Client First.");
                    }
                }

            });
        }
        function CloseModal(id)
        {
        $("#clientModal"+id+"").modal('toggle');
         if(Arry.length>0)
         {
            html='<li></li>';
            for(var i=0;i<Arry.length;i++)
            {
                if(id==Arry[i][1]){
                var abc=Arry[i].join();
                var values= abc.split(',');
                html += '<li><input type="checkbox" name="mainclientcheckboxes'+id+'[]" class="mainclientcheckboxes'+id+'" value="'+Arry[i][0]+'" checked="checked">'+values[1]+'</li>';
            }
        }
            $(".checkboxesMainclient"+id+"").html(html);
            $('.mainclientcheckboxes'+id+'').click(function(){
                            if($(this).prop("checked") == true){
                                var Arry1=[];
                                Arry1.push($(this).val());
                                Arry1.push(id);
                                Arry.push(Arry1);
                             }
                            else if($(this).prop("checked") == false){
                                if(Arry.length>0){
                                for(var i=0;i<Arry.length;i++)
                                {
                                    a=$(this).val();
                                    if(Arry[i][0]==$(this).val())
                                    {
                                        Arry.splice(i, 1);
                                    }
                                }
                              }
                             }
                        });
         }   
        }
      function DeleteClient(data){
        $.ajax({
            url: "{{route('customer.trip.deleteclient')}}",
            data:{_token: $("#token").val(),deletedata:data,tripid:$("#txttripid").val()},
            type: "POST",
            success: function (json) {
                if(json.status==1)
                {
                 toastr['success'](""+json.errMsg+"");   
                }
                else if(json.status==-1)
                {
                    toastr['error'](""+json.errMsg+"");
                }
                else
                {
                }
               }
        });
        }
        function checkgroup(id)
        {
            if($("#groupCheck"+id+"").prop("checked") == true)
            {
                for(var i=0;i<OuterGroup.length;i++)
                 {
                  a=$("#groupCheck"+id+"").val();
                  if(OuterGroup[i][0]==id)
                  {
                   OuterGroup[i][1]='Active';
                  }
                  else
                  { 
                  }
                 }
            }
            else if($("#groupCheck"+id+"").prop("checked") == false)
            {
                for(var i=0;i<OuterGroup.length;i++)
                 {
                  a=$("#groupCheck"+id+"").val();
                  if(OuterGroup[i][0]==id)
                  {
                   OuterGroup[i][1]='Deactive';
                   RemoveGroup(OuterGroup[i]);
                  }
                 }
            }
        }


        function RemoveGroup(data){
        $.ajax({
            url: "{{route('customer.trip.deletegroup')}}",
            data:{_token: $("#token").val(),deletedata:data,tripid:$("#txttripid").val()},
            type: "POST",
            success: function (json) {
                if(json.status==true)
                {
                 toastr['success']("Client delete successfully!");   
                }
               }
        });
        }

        $("#btnAssignClient").click(function() {
          var a=0;
          for(var i=0;i<OuterGroup.length;i++ ){
            if(OuterGroup[i][1]=="Active"){
              a=1;
              break;
            }
          }
          if(a==1){
        $.ajax({
            url: "{{route('customer.trip.store')}}",
            data:{_token: $("#token").val(),value:Arry,tripid:$("#txttripid").val(),outerGroup:OuterGroup},
            type: "POST",
            success: function (json) {
                if(json.status==true)
                {
                 toastr['success'](""+json.errMsg+"");
                 location.reload();   
                }
                else
                {
                    toastr['error'](""+json.errMsg+"");
                }
               }
        });
      }
      else{
        toastr['error']("Please select atleast one group!");
      }

    });
      function UpdateGroup(id){
        $.ajax({
            url: "{{route('customer.trip.updategroup')}}",
            data:{_token: $("#token").val(),groupname:$("#txtgroupdata"+id+"").val(),groupId:id},
            type: "POST",
            success: function (json) {
                if(json.status==true)
                {   
                }
                else
                {
                }
               }
        });
      }
$('.panel-title > a').click(function() {
    $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
       .closest('panel').siblings('panel')
       .find('i')
       .removeClass('fa-angle-down').addClass('fa-angle-up');
});
</script>
@endpush