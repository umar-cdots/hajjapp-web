@extends('adminlte::trip')
@section('title', 'Assign Hotel')

@section('content_header')
    <h1>Assign Hotel</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        

        <div class="box-body">
                    <form action="{{ route('assign.hotel.trip') }}" method="post" class="mrgnTp" onsubmit="return validate()">
                        @csrf
                    <input type="hidden" value="{{ $trip->id }}" name="trip_id">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="trip_id" value="{{ $trip->id }}">
                        <div id="hotel_container">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <label><input type="checkbox" name="" class="selectAllCheckboxes" checked="checked"> &nbsp; Select All</label>
                            </div>
                            <div class="col-md-8 text-right">
                                <a href="#AssignClientModal" class="btn btn-primary btnSubmit"  data-toggle="modal" >Assign Client</a>
                                <a href="#" class="btn btn-primary btnSubmit" id="btnAddHotel">Add Hotel</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="mrgnTp AppenedArilineRows">
                        @if(!$getHotel->isEmpty())
                        @foreach($getHotel as $key => $hotel)
                        <div class="remove{{$hotel->id}}">
                        <input type="hidden" name="hotel_id[]" value="{{$hotel->id}}" id="#txtFlightid{{$hotel->id}}">
                        <div class="col-md-3">
                            <input type="checkbox" name="SelectHotel[]" value="{{$key}}" class="checkboxSelect" id="selecthotel{{$key}}" checked="checked">
                        </div>
                        <div class="col-md-9 text-right">
                            <a href="{{route('trip.hotel.viewassignhotelclient',$hotel->id)}}" class="btn btn-primary btnSubmit">View Assigned Clients</a>
                        </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Select Hotel</label>
                                        <select class="form-control HotelNameSelect2" name="hotels[]" multiple="multiple" required="required">
                                            @if(!empty($hotels))
                                                @foreach($hotels as $hotelabc)
                                                @if($hotelabc->id==$hotel->hotel_id)
                                                    <option value="{{ $hotelabc->id }}" selected="selected">{{ $hotelabc->name }}</option>
                                                    @else
                                                    <option value="{{ $hotelabc->id }}">{{ $hotelabc->name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Stay From</label>
                                        <input type="text" name="stay_from[]" class="form-control datepicker" value="{{$hotel->stay_from}}" required="required" id="Fromdate{{$key}}"
                                        onchange="Checkfromdate('{{$key}}')"
                                        autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Stay To</label>
                                        <input type="text" name="stay_to[]" class="form-control datepicker" value="{{$hotel->stay_to}}" required="required" onchange="Checkdate('{{$key}}')" id="Todate{{$key}}"
                                        autocomplete="off">
                                    </div>
                                </div>
                                {{--<div class="col-md-2">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-control" name="status[]" required="required">
                                            @if($hotel->status=="upcoming_stay")
                                            <option value="">Select</option>
                                            <option value="upcoming_stay" selected="selected">upcoming_stay</option>
                                            <option value="staying">staying</option>
                                            <option value="left">left</option>
                                            @elseif($hotel->status=="staying")
                                            <option value="">Select</option>
                                            <option value="upcoming_stay">upcoming_stay</option>
                                            <option value="staying" selected="selected">staying</option>
                                            <option value="left">left</option>
                                            @endelseif
                                            @elseif($hotel->status=="left")
                                            <option value="">Select</option>
                                            <option value="upcoming_stay">upcoming_stay</option>
                                            <option value="staying">staying</option>
                                            <option value="left" selected="selected">left</option>
                                            @endelseif
                                            @else
                                            <option value="">Select</option>
                                            <option value="upcoming_stay" >upcoming_stay</option>
                                            <option value="staying">staying</option>
                                            <option value="left">left</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>--}}
                                <button type="button" class="btn btn-sm btn-danger remove_field" style="margin: 26px 0 0 15px;"><i class="fa fa-minus" onclick="RemoveRowFromDB('{{$hotel->id}}')"></i></button>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <input type="hidden" name="hotel_id[]" value="0" id="#txtFlightid0">
                        <div class="col-md-3">
                            <input type="checkbox" name="SelectHotel[]" value="0" class="checkboxSelect" id="selecthotel0" checked="checked">
                        </div>
                        <div class="col-md-9 text-right">
                            <a href="#" class="btn btn-primary btnSubmit" onclick="ViewAssignClientMessage()">View Assigned Clients</a>
                        </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Select Hotel</label>
                                        <select class="form-control HotelNameSelect2" name="hotels[]" multiple="multiple" required="required">
                                            @if(!empty($hotels))
                                                @foreach($hotels as $hotel)
                                                    <option value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Stay From</label>
                                        <input type="text" name="stay_from[]" class="form-control datepicker" value="{{ old('stay_from') }}" required="required" id="Fromdate0" autocomplete="off" onchange="Checkfromdate(0)">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Stay To</label>
                                        <input type="text" name="stay_to[]" class="form-control datepicker" value="{{ old('stay_to') }}" required="required" id="Todate0" onchange="Checkdate(0)" autocomplete="off">
                                    </div>
                                </div>
                                {{--<div class="col-md-2">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-control" name="status[]" required="required">
                                            <option value="">Select</option>
                                            <option value="upcoming_stay">upcoming_stay</option>
                                            <option value="staying">staying</option>
                                            <option value="left">left</option>
                                        </select>
                                    </div>
                                </div>--}}
                            </div>
                        @endif
                        </div>
                    </div>
                    <div class="modal fade in" id="AssignClientModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Assign Client</h4>
      </div> 
      <div class="modal-body">
        <div class="row">
                    <div class="col-md-12">
                        @foreach($getTripGroup as $group)
                            <div class="panel-group" id="group" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title"> <input type="checkbox" name="Group[]" id="groupCheck{{$group->group->id}}" onclick="GroupCheck('{{$group->group->id}}')" checked="checked"> 
                                    <a data-toggle="collapse" data-parent="#group" href="#group-{{ $loop->iteration }}" aria-expanded="true" aria-controls="group-{{ $loop->iteration }}">
                                     <i class="accordion_icon fa fa-plus"></i> {{$group->group->group_name}} 
                                    </a>
                                  </h4>
                                </div>
                                <div id="group-{{ $loop->iteration }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    <ul class="checkboxesMain checkboxesMainclient">
                                    @foreach($group->clients as $client)
                                    <div class="row">
                                        <div class="col-md-3">
                                            <li><input type="checkbox"  name="mainclientcheckboxes[]" class="mainclientcheckboxes clientcheckbox{{$group->group->id}}" id="cleintbox{{$client->pivot['id']}}" value="{{$client->pivot['id']}}" onclick="Clinetcheckbox('{{$client->pivot['id']}}')" checked="checked">{{$client->name}}</li>
                                        </div>
                                    <div class="col-md-9">
                                        <input type="text" name="Room[]" placeholder="Room#">
                                    </div>
                                    </div>
                                    

                                    @endforeach
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                        @endforeach
                    </div> 
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="BtnAddGroup">Assign Client</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary btnSubmit">Save</button>
                            </div>
                        </div>
                    </form>
        </div>
    </div>
@endsection
@push('js')
    <script>
        var group=JSON.parse("{{$getHotel}}".replace(/&quot;/g,'"'));
    var x=0;
    var a=group.length;
    if(a>0){
        a=a-1;
    }
    x=a;
        $('.selectAllCheckboxes').click(function(){
        if($('.selectAllCheckboxes').prop("checked")==false){
          $('.checkboxSelect').prop("checked", false);
        }
        else{
            $('.checkboxSelect').prop("checked", true);
        }
        });

            $("#btnAddHotel").click(function (){
                x++;
                $(".AppenedArilineRows").append('<div class="remove'+x+'"><input type="hidden" name="hotel_id[]" value="0" id="#txtFlightid0"><div class="col-md-3"><input type="checkbox" name="SelectHotel[]" value="'+x+'" class="checkboxSelect" id="selecthotel'+x+'" checked="checked"></div><div class="col-md-9 text-right"><a href="#" class="btn btn-primary btnSubmit" onclick="ViewAssignClientMessage()">View Assigned Clients</a></div><div class="row"><div class="col-md-4"><div class="form-group"><label>Select Hotel</label><select class="form-control HotelNameSelect2" name="hotels[]" multiple="multiple" required="required">"@if(!empty($hotels))""@foreach($hotels as $hotel)"<option value="{{ $hotel->id }}">{{ $hotel->name }}</option>"@endforeach""@endif"</select></div></div><div class="col-md-3"><div class="form-group"><label>Stay From</label><input type="text" name="stay_from[]" class="form-control datepicker" value="{{ old("stay_from") }}" required="required" id="Fromdate'+x+'" autocomplete="off" onchange="Checkfromdate('+x+')"></div></div><div class="col-md-3"><div class="form-group"><label>Stay To</label><input type="text" name="stay_to[]" class="form-control datepicker" value="{{ old("stay_to") }}" required="required" id="Todate'+x+'" onchange="Checkdate('+x+')" autocomplete="off"></div></div><button type="button" class="btn btn-sm btn-danger remove_field" style="margin: 26px 0 0 15px;"><i class="fa fa-minus" onclick="RemoveAppenedRow('+x+')"></i></button></div></div>');
                $(".HotelNameSelect2").select2({
                  maximumSelectionLength: 1,
                  placeholder: "Name",
                  tags:true
                  });
                $('.datepicker').datepicker({
                autoclose: true,
                 orientation: 'auto bottom',
                 format : 'yyyy-mm-dd'
              });
            });
            function RemoveAppenedRow(id){
            $(".remove"+id+"").empty();
            var c=0;
            for(var i=0;i<=x;i++){
            if($("#selecthotel"+i+"").val()!=undefined){
               $("#selecthotel"+i+"").val(c);
              c++;
            }
           } 
          }
          function RemoveRowFromDB(hotelid){
            $.ajax({
            url: "{{route('trip.hotel.deletehotelbyid')}}",
            data:{_token: $("#token").val(),deletedata:hotelid},
            type: "POST",
            success: function (json) {
                if(json.status==1)
                {
                toastr['success']("Hotel Delete Successfully"); 
                $(".remove"+hotelid+"").empty();
                 var c=0;
                 for(var i=0;i<=x;i++){
                 if($("#selecthotel"+i+"").val()!=undefined){
                 $("#selecthotel"+i+"").val(c);
                c++;
                }
                }  
                }
                else
                {
                    toastr['error']("Hotel Cannot Delete");
                }
               }
        });
            
           } 
           $("#BtnAddGroup").click(function(){
            $("#AssignClientModal").modal('toggle');
         });
$('.datepicker').datepicker({
                autoclose: true,
                 orientation: 'auto bottom',
                 format : 'yyyy-mm-dd'
              });
        
        $(".HotelNameSelect2").select2({
        maximumSelectionLength: 1,
        placeholder: "Name",
        tags:true
        });
    function Checkdate(id){
        var Tripdate=Date.parse("{{$trip->end_date}}");
        var a=Date.parse($("#Todate"+id+"").val());
        var b=Date.parse($("#Fromdate"+id+"").val());
        if(a>Tripdate){
            toastr['error']("Your trip end date"+" "+"{{$trip->end_date}}"+" "+ "please enter correct date");
        }
        else{
        if(a>b){
        return false;
    }
    else{
        toastr['error']("Please check date range");
        $("#Todate"+id+"").val('');
    }    
        }
}

function Checkfromdate(id){
        var Tripdate=Date.parse("{{$trip->end_date}}");
        var a=Date.parse($("#Todate"+id+"").val());
        var b=Date.parse($("#Fromdate"+id+"").val());
        if(b>Tripdate){
            toastr['error']("Your trip end date"+" "+"{{$trip->end_date}}"+" "+ "please enter correct date");
        }
        else{
        return false;    
        }
}

function ViewAssignClientMessage(){
    toastr['error']("There is no client assigned to this hotel");
}

function validate() {
    var array=[];
    $(".checkboxSelect").each( function () {
       if($(this).prop("checked")==true) {
        array.push($(this).val());
       }
   });
    if(array.length>0){
            return true;
         }
         else{
             toastr['error']("Please select at least one flight");
         return false;  
         }
      }
      function GroupCheck(id){
    if($("#groupCheck"+id+"").prop("checked") == true){
        $(".clientcheckbox"+id+"").prop("checked",true);
    }
    else{
    $(".clientcheckbox"+id+"").prop("checked",false);   
    }
}
    </script>
}
@endpush
