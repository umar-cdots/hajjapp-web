@extends('adminlte::trip')
@section('title', 'View Assigned Client')

@section('content_header')
<h1>View Assigned Client</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
	 <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-6">
            </div>
            <form action="{{route('view.trip.hotel.viewHotelTripByCustomerID')}}" method="POST">
                @csrf
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                @method('POST')
            <div class="row">
                    <div class="col-md-9">
                        @foreach($tripsDetail as $clientset)
                        <input type="hidden" name="hotelid" value="{{$clientset->trip_hotel_id}}">
                        @break;
                        @endforeach
                        <div class="form-group" >
                            <select name="client_id" id="ClientSelect2" class="form-control ClientSelect2" multiple="true">
                                <option value="">Select Client</option>
                                @foreach($tripsDetail as $clientset)
                                <option value="{{ $clientset->clienttrip->client->id }}">{{ $clientset->clienttrip->client->name }}</option>
                                @endforeach        
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
            </div>
            </form>
            <div class="col-md-6">
        </div>
            <table id="viewassignedclient" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Client Name</th>
                    <th>Catagory</th>
                    <th>Group</th>
                    <th>Room Number</th>
                    <th>Primary Phone</th>
                    <th>Capacity</th>
                    <th>Emergency Number</th>
                    <th>More Information</th>
                </tr>
                </thead>
                <tbody>
                @foreach($trips as $key=>$trip)
                    <tr>
                    	<td>{{ $trip->clienttrip->client['name'] }}</td>
                    	<td>{{$trip->clienttrip->client->client_category?$trip->clienttrip->client['client_category']['title']:"--"}}</td>
                    	<td>{{ $trip->clienttrip->clienttrip['group']?$trip->clienttrip->clienttrip['group']['group_name']:"--"}}</td>
                        <td>{{ $trip->room_number?$trip->room_number:"--" }}</td>
                    	<td>{{ $trip->clienttrip->client['primary_phone_number']}}</td>
                    	<td>{{ $trip->clienttrip->clienttrip->trip?$trip->clienttrip->clienttrip->trip->capacity:"--" }}</td>
                    	<td>{{ $trip->clienttrip->client['emergency_phone_number']}}</td>
                        <td>
                            <form action="{{ route('trip.hotel.hoteldestroy', $trip->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                            <a href="{{ route('clients.showprofile', ['id' => $trip->clienttrip->client['user_id']]) }}" target="_blank"><button class="btn btn-xs btn-primary btnSubmit" data-toggle="tooltip" title="Profile">View More Information</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#viewassignedclient').DataTable({
                'lengthChange': false,
                "searching":false,
                "columnDefs": [
                 { orderable: false, targets:7 },
                 ],
            })

        });
    $(".ClientSelect2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Client"//placeholder
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    </script>
@endpush
