w@extends('adminlte::page')

@section('title', 'Trip Management')

@section('content_header')
    <h1>Trips Management</h1>
    <div class="clearfix"></div>
    {{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
            </div>
            <form action="{{route('trips.search')}}" method="POST">
                @csrf
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                @method('POST')
                <div class="row">

                    <div class="col-md-9">
                        <div class="form-group" >
                            <select name="trip_id" class="form-control TripSelect" multiple="true">
                                <option value="">Select Trip</option>
                                @if(!empty($tripforselect2))
                                    @foreach($tripforselect2 as $tripset)
                                        <option value="{{ $tripset->id }}">{{ $tripset->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1" style="padding: 0;">
                        <div class="form-group">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <!-- /.box-header -->
        <div class="box-body dataTables_wrapper tripTable">

            <table class="table table-bordered table-striped dataTable no-footer " id="travelAgency" width="100%">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        @if(\Auth::user()->user_type == "super_admin")
                            <th>Travel Agency</th>
                        @endif
                        <th>Trip Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Qibla Widget</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($trips as $trip)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            @if(\Auth::user()->user_type == "super_admin")
                                <td>
                                    <a href="{{ route('travel_agency.show', ['id' => $trip->travel_agency_id]) }}">{{ $trip->travelAgency->agency_name }}</a>
                                </td>
                            @endif
                            <td>{{ $trip->title }}</td>
                            <td>{{ $trip->start_date }}</td>
                            <td>{{ $trip->end_date }}</td>
                            <td>
                                {{-- <button class="btn btn-xs btn-{{ $trip->is_qibla_widget_enable ? 'success' : 'warning' }}"></button> --}}
                                <input type="checkbox" id="changetripstatus" {{ $trip->is_qibla_widget_enable ? 'Checked' : '' }} data-toggle="toggle" onchange="changetripstatus({{$trip->id}})">

                            </td>
                            <td>
                                <input type="checkbox"  id="changetripactive" {{ $trip->end_trip ? 'Checked' : '' }} data-toggle="toggle" onchange="changetripactive({{$trip->id}})">

                            </td>

                            <td>
                            <a href="{{ route('trips.edit', ['id' => $trip->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('trips.destroy', $trip->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                          </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('css')

@endpush

@push('js')
    <script type="text/javascript">
        $(function () {
          $('#travelAgency').DataTable({
            'lengthChange': false,
            "searching":false,
            "columnDefs": [
                 { orderable: false, targets:[4,5,6] },
                 ],
          });
        });
        $(".TripSelect").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Trip"//placeholder
        });
        function changetripstatus(id) {
        $.ajax({
            url: "{{route('trips.ChangeTripQibla')}}",
            data:{_token: $("#token").val(),id:id},
            type: "POST",
            success: function (json) {
               }
        });
        }
        function changetripactive(id) {
        $.ajax({
            url: "{{route('trips.statuscheck')}}",
            data:{_token: $("#token").val(),id:id},
            type: "POST",
            success: function (json) {
                html = '<option>Select Trip</option>';
                        $(json.tripforselect2).each( function (i,d){
                            html += '<option value="'+ d.id +'">'+ d.title +'</option>';
                        });
                        $("#SelectAllTripDetail").html(html);
            }
        });
        }
    </script>
@endpush
