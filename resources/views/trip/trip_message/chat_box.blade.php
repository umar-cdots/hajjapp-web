<div class="box box-warning direct-chat direct-chat-warning">
    <div class="box-header with-border">
        <h3 class="box-title" id="abx">{{$client->name}}</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool collapseBtn" data-widget="collapse"><i
                        class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body collpaseDiv">
        <!-- Conversations are loaded here -->
        <div class="direct-chat-messages" id="chatbox">
            @if(!empty($client_messages))
                @foreach($client_messages as $client_message)
                    <div class="direct-chat-msg {{ ($client_message->client_id != null && $client_message->agent_id != null) ? 'right' : '' }}">
                        <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left">
                                @if($client_message->agent_id != null)
                                   {{ $client_message->agent->name }}
                                @else
                                    {{ $client_message->client->first_name }}
                                @endif
                            </span>
                            <span class="direct-chat-timestamp pull-right">{{ $client_message->created_at->diffForHumans() }}</span>
                        </div>
                        <!-- /.direct-chat-info -->
                        @if($client_message->message_type==null)
                        <img class="direct-chat-img" src="{{ ($client_message->client->picture_path) ? asset('/storage/profile_images/'. $client_message->client->picture_path ) : asset('images/default_user.png') }}" alt="message user image">
                        @else
                        <img class="direct-chat-img" src="{{ ($getAgent->picture_path) ? asset('/storage/profile_images/'. $getAgent->picture_path ) : asset('images/default_user.png') }}" alt="message user image">
                        @endif
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                            {{ $client_message->message }}
                        </div>
                        <!-- /.direct-chat-text -->
                    </div>
                @endforeach
            @endif
        </div>
        <!--/.direct-chat-messages-->
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <form id="message_form" method="POST">
            <input type="hidden" name="client_id" value="{{ $client_id }}">
            <input type="hidden" name="trip_id" value="{{ $trip_id }}">
            <input type="hidden" name="agent_id" value="{{ Auth()->user()->userType->id }}">
            <div class="input-group">
                <input type="text" id="message" name="message" placeholder="Type Message ..." class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-warning btn-flat" id="send_message_button">Send</button>
                </span>
            </div>
        </form>
    </div>
    <!-- /.box-footer-->
</div>

<script>
    $("#message_form").submit( function(e) {
        e.preventDefault();
        var form = $('#message_form')[0];
        var formData = new FormData(form);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $.ajax({
            type: 'POST',
            url: "{{ route('send.message') }}",
            data: formData,
            processData: false,
            contentType: false,
            //dataType: 'html',
            cache: false,
            beforeSend: function() {
                $("#send_message_button").attr('disable', true);
                $(this).val('Sending');
            },
            success: function( response ){
                if(typeof response == 'string'){
                    $("#chatbox").append(response);
                }else{
                    if(response.status == 'fail'){
                        toastr['error'](response.message);
                    }
                }
            },
            complete: function(){
                $("#message").val('');
                $("#send_message_button").attr('disable', false);
                $(this).val('Send');
               // $(".direct-chat-messages").animate({ scrollTop: $(this)[0].scrollHeight}, 1000);
            },
            error: function( error ){
                toastr['error']('Something went wrong');
            }
        });
    });
    $('.collapseBtn').on('click', function(){
        $('.collpaseDiv').slideToggle();
        $(this).find('i').toggleClass("fa-minus fa-plus");
    });
</script>
