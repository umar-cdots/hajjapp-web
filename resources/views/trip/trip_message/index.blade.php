@extends('adminlte::page')
@section('title', 'Client Messages')

@section('content_header')
    <h1>Client Messages</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <form action="{{ route('trip.client.showmessage') }}" method="POST" id="formId">
                      @csrf
                      @method('POST')
                      <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-10">
                          <div class="form-group" >
                            <select name="trip_id" id="TipSelect2" class="form-control TipSelect2" multiple="true">
                                @if(!empty($trips))
                                    @foreach($trips as $trip)
                                        <option value="{{ $trip->id }}">{{ $trip->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 0;">
                        <div class="form-group">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </div>
                     </div>
                    </div>
                    </form>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Trip Clients</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
              @if($trip_clients!=null)
              <div class="box-body direct-chat-messages" style="{{ ($trip_clients->count() > 12) ? 'overflow-y:scroll;overflow-x:hidden;' : '' }}">
              @else
              <div class="box-body direct-chat-messages">
              @endif  
              <div class="row">
                   <div class="col-md-10 col-sm-10 col-xs-10">
                          <div class="form-group" >
                            <select name="client_id" id="ClientSelect2" class="form-control ClientSelect2" multiple="true">
                                @if(!empty($trip_clients))
                                    @foreach($trip_clients as $trip_client)
                                        <option value="{{ $trip_client->client->id }}">{{ $trip_client->client->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-2" style="padding: 0;">
                        <div class="form-group">
                            <button type="button" class="btn" id="btnSearchClient"><i class="fa fa-search"></i></button>
                        </div>
                     </div>
              </div>
                    <ul class="products-list product-list-in-box showAllClient">
                        @if(!empty($trip_clients))
                            @foreach($trip_clients as $trip_client)
                                <a href="javascript:void(0)" onclick="showChat('{{ $trip_client->client_id }}', '{{ $trip_client->clienttrip->trip_id }}')">
                                    <li class="item">
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-xs-3 newMessageAlertParent">
                                                <img src="{{ ($trip_client->client->picture_path != null) ? asset('/storage/profile_images/'.$trip_client->client->picture_path ) : asset('images/default_user.png') }}" width="100%">
                                                @if($client_messages[$trip_client->client->id]!=null)
                                                <span class="newMessageAlert" id="message{{$trip_client->client->id}}"></span>
                                                @endif
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-xs-9 paddingNone">
                                                 {{ $trip_client->client->name }}
                                            </div>
                                        </div>

                                    </li>
                                </a>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <!-- /.box-body -->

            </div>
        </div>
        <div class="col-md-8" id="chat_container">

        </div>
    </div>
@endsection

@push('js')
    <script>
         if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
        $(".TipSelect2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Trip"//placeholder
        });
        $(".ClientSelect2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Client"//placeholder
        });
        function showChat(client_id, trip_id){
            var client_id = client_id;
            var trip_id = trip_id;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('load.client.message') }}",
                data: {client_id: client_id, trip_id: trip_id},
                dataType: 'html',
                cache: true,
                success: function(response){
                    debugger;
                  $("#chat_container").html(response);
                  $("#message"+client_id+"").html('');
                },
                error: function(error){
                    toastr['error']('something went wrong');
                }
            });
        }
        $("#btnSearchClient").click(function(){
         $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('load.client') }}",
                data: {client_id:$("#ClientSelect2").val()==null?0:$("#ClientSelect2").val()},
                success: function(response){
                  if(response.trip_clients.length>0){
                        $(".showAllClient").empty();
                        for(var i=0;i<response.trip_clients.length;i++){
                        $(".showAllClient").append('<a href="javascript:void(0)" onclick="showChat('+response.trip_clients[i]['client_id']+', '+response.trip_clients[i].clienttrip['trip_id']+')"><li class="item"><div class="row"><div class="col-md-2"><img id="imageshow" src="" width="100%"></div><div class="col-md-10"><span class="text-danger" id="message'+response.trip_clients[i]['client_id']+'"></span>'+response.trip_clients[i].client['name']+'</div></div></li></a>');
                        if(response.trip_clients[i].client['picture_path']!=null){
                            $("#imageshow").attr('src', '{{ asset("storage/profile_images/")}}/'+response.trip_clients[i].client['picture_path']+'');
                        }
                        else{
                         $("#imageshow").attr('src', '{{ asset("images/default_user.png")}}');   
                        }
                      }
                    }
                },
                error: function(error){
                    toastr['error']('something went wrong');
                }
            });   
        });


    </script>
@endpush
