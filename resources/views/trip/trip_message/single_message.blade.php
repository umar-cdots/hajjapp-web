<div class="direct-chat-msg {{ ($new_message->client_id != null && $new_message->agent_id != null) ? 'right' : '' }}">
    <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left">
                                @if($new_message->agent_id != null)
                                    {{ $new_message->agent->name }}
                                @else
                                    {{ $new_message->client->first_name }}
                                @endif
                            </span>
        <span class="direct-chat-timestamp pull-right">{{ $new_message->created_at->diffForHumans() }}</span>
    </div>
    <!-- /.direct-chat-info -->
    <img class="direct-chat-img" src="{{ ($getAgent->picture_path != null) ? asset('/storage/profile_images/'.$getAgent->picture_path ) : asset('images/default_user.png') }}" alt="message user image">
    <!-- /.direct-chat-img -->
    <div class="direct-chat-text">
        {{ $new_message->message }}
    </div>
    <!-- /.direct-chat-text -->
</div>