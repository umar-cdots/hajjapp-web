@extends('adminlte::page')
@section('title', 'Trip Step Pack')

@section('content_header')
    <h1>Select Trip Step Pack</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ $trip->title }}</h3>
        </div>
        <form id="load_step_form" method="post">
            @csrf
            <input type="hidden" name="trip_id" value="{{ $trip->id }}">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Step Pack Name</label>
                            <select name="step_pack_id" class="form-control">
                                <option value="">Select Step Pack</option>
                                @foreach($step_packs as $step_pack)
                                    <option value="{{ $step_pack->id }}">{{ $step_pack->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnSubmit">Load Steps</button>
            </div>
        </form>
    </div>

    <form action="{{ route('trip.steppack') }}" method="post" enctype="multipart/form-data">
        <div id="step_container">

        </div>
    </form>

    <div class="modal fade in" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Section Check List</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>List Name</label>
                        <input type="text" class="form-control" name="list" id="list_name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveCheckList">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('js')
    <script>
        $('.content-area').redactor();

        $("#load_step_form").submit( function(e){
            e.preventDefault();
            var form = $('#load_step_form')[0];
            var formData = new FormData(form);
            $.ajax({
              type: 'POST',
              url: "{{ route('steppack.steps') }}",
               processData: false,
               contentType: false,
               cache: false,
              data: formData,
              success: function(response){
                    if(typeof response == 'string'){
                        $("#step_container").html(response);
                    }else{
                        toastr['error'](response.message);
                    }
              },
              error: function(error){
                  toastr['error']('Something went wrong');
              }
            });  
        });

        function openModal(button){
            var section = $(button).attr('id');
            $("#modal-default").modal('show');
            $("#saveCheckList").click( function(e){
                var list_name = $("#list_name").val();
                $(button).closest('div').find('#check-list-'+section).append('<input type="checkbox" value="'+ list_name +'" name="check_list['+ section +'][]">'+ list_name +'');
                $("#modal-default").modal('hide');
                section = '';
            });
        }
    </script>
@endpush