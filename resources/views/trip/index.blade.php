@extends('adminlte::page')

@section('title', 'Trip Management')

@section('content_header')
    <h1>Trip Management</h1>
    <div class="clearfix"></div>
    {{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
@endsection

@section('content')
    {{--<div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">Trip Management</h3>
            </div>
            @if(Auth::user()->user_type == "mobile_agent")
            
            @else
            <div class="col-md-6 text-right">
                <a href="{{ route('trips.create') }}" class="btn btn-primary">Add Trip</a>
            </div>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="box-body dataTables_wrapper tripTable">
            <table class="table table-bordered table-striped dataTable no-footer " id="travelAgency">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        @if(\Auth::user()->user_type == "super_admin")
                            <th>Travel Agency</th>
                        @endif
                        <th>Trip Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Qibla Widget</th>
                        <th>Steps</th>
                        <th>User Management</th>
                        <th>Change Step</th>
                        <th>Hotel Management</th>
                        <th>Messages</th>
                        <th>Flights</th>
                        <th>Tasks</th>
                        <th>Push Notification</th>
                        <th>Guides</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($trips as $trip)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            @if(\Auth::user()->user_type == "super_admin")
                                <td>
                                    <a href="{{ route('travel_agency.show', ['id' => $trip->travel_agency_id]) }}">{{ $trip->travelAgency->agency_name }}</a>
                                </td>
                            @endif
                            <td>{{ $trip->title }}</td>
                            <td>{{ $trip->start_date }}</td>
                            <td>{{ $trip->end_date }}</td>
                            <td>
                                <button class="btn btn-xs btn-{{ $trip->is_qibla_widget_enable ? 'success' : 'warning' }}">{{ $trip->is_qibla_widget_enable ? 'Enabled' : 'Disabled' }}</button>
                            </td>
                            <td>
                                @if(!empty($trip->stepPack))
                                    {{ $trip->stepPack->title }}
                                @else
                                <a href="{{ route('getStepPacks', $trip->id) }}">Use Existing</a> | <a href="{{ route('customStepPack', $trip->id) }}">Create New</a>
                                @endif
                            </td>

                            <td>
                                <a href="{{ route('customer.trip.index', $trip->id) }}"><button class="btn btn-xs btn-primary" title="Add customer to trip"><i class="fa fa-user-plus"></i></button></a>
                                @if($trip->clients->isNotEmpty())
                                    <a href="{{ route('remove.customer.index', $trip->id) }}"><button class="btn btn-xs btn-primary" title="Remove customer from trip"><i class="fa fa-user-times"></i></button></a>
                                @endif
                            </td>
                            <td>
                                @if($trip->clients->isNotEmpty())
                                    <a href="{{ route('view.trip.customer', $trip->id) }}"><button class="btn btn-xs btn-primary" title="Change customer Step"><i class="fa fa-user"></i></button></a>
                                @endif
                            </td>
                            <td>
                                @if($trip->clients->isNotEmpty())
                                    <a href="{{ route('view.trip.customer.hotel', $trip->id) }}"><button class="btn btn-xs btn-primary" title="Add/View trip customer Hotel"><i class="fa fa-hotel"></i></button></a>
                                @endif
                            </td>
                            <td>
                                @if($trip->clients->isNotEmpty())
                                    <a href="{{ route('trip.client.message', $trip->id) }}"><button class="btn btn-xs btn-success" title="Send/View Messages"><i class="fa fa-envelope"></i></button></a>
                                @endif
                            </td>
                            <td>
                                @if($trip->clients->isNotEmpty())
                                <a href="{{ route('trip.flight', $trip->id) }}"><button class="btn btn-xs btn-primary" title="Add/View trip customer flight"><i class="fa fa-plane"></i></button></a>
                                @endif
                            </td>
                            <td>
                               <a href="#"><button class="btn btn-xs btn-primary" title="Add/View trip tasks"><i class="fa fa-tasks"></i></button></a>
                            </td>
                            <td>
                                @if($trip->clients->isNotEmpty())
                                    <a href="{{ route('trip.notification.index', $trip->id) }}"><button class="btn btn-xs btn-primary" title="Add/View trip notification"><i class="fa fa-bell"></i></button></a>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('trip.guide.index', $trip->id) }}"><button class="btn btn-xs btn-primary" title="Add/View Trip Guide"><i class="fa fa-file"></i></button></a>
                            </td>
                            <td>
                                <button class="btn btn-xs btn-{{ $trip->end_trip==1 ? 'success' : 'warning' }}">{{ $trip->end_trip==1 ? 'Active' : 'Deactive' }}</button>
                            </td>

                            <td>
                            <a href="{{ route('trips.edit', ['id' => $trip->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('trips.destroy', $trip->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                                <form action="{{ route('trips.status', $trip->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Status"><i class="fa fa-close"></i></button>
                                </form>

                          </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>--}}

    <div class="box">
            
            
        <!-- /.box-header -->
        <div class="box-body dataTables_wrapper tripTable">
            <form action="{{route('trips.search')}}" method="POST">
                @csrf
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                @method('POST')
                <div class="row">

                    <div class="col-md-8 col-sm-10 col-xs-10">
                        <div class="form-group" >
                            <select name="trip_id" id="SelectAllTripDetail" class="form-control SelectAllTripDetail" multiple="true">
                                <option value="">Select Trip</option>
                                @if(!empty($tripforselect2))
                                    @foreach($tripforselect2 as $tripset)
                                        <option value="{{ $tripset->id }}">{{ $tripset->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-2" style="padding: 0;">
                        <div class="form-group">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    @if(Auth::user()->user_type == "mobile_agent")
            
                    @else
                     <div class="col-md-3 form-group clientBtnAdd text-right">
                        <a href="{{ route('trips.create') }}" class="btn btn-primary btnSubmit">Add Trip</a>
                    </div>
                     @endif
                    
                </div>
            </form>
            <table class="table table-bordered table-striped dataTable no-footer " id="travelAgency" width="100%">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        @if(\Auth::user()->user_type == "super_admin")
                            <th>Travel Agency</th>
                        @endif
                        <th>Trip Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Qibla Widget</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($trips as $trip)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            @if(\Auth::user()->user_type == "super_admin")
                                <td>
                                    <a href="{{ route('travel_agency.show', ['id' => $trip->travel_agency_id]) }}">{{ $trip->travelAgency->agency_name }}</a>
                                </td>
                            @endif
                            <td>{{ $trip->title }}</td>
                            <td>{{ $trip->start_date }}</td>
                            <td>{{ $trip->end_date }}</td>
                            <td>
                                {{-- <button class="btn btn-xs btn-{{ $trip->is_qibla_widget_enable ? 'success' : 'warning' }}"></button> --}}
                                <input type="checkbox"  id="changetripstatus" {{ $trip->is_qibla_widget_enable ? 'Checked' : '' }} data-toggle="toggle" onchange="changetripstatus({{$trip->id}})">

                            </td>

                            <td>
                                <input type="checkbox"  id="changetripactive" {{ $trip->end_trip ? 'Checked' : '' }} data-toggle="toggle" onchange="changetripactive({{$trip->id}})">

                            </td>
                            @if(Auth::user()->user_type == "mobile_agent")
                            <td>
                                <a href="{{route('changestep',['id' => $trip->id])}}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Change Steps">Change Steps</button></a>
                                <a href="{{ route('trip.notification.index', ['id' => $trip->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Push Notification">Push Notification</button></a>
                          </td>
                            @else
                            <td>
                                <a href="{{route('changestep',['id' => $trip->id])}}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Change Steps">Change Steps</button></a>
                                <a href="{{ route('customStepPack', ['id' => $trip->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            <form action="{{ route('trips.destroy', $trip->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                          </td>
                            @endif
                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('css')

@endpush

@push('js')
    <script type="text/javascript">
        $(function () {
          $('#travelAgency').DataTable({
            // 'lengthChange': false,
            'bFilter' : false,
            "columnDefs": [
                 { orderable: false, targets:[4,5,6] },
                 ],
            // "dom": '<"top"p>rt<"bottom"i><"clear">'
          });
          // $('.dataTables_wrapper .top').wrapAll('<div class="row"></div>');
          //   $('.dataTables_wrapper .top').prepend('<div class="col-md-6 addClientBtn"></div>');
          //   $("body").find('.clientBtnAdd').appendTo(".addClientBtn");
          //   $('.dataTables_paginate ').wrapAll('<div class="col-md-6"></div>');
        });
        $(".SelectAllTripDetail").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Trip"//placeholder
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        function changetripstatus(id) {
        $.ajax({
            url: "{{route('trips.ChangeTripQibla')}}",
            data:{_token: $("#token").val(),id:id},
            type: "POST",
            success: function (json) {
               }
        });
        }

        function changetripactive(id) {
        $.ajax({
            url: "{{route('trips.statuscheck')}}",
            data:{_token: $("#token").val(),id:id},
            type: "POST",
            success: function (json) {
                html = '<option>Select Trip</option>';
                        $(json.tripforselect2).each( function (i,d){
                            html += '<option value="'+ d.id +'">'+ d.title +'</option>';
                        });
                        $("#SelectAllTripDetail").html(html);
            }
        });
        }    
        window.localStorage.setItem('Stepid',undefined);
        window.localStorage.setItem('SectionID',undefined);
    </script>
@endpush
