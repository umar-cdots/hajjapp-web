<div class="box">
    <div class="row" style="margin-top:20px">
        @csrf
    <input type="hidden" name="trip_id" value="{{ $trip_id }}">
        <div class="col-md-6">
            <div class="form-group">
                <label>Step Pack Title</label>
                <input type="text" class="form-control" name="step_pack_title" value="{{ $step_pack->title }}">
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group" style="margin:27px 0 0 0;">
                <label>&nbsp;</label>
                <input type="checkbox" name="existing" value="1"> Existing
            </div>
        </div>
{{--        <div class="col-md-2">--}}
{{--            <div class="form-group" style="margin:27px 0 0 0;">--}}
{{--                <label>&nbsp;</label>--}}
{{--                <button class="btn btn-sm btn-primary">Add Step</button>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    @php $a=0; @endphp
    @foreach($step_pack->steps as $step)
        <div class="box-header">
            <h3>{{ 'Step - '. $loop->iteration . ' '. $step->title }}</h3>
            <div class="form-group col-md-4">
                <label> Step Title</label>
                <input type="text" class="form-control" name="step_title[]" value="{{ $step->title }}">
            </div>
        </div>
        <div class="box-body">
            @php $i=0; @endphp
            @foreach($step->stepSection as $section)
                <div class="row">
                    <div class="col-md-12">
                    <h4>Section {{ $loop->iteration }}</h3>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Section Title</label>
                            <input type="text" class="form-control" name="section_title[{{$loop->parent->index}}][]" value="{{ $section->title }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Image</label><br>
                        <input type="file" name="image[{{ $loop->parent->index }}][]">
                        <img src="{{ asset('storage/sections/'. $section->path) }}" alt="section image" style="width: 100px; height: 100px;">
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label >Message</label>
                            <textarea class="form-control content-area" name="message[{{ $loop->parent->index }}][]">{{ $section->description }}</textarea>
                        </div>
                    </div>

                    <div class="col-md-2">
                        {{--<button class="btn btn-primary btn-sm modalbtn" type="button" id="{{ $section->id }}" onclick="openModal(this)"><i class="fa fa-plus"></i></button>--}}
                        <div class="form-group" id="check-list-{{$section->id}}">
                            <label>Check Lists</label><br>
                            @foreach($section->stepCheckLists as $checklist)
                                <input type="checkbox" name="check_list[{{ $a }}][{{ $i }}][]" value="{{ $checklist->description }}" checked>{{ $checklist->description }} <br>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Important Message</label>
                            <textarea class="form-control content-area" name="important_message[{{ $loop->parent->index }}][]">{{ $section->information }}</textarea>
                        </div>
                    </div>
                </div>
                @php $i++; @endphp
            @endforeach
        </div>
        @php $a++; @endphp
    @endforeach

    <div class="box-footer text-center">
        <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
    </div>
</div>

<script>
    $('.content-area').redactor();
</script>

