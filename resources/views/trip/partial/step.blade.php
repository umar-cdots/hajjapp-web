@foreach($step_pack->steps as $step)
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $step->title }}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" onclick="openBox({{ $loop->iteration }}, this)">
                    <i class="fa fa-plus closed"></i>
                    <i class="fa fa-minus opened" style="display: none;"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="box-{{ $loop->iteration }}" style="display: none;">
            <div id="step-container">
                @foreach($step->stepSection as $section)
                    <div id="section-container">
                        <div class="row" id="section-1">
                            <div class="col-md-11">
                                <h3 class="box-title">Section 1</h3>
                            </div>
                            <div>
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Section Title</label>
                                        <p>{{ $section->title }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <p>{!! $section->description !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Image</label>
                                        <img src="{{asset('storage/sections/'.$section->path)}}" style="width: 100px; height: 100px;">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Information</label>
                                        <p>{!! $section->information !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Checklist</label>
                                    {{--                                    <button class="btn btn-primary btn-sm modalbtn" type="button" id="0" onclick="openModal(this)"><i class="fa fa-plus"></i></button>--}}
                                    <div class="form-group check-list">
                                        @foreach($section->stepCheckLists as $check_list)
                                             {{ $check_list->description }}
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@endforeach
