@extends('adminlte::page')

@section('title', 'Client Category')

@section('content_header')
<h1>Upload Client Category</h1>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            {{-- <h3 class="box-title">Add Client Category</h3> --}}
        </div>
        <form action="{{ route('load.excel.store') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Trip Name</label>
                        <input type="file" name="file" class="form-control">

                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
            </div>
        </form>
    </div>

@endsection



@push('js')

@endpush