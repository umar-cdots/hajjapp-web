@extends('adminlte::tasks')
@section('title', 'Add Tasks')

@section('content_header')
{{-- <a href="{{route('trip.task.index')}}" class="btn btn-primary">Back</a> --}}
    <ol class="breadcrumb">
       {{--  <li><a href="{{route('trip.task.index')}}">Task Management</a></li>
        <li class="active">Add Task</li> --}}
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
    <div class="box">
        <div class="box-body">
                     <form action="{{ route('trip.task.store') }}" method="post">
                         @csrf
                         <div id="task_container">

                             <div class="row">
                                 <div class="col-md-4">
                                     <div class="form-group" {{ $errors->has('task_title') ? 'has-error' : '' }}>
                                         <label>Task Name <span>*</span></label>
                                         <input type="text" name="task_title" class="form-control" placeholder="Task Name" value="{{old('task_title')}}">
                                         @if($errors->has('task_title'))
                                             <span class="text-danger">{{ $errors->first('task_title') }}</span>
                                         @endif
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                     <div class="form-group" {{ $errors->has('staff_id') ? 'has-error' : '' }}>
                                         <label>Assign To</label>
                                         <select name="staff_id" class="form-control select2 staff_id" multiple="multiple">
                                             @if(!empty($users))
                                                 @foreach($users as $user)
                                                     @if($user->staff)
                                                         <option value="{{ $user->staff->id }}" {{ ( old('staff_id')==$user->staff->id ) ? 'selected' : '' }}>{{ $user->staff->name }}</option>
                                                     @endif
                                                 @endforeach
                                             @endif
                                             @if($errors->has('staff_id'))
                                                 <span class="help-block text-danger">{{ $errors->first('staff_id') }}</span>
                                             @endif
                                         </select>
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                     <div class="form-group" >
                                         <label>Trip Name</label>
                                         <select name="trip_id" class="form-control select2 trip_id" multiple="multiple">
                                             @if(!empty($trip))
                                                 @foreach($trip as $trips)
                                                     <option value="{{ $trips->id }}" {{ ( old('trip_id')==$trips->id ) ? 'selected' : '' }} >{{ $trips->title }}</option>
                                                 @endforeach
                                             @endif
                                         </select>
                                     </div>
                                 </div>
                             </div>
                             <div class="row">
                                 <div class="col-md-4">
                                     <div class="form-group" {{ $errors->has('start_date') ? 'has-error' : '' }}>
                                         <label>Start Date <span>*</span></label>
                                         <input type="text" name="start_date" class="form-control datepicker" placeholder="Calendar and time field" value="{{old('start_date')}}">
                                         @if($errors->has('start_date'))
                                             <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                         @endif
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                     <div class="form-group" {{ $errors->has('due_date') ? 'has-error' : '' }}>
                                         <label>End Date <span>*</span></label>
                                         <input type="text" name="due_date" class="form-control datepicker" placeholder="Calendar and time field" value="{{old('due_date')}}">
                                         @if($errors->has('due_date'))
                                             <span class="text-danger">{{ $errors->first('due_date') }}</span>
                                         @endif
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                     <div class="form-group">
                                         <label>Status</label>
                                         <select name="status" class="form-control status" multiple="multiple">
                                             <option value="">Select</option>
                                             <option value="Inprogress" {{ ( old('status')=="Inprogress" ) ? 'selected' : '' }}>InProgress</option>
                                             <option value="Upcoming" {{ ( old('status')=="Upcoming" ) ? 'selected' : '' }}>Upcoming</option>
                                         </select>
                                     </div>
                                 </div>
                             </div>
                             <div class="row">
                                 <div class="col-md-12">
                                     <div class="form-group">
                                         <label>Task Description</label>
                                         <textarea class="form-control" name="description" rows="5" placeholder="Task Description">{{old('description')}}</textarea>
                                     </div>
                                 </div>
                             </div>
                         </div>

             <div class="row">
                 <div class="col-md-12 text-center">
                     <button class="btn btn-primary btnSubmit">Create Task</button>
                 </div>
             </div>
         </form>
     </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">


        $('.timepicker').timepicker({
            showInputs: false,
        });

        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper         = $("#task_container"); //Fields wrapper
            var add_button      = $("#add_section"); //Add button ID

            var x = 0; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row" id="sub_parent'+ x +'">\n' +
                        '                                <div class="col-md-3">\n' +
                        '                                    <div class="form-group">\n' +
                        '                                        <label>Task Title</label>\n' +
                        '                                        <input type="text" name="name[]" class="form-control">\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="col-md-2">\n' +
                        '                                    <div class="form-group">\n' +
                        '                                        <label>Staff</label>\n' +
                        '                                        <select name="staff[]" class="form-control">\n' +
                        '                                               <option value="" selected>Select</option>' +
                        @if(!empty($staffs))
                            @foreach($staffs as $staff)
                            '<option value="{{ $staff->staff->id }}">{{ $staff->staff->name }}</option>\n'+
                        @endforeach
                            @endif
                            '                                        </select>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="col-md-2">\n' +
                        '                                    <div class="form-group">\n' +
                        '                                        <label>Status</label>\n' +
                        '                                        <select name="status[]" class="form-control">\n' +
                        '                                            <option value="">Select</option>\n' +
                        '                                            <option value="upcoming">upcoming</option>\n' +
                        '                                        </select>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="col-md-2">\n' +
                        '                                    <div class="form-group">\n' +
                        '                                        <label>Due Date</label>\n' +
                        '                                        <input type="text" name="due_date[]" class="form-control datepicker">\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="col-md-2 bootstrap-timepicker">\n' +
                        '                                    <div class="form-group">\n' +
                        '                                        <label>Due Time</label>\n' +
                        '                                        <input type="text" name="due_time[]" class="form-control timepicker">\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="col-md-1">\n' +
                        '                                    <label>&nbsp;</label>\n' +
                        '                                    <div class="form-group">\n' +
                        '                                        <label>&nbsp;</label>\n' +
                        '                                        <button type="button" class="btn btn-sm btn-danger remove_field"><i class="fa fa-minus"></i></button>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="col-md-11">\n' +
                        '                                    <div class="form-group">\n' +
                        '                                        <label>Description</label>\n' +
                        '                                        <textarea class="form-control" name="description[]" rows="5"></textarea>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                            </div>'); //add input box
                    $('.timepicker').timepicker({
                        showInputs: false
                    });
                }

            });

            $(wrapper).on("click",".remove_field", function(e){
                e.preventDefault(); $(this).parent('div').remove(); $("#sub_parent"+x).remove(); x--;
            })
        });
        $('.datepicker').datetimepicker();
        $(".trip_id").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Trip"//placeholder
        });
        $(".staff_id").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Staff"//placeholder
        });
        $(".status").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Status"//placeholder
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    </script>
@endpush
