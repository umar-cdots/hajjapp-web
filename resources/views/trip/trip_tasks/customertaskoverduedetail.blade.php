@extends('adminlte::page')
@section('title', 'Trip Tasks')

@section('content_header')
    <h1>Task Management</h1>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
        <table class="table table-bordered table-striped dataTable no-footer" id="trips">
            <thead>
            <tr>
                <th>Task Name</th>
                <th>Trip</th>
                <th>Assign To </th>
                <th>Due Date</th>
                <th>Status</th>
                <th>Actual Date</th>
                <th>Description</th>
                <th>Comments</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($totalduetask))
                @foreach($totalduetask as $key =>$duetask)
                @if(!$duetask->trip->triptasks->isEmpty())
                @if(!$duetask->trip->triptasks->isEmpty())
                    <tr>
                        <td>{{ $duetask->trip->triptasks[$key]["task_title"]?$duetask->trip->triptasks[$key]["task_title"]:"--" }}</td>
                        <td>{{ $duetask->trip->title }}</td>
                        <td>{{ $duetask->trip->triptasks[$key]['staff']?$duetask->trip->triptasks[$key]['staff']["name"]:"--" }}</td>
                        <td>{{ $duetask->trip->triptasks[$key]["due_date"]?$duetask->trip->triptasks[$key]["due_date"]:"--" }}</td>
                        <td>{{ $duetask->trip->triptasks[$key]["status"]?$duetask->trip->triptasks[$key]["status"]:"--" }}</td>
                        <td>{{ $duetask->trip->triptasks[$key]["actual_date"]?$duetask->trip->triptasks[$key]["actual_date"]:"--" }}</td>
                        <td>{{ $duetask->trip->triptasks[$key]["description"]?$duetask->trip->triptasks[$key]["description"]:"--" }} </td>
                        <td>{{ $duetask->trip->triptasks[$key]["comment"]?$duetask->trip->triptasks[$key]["comment"]:"--" }}</td>
                        <td>
                                <a href="{{ route('trip.task.edit', $duetask->trip->triptasks[$key]['id']) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                                <form action="{{ route('trip.task.destroy', $duetask->trip->triptasks[$key]['id']) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                                </form>
                            </td>
                    </tr>
                    @endif
                    @endif
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
$('.timepicker').timepicker({
 showInputs: false,
});
$(function () {
      $('#trips').DataTable({
        'lengthChange': false,
        "columnDefs": [
                 { orderable: false, targets:8 },
                 ],
      });
    });
$(document).ready(function() {
var max_fields      = 10; //maximum input boxes allowed
var wrapper         = $("#task_container"); //Fields wrapper
var add_button      = $("#add_section"); //Add button ID

var x = 0; //initlal text box count
$(add_button).click(function(e){ //on add input button click
e.preventDefault();
if(x < max_fields){ //max input box allowed
    x++; //text box increment
    $(wrapper).append('<div class="row" id="sub_parent'+ x +'">\n' +
        '                                <div class="col-md-3">\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label>Task Title</label>\n' +
        '                                        <input type="text" name="name[]" class="form-control">\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                                <div class="col-md-2">\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label>Staff</label>\n' +
        '                                        <select name="staff[]" class="form-control">\n' +
        '                                               <option value="" selected>Select</option>' +
                                                        @if(!empty($staffs))
                                                        @foreach($staffs as $staff)
                                                             '<option value="{{ $staff->staff->id }}">{{ $staff->staff->name }}</option>\n'+
                                                        @endforeach
                                                        @endif
        '                                        </select>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                                <div class="col-md-2">\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label>Status</label>\n' +
        '                                        <select name="status[]" class="form-control">\n' +
        '                                            <option value="">Select</option>\n' +
        '                                            <option value="upcoming">upcoming</option>\n' +
        '                                        </select>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                                <div class="col-md-2">\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label>Due Date</label>\n' +
        '                                        <input type="text" name="due_date[]" class="form-control datepicker">\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                                <div class="col-md-2 bootstrap-timepicker">\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label>Due Time</label>\n' +
        '                                        <input type="text" name="due_time[]" class="form-control timepicker">\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                                <div class="col-md-1">\n' +
        '                                    <label>&nbsp;</label>\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label>&nbsp;</label>\n' +
        '                                        <button type="button" class="btn btn-sm btn-danger remove_field"><i class="fa fa-minus"></i></button>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                                <div class="col-md-11">\n' +
        '                                    <div class="form-group">\n' +
        '                                        <label>Description</label>\n' +
        '                                        <textarea class="form-control" name="description[]" rows="5"></textarea>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>'); //add input box
        $('.timepicker').timepicker({
            showInputs: false
        });
}

});

$(wrapper).on("click",".remove_field", function(e){
e.preventDefault(); $(this).parent('div').remove(); $("#sub_parent"+x).remove(); x--;
})
});
$('.datepicker').datepicker({
    autoclose: true,
     orientation: 'auto bottom',
     format : 'yyyy/mm/dd'
  });
</script>
@endpush
