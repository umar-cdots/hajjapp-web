@extends('adminlte::page')
@section('title', 'Show Trip')

@section('content_header')
<h1>Trip Details</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
  <div class="box">
            <div class="box-header">
             
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
               <tbody>
                <tr>
                  <th>Travel Agency</th>
                  <td>{{ $trip->travelAgency->agency_name }}</td>
                </tr>
                <tr>
                  <th>Trip Name</th>
                  <td>{{ $trip->title }}</td>
                </tr>
                <tr>
                  <th>Start Date</th>
                  <td>{{ $trip->start_date }}</td>
                </tr>
                <tr>
                  <th>End Date</th>
                  <td>{{ $trip->end_date }}</td>
                </tr>
                <tr>
                  <th>Qibla Widget</th>
                  <td><button class="btn btn-xs btn-{{ $trip->is_qibla_widget_enable ? 'success' : 'warning' }}">{{ $trip->is_qibla_widget_enable ? 'Enabled' : 'Disabled' }}</button></td>
                </tr>
                <tr>
                  <th>Step Pack</th>
                  <td>{{ $trip->stepPack->title }}</td>
                </tr>
                <tr>
                  <th>Image</th>
                  <td><img src="{{ $trip->path }}" alt="{{ $trip->title }}" width="200"></td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection