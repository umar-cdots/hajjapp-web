@extends('adminlte::trip')

@section('title', 'Add Trip')

@section('content_header')
    {{-- <a href="{{route('trips.index')}}" class="btn btn-primary btnSubmit">Back</a> --}}
    <ol class="breadcrumb">
        {{-- <li><a href="{{route('trips.index')}}">Trip Management</a></li>
        <li class="active">Add Trip</li> --}}
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <form action="{{ route('trips.store') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">

                <div class="row">
                    <div class="col-md-3 tripImage">
                        <div class="form-group {{ $errors->has('profile_img') ? 'has-error' : '' }}">
                            <label>Trip Image <span>*</span></label>
                            <input type="file" name="profile_img" id="imgpreview" required>
                            <label><img id="viewImage" src="{{ asset('images/upload.jpg') }}" alt="Upload Image"
                                        height="178"></label>
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('profile_img') }}</span>
                            @endif
                            {{-- <input type="file" name="profile_img" class="propic" id="imgpreview">
                            <label for="file"><img id="viewImage" src="http://placehold.it/100x100" alt="Upload Image" height="178"></label> --}}
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                    <label>Trip Name <span>*</span></label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter Trip Name"
                                           value="{{ old('title') }}" maxlength="30">
                                    @if($errors->has('title'))
                                        <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('capacity') ? 'has-error' : '' }}">
                                    <label>Capacity <span>*</span></label>
                                    <input type="number" name="capacity" class="form-control" min="1"
                                           placeholder="Enter Capacity" value="{{old('capacity')}}">
                                    @if($errors->has('capacity'))
                                        <span class="help-block text-danger">{{ $errors->first('capacity') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('guide_qouta') ? 'has-error' : '' }}">
                                    <label>Guide Quota<span>*</span></label>
                                    <input type="number" name="guide_qouta" class="form-control" min="1"
                                           placeholder="Enter Guide Quota" value="{{old('guide_qouta')}}">
                                    @if($errors->has('guide_qouta'))
                                        <span class="help-block text-danger">{{ $errors->first('guide_qouta') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                                    <label>Start Date <span>*</span></label>
                                    <input type="text" name="start_date" class="form-control datepicker"
                                           placeholder="Enter Start Date" value="{{ old('start_date') }}"
                                           autocomplete="off">
                                    @if($errors->has('start_date'))
                                        <span class="help-block text-danger">{{ $errors->first('start_date') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                                    <label>End Date <span>*</span></label>
                                    <input type="text" name="end_date" class="form-control datepicker"
                                           placeholder="Enter End Date" value="{{ old('end_date') }}"
                                           autocomplete="off">
                                    @if($errors->has('end_date'))
                                        <span class="help-block text-danger">{{ $errors->first('end_date') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('app_menu') ? 'has-error' : '' }}">
                                    <label>App Menu Hide</label>
                                    <input type="checkbox" name="app_menu">
                                    @if($errors->has('app_menu'))
                                        <span class="help-block text-danger">{{ $errors->first('app_menu') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('active_trip') ? 'has-error' : '' }}">
                                    <label>Active Trip</label>
                                    <input type="checkbox" name="active_trip">
                                    @if($errors->has('active_trip'))
                                        <span class="help-block text-danger">{{ $errors->first('active_trip') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('dates_check') ? 'has-error' : '' }}">
                                    <label>App Dates Hide</label>
                                    <input type="checkbox" name="dates_check">
                                    @if($errors->has('dates_check'))
                                        <span class="help-block text-danger">{{ $errors->first('dates_check') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                {{-- <div class="col-md-12">
                  <div class="col-md-3">
                    <div class="form-group">
                    <a href="#" class="btn btn-primary btn-block btnSubmit" disabled>Assign Client</a>
                  </div>
                  </div>
                  <div class="col-md-3">
                   <div class="form-group">
                    <button class="btn btn-primary btn-block btnSubmit" disabled="disabled" type="button">View Assigned Clients</button>
                  </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                    <button class="btn btn-primary btn-block btnSubmit" disabled="disabled" type="button">Create Group</button>
                  </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                    <button class="btn btn-primary btn-block btnSubmit btnInTrip" disabled="disabled" type="button">View Groups</button>
                  </div>
                  </div>
                </div> --}}
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="btn-block">Trip Description</label>
                        <textarea class="form-control" name="description" rows="5"
                                  maxlength="75">{{old('description')}}</textarea>
                    </div>
                </div>
                @if(\Auth::user()->user_type == "super_admin")
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('travel_agency_id') ? 'has-error' : '' }}">
                            <label>Select Travel Agency</label>
                            <select id="travel_agency_id" name="travel_agency_id" class="form-control">
                                <option>Select Travel Agency</option>
                                @foreach($travel_agencies as $travel_agency)
                                    <option value="{{ $travel_agency->id }}">{{ $travel_agency->agency_name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('travel_agency_id'))
                                <span class="help-block text-danger">{{ $errors->first('travel_agency_id') }}</span>
                            @endif
                        </div>
                    </div>
                @endif

                {{--	  @if(\Auth::user()->user_type == "agent" || \Auth::user()->user_type == "staff")--}}
                {{--		  <div class="col-md-4">--}}
                {{--			<div class="form-group {{ $errors->has('step_pack_id') ? 'has-error' : '' }}">--}}
                {{--			  <label>Steps Pack</label>--}}
                {{--			  <select id="step_pack_id" name="step_pack_id" class="form-control">--}}
                {{--					<option>Select Steps Pack</option>--}}
                {{--						@foreach($step_packs as $step_pack)--}}
                {{--							<option value="{{ $step_pack->id }}">{{ $step_pack->title }}</option>--}}
                {{--					  @endforeach--}}
                {{--				</select>--}}
                {{--				@if($errors->has('step_pack_id'))--}}
                {{--				<span class="help-block text-danger">{{ $errors->first('step_pack_id') }}</span>--}}
                {{--			  @endif--}}
                {{--			</div>--}}
                {{--		  </div>--}}
                {{--	  @endif--}}
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create Trip</button>
            </div>
        </form>
    </div>

@endsection



@push('js')
    <script type="text/javascript">


        function TripImageUpload(input) {

            if ($("#imgpreview").prop("files")[0]) {
                switch ($("#imgpreview").val().substring($("#imgpreview").val().lastIndexOf('.') + 1).toLowerCase()) {
                    case 'gif':
                    case 'jpg':
                    case 'png':

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#viewImage').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                        break;
                    default:
                        $("#imgpreview").val('');
                        toastr['error']("File format not correct!");
                        break;
                }
            }

            // if (input.files && input.files[0]) {
            //     var reader = new FileReader();
            //     reader.onload = function (e) {
            //         $('#viewImage').attr('src', e.target.result);
            //     }
            //     reader.readAsDataURL(input.files[0]);
            // }
        }

        $("#imgpreview").change(function () {
            TripImageUpload(this);
        });

        $('select#travel_agency_id').change(function () {
            var travel_agency_id = $(this).val();
            $.ajax({
                url: "{{ route('ajax.loadStepPacks', ['id' => "__TRAVEL_AGENCY_ID__"]) }}".replace("__TRAVEL_AGENCY_ID__", travel_agency_id),
                data: "travel_agency_id=" + travel_agency_id,
                cache: false,
                dataType: "json",
                success: function (response) {
                    var html = "<option>Select Steps Pack</option>";
                    $(response).each(function (index, step_pack) {
                        html += "<option value='" + step_pack.id + "'>" + step_pack.title + "</option>";
                    });
                    $('select#step_pack_id').html(html);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            })
        });
        $('.datepicker').datepicker({
            autoclose: true,
            orientation: 'auto bottom',
            format: 'yyyy/mm/dd'
        });
    </script>
@endpush
