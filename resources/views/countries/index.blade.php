@extends('adminlte::page')
@section('title', 'Countries')

@section('content_header')
<h1>Countries</h1>
{{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection

@section('content')
	<div class="box">
		<div class="box-body travelAgencyTableBtn">
       <div class="col-md-12 text-right">
        <a href="{{ route('countries.create') }}" class="btn btn-primary btnSubmit">Add Countries</a>
      </div>
      <div class="clearfix"></div>
			<table id="countries" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th>Sr#</th>
          	<th>Country Name</th>
            <th>Abbreviation</th>
            <th>Phone Code</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($countries as $country)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $country->name }}</td>
          <td>{{ $country->short_name }}</td>
          <td>{{ $country->phone_code ?? "--" }}</td>
          <td>
          	<a href="{{ route('countries.edit', $country->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
          	<form action="{{ route('countries.destroy', $country->id) }}" method="POST" class="delete_item">
          		@csrf
          		@method('DELETE')
          		<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
          	</form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
		</div>
	</div>
@endsection
@push('js')
  <script type="text/javascript">
    $(function () {
      $('#countries').DataTable({
      	'lengthChange': true,
        "searching": false,
        "columnDefs": [
                 { orderable: false, targets: 4}
                 ],
      });
    });
  </script>
@endpush
