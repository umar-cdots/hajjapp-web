@extends('adminlte::page')
@section('title', 'Edit Country')
@section('content_header')
<h1>Edit Country</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
	<div class="box">
		<form action="{{ route('countries.update',$country->id) }}" method="POST">
			@csrf
			@method('PUT')
		<div class="box-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>Name <span>*</span></label>
						<input type="text" name="name" class="form-control" value="{{ $country->name }}">
						@if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                        @endif
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('short_name') ? 'has-error' : '' }}">
						<label>abbreviation <span>*</span></label>
						<input type="text" name="short_name" class="form-control" value="{{ $country->short_name }}">
						@if($errors->has('short_name'))
                                <span class="help-block text-danger">{{ $errors->first('short_name') }}</span>
                        @endif
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group {{ $errors->has('phone_code') ? 'has-error' : '' }}">
						<label>Phone Code </label>
						<input type="text" name="phone_code" class="form-control" value="{{ $country->phone_code }}">
						@if($errors->has('phone_code'))
                                <span class="help-block text-danger">{{ $errors->first('phone_code') }}</span>
                        @endif
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer text-center">
			<button type="submit" class="btn btn-primary btnSubmit">Update</button>
		</div>
		</form>
	</div>
@endsection

@push('js')
<script type="text/javascript">
</script>
@endpush
