@extends('adminlte::page')
@section('title', 'Airline Details')

@section('content_header')
<h1>Airline Details</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection


@section('content')
  <div class="box">
            <div class="box-header">
              <a href="{{ route('airlines.create') }}" class="btn btn-primary">Add Airline</a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
               <tbody>
                <tr>
                  <th>Travel Agency</th>
                  <td>{{ $airline->travelAgency->agency_name }}</td>
                </tr>
                <tr>
                  <th>Name</th>
                  <td>{{ $airline->name }}</td>
                </tr>
                <tr>
                  <th>Country</th>
                  <td>{{ $airline->country->name }}</td>
                </tr>
                <tr>
                  <th>State</th>
                  <td>{{ $airline->state->name }}</td>
                </tr>
                <tr>
                  <th>City</th>
                  <td>{{ $airline->city->name  }}</td>
                </tr>
                <tr>
                  <th>Address</th>
                  <td>{{ $airline->address }}</td>
                </tr>
                <tr>
                  <th>Phone Number</th>
                  <td>{{ $airline->phone_number }}</td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection