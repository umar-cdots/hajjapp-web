@extends('adminlte::page')
@section('title', 'Edit Airline')
@section('content_header')
<h1>Edit Airline</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection
@section('content')
	<div class="box">
		<form action="{{ route('airlines.update', $airline->id) }}" method="POST">
			@csrf
			@method('PUT')
		<div class="box-body">
			<div class="row">
{{--				<div class="col-md-3">--}}
{{--					<div class="form-group {{ $errors->has('travel_agency') ? 'has-error' : '' }}">--}}
{{--						<label>Travel Agency <span>*</span></label>--}}
{{--						<select class="form-control" name="travel_agency">--}}
{{--							@foreach($agencies as $agency)--}}
{{--								<option value="{{ $agency->id }}">{{ $agency->agency_name  }}</option>--}}
{{--							@endforeach--}}
{{--						</select>--}}
{{--						@if($errors->has('travel_agency'))--}}
{{--                                <span class="help-block text-danger">{{ $errors->first('travel_agency') }}</span>--}}
{{--                        @endif--}}
{{--					</div>--}}
{{--				</div>--}}
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>Name <span>*</span></label>
						<input type="text" name="name" class="form-control" value="{{ $airline->name }}">
						@if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                        @endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
						<label>Country <span>*</span></label>
						<select class="form-control country" name="country" id="country" multiple="multiple">
							@foreach($countries as $country)
								<option value="{{ $country->id }}" {{ ($airline->country_id == $country->id ? 'selected' : '') }}>{{ $country->name }}</option>
							@endforeach
						</select>
						@if($errors->has('country'))
                                <span class="help-block text-danger">{{ $errors->first('country') }}</span>
                        @endif
					</div>
				</div>
{{--				<div class="col-md-3">--}}
{{--					<div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">--}}
{{--						<label>State <span>*</span></label>--}}
{{--						<select class="form-control" name="state" id="state">--}}
{{--							@foreach($states as $state)--}}
{{--								<option value="{{ $state->id }}" {{ ($airline->state_id == $state->id ? 'selected' : '') }}>{{ $state->name }}</option>--}}
{{--							@endforeach--}}
{{--						</select>--}}
{{--						@if($errors->has('state'))--}}
{{--                                <span class="help-block text-danger">{{ $errors->first('state') }}</span>--}}
{{--                        @endif--}}
{{--					</div>--}}
{{--				</div>--}}
{{--				<div class="col-md-3">--}}
{{--					<div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">--}}
{{--						<label>Cities <span>*</span></label>--}}
{{--						<select class="form-control" name="city" id="city">--}}
{{--							@foreach($cities as $city)--}}
{{--								<option value="{{ $city->id }}" {{ ($airline->city_id == $city->id ? 'selected' : '') }}>{{ $city->name }}</option>--}}
{{--							@endforeach--}}
{{--						</select>--}}
{{--						@if($errors->has('city'))--}}
{{--                                <span class="help-block text-danger">{{ $errors->first('city') }}</span>--}}
{{--                        @endif--}}
{{--					</div>--}}
{{--				</div>--}}
{{--				<div class="col-md-3">--}}
{{--					<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">--}}
{{--						<label>Address <span>*</span></label>--}}
{{--						<input type="text" name="address" class="form-control" value="{{ $airline->address }}">--}}
{{--						@if($errors->has('city'))--}}
{{--                                <span class="help-block text-danger">{{ $errors->first('city') }}</span>--}}
{{--                        @endif--}}
{{--					</div>--}}
{{--				</div>--}}
{{--				<div class="col-md-3">--}}
{{--					<div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">--}}
{{--						<label>Phone No. <span>*</span></label>--}}
{{--						<input type="text" name="phone_number" class="form-control" value="{{ $airline->phone_number }}">--}}
{{--						@if($errors->has('phone_number'))--}}
{{--                                <span class="help-block text-danger">{{ $errors->first('phone_number') }}</span>--}}
{{--                        @endif--}}
{{--					</div>--}}
{{--				</div>--}}
			</div>
		</div>
		<div class="box-footer text-center">
			<button type="submit" class="btn btn-primary btnSubmit">Update</button>
		</div>
		</form>
	</div>
@endsection
@push('js')
<script type="text/javascript">
	$(".country").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Country"//placeholder
   });
	$('#country').change(function () {
            var country_id = $(this).val();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
			});
			$.ajax({
				url : "{{ route('ajax.load.states') }}",
				type: "POST",
				dataType: 'JSON',
				data: { country_id: country_id},
				success: function(response){
					var states = '';
					if(response.status == 'success'){
						console.log(response);
						$(response.states).each( function(i,d){
							states += '<option value="'+ d.id +'">'+ d.name +'</option>';
						});

						$("#state").html(states);
					}else{
						toastr['error']('Something went wrong.');
					}
				},
				error: function(error){
					toastr['error']('Something went wrong.');
				}
			});
        });

	$('#state').change(function () {
		var state_id = $(this).val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			}
		});
		$.ajax({
			url : "{{ route('ajax.load.cities') }}",
			type: "POST",
			dataType: 'JSON',
			data: { state_id: state_id},
			success: function(response){
				console.log(response);
				var cities = '';
				if(response.status == 'success'){
					$(response.cities).each( function(i,d){
						cities += '<option value="'+ d.id +'">'+ d.name +'</option>';
					});

					$("#city").html(cities);
				}else{
					toastr['error']('Something went wrong.');
				}
			},
			error: function(error){
				toastr['error']('Something went wrong.');
			}
		});
	});
</script>
@endpush
