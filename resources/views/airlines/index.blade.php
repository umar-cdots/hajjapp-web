@extends('adminlte::page')
@section('title', 'Airlines')

@section('content_header')
<h1>Airlines</h1>
{{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection

@section('content')
	<div class="box">
		<div class="box-body travelAgencyTableBtn">
      <div class="col-md-12 text-right">
        <a href="{{ route('airlines.create') }}" class="btn btn-primary btnSubmit">Add Airline</a>
      </div>
      <div class="clearfix"></div>
			<table id="airlines" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th>Sr#</th>
          	<th>Name</th>
          	<th>Country</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($airlines as $airline)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $airline->name }}</td>
          <td>{{ $airline->country->name ?? '--' }}</td>
          <td>
          	<a href="{{ route('airlines.edit', $airline->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
          	{{-- <a href="{{ route('airlines.show', $airline->id) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Step"><i class="fa fa-eye"></i></button></a> --}}
          	<form action="{{ route('airlines.destroy', $airline->id) }}" method="POST" class="delete_item">
          		@csrf
          		@method('DELETE')
          		<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
          	</form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
		</div>
	</div>
@endsection
@push('js')
  <script type="text/javascript">
    $(function () {
      $('#airlines').DataTable({
      	'lengthChange': true,
        'searching': false,
        "columnDefs": [
                 { orderable: false, targets: 3}
                 ],
      });
    });
  </script>
@endpush
