@extends('adminlte::page')

@section('title', 'Edit Client Category')

@section('content_header')
<h1>Edit Client Category</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            {{-- <h3 class="box-title">Edit Client Category</h3> --}}
        </div>
        <form action="#" method="POST" role="form" enctype="multipart/form-data">
            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label>Trip Name</label>
                        <input type="text" name="title" class="form-control" placeholder="Enter Client Category" value="{{ $client_category->title }}" disabled="true">
                        @if($errors->has('title'))
                            <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection



@push('js')

@endpush