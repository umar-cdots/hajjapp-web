@extends('adminlte::page')

@section('title', 'Add Client Category')

@section('content_header')
<h1>Add Client Category</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <form action="{{ route('client_categories.store') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label>Category Name <span>*</span></label>
                        <input type="text" name="title" class="form-control" placeholder="Enter Client Category" value="{{ old('title') }}">
                        @if($errors->has('title'))
                            <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create</button>
            </div>
        </form>
    </div>

@endsection



@push('js')

@endpush