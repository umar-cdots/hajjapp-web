@extends('adminlte::page')
@section('title', 'Client Categories')

@section('content_header')
    <h1>Client Categories</h1>
   {{--  <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection


@section('content')
    <div class="box">
        <div class="box-body travelAgencyTableBtn">
            <div class="col-md-12 text-right">
                <a href="{{ route('client_categories.create') }}" class="btn btn-primary btnSubmit">Add Category</a>
            </div>
            <div class="clearfix"></div>
            <table id="client_categories" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($client_categories as $client_category)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $client_category->title }}</td>
                        <td>
                            <a href="{{ route('client_categories.edit', ['id' => $client_category->id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                           {{--  <a href="{{ route('client_categories.show', ['id' => $client_category->id]) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button></a> --}}
                            <form action="{{ route('client_categories.destroy', ['id' => $client_category->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client_categories').DataTable({
                'lengthChange': true,
                'searching' : false,
                "columnDefs": [
                 { orderable: false, targets: 2}
                 ],
            })

        });
    </script>
@endpush
