@extends('adminlte::page')

@section('title', 'Edit Client Category')

@section('content_header')
<h1>Edit Client Category</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
@endsection

@section('content')
    <div class="box">
{{--         <div class="box-header">
            <h3 class="box-title">Edit Client Category</h3>
        </div> --}}
        <form action="{{ route('client_categories.update', $client_category->id) }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
           {{ method_field('PUT') }}
            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label>Category Name <span>*</span></label>
                        <input type="text" name="title" class="form-control" placeholder="Enter Client Category" value="{{ $client_category->title }}">
                        @if($errors->has('title'))
                            <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update</button>
            </div>
        </form>
    </div>

@endsection



@push('js')

@endpush