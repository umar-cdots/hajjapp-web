@extends('adminlte::page')
@section('title', 'Compliants')

@section('content_header')
    <h1>Compliants</h1>
    <div class="clearfix"></div>
    {{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection


@section('content')
    <div class="box">

        <div class="box-body">
        <table id="ComplaintTable" class="table table-bordered table-striped dataTable no-footer">
             <thead>
                <tr>
                   <th>Client Name</th>
                    <th>Trip</th>
                    <th>Category</th>
                    <th>Group</th>
                    <th>Feedback Category</th>
                    <th>Feedback</th>
                </tr>
                </thead>
                <tbody>
                @if(count($compliantfeedback))
                @foreach($compliantfeedback as $feedback)
                    <tr>
                        <td>{{ $feedback->clientgrouptrip->client->name ?? '--' }}</td>
                        <td>{{ $feedback->clientgrouptrip->clienttrip->trip->title ?? '--' }}</td>
                        <td>{{ $feedback->clientgrouptrip->client->client_category->title ?? '--' }}</td>
                        <td>{{ $feedback->clientgrouptrip->clienttrip->group->group_name ?? '--' }}</td>
                        <td>{{ $feedback->feedbackcategory->title ?? '--' }}</td>
                        <td>{{ $feedback->feedback ?? '--' }}</td>
                    </tr>
                @endforeach
                @endif
                </tbody>
        </table>
</div>
</div>
</div>

@endsection

@push('js')
<script type="text/javascript">

$(function () {
     $('#ComplaintTable').DataTable({
        "bFilter":false,
     });
});
</script>
@endpush
