@extends('adminlte::tasks')

@section('title', 'Add Client')

@section('content_header')
    {{-- <a href="{{route('clients.index')}}" class="btn btn-primary">Back</a> --}}
    <ol class="breadcrumb">
        {{-- <li><a href="{{route('clients.index')}}">Client Management</a></li>
        <li class="active">Add Client</li> --}}
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <form action="{{ route('clients.store') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Client Name <span>*</span></label>
                            <input type="text" name="name" class="form-control" placeholder="Enter First Name"
                                   value="{{ old('name') }}">
                            @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label>Email(username) <span>*</span></label>
                            <input type="email" name="email" class="form-control" placeholder="Enter Email"
                                   value="{{ old('email') }}">
                            @if($errors->has('email'))
                                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                            <label>Gender <span>*</span></label>
                            <select name="gender" id="" class="form-control select2">
                                <option value="">--Select Gender--</option>
                                <option value="male" {{ ( old('gender')=='male' ) ? 'selected' : '' }}>Male</option>
                                <option value="female" {{ ( old('gender')=='female' ) ? 'selected' : '' }}>Female
                                </option>
                                <option value="other" {{ ( old('gender')=='other' ) ? 'selected' : '' }}>Other</option>
                            </select>
                            @if($errors->has('gender'))
                                <span class="help-block text-danger">{{ $errors->first('gender') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('primary_number') ? 'has-error' : '' }}">
                            <label>Phone Number <span>*</span></label>
                            <input type="text" name="primary_number" class="form-control"
                                   placeholder="Enter Primary Phone No." value="{{ old('primary_number') }}">
                            @if($errors->has('primary_number'))
                                <span class="help-block text-danger">{{ $errors->first('primary_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('secondary_number') ? 'has-error' : '' }}">
                            <label>Backup Phone Number</label>
                            <input type="text" name="secondary_number" class="form-control"
                                   placeholder="Enter Secondary Phone No." value="{{ old('secondary_number') }}">
                            @if($errors->has('secondary_number'))
                                <span class="help-block text-danger">{{ $errors->first('secondary_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('emergency_number') ? 'has-error' : '' }}">
                            <label>Emergency Phone Number</label>
                            <input type="text" name="emergency_number" class="form-control"
                                   placeholder="Enter Emergency Phone No." value="{{ old('emergency_number') }}">
                            @if($errors->has('emergency_number'))
                                <span class="help-block text-danger">{{ $errors->first('emergency_number') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('client_category_id') ? 'has-error' : '' }}">
                            <label>Client Category </label>
                            <select name="client_category_id" id="customer_category"
                                    class="form-control select2 customer_category" multiple="multiple">
                                @foreach($client_categories as $client_category)
                                    <option
                                        value="{{ $client_category->id }}" {{ ( old('client_category_id')== $client_category->id ) ? 'selected' : '' }}>{{ $client_category->title }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('client_category_id'))
                                <span class="help-block text-danger">{{ $errors->first('client_category_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Client Sub Category </label>
                            <select name="client_sub_category_id" class="form-control select2 client_sub_category"
                                    id="client_sub_category" multiple="multiple">
                            </select>
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('client_category') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('paid_amount') ? 'has-error' : '' }}">
                            <label>Amount Paid </label>
                            <select name="paid_amount" id="" class="form-control select2 paid_amount"
                                    multiple="multiple">
                                <option value="" {{ ( old('paid_amount')== null ) ? 'selected' : '' }}>--Select Amount
                                    Paid--
                                </option>
                                <option value="yes" {{ ( old('paid_amount')== 'yes' ) ? 'selected' : '' }}>Yes</option>
                                <option value="no" {{ ( old('paid_amount')== 'no' ) ? 'selected' : '' }}>No</option>
                            </select>
                            @if($errors->has('paid_amount'))
                                <span class="help-block text-danger">{{ $errors->first('paid_amount') }}</span>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('blood_group') ? 'has-error' : '' }}">
                            <label>Blood Group</label>
                            <input type="text" name="blood_group" class="form-control" placeholder="Enter Blood Group"
                                   value="{{ old('blood_group') }}">
                            @if($errors->has('blood_group'))
                                <span class="help-block text-danger">{{ $errors->first('blood_group') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('occupation') ? 'has-error' : '' }}">
                            <label>Occupation</label>
                            <input type="text" name="occupation" class="form-control" placeholder="Enter Occupation"
                                   value="{{ old('occupation') }}">
                            @if($errors->has('occupation'))
                                <span class="help-block text-danger">{{ $errors->first('occupation') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('special_needs') ? 'has-error' : '' }}">
                            <label>Special Needs</label>
                            <input type="text" name="special_needs" class="form-control"
                                   placeholder="Enter Special Need" value="{{ old('special_needs') }}">
                            @if($errors->has('special_needs'))
                                <span class="help-block text-danger">{{ $errors->first('special_needs') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('nationality') ? 'has-error' : '' }}">
                            <label>Nationality </label>
                            <input type="text" name="nationality" class="form-control" placeholder="Enter Nationality"
                                   value="{{ old('nationality') }}">
                            @if($errors->has('nationality'))
                                <span class="help-block text-danger">{{ $errors->first('nationality') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('postal_address') ? 'has-error' : '' }}">
                            <label>Postal Address</label>
                            <input type="text" name="postal_address" class="form-control"
                                   placeholder="Enter Postal Address" value="{{ old('postal_address') }}">
                            @if($errors->has('postal_address'))
                                <span class="help-block text-danger">{{ $errors->first('postal_address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                            <label>Date of Birth</label>
                            <input type="text" name="dob" class="form-control datepicker"
                                   placeholder="Enter Date of Birth" value="{{ old('dob') }}">
                            @if($errors->has('dob'))
                                <span class="help-block text-danger">{{ $errors->first('dob') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('age') ? 'has-error' : '' }}">
                            <label>Age</label>
                            <input type="text" name="age" class="form-control" placeholder="Enter Age"
                                   value="{{ old('age') }}">
                            @if($errors->has('age'))
                                <span class="help-block text-danger">{{ $errors->first('age') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('qualification') ? 'has-error' : '' }}">
                            <label>Qualification</label>
                            <input type="text" name="qualification" class="form-control"
                                   placeholder="Enter Qualification" value="{{ old('qualification') }}">
                            @if($errors->has('qualification'))
                                <span class="help-block text-danger">{{ $errors->first('qualification') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('total_paid_amount') ? 'has-error' : '' }}">
                            <label>Total Paid Amount</label>
                            <input type="text" name="total_paid_amount" class="form-control"
                                   placeholder="Enter Total Paid Amount" value="{{ old('total_paid_amount') }}">
                            @if($errors->has('total_paid_amount'))
                                <span class="help-block text-danger">{{ $errors->first('total_paid_amount') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" name="description"
                                      rows="3">{{ old('description') }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('passport_no') ? 'has-error' : '' }}">
                            <label>Passport No.</label>
                            <input type="text" name="passport_no" class="form-control" value="{{ old('passport_no') }}"
                                   placeholder="Enter Passport No.">
                            @if($errors->has('passport_no'))
                                <span class="help-block text-danger">{{ $errors->first('passport_no') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('cnic') ? 'has-error' : '' }}">
                            <label>CNIC</label>
                            <input type="text" name="cnic" class="form-control" value="{{ old('cnic') }}"
                                   placeholder="Enter CNIC">
                            @if($errors->has('cnic'))
                                <span class="help-block text-danger">{{ $errors->first('cnic') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label>Profile Image</label>
                            <input type="file" name="image" class="form-control" id="imgpreview">
                            @if($errors->has('image'))
                                <span class="help-block text-danger">{{ $errors->first('image') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create Client</button>
            </div>
        </form>
    </div>

@endsection



@push('js')
    <script type="text/javascript">
        function profile(input2) {
            if (input2.files && input2.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#viewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input2.files[0]);
            }
        }

        $("#imgpreview").change(function () {
            profile(this);
        });

        $(".customer_category").select2({
            maximumSelectionLength: 1,
            placeholder: "Select Category"//placeholder
        });
        $(".client_sub_category").select2({
            maximumSelectionLength: 1,
            placeholder: "Select Sub Category"//placeholder
        });
        $(".paid_amount").select2({
            maximumSelectionLength: 1,
            placeholder: "Select Amount Paid"//placeholder
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        $("#customer_category").change(function () {
            var id = $(this).val();
            $.ajax({
                url: "{{ route('get.subcategories.category') }}",
                type: 'POST',
                data: {data: id},
                success: function (response) {
                    if (response.status == 'success') {
                        html = '';
                        toastr['success']("Sub categories has been loaded.");
                        $(response.data).each(function (i, d) {
                            html += '<option value="' + d.id + '">' + d.name + '</option>';
                        });
                        $("#client_sub_category").html(html);
                        $(".select2").select2({
                            maximumSelectionLength: 1,
                            placeholder: "Select Client"//placeholder
                        });
                    } else {
                        toastr['error']("Something went wrong.");
                    }
                },
                error: function (error) {
                    toastr['error']("Something went wrong.");
                }

            });
        });
        //$('.select2').select2();
        $('.datepicker').datepicker({
            autoclose: true,
            orientation: 'auto bottom',
            format: 'yyyy/mm/dd'
        });
    </script>
@endpush
