@extends(\Auth::user()->user_type == "mobile_agent"?'adminlte::page':'adminlte::tasks')

@section('title', 'Client Profile')

@section('content_header')
    {{-- <a href="{{route('clients.index')}}" class="btn btn-primary">Back</a> --}}
    <ol class="breadcrumb">
        {{-- <li><a href="{{route('clients.index')}}">Client Management</a></li>
        <li class="active">Client Profile</li> --}}
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <form method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>{{ $new_complaint }}</h3>
                                <p>Total Complaints</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-exclamation-triangle"></i>
                            </div>
                            <a href="{{route('clients.clientcomplaint',$user->id)}}" class="small-box-footer">View
                                Details <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{$new_alerts}}</h3>
                                <p>Total Alerts</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-user"></i>
                            </div>
                            <a href="{{route('clients.clientalert',$user->id)}}" class="small-box-footer">View Details
                                <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{ $total_trips }}</h3>
                                <p>Active Trips</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-plane"></i>
                            </div>
                            <a href="{{ route('trips.customertripdetail',$user->id) }}" class="small-box-footer">View
                                Details <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    {{-- <div class="col-lg-3 col-xs-6">
                     <!-- small box -->
                     <div class="small-box bg-yellow">
                       <div class="inner">
                         <h3>{{ $total_messages }}</h3>
                         <p>Total Messages</p>
                       </div>
                       <div class="icon">
                         <i class="fa fa-envelope"></i>
                       </div>
                       <a href="#" class="small-box-footer">&nbsp;</a>
                     </div>
                   </div> --}}

                    <div class="col-lg-3 col-xs-6">
                        @if($user->client->picture_path!=null)
                            <img id="viewImage" src="{{ asset('storage/profile_images/'. $user->client->picture_path)}}"
                                 alt="" width="100px"/>
                        @else
                            <img id="viewImage" src="{{ asset('images/userImg.png') }}" alt="" width="100px"/>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Client Name <span>*</span></label>
                            <input type="text" name="name" class="form-control" value="{{ $user->client->name ?? ''}}"
                                   readonly="readonly">
                            @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label>Email(username) <span>*</span></label>
                            <input type="email" name="email" class="form-control" value="{{ $user->email ?? '' }}"
                                   readonly="readonly">
                            @if($errors->has('email'))
                                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                            <label>Gender <span>*</span></label>
                            <select name="gender" id="" class="form-control" readonly="readonly">
                                <option value="male" {{ ($user->client->gender == 'male') ? 'selected' : ''  }}>Male
                                </option>
                                <option value="female" {{ ($user->client->gender == 'female') ? 'selected' : ''  }}>
                                    Female
                                </option>
                                <option value="other" {{ ($user->client->gender == 'other') ? 'selected' : ''  }}>
                                    Other
                                </option>
                            </select>
                            @if($errors->has('gender'))
                                <span class="help-block text-danger">{{ $errors->first('gender') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('primary_number') ? 'has-error' : '' }}">
                            <label>Phone Number <span>*</span></label>
                            <input type="text" name="primary_number" class="form-control"
                                   value="{{ $user->client->primary_phone_number ?? '' }}" readonly="readonly">
                            @if($errors->has('primary_number'))
                                <span class="help-block text-danger">{{ $errors->first('primary_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('secondary_number') ? 'has-error' : '' }}">
                            <label>Backup Phone Number</label>
                            <input type="text" name="secondary_number" class="form-control"
                                   value="{{ $user->client->secondary_phone_number ?? '' }}" readonly="readonly">
                            @if($errors->has('secondary_number'))
                                <span class="help-block text-danger">{{ $errors->first('secondary_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('emergency_number') ? 'has-error' : '' }}">
                            <label>Emergency Phone Number</label>
                            <input type="text" name="emergency_number" class="form-control"
                                   value="{{ $user->client->emergency_phone_number ?? '' }}" readonly="readonly">
                            @if($errors->has('emergency_number'))
                                <span class="help-block text-danger">{{ $errors->first('emergency_number') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('client_category_id') ? 'has-error' : '' }}">
                            <label>Client Category </label>
                            <select name="client_category_id" id="client-category" class="form-control"
                                    readonly="readonly">
                                <option value="">--Select client category--</option>
                                @foreach($client_categories as $client_category)
                                    <option
                                        value="{{ $client_category->id }}" {{ ($client_category->id == $user->client->client_category_id) ? 'selected' : '' }}>{{ $client_category->title }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('client_category_id'))
                                <span class="help-block text-danger">{{ $errors->first('client_category_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Client Sub Category </label>
                            <select name="client_sub_category_id" class="form-control select2" id="client_sub_category"
                                    readonly="readonly">
                                <option value="">--Select Sub Category--</option>
                                @if($subclient_categories)
                                    <option value="{{$subclient_categories->id}}"
                                            selected="selected">{{$subclient_categories->name}}</option>
                                @endif
                            </select>
                            @if($errors->has('client_sub_category_id'))
                                <span
                                    class="help-block text-danger">{{ $errors->first('client_sub_category_id') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('created_at') ? 'has-error' : '' }}">
                            <label>Date Created</label>
                            <input type="text" name="created_at" class="form-control"
                                   value="{{ $user->client->created_at ?? '' }}" readonly="readonly">
                            @if($errors->has('created_at'))
                                <span class="help-block text-danger">{{ $errors->first('created_at') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Next Flight</label>
                            <input type="text" name="next_flight" class="form-control"
                                   value="{{$nextflight->flight->departure_datetime??''}}" readonly="readonly">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('passport_no') ? 'has-error' : '' }}">
                            <label>Passport No.</label>
                            <input type="text" name="passport_no" class="form-control"
                                   value="{{ $user->client->passport_no }}" readonly="readonly">
                            @if($errors->has('passport_no'))
                                <span class="help-block text-danger">{{ $errors->first('passport_no') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('cnic') ? 'has-error' : '' }}">
                            <label>CNIC</label>
                            <input type="text" name="cnic" class="form-control" value="{{ $user->client->cnic }}"
                                   readonly="readonly">
                            @if($errors->has('cnic'))
                                <span class="help-block text-danger">{{ $errors->first('cnic') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('paid_amount') ? 'has-error' : '' }}">
                            <label>Amount Paid</label>
                            <input type="text" name="paid_amount" class="form-control"
                                   value="{{ $user->client->paid_amount ?? '' }}" readonly="readonly">
                            @if($errors->has('paid_amount'))
                                <span class="help-block text-danger">{{ $errors->first('paid_amount') }}</span>
                            @endif
                        </div>
                    </div>
                    {{-- <div class="col-md-4">
                         <div class="form-group {{ $errors->has('occupation') ? 'has-error' : '' }}">
                             <label>Occupation</label>
                             <input type="text" name="occupation" class="form-control" value="{{ $user->client->occupation ?? '' }}" readonly="readonly">
                             @if($errors->has('occupation'))
                                 <span class="help-block text-danger">{{ $errors->first('occupation') }}</span>
                             @endif
                         </div>
                     </div>--}}

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('age') ? 'has-error' : '' }}">
                            <label>Age</label>
                            <input type="text" name="age" class="form-control" value="{{ $user->client->age ?? '' }}"
                                   readonly="readonly">
                            @if($errors->has('age'))
                                <span class="help-block text-danger">{{ $errors->first('age') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('nationality') ? 'has-error' : '' }}">
                            <label>Nationality </label>
                            <input type="text" name="nationality" class="form-control" placeholder="Enter Nationality"
                                   value="{{ $user->client->nationality ?? '' }}" readonly="readonly">
                            @if($errors->has('nationality'))
                                <span class="help-block text-danger">{{ $errors->first('nationality') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('total_paid_amount') ? 'has-error' : '' }}">
                            <label>Total Paid Amount</label>
                            <input type="text" name="total_paid_amount" class="form-control"
                                   value="{{ $user->client->total_paid_amount ?? ''}}" readonly="readonly">
                            @if($errors->has('total_paid_amount'))
                                <span class="help-block text-danger">{{ $errors->first('total_paid_amount') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('blood_group') ? 'has-error' : '' }}">
                            <label>Blood Group</label>
                            <input type="text" name="blood_group" class="form-control"
                                   value="{{ $user->client->blood_group ?? ''}}" readonly="readonly">
                            @if($errors->has('blood_group'))
                                <span class="help-block text-danger">{{ $errors->first('blood_group') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                            <label>Date of Birth</label>
                            <input type="text" name="dob" class="form-control datepicker"
                                   value="{{ $user->client->dob->format("Y-m-d") ?? '' }}" readonly="readonly">
                            @if($errors->has('dob'))
                                <span class="help-block text-danger">{{ $errors->first('dob') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('postal_address') ? 'has-error' : '' }}">
                            <label>Postal Address</label>
                            <input type="text" name="postal_address" class="form-control"
                                   value="{{ $user->client->postal_address ?? ''}}" readonly="readonly">
                            @if($errors->has('postal_address'))
                                <span class="help-block text-danger">{{ $errors->first('postal_address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('personality_comment') ? 'has-error' : '' }}">
                            <label>Personality Comments</label>
                            <input name="personality_comment" class="form-control" type="text"
                                   value="{{ $user->client->personality_comments ?? '' }}" readonly="readonly">
                            @if($errors->has('personality_comment'))
                                <span class="help-block text-danger">{{ $errors->first('personality_comment') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('special_needs') ? 'has-error' : '' }}">
                            <label>Special Needs</label>
                            <input type="text" name="special_needs" class="form-control"
                                   value="{{ $user->client->special_needs ?? '' }}" readonly="readonly">
                            @if($errors->has('special_needs'))
                                <span class="help-block text-danger">{{ $errors->first('special_needs') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                            <label>Description</label>
                            <textarea class="form-control" name="description" rows="3"
                                      readonly="readonly">{{ $user->client->description }}</textarea>
                            @if($errors->has('description'))
                                <span class="help-block text-danger">{{ $errors->first('description') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
            </div>
        </form>
    </div>

@endsection



@push('js')
    <script type="text/javascript">
        $('.datepicker').datepicker({
            autoclose: true,
            orientation: 'auto bottom',
            format: 'yyyy/mm/dd'
        });
        // function readURL(input) {
        //   if (input.files && input.files[0]) {
        //     var reader = new FileReader();
        //     reader.onload = function(e) {
        //       $('#viewImage').attr('src', e.target.result);
        //     }
        //     reader.readAsDataURL(input.files[0]);
        //   }
        // }
        // $("#imgpreview").change(function() {
        //   readURL(this);
        // });
    </script>
@endpush
