@extends('adminlte::page')
@section('title', 'Clients')

@section('content_header')
    <h1>Client Management</h1>
    <div class="clearfix"></div>
   {{--  <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection


@section('content')
    <div class="box">
        <div class="box-body">
            <form action="{{ route('clients.search') }}" method="POST">
                @csrf
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                @method('POST')
            <div class="row">
                    <div class="col-md-7 col-xs-10">
                        <div class="form-group" >
                            <select name="client_id" id="ClientSelect2" class="form-control ClientSelect2" multiple="true">
                                <option value="">Search Client</option>
                                @if(!empty($clientforselect2))
                                    @foreach($clientforselect2 as $clientset)
                                        <option value="{{ $clientset->id }}">{{ $clientset->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1" style="padding: 0;">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="col-md-4 form-group clientBtnAdd text-right">
                        <a href="{{ route('clients.create') }}" class="btn btn-primary btnSubmit">Add Client</a>
                        <a href="#clientuploadModal" class="btn btn-primary btnSubmit" data-toggle="modal" id="BtnclientuploadModal">Upload Client List</a>
                        <a href="{{route('load.excel.downloadclient')}}" class="btn btn-primary btnSubmit">Download Client List</a>
                    </div>
            </div>
            </form>
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Active</th>
                    <th>Phone #</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $client)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $client->getFullName() }}</td>
                        <td>{{ $client->user->email ?? '-' }}</td>
                        <td>{{ $client->client_category->title ?? '-' }}</td>
                        <td>{{ $client->sub_client_category->name ?? '-' }}</td>
                        <td>
                            <input type="checkbox" id="changeclientstatus" {{ $client->status==1 ? "checked" : "" }} data-toggle="toggle" onchange="changeclientstatus({{$client->id}})">
                        </td>
                        <td>{{ $client->primary_phone_number }}</td>
                        <td>
                           <a href="{{ route('clients.edit', ['id' => $client->user_id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            {{-- <a href="{{ route('clients.show', ['id' => $client->user_id]) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button></a> --}}
                            <form action="{{ route('clients.destroy', ['id' => $client->user_id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                            <a href="{{ route('clients.showprofile', ['id' => $client->user_id]) }}"><button class="btn btn-xs btn-primary btnSubmit" data-toggle="tooltip" title="Profile">View Profile</button></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>


<div class="modal fade in" id="clientuploadModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Upload Client From Excel Sheet</h4>
      </div>
      <form action="{{ route('load.excel.store') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>File</label>
                        <input type="file" name="Clientfile" class="form-control">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Upload</button>
       </div>
        </form> 
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@endsection
@push('js')
    <script type="text/javascript">
    
            

        $(function () {
            $('#client').DataTable({
                'lengthChange': true,
                'bFilter': false,
                "columnDefs": [
                 { orderable: false, targets: [5,7] },
                 { width: "20%", targets: 2 },
                 ],
                // "dom": '<"top"lp>rt<"bottom"i><"clear">'
            });
            // $('.dataTables_wrapper .top').wrapAll('<div class="row"></div>');
            // $('.dataTables_wrapper .top').prepend('<div class="col-md-6 addClientBtn"></div>');
            // $("body").find('.clientBtnAdd').appendTo(".addClientBtn");
            // $('.dataTables_length').wrapAll('<div class="col-md-2"></div>');
            // $('.dataTables_paginate ').wrapAll('<div class="col-md-4"></div>');

        });
        function changeclientstatus(id) {
        $.ajax({
            url: "{{route('clients.ChangeClientStatus')}}",
            data:{_token: $("#token").val(),id:id},
            type: "POST",
            success: function (json) {
                        html = '<option>Select</option>';
                        $(json.clientforselect2).each( function (i,d){
                            html += '<option value="'+ d.id +'">'+ d.name +'</option>';
                        });
                        $("#ClientSelect2").html(html);
               }
        });    
    }
    $(".ClientSelect2").select2({
        maximumSelectionLength: 1,
        placeholder: "Select Client"//placeholder
    });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        
        
    </script>
@endpush