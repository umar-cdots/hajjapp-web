@extends('adminlte::page')
@section('title', 'Client Details')

@section('content_header')
<h1>Client Details</h1>
<ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection


@section('content')
  <div class="box">
            <div class="box-header">
              <a href="{{ route('clients.create') }}" class="btn btn-primary">Add Client</a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
               <tbody>
                <tr>
                  <th>Name</th>
                  <td>{{ $user->client->name ?? '-' }}</td>
                </tr>
                 <tr>
                  <th>Client Category</th>
                  <td>{{ $user->client->client_category->title ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Client Sub Category</th>
                  <td>{{ $user->client->sub_client_category->name ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Primary Phone Number</th>
                  <td>{{ $user->client->primary_phone_number ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Secondary Phone Number</th>
                  <td>{{ $user->client->secondary_phone_number ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Emergency Phone Number</th>
                  <td>{{ $user->client->emergency_phone_number ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Email (Username)</th>
                  <td>{{ $user->email ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Occupation</th>
                  <td>{{ $user->client->occupation ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Blood Group</th>
                  <td>{{ $user->client->blood_group ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Decease</th>
                  <td>{{ $user->client->decease ?? '-' }}</td>
                </tr>
                 <tr>
                  <th>Special Needs</th>
                  <td>{{ $user->client->special_needs ?? '-' }}</td>
                </tr>
                 {{-- <tr>
                  <th>Nationality</th>
                  <td>{{ $user->client->nationality->name ?? '-' }}</td>
                </tr> --}}
                <tr>
                  <th>Postal Address</th>
                  <td>{{ $user->client->postal_address ?? '-' }}</td>
                </tr>
                 <tr>
                  <th>Date of Birth</th>
                  <td>{{ ($user->client->dob) ? $user->client->dob->format('Y-m-d') : '-' }}</td>
                </tr>
                 <tr>
                  <th>Gender</th>
                  <td>{{ $user->client->gender ?? '-' }}</td>
                </tr>
                 <tr>
                  <th>Interests</th>
                  <td>{{ $user->client->interest ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Qualification</th>
                  <td>{{ $user->client->qualification ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Personality Comments</th>
                  <td>{{ $user->client->personality_comments ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Passport No.</th>
                  <td>{{ $user->client->passport_no ?? '-' }}</td>
                </tr>
                <tr>
                  <th>CNIC No.</th>
                  <td>{{ $user->client->cnic ?? '-' }}</td>
                </tr>
                <tr>
                  <th>Profile Image</th>
                  <td><img src="{{ url('storage/profile_images/'. $user->client->picture_path) }}" width="200px" alt="Profile Image"></td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection