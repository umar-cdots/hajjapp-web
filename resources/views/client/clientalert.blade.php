@extends('adminlte::page')
@section('title', 'Emergency Alerts')

@section('content_header')
    <h1>Emergency Alerts</h1>
    <div class="clearfix"></div>
    {{-- <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol> --}}
    <div class="clearfix"></div>
@endsection


@section('content')
    <div class="box">
        <div class="box-body">
            <table id="emergencyAlerts" class="table table-bordered">
                <thead>
                <tr>
                    <th>Sr#</th>
                    @if(\Auth::user()->user_type == "super_admin")
                        <th>Travel Agency</th>
                    @endif
                    <th>Client</th>
                    <th>Trip</th>
                    <th>Catagory</th>
                    <th>Group</th>
                    <th>Phone Main</th>
                    <th>Phone Backup</th>
                    <th>Phone Emergency</th>
                    <th>Status</th>
                    <th>Sent Time</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($emergency_alerts as $emergency_alert)
                @if($emergency_alert->status == 'solved')
                    <tr style="background-color: #90EE90">
                        <td>{{ $loop->iteration }}</td>
                        @if(\Auth::user()->user_type == "super_admin")
                            <td>
                                <a href="{{ route('travel_agency.show', ['id' => Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->agency_name }}</a>
                            </td>
                        @endif
                        <td>
                            <a href="{{ route('clients.showprofile', ['id' => Utils::clientByClientTrip($emergency_alert->client_trip_id)->user_id]) }}">{{ Utils::clientByClientTrip($emergency_alert->client_trip_id)->getFullName() }}</a>
                        </td>
                        <td>{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</td>
                       {{-- <td>
                            <a href="{{ route('trips.show', ['id' => Utils::tripByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</a>
                        </td>--}}
                        <td>{{$emergency_alert->clientgtouptrip->client->client_category->title ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->clienttrip->group->group_name ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->primary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->secondary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->emergency_phone_number ?? '--'}}</td>
                        <td>
                            <button class="btn btn-xs btn-{{ $emergency_alert->status == 'sent' ? 'danger' : ($emergency_alert->status == 'seen' ? 'warning' : 'success') }}">{{ $emergency_alert->status == 'sent' ? 'New' : ($emergency_alert->status == 'seen' ? 'Seen' : 'Resolved') }}</button>
                        </td>
                        <td>{{ $emergency_alert->created_at->format('Y-m-d h:i:s a') }}</td>
                        <td>
                            {{-- <a href="{{ route('clients.edit', ['id' => $client->user_id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a> --}}
                            <a target="_blank" href="https://www.google.com/maps?q={{ $emergency_alert->lat }},{{ $emergency_alert->lon }}">
                                <button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Location"><i class="fa fa-map-pin"></i></button>
                            </a>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="seen">
                                <button type="submit" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Mark As Seen"><i class="fa fa-eye"></i></button>
                            </form>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="solved">
                                <button type="submit" class="btn btn-xs btn-success" data-toggle="tooltip" title="Mark As Solved"><i class="fa fa-check"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif
                    @if($emergency_alert->status == 'seen')
                    <tr style="background-color: #F5F5F5">
                        <td>{{ $loop->iteration }}</td>
                        @if(\Auth::user()->user_type == "super_admin")
                            <td>
                                <a href="{{ route('travel_agency.show', ['id' => Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->agency_name }}</a>
                            </td>
                        @endif
                        <td>
                            <a href="{{ route('clients.showprofile', ['id' => Utils::clientByClientTrip($emergency_alert->client_trip_id)->user_id]) }}">{{ Utils::clientByClientTrip($emergency_alert->client_trip_id)->getFullName() }}</a>
                        </td>
                        <td>{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</td>
                       {{-- <td>
                            <a href="{{ route('trips.show', ['id' => Utils::tripByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</a>
                        </td>--}}
                        <td>{{$emergency_alert->clientgtouptrip->client->client_category->title ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->clienttrip->group->group_name ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->primary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->secondary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->emergency_phone_number ?? '--'}}</td>
                        <td>
                            <button class="btn btn-xs btn-{{ $emergency_alert->status == 'sent' ? 'danger' : ($emergency_alert->status == 'seen' ? 'warning' : 'success') }}">{{ $emergency_alert->status == 'sent' ? 'New' : ($emergency_alert->status == 'seen' ? 'Seen' : 'Resolved') }}</button>
                        </td>
                        <td>{{ $emergency_alert->created_at->format('Y-m-d h:i:s a') }}</td>
                        <td>
                            {{-- <a href="{{ route('clients.edit', ['id' => $client->user_id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a> --}}
                            <a target="_blank" href="https://www.google.com/maps?q={{ $emergency_alert->lat }},{{ $emergency_alert->lon }}">
                                <button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Location"><i class="fa fa-map-pin"></i></button>
                            </a>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="seen">
                                <button type="submit" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Mark As Seen"><i class="fa fa-eye"></i></button>
                            </form>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="solved">
                                <button type="submit" class="btn btn-xs btn-success" data-toggle="tooltip" title="Mark As Solved"><i class="fa fa-check"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif
                    @if($emergency_alert->status == 'sent')
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        @if(\Auth::user()->user_type == "super_admin")
                            <td>
                                <a href="{{ route('travel_agency.show', ['id' => Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::travelAgencyByClientTrip($emergency_alert->client_trip_id)->agency_name }}</a>
                            </td>
                        @endif
                        <td>
                            <a href="{{ route('clients.showprofile', ['id' => Utils::clientByClientTrip($emergency_alert->client_trip_id)->user_id]) }}">{{ Utils::clientByClientTrip($emergency_alert->client_trip_id)->getFullName() }}</a>
                        </td>
                        <td>{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</td>
                       {{-- <td>
                            <a href="{{ route('trips.show', ['id' => Utils::tripByClientTrip($emergency_alert->client_trip_id)->id]) }}">{{ Utils::tripByClientTrip($emergency_alert->client_trip_id)->title }}</a>
                        </td>--}}
                        <td>{{$emergency_alert->clientgtouptrip->client->client_category->title ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->clienttrip->group->group_name ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->primary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->secondary_phone_number ?? '--'}}</td>
                        <td>{{$emergency_alert->clientgtouptrip->client->emergency_phone_number ?? '--'}}</td>
                        <td>
                            <button class="btn btn-xs btn-{{ $emergency_alert->status == 'sent' ? 'danger' : ($emergency_alert->status == 'seen' ? 'warning' : 'success') }}">{{ $emergency_alert->status == 'sent' ? 'New' : ($emergency_alert->status == 'seen' ? 'Seen' : 'Resolved') }}</button>
                        </td>
                        <td>{{ $emergency_alert->created_at->format('Y-m-d h:i:s a') }}</td>
                        <td>
                            {{-- <a href="{{ route('clients.edit', ['id' => $client->user_id]) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a> --}}
                            <a target="_blank" href="https://www.google.com/maps?q={{ $emergency_alert->lat }},{{ $emergency_alert->lon }}">
                                <button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Location"><i class="fa fa-map-pin"></i></button>
                            </a>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="seen">
                                <button type="submit" class="btn btn-xs btn-warning" data-toggle="tooltip" title="Mark As Seen"><i class="fa fa-eye"></i></button>
                            </form>
                            <form action="{{ route('emergency_alert.update', ['id' => $emergency_alert->id]) }}" method="POST" class="delete_item">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="status" value="solved">
                                <button type="submit" class="btn btn-xs btn-success" data-toggle="tooltip" title="Mark As Solved"><i class="fa fa-check"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif

                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#emergencyAlerts').DataTable({
                "bFilter":false,
                "columnDefs": [
                 { orderable: false, targets: 10 }
                 ]

            })
            $('form.delete_item').unbind('submit');
        });
    </script>
@endpush
