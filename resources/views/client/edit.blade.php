@extends('adminlte::tasks')

@section('title', 'Edit Client')

@section('content_header')
    {{-- <a href="{{route('clients.index')}}" class="btn btn-primary">Back</a> --}}
    <ol class="breadcrumb">
        {{-- <li><a href="{{route('clients.index')}}">Client Management</a></li>
        <li class="active">Edit Client</li> --}}
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <form action="{{ route('clients.update', $user->id) }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Client Name <span>*</span></label>
                            <input type="text" name="name" class="form-control" placeholder="Enter First Name"
                                   value="{{ $user->client->name ?? ''}}" readonly="readonly">
                            @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label>Email(username) <span>*</span></label>
                            <input type="email" name="email" class="form-control" placeholder="Enter Email"
                                   value="{{ $user->email ?? '' }}" readonly="readonly">
                            @if($errors->has('email'))
                                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                            <label>Gender <span>*</span></label>
                            <input type="text" name="gender" class="form-control" placeholder="Enter Gender"
                                   value="{{ $user->client->gender ?? '' }}" readonly="readonly">
                            @if($errors->has('gender'))
                                <span class="help-block text-danger">{{ $errors->first('gender') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('primary_number') ? 'has-error' : '' }}">
                            <label>Phone Number <span>*</span></label>
                            <input type="text" name="primary_number" class="form-control"
                                   placeholder="Enter Primary Phone No."
                                   value="{{ $user->client->primary_phone_number ?? '' }}" readonly="readonly">
                            @if($errors->has('primary_number'))
                                <span class="help-block text-danger">{{ $errors->first('primary_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('secondary_number') ? 'has-error' : '' }}">
                            <label>Backup Phone Number</label>
                            <input type="text" name="secondary_number" class="form-control"
                                   placeholder="Enter Secondary Phone No."
                                   value="{{ $user->client->secondary_phone_number ?? '' }}" readonly="readonly">
                            @if($errors->has('secondary_number'))
                                <span class="help-block text-danger">{{ $errors->first('secondary_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('emergency_number') ? 'has-error' : '' }}">
                            <label>Emergency Phone Number</label>
                            <input type="text" name="emergency_number" class="form-control"
                                   placeholder="Enter Emergency Phone No."
                                   value="{{ $user->client->emergency_phone_number ?? '' }}" readonly="readonly">
                            @if($errors->has('emergency_number'))
                                <span class="help-block text-danger">{{ $errors->first('emergency_number') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('client_category_id') ? 'has-error' : '' }}">
                            <label>Client Category </label>
                            <select name="client_category_id" id="client-category"
                                    class="form-control select2 client-category" multiple="multiple">
                                @foreach($client_categories as $client_category)
                                    <option
                                        value="{{ $client_category->id }}" {{ ($client_category->id == $user->client->client_category_id) ? 'selected' : '' }}>{{ $client_category->title }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('client_category_id'))
                                <span class="help-block text-danger">{{ $errors->first('client_category_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Client Sub Category </label>
                            <select name="client_sub_category_id" class="form-control select2 client_sub_category"
                                    id="client_sub_category" multiple="multiple">
                                @if($subclient_categories)
                                    <option value="{{$subclient_categories->id}}"
                                            selected="selected">{{$subclient_categories->name}}</option>
                                @endif
                            </select>
                            @if($errors->has('client_sub_category_id'))
                                <span
                                    class="help-block text-danger">{{ $errors->first('client_sub_category_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('occupation') ? 'has-error' : '' }}">
                            <label>Occupation</label>
                            <input type="text" name="occupation" class="form-control" placeholder="Enter Occupation"
                                   value="{{ $user->client->occupation ?? '' }}">
                            @if($errors->has('occupation'))
                                <span class="help-block text-danger">{{ $errors->first('occupation') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('blood_group') ? 'has-error' : '' }}">
                            <label>Blood Group</label>
                            <input type="text" name="blood_group" class="form-control" placeholder="Enter Blood Group"
                                   value="{{ $user->client->blood_group ?? ''}}">
                            @if($errors->has('blood_group'))
                                <span class="help-block text-danger">{{ $errors->first('blood_group') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('total_paid_amount') ? 'has-error' : '' }}">
                            <label>Total Paid Amount</label>
                            <input type="text" name="total_paid_amount" class="form-control"
                                   placeholder="Enter Total Paid Amount" value="{{ $user->client->total_paid_amount}}">
                            @if($errors->has('total_paid_amount'))
                                <span class="help-block text-danger">{{ $errors->first('total_paid_amount') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('special_needs') ? 'has-error' : '' }}">
                            <label>Special Needs</label>
                            <input type="text" name="special_needs" class="form-control"
                                   placeholder="Enter Special Need" value="{{ $user->client->special_needs ?? '' }}">
                            @if($errors->has('special_needs'))
                                <span class="help-block text-danger">{{ $errors->first('special_needs') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('nationality') ? 'has-error' : '' }}">
                            <label>Nationality </label>
                            <input type="text" name="nationality" class="form-control" placeholder="Enter Nationality"
                                   value="{{ $user->client->nationality ?? '' }}">
                            @if($errors->has('nationality'))
                                <span class="help-block text-danger">{{ $errors->first('nationality') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                            <label>Date of Birth</label>
                            <input type="text" name="dob" class="form-control datepicker"
                                   placeholder="Enter Date of Birth" value="{{ $user->client->dob ?? '' }}">
                            @if($errors->has('dob'))
                                <span class="help-block text-danger">{{ $errors->first('dob') }}</span>
                            @endif
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('age') ? 'has-error' : '' }}">
                            <label>Age</label>
                            <input type="text" name="age" class="form-control" placeholder="Enter Age"
                                   value="{{ $user->client->age ?? '' }}">
                            @if($errors->has('age'))
                                <span class="help-block text-danger">{{ $errors->first('age') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('qualification') ? 'has-error' : '' }}">
                            <label>Qualification</label>
                            <input type="text" name="qualification" class="form-control"
                                   placeholder="Enter Qualification" value="{{ $user->client->qualification ?? '' }}">
                            @if($errors->has('qualification'))
                                <span class="help-block text-danger">{{ $errors->first('qualification') }}</span>
                            @endif
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('postal_address') ? 'has-error' : '' }}">
                            <label>Postal Address</label>
                            <input type="text" name="postal_address" class="form-control"
                                   placeholder="Enter Postal Address" value="{{ $user->client->postal_address ?? ''}}">
                            @if($errors->has('postal_address'))
                                <span class="help-block text-danger">{{ $errors->first('postal_address') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('personality_comment') ? 'has-error' : '' }}">
                            <label>Personality Comments</label>
                            <input name="personality_comment" class="form-control" type="text"
                                   placeholder="Enter Personality Comments"
                                   value="{{ $user->client->personality_comments ?? '' }}">
                            @if($errors->has('personality_comment'))
                                <span class="help-block text-danger">{{ $errors->first('personality_comment') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                            <label>Description</label>
                            <textarea class="form-control" name="description"
                                      rows="3">{{ $user->client->description }}</textarea>
                            @if($errors->has('description'))
                                <span class="help-block text-danger">{{ $errors->first('description') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('passport_no') ? 'has-error' : '' }}">
                            <label>Passport No.</label>
                            <input type="text" name="passport_no" class="form-control"
                                   value="{{ $user->client->passport_no }}" placeholder="Enter Passport No.">
                            @if($errors->has('passport_no'))
                                <span class="help-block text-danger">{{ $errors->first('passport_no') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('cnic') ? 'has-error' : '' }}">
                            <label>CNIC</label>
                            <input type="text" name="cnic" class="form-control" value="{{ $user->client->cnic }}"
                                   placeholder="Enter CNIC">
                            @if($errors->has('cnic'))
                                <span class="help-block text-danger">{{ $errors->first('cnic') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('paid_amount') ? 'has-error' : '' }}">
                            <label>Amount Paid</label>
                            <select name="paid_amount" id="" class="form-control select2 paid_amount"
                                    multiple="multiple">
                                @if($user->client->paid_amount==null)
                                    <option value=""></option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                @elseif($user->client->paid_amount=="yes")
                                    <option value=""></option>
                                    <option value="yes" selected="selected">Yes</option>
                                    <option value="no">No</option>
                                    @endelseif
                                @else
                                    <option value=""></option>
                                    <option value="yes">Yes</option>
                                    <option value="no" selected="selected">No</option>
                                @endif
                            </select>
                            @if($errors->has('paid_amount'))
                                <span class="help-block text-danger">{{ $errors->first('paid_amount') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label>Profile Image</label>
                            <input type="file" name="image" class="form-control" id="imgpreview">
                            @if($user->client->picture_path!=null)
                                <img id="viewImage"
                                     src="{{ asset('storage/profile_images/'. $user->client->picture_path)}}" alt=""
                                     width="100px"/>
                            @else
                                <img id="viewImage" src="{{ asset('images/userImg.png') }}" alt="" width="100px"/>
                            @endif
                            @if($errors->has('image'))
                                <span class="help-block text-danger">{{ $errors->first('image') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Update Client</button>
            </div>
        </form>
    </div>

@endsection



@push('js')
    <script type="text/javascript">
        $('.datepicker').datepicker({
            autoclose: true,
            orientation: 'auto bottom',
            format: 'yyyy/mm/dd'
        });

        function changeImage(input) {
            if ($("#imgpreview").prop("files")[0]) {
                switch ($("#imgpreview").val().substring($("#imgpreview").val().lastIndexOf('.') + 1).toLowerCase()) {
                    case 'gif':
                    case 'jpg':
                    case 'png':

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#viewImage').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                        break;
                    default:
                        $("#imgpreview").val('');
                        toastr['error']("File format not correct!");
                        break;
                }
            }
            // if (input.files && input.files[0]) {
            //     var reader = new FileReader();
            //     reader.onload = function (e) {
            //         $('#viewImage').attr('src', e.target.result);
            //     }
            //     reader.readAsDataURL(input.files[0]);
            // }
        }

        $("#imgpreview").change(function () {
            changeImage(this);
        });
        $(".client-category").select2({
            maximumSelectionLength: 1,
            placeholder: "Select Category"//placeholder
        });
        $(".client_sub_category").select2({
            maximumSelectionLength: 1,
            placeholder: "Select Sub Category"//placeholder
        });
        $(".paid_amount").select2({
            maximumSelectionLength: 1,
            placeholder: "Select Amount Paid"//placeholder
        });

        $('.client_category').on('change', function () {
            var data = $(".client_category option:selected").val();
            $.ajax({
                url: "{{ route('get.subcategories.category') }}",
                type: 'POST',
                data: {data: data},
                success: function (response) {
                    if (response.status == 'success') {
                        $("#client_sub_category").empty();
                        html = '<option>--Select sub category</option>';
                        toastr['success']("Sub categories has been loaded.");
                        $(response.data).each(function (i, d) {
                            html += '<option value="' + d.id + '">' + d.name + '</option>';
                        });
                        $("#client_sub_category").html(html);
                    } else {
                        toastr['error']("Something went wrong.");
                    }
                },
                error: function (error) {
                    toastr['error']("Something went wrong.");
                }

            });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    </script>
@endpush
