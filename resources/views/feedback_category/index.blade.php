@extends('adminlte::page')
@section('title', 'Feedback')

@section('content_header')
    <h1>Feedback Categories</h1>
    {{-- <ol class="breadcrumb">
            <li><a href="{{ URL::previous() }}">Back</a></li>
        </ol> --}}<div class="clearfix"></div>
@endsection

@section('content')
    <div class="box">
        <div class="box-body travelAgencyTableBtn">
            <div class="col-md-12 text-right">
                <a href="{{ route('feedback.categories.create') }}" class="btn btn-primary btnSubmit">Add Feedback Category</a>
            </div>
            <div class="clearfix"></div>
            <table id="airlines" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($feedback_categories as $res)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $res->title }}</td>
                        <td>
                            <a href="{{ route('feedback.categories.edit', $res->id) }}"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>
                            {{-- <a href="{{ route('airlines.show', $airline->id) }}"><button class="btn btn-xs btn-success" data-toggle="tooltip" title="View Step"><i class="fa fa-eye"></i></button></a> --}}
                            <form action="{{ route('feedback.categories.destroy', $res->id) }}" method="POST" class="delete_item">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(function () {
            $('#airlines').DataTable({
                'bFilter': false,
                "columnDefs": [
                 { orderable: false, targets: 2}
                 ],
            });
        });
    </script>
@endpush
