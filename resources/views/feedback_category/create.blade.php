@extends('adminlte::page')
@section('title', 'Create Feedback Category')
@section('content_header')
    <h1>Create Feedback Category</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::previous() }}">Back</a></li>
    </ol>
    <div class="clearfix"></div>

@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            {{-- <h3 class="box-title">Create New Airline</h3> --}}
        </div>
        <form action="{{ route('feedback.categories.store') }}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label>Title <span>*</span></label>
                            <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                            @if($errors->has('title'))
                                <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary btnSubmit">Create</button>
            </div>
        </form>
    </div>
@endsection

