<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('super_admin', function ($user){
            if($user->user_type == 'super_admin'){
                return true;
            }
        });

        Gate::define('agent', function ($user){
            if($user->user_type == 'agent'){
                return true;
            }
        });

        Gate::define('client', function ($user){
            if($user->user_type == 'client'){
                return true;
            }
        });

        Gate::define('staff', function ($user){
            if($user->user_type == 'staff'){
                return true;
            }
        });
        Gate::define('mobile_agent', function ($user){
            if($user->user_type == 'mobile_agent'){
                return true;
            }
        });
        
        Passport::routes();
        Passport::personalAccessClientId(4);
    }
}
