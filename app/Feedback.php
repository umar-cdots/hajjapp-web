<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Feedback extends Model
{
    public function feedbackCategory()
    {
    	return $this->belongsTo(FeedbackCategory::class);
    }

    public function clientgrouptrip()
    {
        return $this->belongsTo(GroupClientTrip::class,'client_trip_id');
    }

    //public function trip()
    //{
    //	return $this->belongsToMany(Trip::class, 'client_trips', 'client_id', 'trip_id')
    //					->withPivot(['id', 'status']);
    //}

    //public function tripClient(){
      //  return $this->belongsTo(ClientTrip::class, 'client_trip_id', 'id');
    //}

    /**
     * Scope a query to only include logged in users trips.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLoggedInClient($query)
    {
    	if(\Auth::user()->user_type == 'client')
        	return $query->where('client_id', \Auth::user()->userType->id);
        return $query;
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }
}
