<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class TripTask extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'trip_tasks';

    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }
    
    public function staff(){
        return $this->belongsTo(Staff::class, 'staff_id', 'id');
    }

    public function trip(){
        return $this->belongsTo(Trip::class, 'trip_id', 'id');
    }

    public function setDueDateAttribute($value){
        $this->attributes['due_date'] = date('Y-m-d H:i:s', strtotime($value));
    }
    public function setStartDateAttribute($value){
        $this->attributes['start_date'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function setDueTimeAttribute($value){
        $this->attributes['due_time'] = date('H:i', strtotime($value)).':00';
    }


    public function is_late(){
        if($this->actual_time){
            if($this->actual_date > $this->due_date){
                return 'Yes(in days)';
            }elseif ($this->actual_date == $this->due_date){
                if($this->actual_time > $this->due_time){
                    return 'Yes(in time)';
                }
            }else{
                return 'No';
            }
        }else{
            return '--';
        }
    }
}
