<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCategory extends Model
{
    protected $guarded = [];
    protected $table = 'client_categories';

    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }
    
    public function client_sub_categories(){
        return $this->hasMany(ClientSubCategory::class, 'client_category_id', 'id');
}
}
