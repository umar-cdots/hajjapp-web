<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Flight extends Model
{
	use SoftDeletes;
	protected $guarded = [];

    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent') 
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }

	public function setDepartureDateTimeAttribute($value){
        $this->attributes['departure_datetime'] = date('Y-m-d H:i:s', strtotime($value));
    }
    public function setArrivalDateTimeAttribute($value){
        $this->attributes['arrival_datetime'] = date('Y-m-d H:i:s', strtotime($value));
    }

	public function airline()
	{
		return $this->belongsTo(Airline::class, 'airline_id');
	}

    public function fromAirport()
    {
    	return $this->belongsTo(Airport::class, 'from_airport_id');
    }

    public function toAirport()
    {
    	return $this->belongsTo(Airport::class, 'to_airport_id');
    }

    public function trip()
    {
    	return $this->belongsTo(Trip::class, 'trip_id');
    }

    public function scopeAllRelationsData()
    {
    	return $this->with(['airline', 'fromAirport', 'fromAirport.city', 'fromAirport.country', 'toAirport', 'toAirport.city', 'toAirport.country']);
    }

    public function clientgroup()
    {
        return $this->belongsToMany(GroupClient::class, 'client_trip_airlines');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }
}
