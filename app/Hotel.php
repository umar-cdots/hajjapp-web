<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    use SoftDeletes;
    //public function getPathAttribute($path)
    //{
      //  return asset(str_replace('public/', 'storage/', $path));
    //}
    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }

    public function TripHotelDetail()
    {
        return $this->hasMany(TripHotel::class,'id','hotel_id');
    }
    public function travelAgency()
    {
        return $this->belongsTo(TravelAgency::class);
    }

    public function city()
    {
    	return $this->belongsTo(City::class);
    }

    public function state()
    {
    	return $this->belongsTo(State::class);
    }

    public function country()
    {
    	return $this->belongsTo(Country::class);
    }

}
