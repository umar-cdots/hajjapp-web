<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripGuide extends Model
{
    protected $table = 'trip_guides';
    protected $guarded = [];
}
