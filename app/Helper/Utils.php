<?php
namespace App\Helper;

abstract class Utils
{
	public static function sendNotification($to, $body)
	{
		if(empty($to))
			return false;

	    $payload = array(
	        'to' 	=> $to,
	        'sound' => 'default',
	        'body' 	=> $body['data']['message'] ?? "Notification Recevied",
            'data'  => $body,
            'channelId' => 'message'
	    );

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL 			=> "https://exp.host/--/api/v2/push/send",
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_ENCODING 		=> "",
			CURLOPT_MAXREDIRS 		=> 10,
			CURLOPT_TIMEOUT 		=> 30,
			CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST 	=> "POST",
			CURLOPT_POSTFIELDS 		=> json_encode($payload),
			CURLOPT_HTTPHEADER 		=> array(
				"Accept: application/json",
				"Accept-Encoding: gzip, deflate",
				"Content-Type: application/json",
				"cache-control: no-cache",
				"host: exp.host"
			),
		));

		$response 	= curl_exec($curl);
		$err 		= curl_error($curl);

		curl_close($curl);

		if ($err)
		{
			return false;
		}
		return $response;
	}

	public static function tripByClientTrip($client_trip_id)
	{
		return \App\GroupClientTrip::find($client_trip_id)->clienttrip->trip;
	}

	public static function clientByClientTrip($client_trip_id)
	{
		return \App\GroupClientTrip::find($client_trip_id)->client;
	}

	public static function travelAgencyByClientTrip($client_trip_id)
	{
		return \App\GroupClientTrip::find($client_trip_id)->clienttrip->trip->travelAgency;
	}
}
