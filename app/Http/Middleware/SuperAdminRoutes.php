<?php

namespace App\Http\Middleware;

use Closure;

class SuperAdminRoutes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->user_type == 'super_admin')
            return $next($request);
        return abort(403);
    }
}
