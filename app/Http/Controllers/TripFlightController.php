<?php

namespace App\Http\Controllers;

use App\Airline;
use App\Airport;
use App\ClientTrip;
use App\ClientTripAirline;
use App\Trip;
use App\GroupClientTrip;
use App\Flight;
use Illuminate\Http\Request;

class TripFlightController extends Controller
{
    public function index($id){
        $getFlight=Flight::with('airline')->with('fromAirport')->with('toAirport')->where('trip_id',$id)->get();
        $trip = Trip::findOrFail($id);
        $agency_id = Auth()->user()->userType->travel_agency_id;
        $trip_customers = ClientTrip::where('trip_id', $trip->id)->get();
        $airports = Airport::where('travel_agency_id', $agency_id)->get();
        $getTripGroup=ClientTrip::with('clients')->with('group')->where('trip_id',$id)->get();
        $airlines = Airline::where('travel_agency_id', $agency_id)->orderBy('id', 'DESC')->get();
        
    	return view('trip.trip_flights.index', compact('airlines', 'airports', 'trip', 'trip_customers','getTripGroup','getFlight'));
    }

    public function addFlightForTrip(Request $request){
        //return $request;
        $flightId=0;
         $agency_id = Auth()->user()->userType->travel_agency_id;
        if($request->SelectFlight!=null){
           if($request->mainclientcheckboxes!=null){
            $a = 0;
            foreach ($request->SelectFlight as $airline){
                if($airline!=0){
                    $a=$airline;
                }
                $FromAirportID=0;
                $ToAirportID=0;
                $ArilineId=0;
               
                if(is_numeric($request->from[$a]))
                {
                    $FromAirportID=$request->from[$a];
                }
                else{
                    $Airport=new Airport;
                    $Airport->name=$request->from[$a];
                    $Airport->travel_agency_id=$agency_id;
                    $AirportID=$Airport->save();
                    if($AirportID){
                     $FromAirportID=$Airport->id;       
                    }
                }

                if(is_numeric($request->to[$a]))
                {
                    $ToAirportID=$request->to[$a];
                }
                else{
                    $Airport=new Airport;
                    $Airport->name=$request->to[$a];
                    $Airport->travel_agency_id=$agency_id;
                    $AirportID=$Airport->save();
                    if($AirportID){
                     $ToAirportID=$Airport->id;       
                    }
                }

                if(is_numeric($request->airline[$a]))
                {
                    $ArilineId=$request->airline[$a];
                }
                else{
                    $Airline=new Airline;
                    $Airline->name=$request->airline[$a];
                    $Airline->travel_agency_id=$agency_id;
                    $airlineID=$Airline->save();
                    if($airlineID){
                     $ArilineId=$Airline->id;       
                    }
                }

                $checkFlight=Flight::where('trip_id',$request->trip_id)->where('id',$request->flight_id[$a])->first();
                if($checkFlight){
                    $checkFlight->airline_id = $ArilineId;
                    $checkFlight->departure_datetime = $request->depareture_date_time[$a];
                    $checkFlight->arrival_datetime = $request->arrival_date_time[$a];
                    $checkFlight->from_airport_id = $FromAirportID;
                    $checkFlight->to_airport_id = $ToAirportID;
                    $checkFlight->pnr = $request->reference[$a];
                    $checkFlight->terminal = $request->terminal[$a]==null?'N/A':$request->terminal[$a];
                    $checkFlight->gate = $request->gate[$a]==null?'N/A':$request->gate[$a];
                    $checkFlight->note = $request->note[$a]==null?'N/A':$request->note[$a];
                    $checkFlightID=$checkFlight->save();
                    if($checkFlightID){
                        $flightId=$checkFlight->id;
                    }
                }
                else{
                $Flight =new Flight;
                    $Flight->trip_id = $request->trip_id;
                    $Flight->airline_id = $ArilineId;
                    $Flight->departure_datetime = $request->depareture_date_time[$a];
                    $Flight->arrival_datetime = $request->arrival_date_time[$a];
                    $Flight->from_airport_id = $FromAirportID;
                    $Flight->to_airport_id = $ToAirportID;
                    $Flight->pnr = $request->reference[$a];
                    $Flight->terminal = $request->terminal[$a]==null?'N/A':$request->terminal[$a];
                    $Flight->gate = $request->gate[$a]==null?'N/A':$request->gate[$a];
                    $Flight->note = $request->note[$a]==null?'N/A':$request->note[$a];
                    $FlightID=$Flight->save();
                    if($FlightID){
                        $flightId=$Flight->id;
                    }    
                }
                $checkFlightClient=ClientTripAirline::where('flight_id',$flightId)->get();
                foreach ($checkFlightClient as $key => $value) {
                    $deleteallClient=$value->delete();
                }
                foreach ($request->mainclientcheckboxes as $trip_client){
                $checkClientTrip=ClientTripAirline::where('flight_id',$flightId)->where('client_trip_id',$trip_client)->first();
                if($checkClientTrip){
                }
                else{
                ClientTripAirline::create([
                    'client_trip_id' => $trip_client,
                    'flight_id' => $flightId
                ]);    
                }
            }

            $a++;
        }

        return redirect()->route('trip.flight',$request->trip_id)
            ->with([
                'status' => 'success',
                'message' => 'Flight has been assign to all trip members'
            ]);
        }
        else{
            return redirect()->route('trip.flight',$request->trip_id)
            ->with([
                'status' => 'error',
                'message' => 'Please assign client first'
            ]);
        }
    }
        else{
            return redirect()->route('trip.flight',$request->trip_id)
            ->with([
                'status' => 'error',
                'message' => 'Please select at least one flight'
            ]);
        } 
    }

    public function viewassignclient($flightid){
        $trips=ClientTripAirline::with(array('clientTrip'=>function($q){
            $q->with(array('client'=>function($q){$q->with('client_category');}))->with(array('clienttrip'=>function($q){
                $q->with('group')->with('trip');
            }));

        }))->where('flight_id',$flightid)->get();
        //return $trips;
        $tripsDetail=$trips;
        return view('trip.trip_flights.viewassignclient', compact('trips','tripsDetail'));
    }

    public function viewTripByCustomerID(Request $request){
        //return $request;
           $id=$request->client_id;
           $trips=ClientTripAirline::whereHas('clientTrip',function($q) use($id){
            $q->whereHas('client', function($q) use($id){
                          $q->where('id',$id);
                          });})->with(array('clientTrip'=>function($q){
            $q->with(array('client'=>function($q){$q->with('client_category');}))->with(array('clienttrip'=>function($q){
                $q->with('group')->with('trip');
            }));}))->where('flight_id',$request->flightid)->get();
                          //return $trips;
           $tripsDetail=ClientTripAirline::with(array('clientTrip'=>function($q){
            $q->with(array('client'=>function($q){$q->with('client_category');}))->with(array('clienttrip'=>function($q){
                $q->with('group')->with('trip');
            }));

        }))->where('flight_id',$request->flightid)->get();
          // return $trips;
          return view('trip.trip_flights.viewassignclient', compact('trips','tripsDetail'));
    }

    public function updateFlightForCustomer(Request $request){
        $this->validate($request, [
            'trip_customer_id' => 'required|integer|exists:client_trips,id',
            'airline.*' => 'required|integer|exists:airlines,id',
            'time.*' => 'required',
            'from.*' => 'required|integer|exists:airports,id',
            'to.*' => 'required|integer|exists:airports,id',
            'pnr.*' => 'required|string',
            'status.*' => 'required|string'
        ]);
        foreach ($request->time as $key => $time){
            $new_time[$key] = $this->makeDateTime($time);
        }
        $client_trip_airlines = ClientTripAirline::where('client_trip_id', $request->trip_customer_id)->get();
        $a = 0;
        foreach ($client_trip_airlines as $client_airline){
            $client_airline->update([
                'airline_id' => $request->airline[$a],
                'departure_datetime' => $new_time[$a]['from_time'],
                'arrival_datetime' => $new_time[$a]['to_time'],
                'from_airport_id' => $request->from[$a],
                'to_airport_id' => $request->to[$a],
                'pnr' => $request->pnr[$a],
                'status' => $request->status[$a]
            ]);
            $a++;
        }

        return redirect()->route('trips.index')
            ->with([
                'status' => 'success',
                'message' => 'Flights details for the member has been updated'
            ]);
    }

    public function destroy($id){
        $clientTrip=ClientTripAirline::where('id',$id)->first();
        if($clientTrip){
            $clienttripid=$clientTrip->delete();
            if($clienttripid){
                return redirect()->route('trip.flight.viewassignclient',$clientTrip->flight_id)
            ->with([
                'status' => 'success',
                'message' => 'Client Successfully Deleted'
            ]);
            }
            else{
                  return redirect()->route('trip.flight.viewassignclient',$clientTrip->flight_id)
            ->with([
                'status' => 'error',
                'message' => 'Client Can not Delete'
            ]);
        }
    }
        else{
          return redirect()->route('trip.flight.viewassignclient',$clientTrip->flight_id)
            ->with([
                'status' => 'error',
                'message' => 'Client can not exist'
            ]);    
        }
        
    }

    private function makeDateTime($time){
        //from date
        $from_time = substr($time, 0, strpos($time, '-'));
        $from_time = date('Y-m-d H:i', strtotime($from_time));

        //to date
        $to_time = substr($time, strpos($time, '-'));
        $to_time = substr($to_time, 2);
        $to_time = date('Y-m-d H:i', strtotime($to_time));

        return [
            'from_time' => $from_time.':00',
            'to_time' => $to_time.':00'
        ];
    }

    public function deleteflightbyid(Request $request){
      $status=0;
      $getFlightDetail=Flight::find($request->deletedata);
      if($getFlightDetail){
        $getAssignClient=ClientTripAirline::where('flight_id',$getFlightDetail->id)->get();
        if($getAssignClient){
            foreach ($getAssignClient as $key => $value) {
                $deleteclient=$value->delete();
                if($deleteclient){
                $status=1;   
                }
            }
        }
        $deleteFlightid=$getFlightDetail->delete();
        if($deleteFlightid){
            $status=1;
        }
    }
    else{
        $status=0;
    }
    
      return response()->json(['status' => $status]);
  }
}
