<?php

namespace App\Http\Controllers;

use App\FeedbackCategory;
use App\Feedback;
use Illuminate\Http\Request;

class FeedbackCategoriesController extends Controller
{
    public function index(){
        $feedback_categories = FeedbackCategory::loggedTravelAgency()->get();

        return view('feedback_category.index', compact('feedback_categories'));
    }

    public function create(){
        return view('feedback_category.create');
    }

    public function store(Request $request){
        $this->validate($request, [
           'title' => 'required|string'
        ]);

        FeedbackCategory::create([
           'title' => $request->title,
           'travel_agency_id' =>Auth()->user()->userType->travelAgency->id
        ]);

        return redirect()->route('feedback.categories.index')->with([
            'status' => 'success',
            'message' => 'Feedback category has been created'
        ]);
    }

    public function edit($id){
        $feedback_category = FeedbackCategory::findOrFail($id);

        return view('feedback_category.edit', compact('feedback_category'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'title' => 'required|string'
        ]);
        $feedback = FeedbackCategory::findOrFail($id);
        $feedback->update([
            'title' => $request->title
        ]);

        return redirect()->route('feedback.categories.index')->with([
            'status' => 'success',
            'message' => 'Feedback category has been updated'
        ]);
    }

    public function destroy($id){
        $checkData=Feedback::where('feedback_category_id',$id)->first();
        if($checkData){
            return redirect()->route('feedback.categories.index')->with([
            'status' => 'error',
            'message' => 'Feedback category can not delete it used already in feedback.'
        ]);
        }
        else{
        $feedback = FeedbackCategory::findOrFail($id);
        $feedback->delete();

        return redirect()->route('feedback.categories.index')->with([
            'status' => 'success',
            'message' => 'Feedback category has been deleted.'
        ]);
    }
    }
}
