<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Client;
use App\Staff;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Jobs\SendEmailJob;

class ChangePasswordController extends Controller
{
    public function index(){
    	$changePassword = User::findOrFail(Auth()->user()->id);
    	// dd($chanagePassword);
    	return view('change_password.index', compact('changePassword'));
    }

    public function changepassword(){
        $staffs = Staff::loggedTravelAgency()->get();
        $clients = Client::loggedTravelAgency()->get();
        // dd($chanagePassword);
        return view('change_password.changepassword',compact('staffs','clients'));
    }
    public function clientpassword(Request $request){
            $this->validate($request, [
            'client_id'       =>'required',
            'client_password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
           ]);
            $getdata=user::findOrFail($request->client_id);
            $getdata->password= Hash::make($request->client_password);
            $getdata->save();
            $clientdata = array(
                'email' => $getdata->email, 
                'password' => $request->client_password
            );
         dispatch(new SendEmailJob($getdata->email,Auth()->user()->email,$clientdata));
         return redirect(route('changepassword'))->with(['status' => "success", 'message' => "Password Changed Successfully!"]);
    }

    public function staffpassword(Request $request){
            $this->validate($request, [
            'staff_id'       =>'required',
            'staff_password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
           ]);
            $getdata=user::findOrFail($request->staff_id);
            $getdata->password= Hash::make($request->staff_password);
            $getdata->save();
            $clientdata = array(
                'email' => $getdata->email, 
                'password' => $request->staff_password
            );
         dispatch(new SendEmailJob($getdata->email,Auth()->user()->email,$clientdata));
         return redirect(route('changepassword'))->with(['status' => "success", 'message' => "Password Changed Successfully!"]);
    }

    public function update(Request $request, $id){

    	$this->validate($request, [
    	'password' => 'required',
    	'new_password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
]);


	 	return redirect(route('change_password.index'))->with([
	 		'status' 	=> 'success',
	 		'message'	=> 'Password Changed Successfully!'
	 	]);
	 	
	 	$changePassword = User::findOrFail(Auth()->user()->id);

    }
}
