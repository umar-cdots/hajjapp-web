<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */

    protected function authenticated(Request $request, $user)
    {
        //if(Auth::attempt(['email' => request('email'), 'password' => request('password'),'login_status'=> false]))
        //{
        //    return redirect()->route('dashboard');
       // }
        //else
        //{
        //    Auth::logout();
         //   return redirect()->route('login');
       // }
          if(in_array($user->user_type, ['client']))
        {
            return abort(403, "You're not allowed to use Web Portal.");
        }
    }

    public function logout(Request $request)
    {
        $data = \Auth::user();
        $data->fcm_token = null; 
        $data->login_status = false;
        $data->save();
        $this->guard()->logout();
        

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }
}
