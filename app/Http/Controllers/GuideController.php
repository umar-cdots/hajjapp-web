<?php

namespace App\Http\Controllers;

use App\Guide;
use App\TripGuide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GuideController extends Controller
{
    public function index(){
        $guides = Guide::where('travel_agency_id', Auth()->user()->userType->travelAgency->id)->get();

        return view('guides.index', compact('guides'));
    }

    public function create(){

        return view('guides.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request){
        $this->validate($request, [
            'guides' => 'required',
            'guides.*' => 'mimes:png,pdf,jpeg,jpg,txt,mp4'
        ]);

        if($request->has('guides')){
            foreach ($request->file('guides') as $guide){
                $file_name = $guide->getClientOriginalName();
                $path = Storage::disk('public')->putFileAs(
                    'guides', $guide, $file_name
                );
                Guide::create([
                    'travel_agency_id' => Auth()->user()->userType->travelAgency->id,
                    'title' => $file_name,
                    'path' => 'public/guides/'.$file_name
                ]);
            }

            return back()->with([
                'status' => 'success',
                'message' => 'Guides has been added.'
            ]);
        }
    }

    public function destroy($guide){
        $guide = Guide::findOrFail($guide);
        $guide->delete();

        return redirect()->route('guide.index')->with([
            'status' => 'success',
            'message' => 'Guide has been deleted'
        ]);
    }
}
