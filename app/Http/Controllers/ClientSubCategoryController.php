<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientSubCategory;
use App\ClientCategory;

class ClientSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientSubCategories = ClientSubCategory::loggedTravelAgency()->get();
        return view('client_sub_category.index', compact('clientSubCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client_categories = ClientCategory::loggedTravelAgency()->get();

        return view('client_sub_category.create', compact('client_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'client_category_id'     =>  "required",
            'name'                  =>  "required"
        ]);

        $clientSubCategory                         = new ClientSubCategory;
        $clientSubCategory->client_category_id      = $request->client_category_id;
        $clientSubCategory->name                   = $request->name;
        $clientSubCategory->travel_agency_id       = Auth()->user()->userType->travelAgency->id;
        $clientSubCategory->save();

        return redirect(route('client_sub_categories.index'))->with(['status' => 'success', 'message' => 'Sub Category Added Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientSubCategory = ClientSubCategory::findOrFail($id);
        $clientCategories = ClientCategory::all();
        return view('client_sub_category.edit', compact('clientSubCategory', 'clientCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'client_category_id'     =>  "required",
            'name'                  =>  "required"
        ]);
        $clientSubCategory = ClientSubCategory::findOrFail($id);
        $clientSubCategory->update([
            'client_category_id' => $request->client_category_id,
            'name' => $request->name
        ]);

        return back()->with(['status' => 'success', 'message'=> 'SubCategory has been updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clientSubCategory = ClientSubCategory::findOrFail($id);
        $clientSubCategory->delete();

        return redirect(route('client_sub_categories.index'))->with(['status' => 'success', 'message' => 'Sub Category Deleted Successfully!']);
    }
}
