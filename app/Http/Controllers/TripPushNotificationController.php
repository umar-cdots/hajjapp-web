<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientTrip;
use App\Helper\Utils;
use App\Trip;
use App\TripUpdate;
use App\User;
use App\Staff;
use App\Agent;
use App\Auth;
use App\StaffNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
Use Exception;
use App\GroupClientTrip;

class TripPushNotificationController extends Controller
{
    public function index($id){
        $trip = Trip::findOrFail($id);
        $getTripGroup=ClientTrip::with('clients')->with('group')->where('trip_id',$id)->get();
        $allStaff=Staff::loggedTravelAgency()->get();

        return view('trip.trip_updates.trip_updates', compact('trip','getTripGroup','allStaff'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'push_notification' => 'required|string'
        ]);
        $trip_id=$request->trip_id;
        $getTrip= Trip::where('id',$trip_id)->first();

        if($request->staff!=null){
            if($request->mainclientcheckboxes!=null){

        $trip_clients = GroupClientTrip::with(array('client' => function($q){
                                        $q->with('user');
                                        }))
                                       ->whereHas('clienttrip',function($q) use($trip_id){
                                        $q->where('trip_id',$trip_id);
                                       })->get();
                                       //return $trip_clients;
        //dd($trip_clients[0]->client->user->fcm);
        foreach ($request->mainclientcheckboxes as $groupclientid){
            $trip_clients = GroupClientTrip::with(array('client' => function($q){
                                        $q->with('user');
                                        }))->where('id',$groupclientid)->first();
            if($trip_clients->client->user['fcm_token']!=null){
            $trip_update1 = TripUpdate::create([
           'client_trip_group_id' => $groupclientid,
            'message' => $request->push_notification,
        ]);
            $trip_update = array(
                'client_trip_group_id' => $trip_update1->client_trip_group_id,
                'message'              => $trip_update1->message,
                'trip_id'               => $trip_id
            );
        $body           = array('type' => "trip_update" , 'data' => $trip_update);
            Utils::sendNotification($trip_clients->client->user['fcm_token'], $body);
            }
        }

        foreach ($request->staff as $key => $Staffdata) {
            $StaffInfor=Staff::with('user')->where('id',$Staffdata)->first();
            $StaffNotification = StaffNotification::create([
                'staff_id' => $Staffdata,
                'message' => $request->push_notification,
            ]);

            $body =array('type' => "agent" , 'data' => $StaffNotification);
            Utils::sendNotification($StaffInfor->user['fcm_token'], $body);
        }

       return redirect()->route('trips.index')->with(['status' => 'success', 'message' => 'Push notifications sent to trip '.$getTrip->title.'']);

    }
    else{

        foreach ($request->staff as $key => $Staffdata) {
            $StaffInfor=Staff::with('user')->where('id',$Staffdata)->first();
            $StaffNotification = StaffNotification::create([
                'staff_id' => $Staffdata,
                'message' => $request->push_notification."(Notification)",
            ]);

            $body =array('type' => "agent" , 'data' => $StaffNotification);
            Utils::sendNotification($StaffInfor->user['fcm_token'], $body);
        }

       return redirect()->route('trips.index')->with(['status' => 'success', 'message' => 'Push notifications sent to trip '.$getTrip->title.'']);
    }
    }
    else{
        if($request->mainclientcheckboxes!=null){

        $trip_clients = GroupClientTrip::with(array('client' => function($q){
                                        $q->with('user');
                                        }))
                                       ->whereHas('clienttrip',function($q) use($trip_id){
                                        $q->where('trip_id',$trip_id);
                                       })->get();
                                       //return $trip_clients;
        //dd($trip_clients[0]->client->user->fcm);
        foreach ($request->mainclientcheckboxes as $groupclientid){
            $trip_clients = GroupClientTrip::with(array('client' => function($q){
                                        $q->with('user');
                                        }))->where('id',$groupclientid)->first();
            if($trip_clients->client->user['fcm_token']!=null){
            $trip_update1 = TripUpdate::create([
           'client_trip_group_id' => $groupclientid,
            'message' => $request->push_notification,
        ]);
            $trip_update = array(
                'client_trip_group_id' => $trip_update1->client_trip_group_id,
                'message'              => $trip_update1->message,
                'trip_id'               => $trip_id
            );
        $body           = array('type' => "trip_update" , 'data' => $trip_update);
            Utils::sendNotification($trip_clients->client->user['fcm_token'], $body);
            }
        }
        return redirect()->route('trips.index')->with(['status' => 'success', 'message' => 'Push notifications sent to trip '.$getTrip->title.'']);
    }
    else{
       return redirect()->route('trip.notification.index',$trip_id)->with(['status' => 'error', 'message' => 'Please add client first.']);
    }
    }
    }

    public function loadExcelView(){
        return view('trip.partial.upload_client_with_excelsheet');
    }

    public function storeExcelSheet(Request $request){
        $status=false;
        $message="";
        try{
        if($request->Clientfile!=null){
        $path = $request->Clientfile->getRealPath();
        $data = Excel::load($path, function($reader){
            $reader->setSeparator('_');
        })->get();
        if(count($data)>0){
        foreach ($data as $key => $value){
          $results[] = $value;
        }

        $getdata=Agent::with(['travelAgency','travelAgency.package'])->where('id',Auth()->user()->userType->id)->first();
            $getAllAgent=Agent::where('travel_agency_id',$getdata->travel_agency_id)->get();
            $totalclient=0;
            foreach ($getAllAgent as $key => $value) {
                $Countclient=Client::where('created_by_agent_id',$value->id)->count();
                $totalclient=$totalclient+$Countclient;
            }
            if($totalclient < $getdata->travelagency->package->no_of_customers){
            foreach ($results as $key =>$name){
            if($name->email!=null){
            $user = User::create([
                'name'      => $name->clientname,
                'email'     => $name->email,
                'password'  => bcrypt('ahlanguide123'),
                'user_type' => 'client',
                'status'    => 'active'
            ]);

            Client::create([
               'user_id'                => $user->id,
               'name'                   => $name->clientname,
               'primary_phone_number'   => '0'.$name->phonenumber ,
               'gender'                 => $name->gender,
               'secondary_phone_number' => '0'.$name->secondphonenumber,
               'dob'                    => $name->dob,
               'occupation'             => $name->occupation,
               'personality_comments'   => $name->personalitycomments,
               'special_needs'          => $name->specialneeds,
               'blood_group'            => $name->bloodgroup,
               'nationality'            => $name->nationality,
               'postal_address'         => $name->postaladdress,
               'passport_no'            => $name->passportno,
               'cnic'                   => $name->cnic,
               'total_paid_amount'      => $name->totalpaidamount,
               'paid_amount'            => $name->paidamount,
               'description'            => $name->description,
               "created_by_agent_id"    => Auth()->user()->userType->id,
               'travel_agency_id'       => \Auth::user()->userType->travel_agency_id,
               'status'                 =>true
            ]);
        }
        }
        $status="success";
        $message="Client successfully upload";
            }
            else{
            $status="error";
            $message="You cannot add new client because your client quota was full.";
            }
          }
          else{
            $status="error";
            $message="Your excel sheet was empty.";
             }
          }
    else
    {
        $status="error";
        $message="Please Select File";
    }
}
      catch(Exception $e)
      {
        $status="error";
        $message=$e->getMessage();
        if (strpos($message, 'Duplicate') !== false) {
           $message="Email Should be unique";
         }
         elseif(strpos($message, 'fullname') !== false){
            $message="Your excel sheet empty!";
         }

     }
        return redirect()->route('clients.index')->with(['status' => $status, 'message' => $message]);
    }

    public function storeExcelSheetForStaff(Request $request){
        $status=false;
        $message="";
        try{
        if($request->Stafffile!=null){
        $path = $request->Stafffile->getRealPath();
        $data = Excel::load($path, function($reader){
            $reader->setSeparator('_');
        })->get();
        if(count($data) > 0){
            foreach ($data as $key => $value){
                $results[] = $value;
              }
              foreach ($results as $key =>$name){
                  if($name->staffname!=null){
                  $user = User::create([
                      'name'      => $name->staffname,
                      'email'     => $name->email,
                      'password'  => bcrypt('ahlanguide123'),
                      'user_type' => 'staff',
                      'status'    => 'active'
                  ]);

                  Staff::create([
                     'user_id'                 => $user->id,
                      'name'                   => $name->staffname,
                      'primary_phone_number'   => '0'.$name->phonenumber,
                      'secondary_phone_number' => '0'.$name->secondphonenumber,
                      'type'                   => $name->type,
                      'role'                   => $name->role,
                      'description'            => $name->description,
                      'created_by'             => Auth()->user()->id,
                      'travel_agency_id'       => \Auth::user()->userType->travel_agency_id,
                      'status'=>true
                  ]);
              }
              }
              $status="success";
              $message="Staff successfully upload";
        }
        else{
            $status="error";
            $message="Your excel sheet was empty.";
        }
    }
    else
    {
        $status="error";
        $message="Please Select File";
    }
}
 catch(Exception $e)
      {
        $status="error";
        $message=$e->getMessage();
        if (strpos($message, 'Duplicate') !== false) {
           $message="Email Should be unique";
         }
         elseif(strpos($message, 'fullname') !== false){
            $message="Your excel sheet empty!";
         }
     }

        return redirect()->route('staffs.index')->with(['status' => $status, 'message' => $message]);
    }

    public function downloadclient(){
        $getAllClientInfo=Client::loggedTravelAgency()->with('user')->get()->toArray();
        $client_array[]= array('Client Name','Email','Gender','Phone Number','SecondPhoneNumber','BackupPhoneNumber','PassportNo','cnic','PaidAmount','Age','Nationality','TotalPaidAmount','BloodGroup','DOB','PostalAddress','Personality Comments','Special Needs','Description');

        foreach ($getAllClientInfo as $key => $client) {
        $client_array[] = array(
            'Client Name' => $client['name'] ,
            'Email' => $client['user']['email'] ,
            'Gender' => $client['gender'],
            'PhoneNumber' => $client['primary_phone_number'] ,
            'SecondPhoneNumber' => $client['secondary_phone_number'],
            'BackupPhoneNumber' => $client['emergency_phone_number'],
            'PassportNo' => $client['passport_no'],
            'cnic' => $client['cnic'],
            'PaidAmount' => $client['paid_amount'],
            'Age' => $client['age'],
            'Nationality' => $client['nationality'],
            'TotalPaidAmount' => $client['total_paid_amount'],
            'BloodGroup' => $client['blood_group'],
            'DOB' => $client['dob'],
            'PostalAddress' => $client['postal_address'],
            'Personality Comments' => $client['personality_comments'],
            'Special Needs' => $client['special_needs'],
            'Description' => $client['description'],
        );
        }
        Excel::create('ClientData',function($excel) use($client_array){
            $excel->setTitle('ClientData');
            $excel->sheet('ClientData',function($sheet) use($client_array){
                $sheet->fromArray($client_array,null,'A1',false,false);
            });
        })->download('xlsx');
    }

    public function downloadstaff(){
        $getAllStaffInfo=Staff::loggedTravelAgency()->with('user')->get()->toArray();
        $Staff_array[]= array('StaffName','Type','Role','PhoneNumber','SecondPhoneNumber','BackupPhoneNumber','Email','Description');
        foreach ($getAllStaffInfo as $key => $Staff) {
        $Staff_array[] = array(
            'StaffName' => $Staff['name'] ,
            'Type' => $Staff['type'] ,
            'Role' => $Staff['role'] ,
            'PhoneNumber' => $Staff['primary_phone_number'],
            'SecondPhoneNumber' => $Staff['secondary_phone_number'],
            'SecondPhoneNumber' => $Staff['emergency_phone_number'],
            'Email' => $Staff['user']['email'] ,
            'Description' => $Staff['description']
        );
        }
        Excel::create('StaffData',function($excel) use($Staff_array){
            $excel->setTitle('StaffData');
            $excel->sheet('StaffData',function($sheet) use($Staff_array){
                $sheet->fromArray($Staff_array,null,'A1',false,false);
            });
        })->download('xlsx');
    }
}
