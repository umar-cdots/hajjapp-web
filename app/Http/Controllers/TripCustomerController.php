<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientCategory;
use App\ClientSubCategory;
use App\ClientTrip;
use App\Step;
use App\Trip;
use App\Group;
use App\GroupClientTrip;
use Session;
use App\ClientTripAirline;
use App\ClientTripHotel;
use App\ClientTripStep;
use App\EmergencyAlert;
use App\Feedback;
use App\TripStepPack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TripCustomerController extends Controller
{
    public function index($id){
      $groupList=Group::loggedTravelAgency()->where('status',true)->get();
      $getTripGroup=ClientTrip::with('clients')->with('group')->where('trip_id',$id)->get();
      Session::put('GroupList', $groupList);
      Session::put('GetTripWiseGroup', $getTripGroup);
      return view('trip.trip_customer.assign_trip_customer',compact('groupList','id','getTripGroup'));
    }

    public function store(Request $request){
    $GroupTripID=0;
    $status=false;
    $active=false;
    $errMsg="";
    if($request->value){   
        foreach ($request->value as $key => $value) {

          foreach ($request->outerGroup as $key => $group) {

            if($group[0]==$value[1] && $group[1]=='Active')
            {
              $active=true;
              break;
            }
            else{
             $active=false; 
            }

          }
          if($active==true){
            $getTripcapacity=Trip::find($request->tripid);
            $tripid=$request->tripid;
            $getTotalCustomer=GroupClientTrip::whereHas('clienttrip',function($q)use($tripid){
                                               $q->where('trip_id',$tripid);
                                               })->count();
            //return $getTripcapacity->capacity;
          if($getTotalCustomer<$getTripcapacity->capacity){
          $checkCleitTrip = ClientTrip::where('group_id',$value[1])->where('trip_id',$request->tripid)->first();
          if($checkCleitTrip)
          {
            $GroupTripID=$checkCleitTrip->id;
          }
          else
          {
           $tripClient = new ClientTrip();
           $tripClient->group_id = $value[1];
           $tripClient->trip_id = $request->tripid;
           $tripClient->status =  $getTripcapacity->active_trip == true ? 'ongoing' : 'pending';
           $tripClient->save(); 
           $status=true;
           $errMsg="Client assign successfully!";
           $GroupTripID=$tripClient->id;
          }
          $ClientID=explode(",",$value[0])[0];
          if($GroupTripID!=0)
          {
            $CheckClientID=GroupClientTrip::where('client_trip_id',$GroupTripID)->where('client_id',$ClientID)->first();
            if($CheckClientID)
            {
              $status=true;
              $errMsg="Client assign successfully!";
            }
            else
            {
               $tripClientGroup = GroupClientTrip::create([
              'client_id' => $ClientID,
              'client_trip_id' => $GroupTripID,
           ]);
           $getStepPack = TripStepPack::where('trip_id',$request->tripid)->first();
           if($getStepPack){
             $getStep = Step::where('step_pack_id',$getStepPack->step_pack_id)->first();
             if($getStep){
              $clienttripstep = new ClientTripStep;
              $clienttripstep->client_trip_id=$tripClientGroup->id;
              $clienttripstep->step_id=$getStep->id;
              $clienttripstep->status='current';
              $clienttripstepId=$clienttripstep->save();
             }
             else{

             }
           }
           else{

           }
               $status=true;
               $errMsg="Client assign successfully!";
            }
          }
        }
        else{
          $errMsg="Your Trip Capacity Full!";
        }
          }
        }
        }
      else{
        $errMsg="Please add atleast one group and client";
      }
      return response()->json(['status' => $status,'errMsg'=>$errMsg]);
    }

    public function deleteclient(Request $request){
    $GroupTripID=0;
    $status=0; 
    $errMsg="";  
          $checkCleitTrip=ClientTrip::where('group_id',$request->deletedata[1])->where('trip_id',$request->tripid)->first();
          if($checkCleitTrip)
          {
          $GroupTripID=$checkCleitTrip->id;
          $ClientID=explode(",",$request->deletedata[0])[0];
          if($GroupTripID!=0)
          {
            $CheckClientCount=GroupClientTrip::where('client_trip_id',$GroupTripID)->count();
            if($CheckClientCount>1){
            $CheckClientID=GroupClientTrip::where('client_trip_id',$GroupTripID)->where('client_id',$ClientID)->first();
            if($CheckClientID)
             {
              $deleteClientTripFlight=ClientTripAirline::where('client_trip_id',$CheckClientID->id)->get();
              foreach ($deleteClientTripFlight as $key => $value) {
                $FlightID=$value->delete();
              }
              $deleteClientTripHotel=ClientTripHotel::where('client_trip_id',$CheckClientID->id)->get();
              foreach ($deleteClientTripHotel as $key => $value) {
                $HotelID=$value->delete();
              }
              $deleteClientTripStep=ClientTripStep::where('client_trip_id',$CheckClientID->id)->get();
              foreach ($deleteClientTripStep as $key => $value) {
                $StepID=$value->delete();
              }
              $deleteClientTripEmergencyAlert=EmergencyAlert::where('client_trip_id',$CheckClientID->id)->get();
              foreach ($deleteClientTripEmergencyAlert as $key => $value) {
                $EmergencyAlertID=$value->delete();
              }
              $deleteClientTripFeedback=Feedback::where('client_trip_id',$CheckClientID->id)->get();
              foreach ($deleteClientTripFeedback as $key => $value) {
                $FeedbackID=$value->delete();
              }
              $CheckClient=$CheckClientID->delete();

              if($CheckClient){
                $status=1;
                $errMsg="Client delete successfully!";
              }
              else{
                $errMsg="Client cannot delete";
              }
            }
          }
          else{
          $status=-1;
          $errMsg="One client in group required";
        }
        }
        else{
          $errMsg="Client cannot delete";
        }
      }
      return response()->json(['status' => $status,'errMsg'=>$errMsg]);
    }

    public function deletegroup(Request $request){
    $GroupTripID=0;
    $status=false;   
          $checkCleitTrip=ClientTrip::where('group_id',$request->deletedata[0])->where('trip_id',$request->tripid)->first();
          if($checkCleitTrip)
          {
          $GroupTripID=$checkCleitTrip->id;
          $CheckClientID=GroupClientTrip::where('client_trip_id',$GroupTripID)->get();
            if($CheckClientID)
             {
              foreach ($CheckClientID as $key => $clientdata) {
                $deleteClientTripFlight=ClientTripAirline::where('client_trip_id',$clientdata->id)->get();
              foreach ($deleteClientTripFlight as $key => $value) {
                $FlightID=$value->delete();
              }
              $deleteClientTripHotel=ClientTripHotel::where('client_trip_id',$clientdata->id)->get();
              foreach ($deleteClientTripHotel as $key => $value) {
                $HotelID=$value->delete();
              }
              $deleteClientTripStep=ClientTripStep::where('client_trip_id',$clientdata->id)->get();
              foreach ($deleteClientTripStep as $key => $value) {
                $StepID=$value->delete();
              }
              $deleteClientTripEmergencyAlert=EmergencyAlert::where('client_trip_id',$clientdata->id)->get();
              foreach ($deleteClientTripEmergencyAlert as $key => $value) {
                $EmergencyAlertID=$value->delete();
              }
              $deleteClientTripFeedback=Feedback::where('client_trip_id',$clientdata->id)->get();
              foreach ($deleteClientTripFeedback as $key => $value) {
                $FeedbackID=$value->delete();
              }
              $CheckClient=GroupClientTrip::where('id',$clientdata->id)->delete();
              if($CheckClient){
                $status=true;
              }
              }
              $CleitTrip=$checkCleitTrip->delete();
                if($CleitTrip){
                  $status=true;
                }
            }
          }
      return response()->json(['status' => $status]);
    }

    public function getSubCategoriesWithCategory(Request $request){
        $client_category = ClientCategory::with('client_sub_categories')->where('id',$request->data)->first();
        $client_sub_categories = $client_category->client_sub_categories;
        return response()->json(['status' => 'success', 'data' => $client_sub_categories]);
    }

    public function getClientWithSubCategory(Request $request){
        $client_sub_category = ClientSubCategory::findOrFail($request->data);
        $clients = $client_sub_category->clients;

        return response()->json(['status' => 'success', 'data' => $clients]);
    }

    public function viewTripCustomer($id){
           $tripid=$id;
           $trips=GroupClientTrip::with(array('client' =>function($q){
                          $q->with('client_category');
                          }))->whereHas('clienttrip', function($q) use($tripid){
                          $q->where('trip_id',$tripid);
                          })->with(array ('clienttrip'=>function($q){
            $q->with('trip')->with('group');
           }))->get();
          $tripsDetail=$trips;
          return view('trip.trip_customer.view_trip_customer', compact('trips','tripsDetail'));
    }

    public function viewTripByCustomerID(Request $request){
           $id=$request->client_id;
           $tripid=$request->tripid;
           $trips=GroupClientTrip::whereHas('client', function($q) use($id){
                          $q->where('id',$id);
                          })->with(array('client' =>function($q){
                          $q->with('client_category');
                          }))->whereHas('clienttrip', function($q) use($tripid){
                          $q->where('trip_id',$tripid);
                          })->with(array ('clienttrip'=>function($q){
            $q->with('trip')->with('group');
           }))->get();
           $tripsDetail=GroupClientTrip::with(array('client' =>function($q){
                          $q->with('client_category');
                          }))->whereHas('clienttrip', function($q) use($tripid){
                          $q->where('trip_id',$tripid);
                          })->with(array ('clienttrip'=>function($q){
            $q->with('trip')->with('group');
           }))->get();
          return view('trip.trip_customer.view_trip_customer', compact('trips','tripsDetail'));
    }

    public function changeCurrentStep(Request $request){

        $validator = Validator::make($request->all(), [
           'trip_client_id' => 'required',
           'step_id' => 'required'
        ]);

        if(!$validator->fails()){
            $client_trip_step = ClientTripStep::where('client_trip_id', $request->trip_client_id)->first();
            $client_trip_step->update([
               'step_id' => $request->step_id
            ]);
            return back()->with([
                'status' => 'success',
                'message' => 'Customer Current Step has been changed.'
            ]);
        }else{
            return back()->with([
               'status' => 'error',
               'message' => 'Something went wrong'
            ]);
        }
    }

    public function changeAllCustomerStep(Request $request){
        //dd($request->all());
        $trip_clients = ClientTrip::where('trip_id', $request->trip_id)->get();
        foreach ($trip_clients as $trip_client){
            $client_current_trip_step = ClientTripStep::where('client_trip_id', $trip_client->id)->first();
            $client_current_trip_step->update([
               'step_id' => $request->step
            ]);
        }
        return redirect()->route('trips.index')->with([
            'status' => 'success',
            'message' => 'Current step of all the customer has been changed'
        ]);
    }

    public function removeCustomer($trip_id){
        $trip_clients = ClientTrip::where('trip_id', $trip_id)->get();

        return view('trip.trip_customer.delete_customer_trip', compact('trip_clients'));
    }

    public function removeTripCustomer(Request $request){
        $trip_client = ClientTrip::findOrFail($request->trip_client_id);
        $trip_client->delete();

        return redirect()->route('trips.index')->with([
            'status' => 'success',
            'message' => 'Customer has been remove from trip.'
        ]);
    }

    public function addnewgroup(Request $request)
    {
        $status=false;
        $errMsg="";
        if($request->group_name)
        {
          $CheckGroup=Group::where('group_name',$request->group_name)->first();
          if(!$CheckGroup)
          {
            $group   = new Group();
            $group->group_name = $request->group_name;
            $group->travel_agency_id        =Auth()->user()->userType->travelAgency->id;
            $group->status = true;
            $groupid = $group->save();
            if($groupid)
            {
              $status=true;
              $errMsg="Group has been successfully.";
            }
          }
          else
          {
            $errMsg="group name already exist";
          }
        }
        else
        {
          $errMsg="Please enter group name";
        }
        $groupList=Group::where('status',true)->get();
        return response()->json(['status' => $status,'data' => $groupList,'errMsg'=>$errMsg]);
    }

    public function updategroup(Request $request)
    {
        $status=false;
        $errMsg="";
        if($request->groupname)
        {
          $CheckGroup=Group::where('id',$request->groupId)->first();
          if($CheckGroup)
          {
            $CheckGroup->group_name = $request->groupname;
            $CheckGroupid = $CheckGroup->save();
            if($CheckGroupid)
            {
              $status=true;
              $errMsg="Group has been successfully.";
            }
          }
          else
          {
            $errMsg="group not found";
          }
        }
        else
        {
          $errMsg="Please enter group name";
        }
        $groupList=Group::where('status',true)->get();
        return response()->json(['status' => $status,'data' => $groupList,'errMsg'=>$errMsg]);
    }

    public function getallclient(Request $request)
    {
        $status=false;
        $client=Client::loggedTravelAgency()->orderBy('id','DESC')->where('status',true)->get();
        if($client)
        {
          $status=true;
        }
        return response()->json(['status' => $status,'data' => $client]);
    }
}
