<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TravelAgency;
use App\Trip;
use App\ClientTrip;
use Session;
use App\StepPack;
use App\TripStepPack;
use App\Step;
use App\StepSection;
use App\StepSectionCheckList;
use App\GroupClientTrip;
use App\ClientTripAirline;
use App\ClientTripHotel;
use App\ClientTripStep;
use App\EmergencyAlert;
use App\Feedback;
use App\TripMessage;
use App\Agent;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $CurrentDate=date('Y-m-d');

        $getallTrip = Trip::loggedTravelAgency()->where('end_trip',true)->whereDate('end_date','<=',$CurrentDate)->get();
        foreach ($getallTrip as $key => $value) {
            $tripdata=Trip::where('id',$value->id)->first();
            $tripdata->end_trip = false;
            $tripdata->active_trip = false;
            $tripdata->save();
            $getTripStatus=ClientTrip::where('status','ongoing')->where('trip_id',$value->id)->get();
            foreach ($getTripStatus as $key => $data) {
                $updateData=ClientTrip::where('id',$data->id)->first();
                $updateData->status='completed';
                $updateData->save();
            }
        }

        if(Session::has('checkvalue')){
            $tripforselect2 = Trip::loggedTravelAgency()->where('end_trip',true)->get();
            $trips = Trip::loggedTravelAgency()->where('end_trip',true)->get();
            Session::forget('checkvalue');
        }
        else{
            $tripforselect2 = Trip::loggedTravelAgency()->where('end_trip',true)->get();
            $trips = Trip::loggedTravelAgency()->get();
        }


        return view('trip.index', ['trips' => $trips,'tripforselect2'=>$tripforselect2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getTravelAgency = TravelAgency::where('id',\Auth::user()->userType->travel_agency_id)->first();

        $getdata=Agent::with(['travelAgency','travelAgency.package'])->where('id',Auth()->user()->userType->id)->first();
        if($getTravelAgency->count_trip < $getdata->travelagency->package->no_of_trips){
            $travel_agencies    = [];
            $step_packs         = [];
            $tripInfo=[];
            if(\Auth::user()->user_type == "super_admin")
                $travel_agencies    = TravelAgency::get();
            else
                $step_packs         = StepPack::loggedTravelAgency()->get();

            return view('trip.create', compact('travel_agencies', 'step_packs','tripInfo'));
        }
        else{
            return redirect()->route('trips.index')->with(['status' => 'error', 'message' => 'You cannot add new trip because your trip quota was full. ']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'             => 'required',
            'start_date'        => 'required',
            'end_date'          => 'required',
            'profile_img'       => "image|mimes:png,jpg,jpeg|max:5120|required",
            'capacity'          => 'required|numeric',
            'guide_qouta'       => 'required|numeric'
        ]);
        $getTravelAgency = TravelAgency::where('id',\Auth::user()->userType->travel_agency_id)->first();
        $imageName = null;
        if($request->hasFile('profile_img'))
        {
            $imageName = $request->file('profile_img')->getClientOriginalName();
            $imageName = time().'_'.$imageName;
            $image       = $request->file('profile_img');

            $image_resize = Image::make($image->getRealPath());
//        $image_resize->resize(520, 1080);
            $image_resize->resize(756, 1080);
            $image_resize->save(public_path('storage/profile_images/' .$imageName));
        }
        else
        {
            $imageName = null;
        }

        $trips= new Trip;
        $trips->title                         = $request->title;
        $trips->start_date                    = $request->start_date;
        $trips->end_date                      = $request->end_date;
        $trips->path                          = $imageName;
        $trips->is_qibla_widget_enable        = true;
        $trips->travel_agency_id              = \Auth::user()->user_type == 'super_admin' ? $request->travel_agency_id : \Auth::user()->userType->travel_agency_id;
        $trips->step_pack_id                  = $request->step_pack_id;
        $trips->end_trip                      = true;
        $trips->capacity                      = $request->capacity;
        $trips->description                   = $request->description;
        $trips->guide_qouta                   = $request->guide_qouta;
        $trips->app_menu                      = !$request->has('app_menu')?0:1;
        $trips->dates_check                   = !$request->has('dates_check')?0:1;
        $trips->active_trip                   = !$request->has('active_trip')?0:1;
        $trips->save();

        $tripcount=$getTravelAgency->count_trip;
        $getTravelAgency->count_trip = $tripcount + 1;
        $getTravelAgency->save();
        $tripInfo=Trip::findOrFail($trips->id);
        return redirect(route('trips.edit',$trips->id))->with(['status' => "success", 'message' => "Trip Added Successfully!"]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trip   = Trip::findOrFail($id);
        return view('trip.show', compact('trip'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trip   = Trip::findOrFail($id);
        $travel_agents = TravelAgency::all();
        $step_packs = StepPack::where('travel_agency_id', Auth()->user()->userType->travelAgency->id)->get();
        Session::put('TripID', $id);
        return view('trip.edit', compact('trip', 'travel_agents', 'step_packs'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'             => 'required|string',
            'start_date'        => 'required|string',
            'end_date'          => 'required|string',
            'profile_img'       => 'image|max:5120',
            'capacity'          => 'required|numeric',
            'guide_qouta'          => 'required|numeric'
        ]);


        $imageName = null;
        if($request->hasFile('profile_img'))
        {
            $imageName = $request->file('profile_img')->getClientOriginalName();
            $imageName = time().'_'.$imageName;
            $image       = $request->file('profile_img');

            $image_resize = Image::make($image->getRealPath());
//        $image_resize->resize(520, 1080);
            $image_resize->resize(756, 1080);
            $image_resize->save(public_path('storage/profile_images/' .$imageName));
        }
        else
        {
            $imageName = null;
        }
        if($imageName==null)
        {
            $trip   = Trip::findOrFail($id);
            $trip->title                         = $request->title;
            $trip->start_date                    = $request->start_date;
            $trip->end_date                      = $request->end_date;
            $trip->travel_agency_id              = \Auth::user()->user_type == 'super_admin' ? $request->travel_agency_id : \Auth::user()->userType->travel_agency_id;
            $trip->description                   =$request->description;
            $trip->capacity                      = $request->capacity;
            $trip->guide_qouta                   = $request->guide_qouta;
            $trip->app_menu                      = !$request->has('app_menu')?0:1;
            $trip->dates_check                   = !$request->has('dates_check')?0:1;
            $trip->active_trip                   = !$request->has('active_trip')?0:1;
            $trip->save();
        }
        else
        {
            $trip   = Trip::findOrFail($id);
            $trip->title                         = $request->title;
            $trip->start_date                    = $request->start_date;
            $trip->end_date                      = $request->end_date;
            $trip->path                          = $imageName;
            $trip->travel_agency_id              = \Auth::user()->user_type == 'super_admin' ? $request->travel_agency_id : \Auth::user()->userType->travel_agency_id;
            $trip->description                   =$request->description;
            $trip->capacity                      = $request->capacity;
            $trip->guide_qouta                   = $request->guide_qouta;
            $trip->app_menu                      = !$request->has('app_menu')?0:1;
            $trip->dates_check                   = !$request->has('dates_check')?0:1;
            $trip->active_trip                   = !$request->has('active_trip')?0:1;

            $trip->save();
        }
        $checkStepPack=TripStepPack::where('trip_id',$id)->first();
        if($checkStepPack){
            $getStepPack=StepPack::where('id',$checkStepPack->step_pack_id)->first();
            if($getStepPack){
                $getStepPack->title=$trip->title;
                $getStepPackid=$getStepPack->save();
            }
        }
        if($trip->active_trip == true){
            $getAllTripbyTravelAgency = Trip::where('travel_agency_id',\Auth::user()->userType->travel_agency_id)->where('id','!=',$id)->pluck('id')->toArray();
            $tripClient = ClientTrip::where('trip_id',$trip->id)->update([
                'status' => 'ongoing'
            ]);
            foreach($getAllTripbyTravelAgency as $trip_id){
                $tripClient = ClientTrip::where('trip_id',$trip_id)->where('status','ongoing')->update([
                    'status' => 'pending'
                ]);
            }
        }
        else{
            $tripClient = ClientTrip::where('trip_id',$trip->id)->where('status','ongoing')->update([
                'status' => 'pending'
            ]);
        }

        return redirect(route('trips.edit',$trip->id))->with(['status' => "success", 'message' => "Trip Updated Successfully!"]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trip   = Trip::findOrFail($id);
        $deleteStepPack=TripStepPack::where('trip_id',$id)->first();
        if($deleteStepPack){
            $getStepPack=StepPack::where('id',$deleteStepPack->step_pack_id)->first();
            if($getStepPack){
                $deletestep=Step::where('step_pack_id',$getStepPack->id)->get();
                foreach ($deletestep as $key => $getstep) {
                    $deletesection=StepSection::where('step_id',$getstep->id)->get();
                    foreach ($deletesection as $key => $getsection) {
                        $deleteChecklist=StepSectionCheckList::where('step_section_id',$getsection->id)->get();
                        foreach ($deleteChecklist as $key => $getchecklist) {
                            $getchecklistid=$getchecklist->delete();
                        }
                        $getsectionid=$getsection->delete();
                    }
                    $getstepid=$getstep->delete();
                }
                $getStepPackid=$getStepPack->delete();
            }
            $deleteStepPackid=$deleteStepPack->delete();
        }
        $getClientTrip=ClientTrip::where('trip_id',$id)->get();
        foreach ($getClientTrip as $key => $grouptrip) {
            $getGroupClientData=GroupClientTrip::where('client_trip_id',$grouptrip->id)->get();
            foreach ($getGroupClientData as $key => $groupclientdata) {
                $deleteClientTripFlight=ClientTripAirline::where('client_trip_id',$groupclientdata->id)->get();
                foreach ($deleteClientTripFlight as $key => $value) {
                    $FlightID=$value->delete();
                }
                $deleteClientTripHotel=ClientTripHotel::where('client_trip_id',$groupclientdata->id)->get();
                foreach ($deleteClientTripHotel as $key => $value) {
                    $HotelID=$value->delete();
                }
                $deleteClientTripStep=ClientTripStep::where('client_trip_id',$groupclientdata->id)->get();
                foreach ($deleteClientTripStep as $key => $value) {
                    $StepID=$value->delete();
                }
                $deleteClientTripEmergencyAlert=EmergencyAlert::where('client_trip_id',$groupclientdata->id)->get();
                foreach ($deleteClientTripEmergencyAlert as $key => $value) {
                    $EmergencyAlertID=$value->delete();
                }
                $deleteClientTripFeedback=Feedback::where('client_trip_id',$groupclientdata->id)->get();
                foreach ($deleteClientTripFeedback as $key => $value) {
                    $FeedbackID=$value->delete();
                }
                $deleteClientTripMessage=TripMessage::where('trip_id',$id)->get();
                foreach ($deleteClientTripMessage as $key => $value) {
                    $TripMessageID=$value->delete();
                }
                $groupclientdataid=$groupclientdata->delete();
            }
            $grouptripid=$grouptrip->delete();
        }
        $trip->delete();

        return redirect(route('trips.index'))->with(['status' => "success", 'message' => "Trip Deleted Successfully!"]);
    }

    public function statuscheck(Request $request)
    {
        $trip   = Trip::findOrFail($request->id);
        $trip->end_trip=$trip->end_trip==0?true:false;
        $trip->save();
        $tripforselect2 = Trip::loggedTravelAgency()->where('end_trip',true)->get();
        $errorMsg=$trip->end_trip==0?"Trip Deactive Successfully":"Trip Active Successfully";
        $status=$trip->end_trip==0?"warning":"success";
        return response()->json(['status' => $status,'message'=>$errorMsg,'tripforselect2'=>$tripforselect2]);
    }

    public function search(Request $request)
    {
        $tripforselect2 = Trip::loggedTravelAgency()->where('end_trip',true)->get();
        if($request->trip_id)
        {
            $trips = Trip::loggedTravelAgency()->where('id',$request->trip_id)->get();
        }
        else
        {
            $trips = Trip::loggedTravelAgency()->get();
        }

        return view('trip.index', ['trips' => $trips,'tripforselect2'=>$tripforselect2]);
    }

    public function staffdetail($id)
    {
        $trips = Trip::whereHas('tripTasks', function($q) use ($id){
            $q->whereHas('staff', function($q) use ($id){
                $q->where('staff_id',$id);});
        })->where('end_trip',true)->get();
        $tripforselect2 =$trips;
        return view('trip.staffdetail', compact('trips','tripforselect2'));
    }

    public function customertripdetail($id)
    {

        $trips = ClientTrip::whereHas('clients', function($q) use ($id){
            $q->where('user_id',$id);
        })->with(array('trip'=>function($q){
            $q->where('end_trip',true);
        }))->get();
        //return $trips;
        $tripforselect2 =$trips;
        return view('trip.customertripdetail', compact('trips','tripforselect2'));
    }

    public function ChangeTripQibla(Request $request)
    {
        $trip   = Trip::findOrFail($request->id);
        $trip->is_qibla_widget_enable=$trip->is_qibla_widget_enable==0?true:false;
        $trip->save();
        $status=$trip->status==0?"warning":"success";
        return response()->json(['status' => $status]);
    }
}
