<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientTrip;
use App\ClientTripMessage;
use App\Helper\Utils;
use App\Trip;
use App\Agent;
use App\TripMessage;
use App\User;
use App\GroupClientTrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Session;
use App\TravelAgency;

class TripMessageController extends Controller
{
    public function index()
    {

        $trips = Trip::loggedTravelAgency()->get();
        $trip_clients = [];
        return view('trip.trip_message.index', compact('trips', 'trip_clients'));
    }

    public function showmessage(Request $request)
    {
        $trips = Trip::loggedTravelAgency()->get();
        $trip_id = $request->trip_id;

        Session::put('trip_id', $trip_id);
        $trip_clients = GroupClientTrip::with(['client', 'clienttrip', 'clienttrip.trip'])
            ->whereHas('clienttrip', function ($q) use ($trip_id) {
                $q->where('trip_id', $trip_id);
            })->get();
        //return $trip_clients;
        foreach ($trip_clients as $trip_client) {
            $client_messages[$trip_client->client->id] = TripMessage::where('trip_id', $trip_id)->where('client_id', $trip_client->client->id)->where('admin_seen_status', false)->first();
        }

        if (!empty($trip_clients)) {
            //if trip have client

            return view('trip.trip_message.index', compact('trips', 'trip_clients', 'client_messages'));
        } else {
            //if trip contain no client
            return back()->with([
                'status' => 'error',
                'message' => 'Please all the customer in the trip first.'
            ]);
        }

        return view('trip.trip_message.index');
    }

    public function loadClientMessages(Request $request)
    {
        $client_id = $request->client_id;
        $trip_id = $request->trip_id;
        TripMessage::where(['client_id' => $client_id, 'trip_id' => $trip_id])->update(['admin_seen_status' => 1]);
        //$getdata=TripMessage::where('trip_id', $request->trip_id)->where('client_id', $request->client_id)->where('seen_status',false)->get();
        //foreach ($getdata as $key => $value) {
        //$value->seen_status=true;
        //$value->save();
        //}
        $client_messages = TripMessage::with(['client', 'agent'])->where('trip_id', $request->trip_id)
            ->where('client_id', $request->client_id)
            ->get();
        //return $client_messages;
        if (Auth()->user()->userType == "agent") {
            $getAgent = Agent::findOrfail(Auth()->user()->userType->id);
        } else {

            $getAgent = Agent::where('travel_agency_id', Auth()->user()
                ->userType->travel_agency_id)->first();
        }

        $client = Client::where('id', $client_id)->first();

        return view('trip.trip_message.chat_box', compact('client_messages', 'client_id', 'trip_id', 'getAgent', 'client'));
    }

    public function loadClient(Request $request)
    {
        $trip_id = Session::get('trip_id');
        $trip_clients = GroupClientTrip::with(['client', 'clienttrip', 'clienttrip.trip'])
            ->whereHas('clienttrip', function ($q) use ($trip_id) {
                $q->where('trip_id', $trip_id);
            })->where('client_id', $request->client_id)->get();
        foreach ($trip_clients as $trip_client) {
            if (!is_null($trip_client->client)) {
                $client_messages[$trip_client->client->id] = TripMessage::where('trip_id', $trip_id)->where('client_id', $trip_client->client->id)->where('admin_seen_status', false)->first();
            }
        }

        return response()->json(['trip_clients' => $trip_clients, 'client_messages' => $client_messages]);
    }

    public function sendMessages(Request $request)
    {
        $CurrentDate = date('Y-m-d');
        $validate = Validator::make($request->all(), [
            'trip_id' => 'required|exists:trips,id',
            'client_id' => 'required|exists:clients,id',
            'agent_id' => 'required|exists:agents,id',
        ]);
        if ($validate->fails()) {
            $data = [
                'status' => 'fail',
                'message' => 'Something went wrong'
            ];

            return response()->json($data);
        } else {
            $trip = Trip::findOrfail($request->trip_id);
            if ($trip->end_trip > $CurrentDate) {
                $data = [
                    'status' => 'fail',
                    'message' => 'Your trip expired'
                ];

                return response()->json($data);
            } else {
                $client = Client::findOrFail($request->client_id);
                $user = User::where('id', $client->user_id)->first();
                $fcm = $user->fcm_token;
                $new_message = TripMessage::create([
                    'travel_agency_id' => $trip->travel_agency_id,
                    'trip_id' => $trip->id,
                    'client_id' => $request->client_id,
                    'agent_id' => $request->agent_id,
                    'message' => $request->message,
                    'client_seen_status' => false,
                    'admin_seen_status' => true,
                    'message_type' => 1
                ]);
                $getAgent = Agent::where('id', $request->agent_id)->first();
                if ($fcm) {
                    $body = array('type' => "message", 'data' => $new_message->toArray());

                    //send message
                    Utils::sendNotification($fcm, $body);


                }
                return view('trip.trip_message.single_message', compact('new_message', 'getAgent'));

            }
        }
    }
}
