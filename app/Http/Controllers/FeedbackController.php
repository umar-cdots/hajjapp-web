<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\FeedbackCategory;
use Illuminate\Http\Request;
use Session;

class FeedbackController extends Controller
{
    public function index()
    {
        $valuecheck = 0;
        if (Session::has('checkvalue')) {
            $valuecheck = 1;
            Session::forget('checkvalue');
        } else {
            $valuecheck = 0;
        }
        $getComplaintId = FeedbackCategory::where('title', 'complaints')->where('travel_agency_id', auth()->user()->userType->travel_agency_id)->whereNull('deleted_at')->first();
        if ($getComplaintId) {

            $compliantfeedback = Feedback::where('feedback_category_id', $getComplaintId->id)->with(['clientgrouptrip', 'clientgrouptrip.client', 'clientgrouptrip.client.client_category', 'clientgrouptrip.clienttrip', 'clientgrouptrip.clienttrip.trip', 'clientgrouptrip.clienttrip.group'])
                ->whereHas('clientgrouptrip.client', function ($q) {
                    $q->where('travel_agency_id', Auth()->user()->userType->travel_agency_id);
                })->with('feedbackCategory')->orderBy('id', 'DESC')->get();
//            dd($compliantfeedback);
//
            $allfeedbacks = Feedback::with(['clientgrouptrip', 'clientgrouptrip.client', 'clientgrouptrip.client.client_category', 'clientgrouptrip.clienttrip', 'clientgrouptrip.clienttrip.trip', 'clientgrouptrip.clienttrip.group'])
                ->whereHas('clientgrouptrip.client', function ($q) {
                    $q->where('travel_agency_id', Auth()->user()->userType->travel_agency_id);
                })->with('feedbackCategory')->where('feedback_category_id', '!=', $getComplaintId->id)->orderBy('id', 'DESC')->get();

        } else {

            $compliantfeedback = [];
            $allfeedbacks = Feedback::with(['clientgrouptrip', 'clientgrouptrip.client', 'clientgrouptrip.client.client_category', 'clientgrouptrip.clienttrip', 'clientgrouptrip.clienttrip.trip', 'clientgrouptrip.clienttrip.group'])
                ->whereHas('clientgrouptrip.client', function ($q) {
                    $q->where('travel_agency_id', Auth()->user()->userType->travel_agency_id);
                })
                ->with('feedbackCategory')->orderBy('id', 'DESC')->get();
        }
        //foreach ($allfeedbacks as $key => $value) {
        //	return $value->clientgrouptrip->client->client_category;
        //}
        //return $allfeedbacks;
        return view('feedback.index', compact('allfeedbacks', 'compliantfeedback', 'valuecheck'));
    }
}
