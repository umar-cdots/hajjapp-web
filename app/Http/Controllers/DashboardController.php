<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TripTask;
use App\Client;
use App\Trip;
use App\Staff;
use App\TripMessage;
use App\EmergencyAlert;
use App\TravelAgency;
use App\Agent;
use App\Feedback;
use App\Package;
use App\FeedbackCategory;
use App\ClientTripAirline;
use App\GroupClientTrip;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use App\StaffNotification;
Use Exception;
use App\Guide;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use Carbon\Carbon;
use DateTime;
use App\User;
use App\ClientTrip;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function message(){
        return view('message');
    }
    public function LicenseExpriy(){
        return view('LicenseExpriy');
    }
    public function index()
    {

       $newutctime = date(DateTime::ISO8601);
        $CurrentDate=date('Y-m-d');
        $total_tasks = '';
        $new_complaint='';
        $completed_tasks = '';
        $total_travelAgency = '';
        $total_packages = '';
        $total_clients = '';
        $total_trips = '';
        $total_messages = '';
        $overduetask = '';
        $client_trip_ids = [];
        $getemergencyalerts='';
        $getFeedback='';
        $emergency_alerts = '';
        $getTripMessage='';
        $taskduetodays = '';
        $clientqota='';
        $tripqota='';
        $StaffNotificationData='';
        $tripcapacity=[];
        $getFlightDepature=[];
        $tripqouta=[];
        $checklicense='';
        $getTask='';
       //Auth::logout();
       try{
        if(Auth()->user()->user_type == "super_admin"){
             $total_travelAgency = TravelAgency::count();
             $total_packages = Package::count();
              // $total_trips_for_superadmin = Trip::count();
            // $total_agents = Agent::count();
        }else if(Auth()->user()->user_type == 'agent'){
            $getAgency  =TravelAgency::with('package')->where('id',Auth()->user()->userType->travel_agency_id)->first();
            $newDate= date('Y-m-d', strtotime("+5 days"));
            $newDateMonth= date('Y-m-d', strtotime("+30 days"));
            if($CurrentDate>=$getAgency->package_end_date){
            Auth::logout();
            return view('LicenseExpriy');
            }
            else{
            $getComplaintId=FeedbackCategory::where('title','Complaints')->first();
            if($getComplaintId){
                $new_complaint = Feedback::whereHas('clientgrouptrip.client',function($q){
                    $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id);
                })->where('feedback_category_id',$getComplaintId->id)->count();
            }
            else{
                $new_complaint = 0;
            }
            
            $total_trips = Trip::where('travel_agency_id', Auth()->user()->userType->travel_agency_id)->where('end_trip',true)->count();
            $total_messages = TripMessage::where('agent_id', Auth()->user()->userType->id)->count();

            $client_trip_ids = \App\ClientTrip::whereIn('trip_id', \App\Trip::loggedTravelAgency()->pluck('id'))->pluck('id');
            $emergency_alerts= EmergencyAlert::with(['clientgtouptrip.clienttrip.group'])
                                              ->whereHas('clientgtouptrip.client',function($q){
                                               $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id);
                                                 })->where('status','sent')
                                              ->orderBy('id','DESC')->get();

            $taskduetodays = TripTask::loggedTravelAgency()->with(['trip','staff'])->whereDate('due_date',$CurrentDate)->where('status','!=','Complete')->orderBy('id', 'desc')->get();

            $overduetask = TripTask::loggedTravelAgency()->whereDate('due_date','<',$CurrentDate)->where('status','!=','Complete')->count();

             //foreach ($overduetask as $key => $value) {
              //return $value->created_at;
             //}
            //return $newDate;
            $getFlightdata=ClientTripAirline::with(['flight','flight.airline','clienttrip.client'                    ,'clienttrip.clienttrip.trip'])
                                    ->whereHas('clienttrip.client',function($q){
                                               $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id);
                                                 })->groupBy('flight_id')->orderBy('id','DESC')
                                    ->get();
            foreach ($getFlightdata as $key => $value) {
                $from1 = date("Y-m-d",  strtotime( str_replace('/', '-',$value->flight->departure_datetime))); 
                $to = \Carbon\Carbon::createFromFormat('Y-m-d', $CurrentDate);
                $from = \Carbon\Carbon::createFromFormat('Y-m-d', $from1);
                $diff_in_days = $from->diffInDays($to);
                if($to>$from){
                 $diff_in_days=-1;   
                }
                //return $diff_in_days;
                if($diff_in_days<=5 && $diff_in_days>=0){
                    $getFlightDepature[] = array(
                       'message' => 'Flight'." ". $value->flight->airline->name ." ". 'for trip '." ".$value->clienttrip->clienttrip->trip->title." ".'is departing on'." ". $value->flight->departure_datetime);
                }
                                    
            }

            $perviousDate=date('Y-m-d', strtotime("-1 days"));

            $getTask=TripTask::loggedTravelAgency()->where('staff_id', Auth()->user()->userType->staff_id)
                                                   ->whereBetween(DB::raw('DATE(created_at)'), array($perviousDate, $CurrentDate))->orderBy('created_at','DESC')->get();

            $getFeedback=Feedback::with(['clientgrouptrip','clientgrouptrip.client','clientgrouptrip.clienttrip.trip'])->whereHas('clientgrouptrip.client',function($q){
                                                   $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id);
                                                 })->whereBetween(DB::raw('DATE(created_at)'), array($perviousDate, $CurrentDate))->orderBy('created_at','DESC')->get();

            $getemergencyalerts= EmergencyAlert::whereHas('clientgtouptrip.client',function($q){
                                               $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id);
                                                 })->whereBetween(DB::raw('DATE(created_at)'), array($perviousDate, $CurrentDate))->orderBy('created_at','DESC')->get();

            $getTripMessage= TripMessage::with(['client','trip','agent'])->whereHas('client',function($q){
                                               $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id);
                                                 })->whereBetween(DB::raw('DATE(created_at)'), array($perviousDate, $CurrentDate))->orderBy('created_at','DESC')->get();
            
            $Countclient=Client::where('travel_agency_id',Auth()->user()->userType->travel_agency_id)->count();
            
            if($getAgency->count_trip == $getAgency->package->no_of_trips){
               $tripqota="Your quota to add another trip is finished"; 
            }
            if($Countclient==$getAgency->package->no_of_customers){
                $clientqota="Your quota to add another client is finished";
            }
            $getAllTrip=Trip::where('travel_agency_id',Auth()->user()->userType->travel_agency_id)->get();
            foreach ($getAllTrip as $key => $value) {
                $abc=$value->id;
                $getClientCount=GroupClientTrip::whereHas('clienttrip',function($q) use($abc){
                                             $q->where('trip_id',$abc);
                                             })->count();
                if($getClientCount){
                    $gettotal=$value->capacity-$getClientCount;
                    if($gettotal<=5){
                        //$seats=5-$gettotal;
                       $tripcapacity[] = array(
                       'message' => $gettotal.' more seats left for Trip'." ".$value->title);        
                    }
                }
                
            }
            foreach ($getAllTrip as $key => $value) {
                $getGuideQuota=Guide::where('trip_id',$value->id)->count();
                if($getGuideQuota){
                    if($getGuideQuota==$value->guide_qouta){
                       $tripqouta[] = array(
                       'message' => 'Your quota to add another Guide is finished for Trip'." ".$value->title);
                    }
                }
                
            }
            $to = \Carbon\Carbon::createFromFormat('Y-m-d', $CurrentDate);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d', $getAgency->package_end_date);
            $diff_in_days = $from->diffInDays($to);
            if($diff_in_days<=30 && $diff_in_days>=0){
                $checklicense='Your Ahlan guide license is expiring on'." ".$getAgency->package_end_date;
            }    
            }

        }else if(Auth()->user()->user_type == 'client'){
            $getAgency  =TravelAgency::with('package')->where('id',Auth()->user()->userType->travel_agency_id)->first();
            $newDateMonth= date('Y-m-d', strtotime("+30 days"));
            if($CurrentDate>=$getAgency->package_end_date){
            Auth::logout();
            return view('LicenseExpriy');
            }
            else{
            Auth::logout();
            return view('message');
        }
        }else if(Auth()->user()->user_type == 'mobile_agent'){
            $getAgency  =TravelAgency::with('package')->where('id',Auth()->user()->userType->travel_agency_id)->first();
            $newDateMonth= date('Y-m-d', strtotime("+30 days"));
            if($CurrentDate>=$getAgency->package_end_date){
            Auth::logout();
            return view('LicenseExpriy');
            }
            else{
            $getallTrip = Trip::loggedTravelAgency()->where('end_trip',true)->whereDate('end_date','<=',$CurrentDate)->get();
            foreach ($getallTrip as $key => $value) {
            $tripdata=Trip::where('id',$value->id)->first();
            $tripdata->end_trip=false;
            $tripdata->save();
            $getTripStatus=ClientTrip::where('status','ongoing')->where('trip_id',$value->id)->get();
            foreach ($getTripStatus as $key => $data) {
            $updateData=ClientTrip::where('id',$data->id)->first();
            $updateData->status='completed';
            $updateData->save();
          }
        }
        $tripforselect2 = Trip::loggedTravelAgency()->where('end_trip',true)->get();
        $trips = Trip::loggedTravelAgency()->get();
             return view('trip.index', ['trips' => $trips,'tripforselect2'=>$tripforselect2]);   
            }
        }else{
            $getAgency  =TravelAgency::with('package')->where('id',Auth()->user()->userType->travel_agency_id)->first();
            $newDateMonth= date('Y-m-d', strtotime("+30 days"));
            //return $CurrentDate;
            if($CurrentDate>=$getAgency->package_end_date){
            Auth::logout();
            return view('LicenseExpriy');
            }
            else{
                 $perviousDate=date('Y-m-d', strtotime("-1 days"));
            $total_tasks = TripTask::loggedTravelAgency()->where('staff_id', Auth()->user()->userType->id)->count();
            $completed_tasks = TripTask::loggedTravelAgency()->where('staff_id', Auth()->user()->userType->id)->where('status', 'complete')->count();

            $StaffNotificationData=StaffNotification::with('staffinfo')->where('staff_id', Auth()->user()->userType->id)                                      ->whereBetween(DB::raw('DATE(created_at)'),                           array($perviousDate, $CurrentDate))->orderBy('created_at','DESC')->get();
            //return $StaffNotificationData;
         }
        }
     }
     catch(Exception $e)
     {
        $status="error";
        $message=$e->getMessage();
        Auth::logout();
        return view('auth.login'); 
     }
        return view('home', compact('total_tasks', 'completed_tasks', 'total_clients', 'total_trips','total_messages','overduetask','total_travelAgency','total_packages', 'taskduetodays','emergency_alerts','new_complaint','getFlightDepature','getFeedback','getemergencyalerts','getTripMessage','clientqota','tripqota','tripcapacity','tripqouta','checklicense','StaffNotificationData','getTask','now','newutctime'));
        
    }

    /**
     * Show the form for creating aew resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function SaveImageData(Request $request)
    {
        $status=false;
        $errMsg="";
        if(Auth()->user()->user_type == 'agent')
        {
            $agent=Agent::findOrFail(Auth()->user()->userType->id);
            $imageName = null;
           if($request->hasFile('img'))
            {
            $imageName = $request->file('img')->getClientOriginalName();
            $imageName = time().'_'.$imageName;
            $image       = $request->file('img');

        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(200, 50);
        $image_resize->save(public_path('storage/profile_images/' .$imageName));
            //$path =Storage::disk('public')->putFileAs(
            //    'profile_images', $request->file('img'), $imageName
           // ); 
            
        }
        else
        {
            $imageName = null;
            return "bye";
        }
        $agent->picture_path=$imageName;
        $agentid=$agent->save();
        if($agentid)
        {
            if($agent->staff_id){
             $staffdata=Staff::where('id',$agent->staff_id)->first();
             $staffdata->img=$imageName;
             $staffdata->save();
            }
            $status=true;
            $errMsg="Image Upload Successfully";
        }
        else
        {
            $status=false;
            $errMsg="Image Not Upload";
        }
        }
        else
        {
            $status=false;
            $errMsg="Its Only for Agent";
        }
        return response()->json(['status' => $status,'errMsg'=>$errMsg]);
    }

    public function ShowAdminImage(Request $request)
    {
        $status=false;
        $errMsg="";
        $agent="";
        if(Auth()->user()->user_type == 'agent')
        {
            $agent=Agent::with('travelAgency')->where('id',Auth()->user()->userType->id)->first();
            //return $agent; 
         if($agent)
         {
            $status=true;
            $errMsg="Image Show Successfully";
        }
        else
        {
            $status=false;
            $errMsg="Agent Not Find";
        }

        }
        elseif(Auth()->user()->user_type == 'staff' || Auth()->user()->user_type == 'mobile_agent' )
        {
         $staff=Staff::findOrFail(Auth()->user()->userType->id);
         $agent=Agent::with('travelAgency')->where('user_id',$staff->created_by)->first();
         if($agent)
         {
            $status=true;
            $errMsg="Image Show Successfully";
        }
        else
        {
            $status=false;
            $errMsg="Agent Not Find";
        }
        }
        else
        {
            $status=false;
            $errMsg="Its Only for Agent";
        }
        if($agent==""){
         $status=true;   
        }
        return response()->json(['status' => $status,'errMsg'=>$errMsg,'agent'=>$agent]);
    }

    public function useForSession(Request $request)
    {
        $status=false;
        if($request->type=="completed_tasks"){
            Session::put('checkvalue',1);
            $status=true;
        }
        else if($request->type=="complaint"){
            Session::put('checkvalue',1);
            $status=true;
        }
        else if($request->type=="over_due_task"){
            Session::put('checkvalue',1);
            $status=true;
        }
        else if($request->type=="trip"){
            Session::put('checkvalue',1);
            $status=true;
        }
        
        return response()->json(['status' => $status]);
    }

    public function usefortimezone(Request $request)
    {
        $status="false";
        $getuser=User::where('id',\Auth()->user()->id)->first();
        if($getuser){
            $getuser->login_status = true;
            $getuser->time_zone = $request->timezone;
            $getuser->save();
            $status="true";
        }
        return response()->json(['status' => $status]);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registerFCM($id)
    {
       // $request->validate([
         //   'fcm_token' => 'required'
        //]);
        if($id=='ahlanguide'){

        }
        else{
        Auth()->user()->fcm_token = $id;
        Auth()->user()->save();
        }        
        return redirect(route('homepage'));
    }
}
