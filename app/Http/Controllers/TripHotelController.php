<?php

namespace App\Http\Controllers;

use App\ClientTrip;
use App\ClientTripHotel;
use App\ClientTripStep;
use App\EmergencyAlert;
use App\FeedbackCategory;
use App\Hotel;
use App\Trip;
use App\TripHotel;
use Illuminate\Http\Request;

class TripHotelController extends Controller
{
    public function viewTripCustomerHotel($id)
    {
        $getHotel=TripHotel::with('hotel')->where('trip_id',$id)->get();
        $trip = Trip::findOrFail($id);
        $hotels = Hotel::loggedTravelAgency()->get();
        $getTripGroup=ClientTrip::with('clients')->with('group')->where('trip_id',$id)->get();
        $trip_clients = ClientTrip::where('trip_id', $trip->id)->get();

        return view('trip.trip_hotels.index', compact('trip_clients', 'trip', 'hotels','getTripGroup','getHotel'));
    }

    public function assignHotelToTrip(Request $request)
    {
        $hotelId=0;
        if($request->SelectHotel!=null){
            $agency_id = Auth()->user()->userType->travel_agency_id;
           if($request->mainclientcheckboxes!=null){
            $a = 0;
            foreach ($request->SelectHotel as $hotel){
                if($hotel!=0){
                    $a=$hotel;
                }
                if(is_numeric($request->hotels[$a]))
                {
                    $GetHotelID=$request->hotels[$a];
                }
                else{
                    $hotelget=new Hotel;
                    $hotelget->name=$request->hotels[$a];
                    $hotelget->travel_agency_id=$agency_id;
                    $hotelID=$hotelget->save();
                    if($hotelID){
                     $GetHotelID=$hotelget->id;       
                    }
                }
                $checkHotel=TripHotel::where('trip_id',$request->trip_id)->where('id',$request->hotel_id[$a])->first();
                if($checkHotel){
                    $checkHotel->hotel_id = $GetHotelID;
                    $checkHotel->stay_from = $request->stay_from[$a];
                    $checkHotel->stay_to = $request->stay_to[$a];
                    $checkHotel->status = $request->status[$a];
                    $checkHotelID=$checkHotel->save();
                    if($checkHotelID){
                        $hotelId=$checkHotel->id;
                    }
                }
                else{
                $Hotel =new TripHotel;
                    $Hotel->trip_id =$request->trip_id;
                    $Hotel->hotel_id = $GetHotelID;
                    $Hotel->stay_from = $request->stay_from[$a];
                    $Hotel->stay_to = $request->stay_to[$a];
                    $Hotel->status = $request->status[$a];
                    $HotelID=$Hotel->save();
                    if($HotelID){
                        $hotelId=$Hotel->id;
                    }    
                }
                $checkHotelClient=ClientTripHotel::where('trip_hotel_id',$hotelId)->get();
                foreach ($checkHotelClient as $key => $value) {
                    $deleteallClient=$value->delete();
                }
                foreach ($request->mainclientcheckboxes as $key => $trip_client){
                $checkClientTrip=ClientTripHotel::where('trip_hotel_id',$hotelId)->where('client_trip_id',$trip_client)->first();
                if($checkClientTrip){
                    if($request->Room[$key]!=null){
                        $checkClientTrip->room_number=$request->Room[$key];
                        $checkClientTripid=$checkClientTrip->save();
                        if($checkClientTripid){

                        }
                    }
                }
                else{
                ClientTripHotel::create([
                    'client_trip_id' => $trip_client,
                    'trip_hotel_id' => $hotelId,
                    'room_number' => $request->Room[$key]
                ]);    
                }
            }

            $a++;
        }

        return redirect()->route('view.trip.customer.hotel',$request->trip_id)
            ->with([
                'status' => 'success',
                'message' => 'Hotel has been assign to trip'
            ]);
        }
        else{
            return redirect()->route('view.trip.customer.hotel',$request->trip_id)
            ->with([
                'status' => 'error',
                'message' => 'Please assign client first'
            ]);
        }
    }
        else{
            return redirect()->route('view.trip.customer.hotel',$request->trip_id)
            ->with([
                'status' => 'error',
                'message' => 'Please select at least one Hotel'
            ]);
        }
    }

    public function viewassignhotelclient($hotelId){
        $trips=ClientTripHotel::with(array('clientTrip'=>function($q){
            $q->with(array('client'=>function($q){$q->with('client_category');}))->with(array('clienttrip'=>function($q){
                $q->with('group')->with('trip');
            }));

        }))->where('trip_hotel_id',$hotelId)->get();
        $tripsDetail=$trips;
        return view('trip.trip_hotels.viewassignhotelclient', compact('trips','tripsDetail'));
    }

    public function viewHotelTripByCustomerID(Request $request){
        //return $request;
           $id=$request->client_id;
           $trips=ClientTripHotel::whereHas('clientTrip',function($q) use($id){
            $q->whereHas('client', function($q) use($id){
                          $q->where('id',$id);
                          });})->with(array('clientTrip'=>function($q){
            $q->with(array('client'=>function($q){$q->with('client_category');}))->with(array('clienttrip'=>function($q){
                $q->with('group')->with('trip');
            }));}))->where('trip_hotel_id',$request->hotelid)->get();
                          //return $trips;
           $tripsDetail=ClientTripHotel::with(array('clientTrip'=>function($q){
            $q->with(array('client'=>function($q){$q->with('client_category');}))->with(array('clienttrip'=>function($q){
                $q->with('group')->with('trip');
            }));

        }))->where('trip_hotel_id',$request->hotelid)->get();
          // return $trips;
          return view('trip.trip_hotels.viewassignhotelclient', compact('trips','tripsDetail'));
    }

    public function assignHotelToSingleCustomer(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'client_trip_id' => 'required|integer',
            'hotel' => 'required|integer',
            'stay_from' => 'required|string',
            'stay_to' => 'required|string',
            'status' => 'required|string'
        ]);
        ClientTripHotel::create([
            'client_trip_id' => $request->client_trip_id,
            'hotel_id' => $request->hotel,
            'stay_from' => $request->stay_from,
            'stay_to' => $request->stay_to,
            'status' => $request->status
        ]);

        return redirect()->route('trips.index')->With([
            'status' => 'success',
            'message' => 'Hotel has been assign to specific customer'
        ]);
    }

    public function hoteldestroy($id){
        $clientTrip=ClientTripHotel::where('id',$id)->first();
        if($clientTrip){
            $clienttripid=$clientTrip->delete();
            if($clienttripid){
                return redirect()->route('trip.hotel.viewassignhotelclient',$clientTrip->trip_hotel_id)
            ->with([
                'status' => 'success',
                'message' => 'Client Successfully Deleted'
            ]);
            }
            else{
                  return redirect()->route('trip.hotel.viewassignhotelclient',$clientTrip->trip_hotel_id)
            ->with([
                'status' => 'error',
                'message' => 'Client Can not Delete'
            ]);
        }
    }
        else{
          return redirect()->route('trip.hotel.viewassignhotelclient',$clientTrip->trip_hotel_id)
            ->with([
                'status' => 'error',
                'message' => 'Client can not exist'
            ]);    
        }
        
    }

    public function deletehotelbyid(Request $request){
      $status=0;
      $getHotelDetail=TripHotel::find($request->deletedata);
      if($getHotelDetail){
        $getAssignClient=ClientTripHotel::where('trip_hotel_id',$getHotelDetail->id)->get();
        if($getAssignClient){
            foreach ($getAssignClient as $key => $value) {
                $deleteclient=$value->delete();
                if($deleteclient){
                $status=1;   
                }
            }
        }
        $deleteHotelid=$getHotelDetail->delete();
        if($deleteHotelid){
            $status=1;
        }
    }
    else{
        $status=0;
    }
    
      return response()->json(['status' => $status]);
  }
}
