<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airline;
use App\TravelAgency;
use App\Country;
use App\State;
use App\City;

class AirlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $airlines = Airline::loggedTravelAgency()->get();
        // $travelAgency = TravelAgency::all();
         
        return view('airlines.index', compact('airlines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $airlines = Airline::all();
        $agencies = TravelAgency::all();
        $countries = Country::all();
        return view('airlines.create', compact('agencies', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'              => 'required',
            'country'           => 'required',
        ]);

        $airline                    = new Airline;
        $airline->travel_agency_id     = Auth()->user()->userType->travelAgency->id;
        $airline->name                 = $request->name;
        $airline->country_id           = $request->country;
//        $airline->state_id             = $request->state;
//        $airline->city_id              = $request->city;
//        $airline->address              = $request->address;
//        $airline->phone_number         = $request->phone_number;

        $airline->save();

        return redirect(route('airlines.index'))->with(['status' => "success", 'message' => "Airline Added Successfully!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $airline = Airline::findOrFail($id);
        return view('airlines.show', compact('airline'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $airline = Airline::findOrFail($id);
        $agencies = TravelAgency::all();
        $countries = Country::all();
//        $states = State::where('country_id', $airline->country_id)->get();
//        $cities = City::where('state_id', $airline->state_id)->get();
         return view('airlines.edit', compact('airline','agencies', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
//            'travel_agency'     => 'required',
            'name'              => 'required',
            'country'           => 'required',
//            'state'             => 'required',
//            'city'              => 'required',
//            'address'           => 'required',
//            'phone_number'      => 'required'
        ]);
        $airline = Airline::findOrFail($id);
        $airline->update([
            'travel_agency_id' => Auth()->user()->userType->travelAgency->id,
            'name' => $request->name,
            'country_id' => $request->country,
//            'state_id' => $request->state,
//            'city_id' => $request->city,
//            'address' => $request->address,
//            'phone_number' => $request->phone_number
        ]);

        return redirect(route('airlines.index'))->with(['status' => "success", 'message' => "Airline Updated Successfully!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $airline = Airline::findOrFail($id);
        $airline->delete();

        return redirect(route('airlines.index'))->with(['status' => "success", 'message' => "Airline Deleted Successfully!"]);
    }
}
