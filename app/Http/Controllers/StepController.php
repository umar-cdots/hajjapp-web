<?php

namespace App\Http\Controllers;

use App\Step;
use App\StepPack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StepController extends Controller
{
    public function create(){
        $step_packs = Auth()->user()->agent->travelAgency->stepPack;
        
        return view('step.create', compact('step_packs'));
    }

    public function store(Request $request){
        $this->validate($request, [
           'title' => 'required|string|min:3',
           // 'last_video_path' => 'mimes:mp4,3gp|max:1000000'
        ]);
        $videoName = null;
        if( $request->hasFile('last_video_path')){
            $videoName = $request->file('last_video_path')->getClientOriginalName();
            $videoName = time().'_'.$videoName;

            $path = Storage::disk('public')->putFileAs(
                'step_videos', $request->file('last_video_path'), $videoName
            );
        }else{
            $videoName = null;
        }

        Step::create([
           'title' => $request->title,
           'last_video_path' => $videoName
        ]);

        return back()->with(['status' => 'success', 'message' => 'Step has been created.']);
    }
}
