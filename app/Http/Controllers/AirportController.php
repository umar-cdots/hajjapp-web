<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airport;
use App\Country;
use App\City;
use App\State;

class AirportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $airports = Airport::loggedTravelAgency()->get();
        return view('airports.index', compact('airports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('airports.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => 'required',
            'code'      => 'required',
            'country'   => 'required',
            'city'      => 'required'
        ]);

        $airport = new Airport;
        $airport->name       = $request->name;
        $airport->code       = $request->code;
        $airport->country_id = $request->country;
        $airport->travel_agency_id       = Auth()->user()->userType->travel_agency_id;
        $airport->city_id    = $request->city;

        $airport->save();

         return redirect(route('airports.index'))->with(['status' => "success", 'message' => "Airport Added Successfully!"]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $airport = Airport::findOrFail($id);
        return view('airports.show', compact('airport'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $airports = Airport::findOrFail($id);
        $countries = Country::all();
//        $states = State::where('country_id', $airports->country_id)->get();
//        $cities = City::where('state_id', $airports->state_id)->get();
        return view('airports.edit', compact('airports', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'name'      => 'required',
            'code'      => 'required',
            'country'   => 'required',
            'city'      => 'required'
        ]);

        $airport             = Airport::findOrFail($id);
        $airport->name       = $request->name;
        $airport->code       = $request->code;
        $airport->country_id = $request->country;
        $airport->city_id    = $request->city;

        $airport->save();

         return redirect(route('airports.index'))->with(['status' => "success", 'message' => "Airport Updated Successfully!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $airport = Airport::findOrFail($id);
        $airport->delete();

        return redirect(route('airports.index'))->with(['status' => "success", 'message' => "Airport Deleted Successfully!"]);
    }
}
