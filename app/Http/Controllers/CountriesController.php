<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        return view('countries.index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'              => 'required',
            'short_name'        => 'required',
            'phone_code'        => 'nullable|numeric',
        ]);

        $county                    = new Country;
        $county->name                 = $request->name;
        $county->short_name           = $request->short_name;
        $county->phone_code           = $request->phone_code;

        $county->save();

        return redirect(route('countries.index'))->with(['status' => "success", 'message' => "Country Added Successfully!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        return view('countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'              => 'required',
            'short_name'        => 'required',
            'phone_code'        => 'nullable|numeric',
        ]);

        $county                    =Country::findOrFail($id);
        $county->name                 = $request->name;
        $county->short_name           = $request->short_name;
        $county->phone_code           = $request->phone_code;

        $county->save();

        return redirect(route('countries.index'))->with(['status' => "success", 'message' => "Country updated Successfully!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Country = Country::findOrFail($id);
        $Country->delete();

        return redirect(route('countries.index'))->with(['status' => "success", 'message' => "Country Deleted Successfully!"]);
    }
}
