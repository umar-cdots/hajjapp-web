<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Hotel;
use App\State;
use App\City;
use Illuminate\Support\Facades\Storage;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = Hotel::with(['country','state','city'])->loggedTravelAgency()->get();
        return view('hotels.index', compact('hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('hotels.create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'              => 'required',
            'phone_number'      => "nullable|numeric|digits_between:11,20",
            "path"              => "image|mimes:png,jpg,jpeg|max:5120|nullable",
        ]);

        $imageName = null;
        if($request->hasFile('path'))
        {
            $imageName = $request->file('path')->getClientOriginalName();
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'hotel_images', $request->file('path'), $imageName
            );
        }
        else
        {
            $imageName = null;
        }

        $hotel                       = new Hotel;
        $hotel->name                 = $request->name;
        $hotel->travel_agency_id     = \Auth::user()->user_type == 'super_admin' ? $request->travel_agency_id : \Auth::user()->userType->travel_agency_id;
        $hotel->country_id           = $request->country;
        $hotel->state_id             = $request->state;
        $hotel->city_id              = $request->city;
        $hotel->address              = $request->address;
        $hotel->map_url              = $request->map_url;
        $hotel->phone_number         = $request->phone_number;
        $hotel->path                 = $imageName;

        $hotel->save();

        return redirect(route('hotels.index'))->with(['status' => "success", 'message' => "Hotel Added Successfully!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::all();
        $hotel = Hotel::with(['country','state','city'])->where('id',$id)->first();
        return view('hotels.edit', compact('hotel','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'              => 'required',
            'phone_number'      => "nullable|numeric|digits_between:11,20",
            "path"              => "image|mimes:png,jpg,jpeg|max:5120|nullable",
        ]);

        $imageName = null;
        if($request->hasFile('path'))
        {
            $imageName = $request->file('path')->getClientOriginalName();
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'hotel_images', $request->file('path'), $imageName
            );
        }
        else
        {
            $imageName = null;
        }
        if($imageName == null){
            $hotel                       =Hotel::findOrFail($id);
        $hotel->name                 = $request->name;
        $hotel->travel_agency_id     = \Auth::user()->user_type == 'super_admin' ? $request->travel_agency_id : \Auth::user()->userType->travel_agency_id;
        $hotel->country_id           = $request->country;
        $hotel->state_id             = $request->state;
        $hotel->city_id              = $request->city;
        $hotel->address              = $request->address;
        $hotel->map_url              = $request->map_url;
        $hotel->phone_number         = $request->phone_number;

        $hotel->save();
        }
        else{
            $hotel                       =Hotel::findOrFail($id);
        $hotel->name                 = $request->name;
        $hotel->travel_agency_id     = \Auth::user()->user_type == 'super_admin' ? $request->travel_agency_id : \Auth::user()->userType->travel_agency_id;
        $hotel->country_id           = $request->country;
        $hotel->state_id             = $request->state;
        $hotel->city_id              = $request->city;
        $hotel->address              = $request->address;
        $hotel->map_url              = $request->map_url;
        $hotel->phone_number         = $request->phone_number;
        $hotel->path                 = $imageName;

        $hotel->save();
        }

        

        return redirect(route('hotels.index'))->with(['status' => "success", 'message' => "Hotel Updated Successfully!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Hotel = Hotel::findOrFail($id);
        $Hotel->delete();

        return redirect(route('hotels.index'))->with(['status' => "success", 'message' => "Hotel Deleted Successfully!"]);
    }
}
