<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TravelAgency;
use App\User;
use App\Client;
use App\Staff;
use App\Agent;
use App\Package;
use Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TravelAgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $travelAgencies = TravelAgency::with(['agent','agent.user'])->whereHas('agent',function($q){
                                                       $q->whereNull('staff_id');
                                                      })->get();
        //return $travelAgencies;
        return view('travel_agency.index', compact('travelAgencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $packages       = Package::all();
        return view('travel_agency.create', compact('packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
//            'package'        => "sometimes|exists:packages,id",
                'agency_name'       => "required|unique:travel_agencies|regex:/^[\pL\s\-]+$/u",
                'email'             => "required|unique:users",
                'name'              => "required|regex:/^[\pL\s\-]+$/u",
                'password'          => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
                'phone_no'          => "required|numeric|min:11",
                'phone_no_sec'      => "required|numeric|min:11",
                'path'              => "required",
                'start_date'        => 'sometimes',
                'end_date'          => 'sometimes',
                'package'           =>'required'
            ],
            [
                'password.regex' => 'Password must contain at least one number and both uppercase and lowercase letters.'
            ]);

        $travelAgency                 = new TravelAgency;
        if($request->has('package')):
             $travelAgency->package_id           = $request->package;
             $travelAgency->package_start_date   = $request->start_date;
             $travelAgency->package_end_date   = $request->end_date;
        endif;
        $imageName = null;
        if($request->hasFile('path'))
        {
            $imageName = $request->file('path')->getClientOriginalName();
            $imageName = time().'_'.$imageName;
            $image       = $request->file('path');

            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(200, 50);
            $image_resize->save(public_path('storage/profile_images/' .$imageName));

        }
        else
        {
            $imageName = null;
        }
        $travelAgency->agency_name          = $request->agency_name;
        $travelAgency->path                 = $imageName;
        $travelAgency->status               = "active";
        $travelAgency->save();

        $user             = new User;
        $user->email      = $request->email;
        $user->name       = $request->name;
        $user->password   = bcrypt($request->password);
        $user->user_type  = "agent";
        $user->status     ="active";
        $user->save();

        $agent                      = new Agent;
        $agent->travel_agency_id    = $travelAgency->id;
        $agent->user_id             = $user->id;
        $agent->first_name          = explode(' ', $user->name)[0];
        $agent->last_name           = explode(' ', $user->name)[1] ?? '';
        $agent->phone_no            = $request->phone_no;
        $agent->phone_no_sec        = $request->phone_no_sec;
        $agent->picture_path        =$imageName;
        $agent->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Agency Added Successfully!');
        return redirect(route('travel_agency.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $travel_agency = TravelAgency::findOrFail($id);
        $agent = Agent::with('user')->where('travel_agency_id', $travel_agency->id)->first();
        return view('travel_agency.show', compact('travel_agency','agent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $travel_agency  = TravelAgency::findOrFail($id);
        $packages       = Package::all();
        $agent = Agent::where('travel_agency_id', $travel_agency->id)->whereNull('staff_id')->first();
        return view('travel_agency.edit', compact('travel_agency', 'packages','agent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->validate($request,[
//            'agency_name'       => "required",
             'name'              => "required|regex:/^[\pL\s\-]+$/u",
             'email'             => "required",
             'phone_no'          => "required|numeric|min:11",
             'phone_no_sec'      => "required|numeric|min:11",
             'path'              => "sometimes|mimes:png,jgp,jpeg",
             'start_date'        => 'sometimes',
             'end_date'          => 'sometimes',
             'package'           =>'required'
        ]);
        $travel_agency = TravelAgency::findOrFail($id);
       // return $travel_agency;
        // $travel_agency->agency_name = $request->agency_name;
        //if($request->has('package')):
          //   $travelAgency->package_id           = $request->package;
            // $travelAgency->package_start_date   = $request->start_date;
             //$travelAgency->package_end_date   = $request->end_date;
        //endif;
        $imageName = null;
        if($request->hasFile('path'))
        {
            $imageName = $request->file('path')->getClientOriginalName();
            $imageName = time().'_'.$imageName;
            $image       = $request->file('path');

            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(200, 50);
            $image_resize->save(public_path('storage/profile_images/' .$imageName));
        }
        else
        {
            $imageName = null;
        }
        $user             = User::findOrFail($request->user_id);
        $user->name       = $request->name;
        $user->email      = $request->email;
        $user->save();
        if($imageName == null){
        if($request->has('package')):
             $travel_agency->package_id  = $request->package;
             $travel_agency->package_start_date   = $request->start_date;
             $travel_agency->package_end_date   = $request->end_date;
              $travel_agency->save();
         endif;
         $agent                      = Agent::findOrFail($request->agent_id);
        $agent->travel_agency_id    = $travel_agency->id;
        $agent->user_id             = $user->id;
        $agent->first_name          = explode(' ', $user->name)[0];
        $agent->last_name           = explode(' ', $user->name)[1] ?? '';
        $agent->phone_no            = $request->phone_no;
        $agent->phone_no_sec        = $request->phone_no_sec;
        $agent->save();
        }
        else{
            $travel_agency->path         = $imageName;
        if($request->has('package')):
             $travel_agency->package_id  = $request->package;
             $travel_agency->package_start_date   = $request->start_date;
             $travel_agency->package_end_date   = $request->end_date;
         endif;
        $travel_agency->save();
        $agent                      = Agent::findOrFail($request->agent_id);
        $agent->travel_agency_id    = $travel_agency->id;
        $agent->user_id             = $user->id;
        $agent->first_name          = explode(' ', $user->name)[0];
        $agent->last_name           = explode(' ', $user->name)[1] ?? '';
        $agent->phone_no            = $request->phone_no;
        $agent->phone_no_sec        = $request->phone_no_sec;
        $agent->picture_path        =$imageName;
        $agent->save();
        }
        

        

        

        // $package = \App\Package::find($id);
        // $package->package_name = $request->package_name;
        // $package->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Agency Updated Successfully!');
        return redirect(route('travel_agency.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $travel_agency = TravelAgency::findOrFail($id);
        $getAllCustomer=Client::where('travel_agency_id',$id)->get();
        foreach ($getAllCustomer as $key => $customer) {
          User::where('id',$customer->user_id)->delete();
        }
        $getAllStaff=Staff::where('travel_agency_id',$id)->get();
        foreach ($getAllStaff as $key => $staff) {
            User::where('id',$staff->user_id)->delete();
        }
        $getAllAgent=Agent::where('travel_agency_id',$id)->get();
        foreach ($getAllAgent as $key => $agent) {
            User::where('id',$agent->user_id)->delete();
        }
        $travel_agency->delete();

        return redirect(route('travel_agency.index'))->with(['status' => "success", 'message' => "Agency Removed Successfully!"]);
    }
}
