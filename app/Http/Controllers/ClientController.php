<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientCategory;
use App\ClientSubCategory;
use App\Nationality;
use App\User;
use App\EmergencyAlert;
use App\Trip;
use App\TripTask;
use App\ClientTrip;
use App\GroupClientTrip;
use App\FeedbackCategory;
use App\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Jobs\SendEmailJob;
use App\Auth;
use App\Agent;
use App\ClientTripAirline;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::loggedTravelAgency()->orderBy('id','DESC')->get();
        $clientforselect2 = Client::loggedTravelAgency()->where('status',true)->get();
        return view('client.index', compact('clients','clientforselect2'));
    }

    public function search(Request $request)
    {
        $clientforselect2 = Client::loggedTravelAgency()->orderBy('id','DESC')->where('status',true)->get();
        if($request->client_id)
        {
            $clients = Client::where('id',$request->client_id)->orderBy('id','DESC')->get();
        }
        else
        {
            $clients = Client::loggedTravelAgency()->orderBy('id','DESC')->get();
        }

        return view('client.index', ['clients' => $clients,'clientforselect2'=>$clientforselect2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getdata=Agent::with(['travelAgency','travelAgency.package'])->where('id',Auth()->user()->userType->id)->first();
            $getAllAgent=Agent::where('travel_agency_id',$getdata->travel_agency_id)->get();
            $totalclient=0;
            foreach ($getAllAgent as $key => $value) {
                $Countclient=Client::where('created_by_agent_id',$value->id)->count();
                $totalclient=$totalclient+$Countclient;
            }
            if($totalclient<$getdata->travelagency->package->no_of_customers){
        $client_categories  = ClientCategory::loggedTravelAgency()->get();
        $nationalities      = Nationality::all();

        return view('client.create', compact('client_categories', 'nationalities'));
    }
    else{
    return redirect()->route('clients.index')->with(['status' => 'error', 'message' => 'You cannot add new client because your client quota was full. ']);   
    }
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
              "name" => "required|min:2|regex:/^[\pL\s\-]+$/u",
              "email" => 'required|email|string|max:255|unique:users',
              //'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
              // "customer_category"  => "required",
              "primary_number" => "required|numeric|digits_between:11,20",
              "secondary_number" => "numeric|nullable|digits_between:11,20",
              "emergency_number" => "numeric|nullable|digits_between:11,20",
              "occupation" => "string||nullable",
              "blood_group" => "string||nullable",
              "decease" => "string|nullable",
              "special_needs" => "string|nullable",
              "gender" => "required|string|nullable",
              "age" => "numeric|nullable",
              "qualification" => "string|nullable",
              "postal_address" => "string|nullable",
              "personality_comment" => "string|nullable",
              "profile_image" => "image|mimes:png,jpg,jpeg|max:5120|nullable",
        ]);
        $imageName = null;
        if($request->hasFile('image'))
        {
            $imageName = $request->file('image')->getClientOriginalName();
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'profile_images', $request->file('image'), $imageName
            );
        }
        else
        {
            $imageName = null;
        }

        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => Hash::make("ahlanguide123"),
            "user_type" => 'client',
            "status" => 'active'
        ]);


        $client = Client::create([
              "user_id" => $user->id,
              "name" => $request->name,
              "client_category_id"   => $request->client_category_id,
              "sub_client_category_id"    => $request->client_sub_category_id,
              "primary_phone_number" => $request->primary_number,
              "secondary_phone_number" => $request->secondary_number,
              "emergency_phone_number" => $request->emergency_number,
              "occupation" => $request->occupation,
              "blood_group" => $request->blood_group,
              "decease" => $request->decease,
              "special_needs" => $request->special_needs,
              "nationality" => $request->nationality,
              "postal_address" => $request->postal_address,
              'dob' => $request->dob,
              "gender" => $request->gender,
              "age" => $request->age,
              "qualification" => $request->qualification,
              "personality_comments" => $request->personality_comment,
              "passport_no"         => $request->passport_no,
              "cnic"                => $request->cnic,
              "picture_path" => $imageName,
              "total_paid_amount" => $request->total_paid_amount,
              "description" => $request->description,
              "paid_amount" =>$request->paid_amount,
              "status" => true,
              "created_by_agent_id" => Auth()->user()->userType->id,
              'travel_agency_id' => Auth()->user()->userType->travelAgency->id
        ]);
        $clientdata = array(
                'email' => $user->email, 
                'password' => 'ahlanguide123'
            );
         //dispatch(new SendEmailJob($user->email,Auth()->user()->email,$clientdata));
         Mail::to($user->email)->send(new SendMail(Auth()->user()->email,$clientdata));
         return redirect()->route('clients.index')->with(['status' => 'success', 'message' => 'Client has been added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user   = User::findOrFail($id);
        // dd($user->userType->primary_phone_number);
        return view('client.show', compact('user','client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $client_categories = ClientCategory::loggedTravelAgency()->get();
        $subclient_categories = ClientSubCategory::where('id',$user->client['sub_client_category_id'])->first();
        $nationalities = Nationality::all();

        return view('client.edit', compact('user', 'client_categories', 'nationalities','subclient_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        $user = User::findOrFail($id);
        $this->validate($request, [
            "name" => "required|min:2|regex:/^[\pL\s\-]+$/u",
            "email" => 'required|email|string|max:255',
            "primary_number" => "required|numeric|digits_between:11,20",
            "secondary_number" => "numeric|nullable|digits_between:11,20",
            "emergency_number" => "numeric|nullable|digits_between:11,20",
           // 'password' => 'nullable|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            "occupation" => "string|nullable",
            "blood_group" => "string|nullable",
            "decease" => "string|nullable",
            "special_needs" => "string|nullable",
            "dob" => 'nullable',
            "gender" => "required|string",
            "age" => "numeric|nullable",
            "qualification" => "string|nullable",
            "postal_address" => "string|nullable",
            "personality_comment" => "string|nullable",
            "profile_image" => "image|mimes:png,jpg,jpeg|max:5120"
        ]);

        $imageName = null;
        if( $request->hasFile('image')){
            $imageName = $request->file('image')->getClientOriginalName();
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'profile_images', $request->file('image'), $imageName
            );
        }else{
            $imageName = $user->client->picture_path;
        }
          $user->update([
            "name" => $request->name,
            "email" => $request->email,
            "user_type" => 'client',
            "status" => 'active'
        ]);
        if($imageName==null)
        {
          $user->client->update([
            "name" => $request->name,
            "client_category_id"   => $request->client_category_id,
            "sub_client_category_id"    => $request->client_sub_category_id,
            "primary_phone_number" => $request->primary_number,
            "secondary_phone_number" => $request->secondary_number,
            "emergency_phone_number" => $request->emergency_number,
            "occupation" => $request->occupation,
            "blood_group" => $request->blood_group,
            "decease" => $request->decease,
            'dob' => $request->dob,
            "special_needs" => $request->special_needs,
            "nationality" => $request->nationality,
            "gender" => $request->gender,
            "age" => $request->age,
            "qualification" => $request->qualification,
            "postal_address" => $request->postal_address,
            "personality_comments" => $request->personality_comment,
            "total_paid_amount" => $request->total_paid_amount,
            "description" => $request->description,
            "paid_amount" =>$request->paid_amount,
            "created_by_agent_id" => Auth()->user()->userType->id
        ]);
        }
        else
        {
          $user->client->update([
            "name" => $request->name,
            "client_category_id"   => $request->client_category_id,
            "sub_client_category_id"    => $request->client_sub_category_id,
            "primary_phone_number" => $request->primary_number,
            "secondary_phone_number" => $request->secondary_number,
            "emergency_phone_number" => $request->emergency_number,
            "occupation" => $request->occupation,
            "blood_group" => $request->blood_group,
            "decease" => $request->decease,
            'dob' => $request->dob,
            "special_needs" => $request->special_needs,
            "nationality" => $request->nationality,
            "gender" => $request->gender,
            "age" => $request->age,
            "qualification" => $request->qualification,
            "postal_address" => $request->postal_address,
            "personality_comments" => $request->personality_comment,
            "picture_path" => $imageName,
            "total_paid_amount" => $request->total_paid_amount,
            "description" => $request->description,
            "paid_amount" =>$request->paid_amount,
            "created_by_agent_id" => Auth()->user()->userType->id
        ]);
        }
        return redirect()->route('clients.index')->with(['status' => 'success', 'message' => 'Client has been updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->client->delete();
        $user->delete();

        return back()->with([
                'status' => 'success',
                'message' => 'Client has been deleted'
            ]);
    }

    public function showprofile($id)
    {
      $CurrentDate=date('Y-m-d');
        $new_complaint='';
        $total_trips='';
        $overduetask='';
        $new_alerts=0;
        $ClientID=$id;

        $getComplaintId=FeedbackCategory::where('title','Complaints')->first();
            if($getComplaintId){
                $new_complaint = Feedback::whereHas('clientgrouptrip',function($q) use($ClientID){
                                           $q->whereHas('client',function($q) use($ClientID){
                                            $q->where('user_id',$ClientID);
                                           });
                                          })->where('feedback_category_id',$getComplaintId->id)->count();
            }
            else{
                $new_complaint = 0;
            }
        //$total_trips = Trip::where('travel_agency_id', Auth()->user()->userType->id)->where('end_trip',true)->count();
        //dd(User::findOrFail($ClientID)->client->trips);

        //$total_trips = ClientTrip::with('clients')->with('trip')->with('group')->get();

        $total_trips = ClientTrip::whereHas('clients', function($q) use ($ClientID){
                                         $q->where('user_id',$ClientID);
                                         })->whereHas('trip', function($q){
                                         $q->where('end_trip',true);
                                      })->count();
          
        //$total_trips = Trip::where('client_id', $ClientID->client->id)->where('end_trip', 1)->count();

          $overduetask1 = ClientTrip::with(array('clients'=>function($query) use($ClientID){
                                     $query->where('user_id',$ClientID);
                                     }))->with(array('trip'=>function($q){
                                          $q->with(array('tripTasks'=>function($q){
                                          }));
                                         }))->get();
          $new_alerts =EmergencyAlert::whereHas('clientgtouptrip',function($q) use($ClientID){                               $q->whereHas('client',function($q) use($ClientID){
                                       $q->where('user_id',$ClientID);
                                       }); 
                                      })->count();

         $overduetask = ClientTrip::whereHas('clients', function($q) use ($ClientID){
                                         $q->where('user_id',$ClientID);
                                         })
                                       ->
                                        whereHas('trip', function($q) use($CurrentDate){
                                         $q->whereHas('tripTasks', function($q) use($CurrentDate){
                                         $q->where('due_date','<',$CurrentDate);
                                      });
                                      })->count();
                                                                       

        $user = User::findOrFail($id);
        $clientid=$user->id;
        $nextflight=ClientTripAirline::with('flight')->whereHas('clienttrip.client',function($q) use($clientid){
                                        $q->where('user_id',$clientid);
                                        })->whereHas('flight',function($q) use($CurrentDate){
                                        $q->where('departure_datetime','>',$CurrentDate);
                                        })->first();
        //return $nextflight;
        $client_categories = ClientCategory::all();
        $subclient_categories = ClientSubCategory::where('id',$user->client['sub_client_category_id'])->first();
        $nationalities = Nationality::all();

        return view('client.showprofile', compact('user', 'client_categories', 'nationalities','total_trips','new_complaint','overduetask','subclient_categories','new_alerts','nextflight'));
    }

    public function ChangeClientStatus(Request $request)
    {
        $client   = Client::findOrFail($request->id);
        $client->status=$client->status==0?true:false;
        $client->save();
        $clientforselect2 = Client::loggedTravelAgency()->where('status',true)->get();

        $status=$client->status==0?"warning":"success";
        return response()->json(['status' => $status,'clientforselect2'=>$clientforselect2]);
    }

    public function clientcomplaint($id){
      $ClientID=$id;
      $getComplaintId=FeedbackCategory::where('title','Complaints')->first();
        if($getComplaintId){
          $compliantfeedback=Feedback::whereHas('clientgrouptrip',function($q) use($ClientID){
                                           $q->whereHas('client',function($q) use($ClientID){
                                            $q->where('user_id',$ClientID);
                                           });
                                          })->with(['clientgrouptrip','clientgrouptrip.client','clientgrouptrip.client.client_category','clientgrouptrip.clienttrip','clientgrouptrip.clienttrip.trip','clientgrouptrip.clienttrip.group'])->with('feedbackCategory')->where('feedback_category_id',$getComplaintId->id)->get();
        }
        else{
        $compliantfeedback=[];
        }
        //foreach ($allfeedbacks as $key => $value) {
        //  return $value->clientgrouptrip->client->client_category;
        //}
        //return $allfeedbacks;
        return view('client.clientcomplaint', compact('compliantfeedback'));
    }

    public function clientalert($id){
      $ClientID=$id;
      $emergency_alerts= EmergencyAlert::whereHas('clientgtouptrip',function($q) use($ClientID){                               $q->whereHas('client',function($q) use($ClientID){
                                       $q->where('user_id',$ClientID);
                                       }); 
                                      })->with(['clientgtouptrip.clienttrip.group'])->
                                         orderBy('id','DESC')->get();
        return view('client.clientalert', compact('emergency_alerts'));
    }
}
