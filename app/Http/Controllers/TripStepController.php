<?php

namespace App\Http\Controllers;

use App\Step;
use App\StepPack;
use App\StepSection;
use App\StepSectionCheckList;
use App\Trip;
use Illuminate\Http\Request;

class TripStepController extends Controller
{
    public function index($trip_id)
    {
        $trip = Trip::findOrFail($trip_id);
        $trip_steps = $trip->stepPack->steps;
        return $trip_steps;
        return view('step.index', compact('trip_steps'));
    }

    public function addCustomStepPack(Request $request)
    {
        $this->validate($request, [
            'title' => 'string|required',
        ]);

        //create the step pack
        $step_pack = StepPack::create([
            'title' => $request->title,
            'travel_agency_id' => Auth()->user()->userType->travelAgency->id
        ]);
        //assign step pack to trip
        Trip::find($request->trip_id)->update([
            'step_pack_id' => $step_pack->id
        ]);
        return response()->json(['status' => 'success', 'step_pack' => $step_pack]);
    }

    public function assignStepPackToTrip(Request $request)
    {
        if ($request->has('existing')) {
            $trip = Trip::findOrFail($request->trip_id);
            $trip->update([
                'step_pack_id' => $request->step_pack_id
            ]);
        } else {
            //check is any steppack exist with same name
            $step_pack = StepPack::where('title', $request->step_pack_title)->orderByDESC('id')->first();
            if (!empty($step_pack)) {
                $step_pack_title = $step_pack->title . '-1';
            } else {
                $step_pack_title = $request->step_pack_title;
            }
            //create new steppack
            $new_step_pack = StepPack::create([
                'title' => $step_pack_title,
                'travel_agency_id' => Auth()->user()->userType->travelAgency->id
            ]);

            foreach ($request->step_title as $key => $step_title) {
                $new_step = Step::create([
                    'title' => $step_title,
                    'step_pack_id' => $new_step_pack->id
                ]);

                $a = 0;
                foreach ($request->section_title[$key] as $key2 => $section_title) {
                    $new_section = StepSection::create([
                        'step_id' => $new_step->id,
                        'title' => $section_title,
                        'description' => $request->message[$key][$a],
                        'information' => $request->important_message[$key][$a]
                    ]);

                    foreach ($request->check_list[$key][$key2] as $checklist) {
                        StepSectionCheckList::create([
                            'step_section_id' => $new_section->id,
                            'description' => $checklist
                        ]);
                    }
                    $a++;
                }
            }

            $trip = Trip::findOrFail($request->trip_id);
            $trip->update([
                'step_pack_id' => $new_step_pack->id
            ]);
        }

        return redirect()->route('trips.index')->with([
            'status' => 'success',
            'message' => 'Step Pack has been add to trip'
        ]);
    }
}
