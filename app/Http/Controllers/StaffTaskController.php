<?php

namespace App\Http\Controllers;

use App\TripTask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class StaffTaskController extends Controller
{
    public function index(){
        if(Session::has('checkvalue')){
            $trip_tasks = TripTask::with('trip')->where('staff_id', Auth()->user()->userType->id)->where('status','complete')->get();
            Session::forget('checkvalue');
        }
        else{
        $trip_tasks = TripTask::with('trip')->where('staff_id', Auth()->user()->userType->id)->get();    
        }
        

    	return view('staff_task.index', compact('trip_tasks'));
    }

    public function completeTask(Request $request){
        $success=false;
        $today_date = date('Y-m-d H:i:s');
       // dd($now_time);
        $trip_task = TripTask::findOrFail($request->task_id);
        $trip_task->update([
            'actual_date' => $today_date,
            'status' => 'complete',
            'comment'=>$request->comment
        ]);
        $success=true;
        return response()->json(['success' => $success]);
    }
}
