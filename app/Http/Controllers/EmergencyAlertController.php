<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmergencyAlert;
use App\TripUpdate;

class EmergencyAlertController extends Controller
{
    public function index($type = 'all')
    {
       $emergency_alerts= EmergencyAlert::with(['clientgtouptrip.clienttrip.group'])
                                          ->whereHas('clientgtouptrip.client',function($q){
                                            $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id);
                                          })
                                          ->orderBy('id','DESC')->get();
                                         
       //return $emergency_alerts;
    	//$client_trip_ids = \App\ClientTrip::whereIn('trip_id', \App\Trip::loggedTravelAgency()->pluck('id'))->pluck('id');
    	//if($type == 'all')
    	//{
    		//$emergency_alerts = EmergencyAlert::whereIn('client_trip_id', $client_trip_ids)
    		//									->get();
    	//}
    	//else
    	//{
    	//	$emergency_alerts = EmergencyAlert::whereIn('client_trip_id', $client_trip_ids)
    	//										->where('status', 'sent')
    	//											->get();
        //}
    	return view('emergency_alert.index', compact("emergency_alerts"));
    }

    public function update(Request $request, $id)
    {
    	$request->validate([
    		'status'	=> "required|in:seen,solved"
    	]);

    	$emergency_alert 			= EmergencyAlert::findOrFail($id);
    	$emergency_alert->status 	= $request->status;
    	$emergency_alert->save();

    	return back()->with(['status' => "success", 'message' => "Status Updated."]);
    }
}
