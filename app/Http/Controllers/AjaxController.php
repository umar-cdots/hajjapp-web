<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\State;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function loadStates(Request $request){
        $states = State::where('country_id', $request->country_id)->get();

        return response()->json([
           'status' => 'success',
           'states' => $states
        ]);
    }

    public function loadCities(Request $request){
        $a=$request->country_id;
        //$cities = City::all();
        $cities = City::whereHas('state',function($q) use ($a){
                         $q->where('country_id',$a);
                    })->get();


        return response()->json([
            'status' => 'success',
            'cities' => $cities
        ]);
    }
}
