<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\User;
use App\Client;
use App\Trip;
use App\EmergencyAlert;
use App\TripTask;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Jobs\SendEmailJob;
use App\Agent;

use Intervention\Image\ImageManagerStatic as Image;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = Staff::loggedTravelAgency()->get();
        $staffforselect2 = Staff::loggedTravelAgency()->whereNull('deleted_at')->where('status',true)->get();
        return view('staff.index', compact('staffs','staffforselect2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('staff.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $UserId=0;
        $email="";
        $this->validate($request,[
            'name'                      => "required|min:2|regex:/^[\pL\s\-]+$/u",
            'email'                     => 'required|email|string|max:255|unique:users',
            //'password'                  => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'primary_phone_number'      => "required|digits_between:11,20",
            'secondary_phone_number'    => "required|digits_between:11,20",
            'emergency_phone_number'    => "required|digits_between:11,20",
            "image" => "image|mimes:png,jpg,jpeg|max:5120|nullable",
            "role"  =>"required"
        ]);
        $imageName = null;
        if($request->hasFile('image'))
            {
            $imageName = $request->file('image')->getClientOriginalName();
            $imageName = time().'_'.$imageName;
            $image       = $request->file('image');

        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(200, 50);
        $image_resize->save(public_path('storage/profile_images/' .$imageName)); 
            
        }
        else
        {
            $imageName = null;
        }
        if($request->role=="Admin"){
        $user1                           = new User;
        $user1->name                     =  $request->name;
        $user1->email                    =  $request->email;
        $user1->password                 =  bcrypt('ahlanguide123');
        $user1->user_type                =  "agent";
        $user1->status                   = 'active';
        $user1->save();
        $UserId=$user1->id;
        $email=$user1->name;

        $staff                          = new Staff;
        $staff->name                    = $request->name;
        $staff->user_id                 = $UserId;
        $staff->primary_phone_number    = $request->primary_phone_number;
        $staff->secondary_phone_number  = $request->secondary_phone_number;
        $staff->emergency_phone_number  = $request->emergency_phone_number;
        $staff->type                    = $request->type;
        $staff->role                    = $request->role;
        $staff->description             = $request->description;
        $staff->img                     = $imageName;
        $staff->status                  = true;
        $staff->created_by              = Auth()->user()->id;
        $staff->travel_agency_id        = Auth()->user()->userType->travelAgency->id;
        $staff->save();


        $agent =new Agent;
        $agent->travel_agency_id = Auth()->user()->userType->travelAgency->id;
        $agent->user_id          = $UserId;
        $agent->first_name       = $request->name;
        $agent->last_name        = "";
        $agent->phone_no         = $request->primary_phone_number;
        $agent->phone_no_sec     = $request->secondary_phone_number;
        $agent->picture_path     = $imageName;
        $agent->staff_id         = $staff->id;
        $agent->save();

        }
        else if($request->role=="mobile_agent"){

        $user                           = new User;
        $user->name                     =  $request->name;
        $user->email                    =  $request->email;
        $user->password                 =  bcrypt('ahlanguide123');
        $user->user_type                =  "mobile_agent";
        $user->status                   = 'active';
        $user->save();
        $UserId=$user->id;
        $email=$user->name;

        $staff                          = new Staff;
        $staff->name                    = $request->name;
        $staff->user_id                 = $UserId;
        $staff->primary_phone_number    = $request->primary_phone_number;
        $staff->secondary_phone_number  = $request->secondary_phone_number;
        $staff->emergency_phone_number  = $request->emergency_phone_number;
        $staff->type                    = $request->type;
        $staff->role                    = $request->role;
        $staff->description             = $request->description;
        $staff->img                     = $imageName;
        $staff->status                  = true;
        $staff->created_by              = Auth()->user()->id;
        $staff->travel_agency_id        = Auth()->user()->userType->travelAgency->id;
        $staff->save();

        }
        else{
        $user                           = new User;
        $user->name                     =  $request->name;
        $user->email                    =  $request->email;
        $user->password                 =  bcrypt('ahlanguide123');
        $user->user_type                =  "staff";
        $user->status                   = 'active';
        $user->save();
        $UserId=$user->id;
        $email=$user->name;

        $staff                          = new Staff;
        $staff->name                    = $request->name;
        $staff->user_id                 = $UserId;
        $staff->primary_phone_number    = $request->primary_phone_number;
        $staff->secondary_phone_number  = $request->secondary_phone_number;
        $staff->emergency_phone_number  = $request->emergency_phone_number;
        $staff->type                    = $request->type;
        $staff->role                    = $request->role;
        $staff->description             = $request->description;
        $staff->img                     = $imageName;
        $staff->status                  = true;
        $staff->created_by              = Auth()->user()->id;
        $staff->travel_agency_id        = Auth()->user()->userType->travelAgency->id;
        $staff->save();

        }
        
        $clientdata = array(
                'email' => $email, 
                'password' => 'ahlanguide123'
            );
         dispatch(new SendEmailJob($email,Auth()->user()->email,$clientdata));
        return redirect(route('staffs.index'))->with(['status' => 'success', 'message' => 'Staff Added Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = Staff::findOrFail($id);
        return view('staff.show', compact('staff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = Staff::with('user')->where('id',$id)->first();
        return view('staff.edit', compact('staff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $UserId=0;
        $this->validate($request,[
            'name'                      => "required|min:2|regex:/^[\pL\s\-]+$/u",
            'primary_phone_number'      => "required|digits_between:11,20",
            'secondary_phone_number'    => "required|digits_between:11,20",
            'emergency_phone_number'    => "required|digits_between:11,20",
            "image" => "image|mimes:png,jpg,jpeg|max:5120|nullable",
            "role"  =>"required"
        ]);

        $imageName = null;
        if($request->hasFile('image'))
            {
            $imageName = $request->file('image')->getClientOriginalName();
            $imageName = time().'_'.$imageName;
            $image       = $request->file('image');

        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(200, 50);
        $image_resize->save(public_path('storage/profile_images/' .$imageName)); 
            
        }
        else
        {
            $imageName = null;
        }

        $staff = Staff::findOrFail($id);
        if($request->role == "Admin"){
            if($staff->role== "Admin"){
                $getAgent=Agent::where('staff_id',$staff->id)->first();
                $getAgent->picture_path     = $imageName == null?$getAgent->picture_path:$imageName;
                $getAgent->save();
                $user  = User::where('id',$staff->user_id)->first();
                $user->name       = $request->name;
                $user->user_type  =  "agent";
                $user->save();
            }
            else if($staff->role== "mobile_agent"){
                $getemail=User::where('id',$staff->user_id)->first();
                $getemail->name                     =  $request->name;
                $getemail->user_type                =  "agent";
                $getemail->status                   = 'active';
                $getemail->save();
                $UserId=$getemail->id;

                $agent =new Agent;
                $agent->travel_agency_id = Auth()->user()->userType->travelAgency->id;
                $agent->user_id          = $UserId;
                $agent->first_name       = $request->name;
                $agent->last_name        = "";
                $agent->phone_no         = $request->primary_phone_number;
                $agent->phone_no_sec     = $request->secondary_phone_number;
                $agent->picture_path     = $imageName == null? $staff->img :$imageName;
                $agent->staff_id         = $staff->id;
                $agent->save();
            }
            else{
                $getemail=User::where('id',$staff->user_id)->first();
                $getemail->name                     =  $request->name;
                $getemail->user_type                =  "agent";
                $getemail->status                   = 'active';
                $getemail->save();
                $UserId=$getemail->id;

                $agent =new Agent;
                $agent->travel_agency_id = Auth()->user()->userType->travelAgency->id;
                $agent->user_id          = $UserId;
                $agent->first_name       = $request->name;
                $agent->last_name        = "";
                $agent->phone_no         = $request->primary_phone_number;
                $agent->phone_no_sec     = $request->secondary_phone_number;
                $agent->picture_path     = $imageName == null? $staff->img :$imageName;
                $agent->staff_id         = $staff->id;
                $agent->save();
            }
        }
        else if($request->role == "mobile_agent"){
            if($staff->role == "mobile_agent"){

            }
            else if($staff->role == "Admin"){
                $getAgent=Agent::where('staff_id',$staff->id)->first();
                if($getAgent){
                    $userdata=User::where('id',$getAgent->user_id)->first();
                    $getAgent->delete();
                    $userdata->name                     =  $request->name;
                    $userdata->user_type                =  "mobile_agent";
                    $userdata->status                   = 'active';
                    $userdata->save();
                    $UserId=$userdata->id;
                }
            }
            else{
                    $userdata=User::where('id',$staff->user_id)->first();
                    $userdata->name                     =  $request->name;
                    $userdata->user_type                =  "mobile_agent";
                    $userdata->status                   = 'active';
                    $userdata->save();
                    $UserId=$userdata->id;
            }
        }
        else{
            if($staff->role=="Staff"){

            }
            else if($staff->role=="Admin"){
                $getAgent=Agent::where('staff_id',$staff->id)->first();
                if($getAgent){
                    $userdata=User::where('id',$getAgent->user_id)->first();
                    $getAgent->delete();
                    $userdata->name                     =  $request->name;
                    $userdata->user_type                =  "staff";
                    $userdata->status                   = 'active';
                    $userdata->save();
                    $UserId=$userdata->id;
                }
            }
            else{
                    $userdata=User::where('id',$staff->user_id)->first();
                    $userdata->name                     =  $request->name;
                    $userdata->user_type                =  "staff";
                    $userdata->status                   = 'active';
                    $userdata->save();
                    $UserId=$userdata->id;
                }
            }

        $staff->name                    = $request->name;
        $staff->primary_phone_number    = $request->primary_phone_number;
        $staff->secondary_phone_number  = $request->secondary_phone_number;
        $staff->emergency_phone_number  = $request->emergency_phone_number;
        $staff->type                    = $request->type;
        $staff->role                    = $request->role;
        $staff->img                     = $imageName==null?$staff->img:$imageName;
        $staff->description             = $request->description;
        $staff->updated_by              =Auth()->user()->id;
        $staff->save();

        return redirect(route('staffs.index'))->with(['status' => 'success', 'message' => 'Staff Updated Successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = Staff::findOrFail($id);
        $user = User::where('id', $staff->user_id)->first();
        $staff->delete();
        $user->delete();

        return redirect(route('staffs.index'))->with(['status' => 'success', 'message' => 'Staff Deleted Successfully!']);
    }

    public function viewprofile($id)
    {
        $CurrentDate=date('Y-m-d');
        $new_complaint='';
        $total_trips='';
        $overduetask='';
        $StaffID=$id;

        $new_complaint = '0';
        //$total_trips = Trip::where('travel_agency_id', Auth()->user()->userType->id)->where('end_trip',true)->count();
        $total_trips = Trip::whereHas('tripTasks', function($q) use ($StaffID){
            $q->whereHas('staff', function($q) use ($StaffID){
            $q->where('staff_id',$StaffID);});
            })->where('end_trip',true)->count();

        $overduetask = TripTask::whereHas('staff', function($q) use ($StaffID){
            $q->where('staff_id',$StaffID);
            })->whereDate('due_date','<',$CurrentDate)->where('status','!=','Complete')->count();

        $staff = Staff::with('user')->findOrFail($id);
        return view('staff.viewprofile', compact('staff','new_complaint','total_trips','overduetask'));
    }

    public function search(Request $request)
    {
        $staffforselect2 = Staff::loggedTravelAgency()->whereNull('deleted_at')->where('status',true)->get();
        if($request->staff_id)
        {
            $staffs = Staff::loggedTravelAgency()->whereNull('deleted_at')->where('id',$request->staff_id)->get();
        }
        else
        {
            $staffs = Staff::loggedTravelAgency()->whereNull('deleted_at')->get();
        }

        return view('staff.index', ['staffs' => $staffs,'staffforselect2'=>$staffforselect2]);
    }

    public function ChangeStaffStatus(Request $request)
    {
        $staff   = Staff::findOrFail($request->id);
        $staff->status=$staff->status==0?true:false;
        $staff->save();
        $staffforselect2 = Staff::loggedTravelAgency()->whereNull('deleted_at')->where('status',true)->get();
        $status=$staff->status==0?"warning":"success";
        return response()->json(['status' => $status,'staffforselect2'=>$staffforselect2]);
    }
}
