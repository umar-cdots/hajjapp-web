<?php

namespace App\Http\Controllers\API\V1\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Get the personal client name for Passport oAuth login.
     *
     * @return string
     */
    public function personalClientName()
    {
        return 'Personal Grant - Mobile Apps';
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);


        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if(!in_array($user->user_type, ['super_admin', 'admin', 'agent', 'staff','mobile_agent']))
        {
            return abort(403, "You're not eligible to use this App.");
        }
    	elseif($user->status != 'active')
    	{
    		if($user->status == 'pending')
    			return abort(423, "Authentication pending yet.");
    		elseif($user->status == 'deactive')
    			return abort(423, "Your account is not active. Please contact administration.");
    		elseif($user->status == 'suspended')
    			return abort(423, "Your account is suspended. Please contact administration.");
    		else
    			return abort(423, "Your account is not accessible for now. Please try again shortly.");
    	}
    	$token 			= $user->createToken($this->personalClientName(), ['*']);
    	$user->token 	= $token->accessToken;


        return $user;
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $request->user()->update([
            'fcm_token' => null
        ]);
        Auth::logout();
        return $this->loggedOut($request);
    }

    public function registerFCM(Request $request)
    {
        $request->validate([
            'fcm_token' => 'required'
        ]);

        \Auth::user()->fcm_token = $request->fcm_token;
        \Auth::user()->save();

        return;
    }
}
