<?php

namespace App\Http\Controllers\API\V1\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EmergencyAlert;
use Utils;
use Carbon\Carbon;

class EmergencyAlertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_trip_id)
    {
        return EmergencyAlert::where('client_trip_id', $client_trip_id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $client_trip_id)
    {
        $CurrentDate=date('Y-m-d');
        $current = Carbon::now();
        $request->validate([
            // 'client_trip_id'    => 'required|exists:client_trips,id',
            // 'message'           => 'required',
            'lat'               => 'required|numeric',
            'lon'               => 'required|numeric',
        ]);

//        if(\Auth::user()->userType->id != \App\ClientTrip::find($client_trip_id)->id)
//            return abort(403);

        $emergency_alert                 = new EmergencyAlert;
        $emergency_alert->client_trip_id = $client_trip_id;
        // $emergency_alert->message        = $request->message;
        $emergency_alert->lat            = $request->lat;
        $emergency_alert->lon            = $request->lon;
        $emergency_alert->save();

        /* Send Push to All Agents */
        $user_ids       = \App\Agent::where('travel_agency_id', Utils::tripByClientTrip($client_trip_id)->travel_agency_id)->pluck('user_id');
        $fcms           = \App\User::whereIn('id', $user_ids)->pluck('fcm_token');

        $body           = array('type' => "emergency_alert" , 'data' => ['message' => "Emergency Alert Recevied."]);

        foreach($fcms as $fcm)
        {
            Utils::sendNotification($fcm, $body);
        }

        return response()->json(['status' => true, 'message' => 'Alert has been received, we will get back to you soon']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(EmergencyAlert $id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmergencyAlert $id)
    {
        $request->validate([
            // 'client_trip_id'    => 'required|exists:client_trips,id',
            // 'message'           => 'required',
            'lat'               => 'required|numeric',
            'lon'               => 'required|numeric',
        ]);

        $emergency_alert                 = $id;
        // $emergency_alert->client_trip_id = $request->client_trip_id;
        // $emergency_alert->message        = $request->message;
        $emergency_alert->lat            = $request->lat;
        $emergency_alert->lon            = $request->lon;
        $emergency_alert->save();

        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmergencyAlert $id)
    {
        $id->delete();
    }
}
