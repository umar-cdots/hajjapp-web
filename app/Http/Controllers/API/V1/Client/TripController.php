<?php

namespace App\Http\Controllers\API\V1\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Trip;
use App\Client;
use App\ClientTrip;
use App\ClientTripAirline;
use App\ClientTripHotel;
use App\ClientTripStep;
use App\FeedbackCategory;
use App\GroupClientTrip;
use App\Staff;
use App\TripUpdate;
// use App\ClientTripMessage;
use App\EmergencyAlert;

class TripController extends Controller
{
    public function index()
    {
    	$trips 	= \Auth::user()->userType->clienttrips()->with(['trip','trip.travelAgency','trip.travelAgency.package','trip.tripGuide'])->get();
        //return $trips;
       // $getCleint=GroupClientTrip::where('client_id',$trips->id)->get();
    	$trips->each(function($trip, $key) use($trips) {
    		$trips[$key]->trip_flights        = ClientTripAirline::with(['flight','flight.airline', 'flight.fromAirport', 'flight.fromAirport.city', 'flight.fromAirport.country', 'flight.toAirport', 'flight.toAirport.city', 'flight.toAirport.country'])->where('client_trip_id', $trip->pivot->id)->get();
    		$trips[$key]->trip_hotels         = ClientTripHotel::with(['triphoteldetail','triphoteldetail.hotel'])->where('client_trip_id', $trip->pivot->id)->get();
            $trips[$key]->current_step        = ClientTripStep::with(['step', 'step.stepSection', 'step.stepSection.stepCheckLists'])->where(['client_trip_id' => $trip->pivot->id, 'status' => "current"])->first();
            $trips[$key]->feedback_categories = FeedbackCategory::where('travel_agency_id',\Auth()->user()->userType->travel_agency_id)->get();
            $trips[$key]->staff = Staff::all();
            // $trips[$key]->trip_messages       = ClientTripMessage::where('client_trip_id', $trip->pivot->id)->get();
          $trips[$key]->last_emergency_alert_datetime = EmergencyAlert::where('client_trip_id', $trip->pivot->id)->orderBy('id', 'DESC')->first()->created_at ?? null;

          $trips[$key]->push_notification = TripUpdate::with(['clientgroup','clientgroup.clienttrip'])->where('client_trip_group_id', $trip->pivot->id)->orderBy('id', 'DESC')->limit(3)->get();
    	});

    	return $trips;
    }

    public function show($id)
    {
    	$trip 	= \Auth::user()->userType
    								->clienttrips()
                                    ->with(['trip','trip.travelAgency','trip.travelAgency.package','trip.tripGuide'])
    									->where('trip_id', $id)
    										->first();


    	if(!empty($trip))
    	{
    		$trip->trip_flights = ClientTripAirline::with(['flight','flight.airline', 'flight.fromAirport', 'flight.fromAirport.city', 'flight.fromAirport.country', 'flight.toAirport', 'flight.toAirport.city', 'flight.toAirport.country'])->where('client_trip_id', $trip->pivot->id)->get();
    		$trip->trip_hotels 	       = ClientTripHotel::with(['triphoteldetail','triphoteldetail.hotel'])->                                          where('client_trip_id', $trip->pivot->id)->get();
            $trip->current_step        = ClientTripStep::with(['step', 'step.stepSection', 'step.stepSection.stepCheckLists'])->where(['client_trip_id' => $trip->pivot->id, 'status' => "current"])->first();
            $trip->feedback_categories = FeedbackCategory::where('travel_agency_id',\Auth()->user()->userType->travel_agency_id)->get();
            $trip->staff = Staff::all();
            // $trip->trip_messages       = ClientTripMessage::where('client_trip_id', $trip->pivot->id)->get();
            $trip->last_emergency_alert_datetime = EmergencyAlert::where('client_trip_id', $trip->pivot->id)->orderBy('id', 'DESC')->first()->created_at ?? null;
            $trip->push_notification = TripUpdate::with(['clientgroup','clientgroup.clienttrip'])->where('client_trip_group_id', $trip->pivot->id)->orderBy('id', 'DESC')->limit(3)->get();

    		return $trip;
    	}

    	return abort(403, "Access Denied.");
    }

    public function latest()
    {
    	$trip  = \Auth::user()->userType
                                    ->clienttrips()
                                    ->with(['trip','trip.travelAgency','trip.travelAgency.package','trip.tripGuide'])
                                        ->where('status', 'ongoing')
                                            ->first();


        if(!empty($trip))
        {
            $trip->trip_flights = ClientTripAirline::with(['flight','flight.airline', 'flight.fromAirport', 'flight.fromAirport.city', 'flight.fromAirport.country', 'flight.toAirport', 'flight.toAirport.city', 'flight.toAirport.country'])->where('client_trip_id',                                            $trip->pivot->id)->get();
            $trip->trip_hotels         = ClientTripHotel::with(['triphoteldetail','triphoteldetail.hotel'])->                                         where('client_trip_id', $trip->pivot->id)->get();
            $trip->current_step        = ClientTripStep::with(['step', 'step.stepSection', 'step.stepSection.stepCheckLists'])->where(['client_trip_id' => $trip->pivot->id, 'status' => "current"])->first();
             $trip->feedback_categories = FeedbackCategory::where('travel_agency_id',\Auth()->user()->userType->travel_agency_id)->get();
             $trip->staff = Staff::all();
            // $trip->trip_messages       = ClientTripMessage::where('client_trip_id', $trip->pivot->id)->get();
            $trip->last_emergency_alert_datetime = EmergencyAlert::where('client_trip_id', $trip->pivot->id)->orderBy('id', 'DESC')->first()->created_at ?? null;
            $trip->push_notification = TripUpdate::with(['clientgroup','clientgroup.clienttrip'])->where('client_trip_group_id', $trip->pivot->id)->orderBy('id', 'DESC')->limit(3)->get();
    	}

		return $trip;
    }
}
