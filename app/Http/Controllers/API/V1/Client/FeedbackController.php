<?php

namespace App\Http\Controllers\API\V1\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feedback;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_trip_id)
    {
        return Feedback::with('feedbackCategory')->where('client_trip_id', $client_trip_id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $client_trip_id)
    {
        $request->validate([
            // 'client_trip_id'        => 'required|exists:client_trips,id',
            'feedback_category_id'  => 'required|exists:feedback_categories,id',
            'feedback'              => 'required'
        ]);

        $feedback                           = new Feedback;
        $feedback->client_trip_id           = $client_trip_id;
        $feedback->feedback_category_id     = $request->feedback_category_id;
        $feedback->staff_id                 = $request->staff_id;
        $feedback->feedback                 = $request->feedback;
        $feedback->save();

        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $id)
    {
        $request->validate([
            // 'client_trip_id'        => 'required|exists:client_trips,id',
            'feedback_category_id'  => 'required|exists:feedback_categories,id',
            'feedback'              => 'required'
        ]);

        $feedback                           = $id;
        $feedback->client_trip_id           = $request->client_trip_id;
        $feedback->feedback_category_id     = $request->feedback_category_id;
        $feedback->staff_id                 = $request->staff_id;
        $feedback->feedback                 = $request->feedback;
        $feedback->save();

        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $id)
    {
        $id->delete();

        return;
    }
}
