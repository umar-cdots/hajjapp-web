<?php

namespace App\Http\Controllers\API\V1\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Storage;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Get the personal client name for Passport oAuth login.
     *
     * @return string
     */
    public function personalClientName()
    {
        return 'Personal Grant - Mobile Apps';
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if(!in_array($user->user_type, ['client']))
        {
            return abort(403, "You're not eligible to use this App.");
        }
    	elseif($user->status != 'active')
    	{
    		if($user->status == 'pending')
    			return abort(423, "Authentication pending yet.");
    		elseif($user->status == 'deactive')
    			return abort(423, "Your account is not active. Please contact administration.");
    		elseif($user->status == 'suspended')
    			return abort(423, "Your account is suspended. Please contact administration.");
    		else
    			return abort(423, "Your account is not accessible for now. Please try again shortly.");
        }
        $user->login_status = true;
        $user->save();
    	$token 			= $user->createToken($this->personalClientName(), ['*']);
    	$user->token 	= $token->accessToken;
        $user->userType;
       
        return $user;
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $data = $request->user();
        $data->fcm_token = null; 
        $data->login_status = false;
        $data->save();

        return $this->loggedOut($request);
    }

    public function resetcheck(Request $request)
    {
        $status="false";
        $request->validate([
            'email' => 'required|email'
        ]);
          $getEmail= user::where('email',$request->email)->first();
          if($getEmail){
            $status="true";
          }

        return $status;
    }

    public function update(Request $request)
    {
        $request->validate([
            'name'                      => 'required|string',
            'email'                     => 'required|unique:users,email,' . \Auth::user()->id,
            'password'                  => 'nullable|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'primary_phone_number'      => "required|numeric|digits_between:11,20",
            'secondary_phone_number'    => "numeric|nullable|digits_between:11,20",
            'emergency_phone_number'    => "numeric|nullable|digits_between:11,20",
            'occupation'                => "string||nullable",
            'blood_group'               => "string||nullable",
            'special_needs'             => "string||nullable",
            'postal_address'            => 'required',
            'dob'                       => 'required|date',
            'gender'                    => 'required|in:male,female,not_specified',
            'interest'                  => 'sometimes|nullable',
            'qualification'             => "string|nullable",
            'personality_comments'      => "string|nullable",
            'cnic'                      => "string|nullable",
            'passport_no'               => "string|nullable"
        ]);

        \Auth::user()->name         = $request->name;
        \Auth::user()->email        = $request->email;
        \Auth::user()->password     = !empty($request->password) ? bcrypt($request->password) : \Auth::user()->password;
        \Auth::user()->save();

        $user   = \Auth::user()->userType;
        $user->name           = $request->name;
        $user->primary_phone_number = $request->primary_phone_number;
        $user->secondary_phone_number   = $request->secondary_phone_number;
        $user->emergency_phone_number   = $request->emergency_phone_number;
        $user->occupation           = $request->occupation;
        $user->blood_group          = $request->blood_group;
        $user->special_needs         = $request->special_needs;
        $user->postal_address       = $request->postal_address;
        $user->dob                  = $request->dob;
        $user->gender               = $request->gender;
        $user->qualification        = $request->qualification;
        $user->personality_comments = $request->personality_comments;
        $user->cnic                 = $request->cnic;
        $user->passport_no          = $request->passport_no;
        $user->save();

        return Auth()->user();
    }

    public function displayPhoto(Request $request)
    {
        $request->validate([
            'picture'  => 'required|image|max:5120'
        ]);

        $imageName = null;
        if($request->hasFile('picture'))
        {
            $imageName = $request->file('picture')->getClientOriginalName();
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'profile_images', $request->file('picture'), $imageName
            );
        }
        else
        {
            $imageName = null;
        }
        if($imageName!=null){
        \Auth::user()->userType->picture_path = $imageName;
        \Auth::user()->userType->save();     
        }
        return Auth()->user();
    }

    public function registerFCM(Request $request)
    {
        $request->validate([
            'fcm_token' => 'required'
        ]);

        \Auth::user()->fcm_token = $request->fcm_token;
        \Auth::user()->save();

        return;
    }
}
