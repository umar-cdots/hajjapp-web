<?php

namespace App\Http\Controllers\API\V1\Client;

use App\Helper\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Trip;
use App\TripMessage;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($trip_id)
    {
        return TripMessage::with(['agent','client'])->where(['client_id' => \Auth::user()->userType->id, 'trip_id' => $trip_id])->orderBy('id', 'DESC')->get();
//        if(!empty($trip_messages)){
//            session()->set(['previous_message' => $trip_messages]);
//
            TripMessage::where(['client_id' => \Auth::user()->userType->id, 'trip_id' => $trip_id])->update(['client_seen_status' => 1]);
//
//            return session()->get('previous_message');
//        }else{
//            return response()->json([]);
//        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'trip_id'   => 'required|exists:trips,id',
            'message'   => 'required'
        ]);

        $trip   = Trip::find($request->trip_id);
       // if($trip->clients()->where('client_id', \Auth::user()->userType->id)->count() > 0)
       // {
            $message                    = new TripMessage;
            $message->travel_agency_id  = $trip->travel_agency_id;
            $message->trip_id           = $trip->id;
            $message->client_id         = \Auth::user()->userType->id;
            $message->message           = $request->message;
            $message->status            = 0;
            $message->admin_seen_status = 0;
            $message->client_seen_status = 1;
            $message->save();

            /* Send Push to All Agents */
            $user_ids       = \App\Agent::where('travel_agency_id', $trip->travel_agency_id)->pluck('user_id');
            $fcms           = \App\User::whereIn('id', $user_ids)->pluck('fcm_token');

            $body           = array('type' => "message" , 'data' => $message->toArray());

            foreach($fcms as $fcm)
            {
                Utils::sendNotification($fcm, $body);
            }

            return;
        //}

        //return abort(403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = TripMessage::where(['client_id' => \Auth::user()->userType->id, 'id' => $id])->firstOrFail();
        $message->delete();

        return;
    }
}
