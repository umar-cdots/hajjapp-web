<?php

namespace App\Http\Controllers;

use App\Guide;
use App\Trip;
use App\TripGuide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Session;
class TripGuideController extends Controller
{
    public function index($trip_id){   
        $trip = Trip::findOrFail($trip_id);
        $guides = Guide::where('trip_id', $trip->id)->get();
        Session::put('TripId',$trip->id);
        //$guides = Trip::with('tripGuide')->where('id', $trip->id)->get();
        return view('trip.trip_guides.index', compact('trip', 'guides'));
    }

    public function store(Request $request){
//return $request;
        $this->validate($request, [
            'guide_name' => 'required',
            'guides' => 'mimes:png,pdf,jpeg,jpg,txt'
        ]);
        $imageName = null;
        $counttotalguide=Guide::where('trip_id',$request->Tripid)->count();
        $getTripDetail=Trip::where('id',$request->Tripid)->first();
        if($counttotalguide!=$getTripDetail->guide_qouta){
        if($request->hasFile('guides'))
        {
            $imageName = $request->file('guides')->getClientOriginalName();
            $imageName = time().'_'.$imageName;

            $path = Storage::disk('public')->putFileAs(
                'guides', $request->file('guides'), $imageName
            );
        }
        else
        {
            $imageName = null;
            if($request->youtube_link==null){
                return redirect()->route('trip.guide.index',$request->Tripid)->with([
            'status' => 'error',
            'message' => 'Please enter atleast file or youtube link'
        ]);
            }
        }
        if($imageName==null){
            Guide::create([
                    'travel_agency_id' => Auth()->user()->userType->travelAgency->id,
                    'title' => $request->guide_name,
                    'trip_id'=>$request->Tripid,
                    'youtube_link'=>$request->youtube_link
                ]);
        }
        else{
            Guide::create([
                    'travel_agency_id' => Auth()->user()->userType->travelAgency->id,
                    'title' => $request->guide_name,
                    'trip_id'=>$request->Tripid,
                    'path' => $imageName,
                    'youtube_link'=>$request->youtube_link
                ]);
        }
    }
    else{
        return redirect()->route('trip.guide.index',$request->Tripid)->with([
            'status' => 'error',
            'message' => 'Your guide quota was full'
            ]);
    }
        

          return redirect()->route('trip.guide.index',$request->Tripid)->with([
            'status' => 'success',
            'message' => 'Guide has been added to trip successfully'
        ]);

        //$this->validate($request, [
          //  'guides.*' => 'required|exists:guides,id',
            //'trip_id' => 'required|exists:trips,id'
        //]);

        //$trip = Trip::findOrFail($request->trip_id);
        //$trip->tripGuide()->sync($request->guides);

        //return redirect()->route('trips.index')->with([
          //  'status' => 'success',
            //'message' => 'Guides has been added to trip.'
        //]);
    }

    public function destroy($guide){
        $guide = Guide::findOrFail($guide);
        $guide->delete();

        return redirect()->route('trip.guide.index',Session::get('TripId'))->with([
            'status' => 'success',
            'message' => 'Guide has been deleted'
        ]);
    }
}
