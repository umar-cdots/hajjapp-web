<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\TravelAgency;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();
        return view('package.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'             => 'required|string|unique:packages',
            'no_of_trips'       => 'required||numeric',
            'no_of_customers'   => 'required||numeric',
            'no_of_steps'       => 'required||numeric',
            'no_of_section'     => 'required||numeric',
//            'start_date'          => 'required',
//            'expiry_date'       => 'required'
        ]);

        $package = new Package;
        $package->title                = $request->title;
        $package->no_of_trips          = $request->no_of_trips;
        $package->no_of_customers      = $request->no_of_customers;
        $package->no_of_guides         = 0;
        $package->no_of_steps          = $request->no_of_steps;
        $package->no_of_section        = $request->no_of_section;
//        $package->start_date             = $request->start_date;
//        $package->expiry_date          = $request->expiry_date;

        $package->save();

        return redirect(route('packages.index'))->with(['status' => "success", 'message' => "Package Added Successfully!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::findOrFail($id);
        return view('package.show', compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::findOrFail($id);
        return view('package.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'             => 'required|string',
            'no_of_trips'       => 'required|numeric',
            'no_of_customers'   => 'required|numeric',
            'no_of_steps'       => 'required||numeric',
            'no_of_section'     => 'required||numeric',
//            'start_date'        => 'required',
//            'expiry_date'       => 'required'
        ]);

        $package = Package::findOrFail($id);
        $package->title                = $request->title;
        $package->no_of_trips          = $request->no_of_trips;
        $package->no_of_customers      = $request->no_of_customers;
        $package->no_of_steps          = $request->no_of_steps;
        $package->no_of_section        = $request->no_of_section;
//        $package->start_date             = $request->start_date;
//        $package->expiry_date          = $request->expiry_date;

        $package->save();

        return redirect(route('packages.index'))->with(['status' => "success", 'message' => "Package Updated Successfully!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getdata=TravelAgency::where('package_id',$id)->first();
        if($getdata){
            return redirect(route('packages.index'))->with(['status' => "error", 'message' => "You can not delete package"]);
        }
        else{
        $package = Package::findOrFail($id);
        $package->delete();

        return redirect(route('packages.index'))->with(['status' => "success", 'message' => "Package Deleted Successfully!"]);
    }
    }
}
