<?php

namespace App\Http\Controllers;

use App\Staff;
use App\Trip;
use App\TripTask;
use App\User;
use Carbon\Carbon;
use App\ClientTrip;
use Illuminate\Http\Request;
use Auth;
use App\StaffNotification;
use App\Helper\Utils;
use Session;

class TripTaskController extends Controller
{
    //public function index($trip_id){
        //$trip = Trip::findOrFail($trip_id);
       // $users = User::with('staff')->where('user_type', 'staff')->get();
      //  return view('trip.trip_tasks.index', compact('trip', 'users'));
    //}

    public function index(){
      
        $CurrentDate=date('Y-m-d');
        $valuecheck=0;
        if(Session::has('checkvalue')){
            $valuecheck=1;
            Session::forget('checkvalue');
        }
        else{
            $valuecheck=0;
        }
        $totaltask = TripTask::loggedTravelAgency()->with(['trip','staff'])->orderBy('id', 'desc')->get();
         $totalduetask = TripTask::loggedTravelAgency()->with(['trip','staff'])->whereDate('due_date','<',$CurrentDate)->where('status','!=','Complete')->orderBy('id', 'desc')->get();
        //$totaltask->filter( )
        return view('trip.trip_tasks.index',compact('totaltask','totalduetask','valuecheck'));
    }

    public function create(){
        $trip = Trip::loggedTravelAgency()->where('end_trip',true)->get();
        $users = User::with('staff')->whereHas('staff',function($q){
                                       $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id); 
                                      })->get();
        return view('trip.trip_tasks.create',compact('users','trip'));
    }

    public function addTasks(Request $request){

        $this->validate($request, [
              "task_title" => "required",
              "due_date" => 'required|string',
              "start_date" => 'required|string',
        ]);

            $trip_task = TripTask::create([
                'trip_id' => $request->trip_id,
                'staff_id' => $request->staff_id,
                'task_title' => $request->task_title,
                'status' => $request->status,
                'due_date' => $request->due_date,
                'start_date' => $request->start_date,
                'description' => $request->description,
                'travel_agency_id' =>\Auth::user()->userType->travel_agency_id
            ]);
            if($request->staff_id){
            //sending notifications
            $staff = Staff::where('id', $request->staff_id)->first();
            $user = $staff->user;
            $StaffNotification = StaffNotification::create([
                  'staff_id' => $staff->id,
                  'message' => $request->task_title."(Task Assigned)",
            ]);
            if($user->fcm_token != null){
                $body = array('type' => "message" , 'data' => $trip_task->toArray());
                Utils::sendNotification($user->fcm_token, $body);
            }
        }

        return redirect()->route('trip.task.index')->with(['status' => 'success', 'message' => 'Tasks has been assigns to the staff']);
    }

    public function edit($id)
    {
        $triptasks = TripTask::findOrFail($id);
        $trip = Trip::loggedTravelAgency()->where('end_trip',true)->get();
        $users = User::with('staff')->whereHas('staff',function($q){
                                       $q->where('travel_agency_id',Auth()->user()->userType->travel_agency_id); 
                                      })->get();
        return view('trip.trip_tasks.edit', compact('triptasks','trip','users'));
    }

    public function destroy($id)
    {
        $triptask   = TripTask::findOrFail($id);
        $triptask->delete();

        return redirect(route('trip.task.index'))->with(['status' => "success", 'message' => "Trip Task Deleted Successfully!"]);
    }

    public function update(Request $request){

        $this->validate($request, [
              "task_title" => "required",
              "due_date" => 'required|string',
              "start_date" => 'required|string',
        ]);
        $today_date = date('Y-m-d H:i:s');
           $trip_task = TripTask::findOrFail($request->id);
           if($request->status=="Complete"){
            $trip_task->trip_id = $request->trip_id;
                $trip_task->staff_id = $request->staff_id;
                $trip_task->task_title = $request->task_title;
                $trip_task->status = $request->status;
                $trip_task->due_date = $request->due_date;
                $trip_task->actual_date = $today_date;
                $trip_task->start_date = $request->start_date;
                $trip_task->description = $request->description;
                $trip_task->comment = $request->comment;
                $trip_task->travel_agency_id = \Auth::user()->userType->travel_agency_id;
           }
           else{
            $trip_task->trip_id = $request->trip_id;
                $trip_task->staff_id = $request->staff_id;
                $trip_task->task_title = $request->task_title;
                $trip_task->status = $request->status;
                $trip_task->due_date = $request->due_date;
                $trip_task->start_date = $request->start_date;
                $trip_task->description = $request->description;
                $trip_task->comment = $request->comment;
                $trip_task->travel_agency_id = \Auth::user()->userType->travel_agency_id;
           }
                
                $trip_task->save();

        return redirect()->route('trip.task.index')->with(['status' => 'success', 'message' => 'Tasks has been updated']);
    }

    public function stafftaskoverduedetail($id){
        $CurrentDate=date('Y-m-d');
        $totalduetask = TripTask::with(['trip','staff'])->whereHas('staff', function($q) use ($id){
            $q->where('staff_id',$id);
            })->whereDate('due_date','<',$CurrentDate)->where('status','!=','Complete')->get();
        //return $totalduetask;
        //$totaltask->filter( )
        return view('trip.trip_tasks.stafftaskoverduedetail',compact('totalduetask'));
    }

    public function customertaskoverduedetail($id){
        $CurrentDate=date('Y-m-d');
        $totalduetask = ClientTrip::whereHas('clients', function($q) use ($id){
                                         $q->where('user_id',$id);
                                         })->with(array('trip'=>function($q) use($CurrentDate){
                                          $q->with(array('tripTasks'=>function($q) use($CurrentDate){
                                          $q->where('due_date','<',$CurrentDate)->with('staff');
                                          }));
                                         }))->get();
                                         //return $totalduetask;
                                         //foreach ($totalduetask as $key => $value) {
                                           //  return $value->trip->triptasks[$key]['staff'];
                                         //}
                                         
                                         //foreach ($totalduetask as $key => $value) {
                                             # code...
                                           // return $value->trip->triptasks;
                                         //}
        //$totaltask->filter( )
        return view('trip.trip_tasks.customertaskoverduedetail',compact('totalduetask'));
    }

}
