<?php

namespace App\Http\Controllers;

use App\Step;
use App\StepSection;
use App\StepSectionCheckList;
use App\TravelAgency;
use App\Trip;
use Illuminate\Http\Request;
use App\StepPack;
use App\TripStepPack;
use Session;
use App\ClientTrip;
use App\ClientTripStep;
use App\GroupClientTrip;
use Illuminate\Support\Facades\Storage;
use App\Helper\Utils;

class StepPackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $step_packs = StepPack::loggedTravelAgency()->get();

        return view('step_pack.index', compact('step_packs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $travel_agencies = TravelAgency::all();

        return view('step_pack.create', compact('travel_agencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|min:3',
            'travel_agency_id' => 'required|integer|exists:travel_agencies,id'
        ]);

        StepPack::create([
            'title' => $request->title,
            'travel_agency_id' => $request->travel_agency_id
        ]);

        return redirect()->route('step_pack.index')->with(['status' => 'success', 'message' => 'Step pack has been created']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $step_pack = StepPack::findOrFail($id);
        $travel_agencies = TravelAgency::all();

        return view('step_pack.show', compact('step_pack', 'travel_agencies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $step_pack = StepPack::findOrFail($id);
        $travel_agencies = TravelAgency::all();

        return view('step_pack.edit', compact('step_pack', 'travel_agencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $steppack = StepPack::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|string|min:3',
            'travel_agency_id' => 'required|integer|exists:travel_agencies,id'
        ]);

        $steppack->update([
            'title' => $request->title,
            'travel_agency_id' => $request->travel_agency_id
        ]);

        return back()->with(['status' => 'success', 'message' => 'Step pack has been created']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */


    public function stepPacksByTravelAgency($travel_agency_id)
    {
        return StepPack::where('travel_agency_id', $travel_agency_id)->get();
    }

    public function changestep($id)
    {

        $getalltrips = TripStepPack::where('trip_id', $id)->first();
        $trip = Trip::where('id', $id)->first();
        Session::put('TripID', $id);
        Session::put('TripName', $trip->title);
        $getTripGroup = ClientTrip::with('clients')->with('group')->where('trip_id', $id)->get();
        if ($getalltrips) {
            $getSteps = Step::where('step_pack_id', $getalltrips->step_pack_id)->get();
        } else {
            $getSteps = [];
        }
        return view('trip.changestep', compact('getSteps', 'getTripGroup'));
    }

    public function getStepPacks($id)
    {
        $step_packs = StepPack::loggedTravelAgency()->get();
        return view('trip.steppacks', compact('step_packs', 'trip'));
    }

    public function GetAllSteps(Request $request)
    {
        $getStrpPackid = TripStepPack::where('trip_id', $request->TripId)->first();
        if ($getStrpPackid) {
            $Steps = Step::with(array('stepsection' => function ($q) {
                $q->with('stepCheckLists');
            }))->where('step_pack_id', $getStrpPackid->step_pack_id)->get();
        } else {
            $Steps = [];
        }
        return response()->json(['Step' => $Steps]);
    }

    public function customStepPack($id)
    {
        $trips = TripStepPack::with('steppack')->whereHas('steppack', function ($q) {
            $q->where('travel_agency_id', Auth()->user()->userType->travelAgency->id);
        })->get();
        $tripdata = Trip::where('id', $id)->first();
        $getPackage = TravelAgency::with('package')->where('id', Auth()->user()->userType->travel_agency_id)->first();
        //return $getPackage;
        Session::put('TripID', $id);
        Session::put('TripName', $tripdata->title);
        Session::put('TotalStep', $getPackage->package->no_of_steps);
        Session::put('TotalSection', $getPackage->package->no_of_section);
        return view('trip.customsteppack', compact('trips'));
    }

    public function SearchAllSteps(Request $request)
    {
        $Steps = [];
        foreach ($request->StepPackId as $key => $value) {
            $Steps1 = Step::with(array('stepsection' => function ($q) {
                $q->with('stepCheckLists');
            }))->where('step_pack_id', $value)->get();
            Session::push('Steps', $Steps1);
        }
        if (Session::has('Steps')) {
            $Steps = Session::get('Steps');
            Session::forget('Steps');
        } else {
            $Steps = [];
        }
        //return $Steps;
        return response()->json(['Step' => $Steps]);
    }

    public function addStepToStepPack(Request $request)
    {
        //return json_decode($request->value);
        $status = 0;
        $errMsg = "";
        $GetAllSteps = 0;
        $StepId = 0;
        $SectionId = 0;
        $checkTrip = 0;
        $currentstepchange = 0;
        if (count(json_decode($request->value)) > 0) {
            $checkTripStep = TripStepPack::where('trip_id', $request->TripID)->first();
            if ($checkTripStep) {
                $StepPackId = $checkTripStep->step_pack_id;
            } else {
                $Trip = Trip::find($request->TripID);
                $steppacK = new StepPack;
                $steppacK->title = $Trip->title;
                $steppacK->travel_agency_id = \Auth::user()->user_type == 'super_admin' ? $request->travel_agency_id : \Auth::user()->userType->travel_agency_id;
                $steppacKID = $steppacK->save();

                if ($steppacKID) {
                    $StepPackId = $steppacK->id;
                    $tripsteppack = new TripStepPack;
                    $tripsteppack->trip_id = $Trip->id;
                    $tripsteppack->step_pack_id = $StepPackId;
                    $tripsteppackID = $tripsteppack->save();
                }

            }
            foreach (json_decode($request->value) as $step => $StepValue) {
                $StepVideoName = null;
                if ($request->hasFile('StepVideo' . $step . '')) {
                    $StepVideoName = $request->file('StepVideo' . $step . '')->getClientOriginalName();
                    $StepVideoName = trim($StepVideoName, " ");
                    $StepVideoName = time() . '_' . $StepVideoName;

                    $path = Storage::disk('public')->putFileAs(
                        'StepViodes', $request->file('StepVideo' . $step . ''), $StepVideoName
                    );
                } else {
                    $StepVideoName = null;
                }
                $updateSteps = Step::where('id', $StepValue[0])->where('step_pack_id', $StepPackId)->first();
                if ($updateSteps) {
                    if ($StepVideoName == null) {
                        $updateSteps->title = $StepValue[1];
                        $updateSteps->sort = $StepValue[2];
                        $stepsID = $updateSteps->save();
                        if ($stepsID) {
                            $StepId = $updateSteps->id;
                            $status = 1;
                            $errMsg = "Data Saved successfully!";
                        }
                    } else {
                        $updateSteps->title = $StepValue[1];
                        $updateSteps->sort = $StepValue[2];
                        $updateSteps->last_video_path = $StepVideoName;
                        $stepsID = $updateSteps->save();
                        if ($stepsID) {
                            $StepId = $updateSteps->id;
                            $status = 1;
                            $errMsg = "Data Saved successfully!";
                        }
                    }
                } else {
                    $steps = new Step;
                    if ($StepVideoName == null) {
                        $steps->step_pack_id = $StepPackId;
                        $steps->title = $StepValue[1];
                        $steps->sort = $StepValue[2];
                        $stepsID = $steps->save();
                        if ($stepsID) {
                            $StepId = $steps->id;
                            $status = 1;
                            $errMsg = "Data Saved successfully!";
                        }
                    } else {
                        $steps->step_pack_id = $StepPackId;
                        $steps->title = $StepValue[1];
                        $steps->sort = $StepValue[2];
                        $steps->last_video_path = $StepVideoName;
                        $stepsID = $steps->save();
                        if ($stepsID) {
                            $StepId = $steps->id;
                            $status = 1;
                            $errMsg = "Data Saved successfully!";
                        }
                    }
                    if ($currentstepchange == 0) {
                        $getClientsTrip = ClientTrip::where('trip_id', $request->TripID)->get();
                        foreach ($getClientsTrip as $key => $clienttrip) {
                            $getAllClient = GroupClientTrip::where('client_trip_id', $clienttrip->id)->get();
                            foreach ($getAllClient as $key => $clientdata) {
                                $getStep = ClientTripStep::where('client_trip_id', $clientdata)->first();
                                if ($getStep) {
                                    $currentstepchange = 1;
                                } else {
                                    $clienttripstep = new ClientTripStep;
                                    $clienttripstep->client_trip_id = $clientdata->id;
                                    $clienttripstep->step_id = $StepId;
                                    $clienttripstep->status = 'current';
                                    $clienttripstepId = $clienttripstep->save();
                                    if ($clienttripstepId) {
                                        $currentstepchange = 1;
                                    }
                                }

                            }
                        }
                    }
                }
                foreach ($StepValue[3] as $key1 => $SectionValue) {
                    $imageName = null;
                    $image = 'image' . $step . '' . $key1 . '';
                    if ($request->hasFile($image)) {
                        $imageName = $request->file($image)->getClientOriginalName();
                        $imageName = time() . '_' . $imageName;

                        $path = Storage::disk('public')->putFileAs(
                            'StepImages', $request->file($image), $imageName
                        );
                    } else {
                        $imageName = null;
                    }
                    $checkSection = StepSection::where('id', $SectionValue[0])->where('step_id', $StepId)->first();
                    if ($checkSection) {
                        if ($imageName == null) {
                            $checkSection->title = $SectionValue[1];
                            $checkSection->description = $SectionValue[2];
                            $checkSection->information = $SectionValue[3];
                            $SectionsID = $checkSection->save();
                            if ($SectionsID) {
                                $SectionId = $checkSection->id;
                                $status = 1;
                                $errMsg = "Data Saved successfully!";
                            }
                        } else {
                            //return $imageName;
                            $checkSection->title = $SectionValue[1];
                            $checkSection->description = $SectionValue[2];
                            $checkSection->information = $SectionValue[3];
                            $checkSection->path = $imageName;
                            $SectionsID = $checkSection->save();
                            if ($SectionsID) {
                                $SectionId = $checkSection->id;
                                $status = 1;
                                $errMsg = "Data Saved successfully!";
                            }
                        }

                    } else {
                        if ($imageName == null) {
                            $Sections = new StepSection;
                            $Sections->step_id = $StepId;
                            $Sections->title = $SectionValue[1];
                            $Sections->description = $SectionValue[2];
                            $Sections->information = $SectionValue[3];
                            $SectionsID = $Sections->save();
                            if ($SectionsID) {
                                $SectionId = $Sections->id;
                                $status = 1;
                                $errMsg = "Data Saved successfully!";
                            }
                        } else {
                            $Sections = new StepSection;
                            $Sections->step_id = $StepId;
                            $Sections->title = $SectionValue[1];
                            $Sections->description = $SectionValue[2];
                            $Sections->information = $SectionValue[3];
                            $Sections->path = $imageName;
                            $SectionsID = $Sections->save();
                            if ($SectionsID) {
                                $SectionId = $Sections->id;
                                $status = 1;
                                $errMsg = "Data Saved successfully!";
                            }
                        }
                    }

                    foreach ($SectionValue[5] as $key => $CheckListValue) {
                        $checkListData = StepSectionCheckList::where('id', $CheckListValue[0])->where('step_section_id', $SectionId)->first();
                        if ($checkListData) {
                            $checkListData->description = $CheckListValue[1];
                            $CheckListID = $checkListData->save();
                            if ($CheckListID) {
                                $status = 1;
                                $errMsg = "Data Saved successfully!";
                            }
                        } else {
                            $CheckList = new StepSectionCheckList;
                            $CheckList->step_section_id = $SectionId;
                            $CheckList->description = $CheckListValue[1];
                            $CheckListID = $CheckList->save();
                            if ($CheckListID) {
                                $status = 1;
                                $errMsg = "Data Saved successfully!";
                            }
                        }
                    }
                }
            }
        } else {
            $status = 0;
            $errMsg = "Please add atleast one step!";
        }
        return response()->json(['status' => $status, 'errMsg' => $errMsg]);
    }

    public function deletechecklist(Request $request)
    {
        $status = false;
        $getChecklist = StepSectionCheckList::find($request->deletedata);
        if ($getChecklist) {
            $getChecklistid = $getChecklist->delete();
            if ($getChecklistid) {
                $status = true;
            }
        } else {
            $status = false;
        }

        return response()->json(['status' => $status]);
    }

    public function deletestep(Request $request)
    {
        $status = 0;
        $getStep = Step::where('id', $request->deletedata)->first();
        $getTotalSteps = Step::where('step_pack_id', $getStep->step_pack_id)->count();
        if ($getTotalSteps > 1) {
            $deleteSection = StepSection::where('step_id', $getStep->id)->get();
            if ($deleteSection) {
                foreach ($deleteSection as $key => $value) {
                    $deletechecklistdata = StepSectionCheckList::where('step_section_id', $value->id)->get();
                    if ($deletechecklistdata) {
                        foreach ($deletechecklistdata as $key => $value) {
                            $deletechecklistdataid = $value->delete();
                            if ($deletechecklistdataid) {
                                $status = 1;
                            }
                        }

                    }
                    $deleteSectionid = $value->delete();
                    if ($deleteSectionid) {
                        $status = 1;
                    }
                }
            }
            $getStepid = $getStep->delete();
            if ($getStepid) {
                $status = 1;
            }
        } else {
            $status = -1;
        }
        return response()->json(['status' => $status]);
    }
    public function deletestepVideo(Request $request)
    {
        $status = 0;
        $getStep = Step::where('id', (int)$request->deletedata)->first();

        $getStep->last_video_path = null;
        $getStep->save();

        $status = 1;

        return response()->json(['status' => $status]);
    }

    public function loadStepPackSteps(Request $request)
    {
        $step_pack = StepPack::findOrFail($request->step_pack_id);
        $trip_id = $request->trip_id;

        return view('trip.partial.multiple_step', compact('step_pack', 'trip_id'));
    }

    public function storechangestep(Request $request)
    {

        $status = '';
        $errMsg = '';
        if ($request->mainclientcheckboxes != null) {
            foreach ($request->mainclientcheckboxes as $key => $value) {
                $checkClient = ClientTripStep::where('client_trip_id', $value)->first();
                if ($checkClient) {
                    $checkClient->step_id = $request->Step;
                    $checkClient->status = 'current';
                    $checkClientId = $checkClient->save();
                    if ($checkClientId) {
                        $status = 'success';
                        $errMsg = 'Step Change successfully';
                        $getclientInfo = GroupClientTrip::with(['client', 'client.user', 'clienttrip'])->where('id', $value)->first();
                        $trip_update = array(
                            'client_trip_group_id' => $getclientInfo->id,
                            'message' => 'Step Change Successfully.',
                            'trip_id' => $getclientInfo->clienttrip->trip_id
                        );
                        $body = array('type' => "trip_update", 'data' => $trip_update);
                        Utils::sendNotification($getclientInfo->client->user['fcm_token'], $body);
                    }
                } else {
                    $clienttripstep = new ClientTripStep;
                    $clienttripstep->client_trip_id = $value;
                    $clienttripstep->step_id = $request->Step;
                    $clienttripstep->status = 'current';
                    $clienttripstepId = $clienttripstep->save();
                    if ($clienttripstepId) {
                        $status = 'success';
                        $errMsg = 'Step Change successfully';
                        $getclientInfo = GroupClientTrip::with(['client', 'client.user', 'clienttrip'])->where('id', $value)->first();
                        $trip_update = array(
                            'client_trip_group_id' => $getclientInfo->id,
                            'message' => 'Step Change Successfully.',
                            'trip_id' => $getclientInfo->clienttrip->trip_id
                        );
                        $body = array('type' => "trip_update", 'data' => $trip_update);
                        Utils::sendNotification($getclientInfo->client->user['fcm_token'], $body);
                        // $emailJob = new SendEmailJob($getclientInfo->client->user->email,auth()->user()->email);
                        //dispatch($emailJob);
                    }
                }
            }
        } else {
            $status = 'error';
            $errMsg = 'Please assign client and then save';
        }


        return redirect()->route('changestep', Session::get('TripID'))->with(['status' => $status, 'message' => $errMsg]);
    }

    public function viewassignclient($id)
    {
        $trips = ClientTripStep::with(array('clientTrip' => function ($q) {
            $q->with(array('client' => function ($q) {
                $q->with('client_category');
            }))->with(array('clienttrip' => function ($q) {
                $q->with('group')->with('trip');
            }));

        }))->where('step_id', $id)->get();
        $tripsDetail = $trips;
        return view('trip.viewassignclient', compact('trips', 'tripsDetail'));
    }

    public function viewTripByCustomerID(Request $request)
    {
        //return $request;
        $id = $request->client_id;
        $trips = ClientTripStep::whereHas('clientTrip', function ($q) use ($id) {
            $q->whereHas('client', function ($q) use ($id) {
                $q->where('id', $id);
            });
        })->with(array('clientTrip' => function ($q) {
            $q->with(array('client' => function ($q) {
                $q->with('client_category');
            }))->with(array('clienttrip' => function ($q) {
                $q->with('group')->with('trip');
            }));
        }))->where('step_id', $request->step_id)->get();
        // return $trips;
        $tripsDetail = ClientTripStep::with(array('clientTrip' => function ($q) {
            $q->with(array('client' => function ($q) {
                $q->with('client_category');
            }))->with(array('clienttrip' => function ($q) {
                $q->with('group')->with('trip');
            }));

        }))->where('step_id', $request->step_id)->get();
        // return $trips;
        return view('trip.viewassignclient', compact('trips', 'tripsDetail'));
    }

    public function destroy($id)
    {
        $clientTrip = ClientTripStep::where('id', $id)->first();
        if ($clientTrip) {
            $clienttripid = $clientTrip->delete();
            if ($clienttripid) {
                return redirect()->route('changestep.viewassignclient', $clientTrip->step_id)
                    ->with([
                        'status' => 'success',
                        'message' => 'Client Successfully Deleted'
                    ]);
            } else {
                return redirect()->route('changestep.viewassignclient', $clientTrip->step_id)
                    ->with([
                        'status' => 'error',
                        'message' => 'Client Can not Delete'
                    ]);
            }
        } else {
            return redirect()->route('changestep.viewassignclient', $clientTrip->step_id)
                ->with([
                    'status' => 'error',
                    'message' => 'Client can not exist'
                ]);
        }

    }

    public function deletesection(Request $request)
    {
        //return $request;
        $status = false;
        $getChecksection = StepSection::find($request->deletedata);
        if ($getChecksection) {
            $getChecksectionid = $getChecksection->delete();
            if ($getChecksectionid) {
                $status = true;
            }
        } else {
            $status = false;
        }

        return response()->json(['status' => $status]);
    }
}
