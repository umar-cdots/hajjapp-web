<?php

namespace App\Http\Controllers;

use App\ClientCategory;
use Illuminate\Http\Request;

class ClientCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client_categories = ClientCategory::loggedTravelAgency()->get();

        return view('client_category.index', compact('client_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('client_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string'
        ]);

        ClientCategory::create([
           'title' => $request->title,
            'travel_agency_id' => Auth()->user()->userType->travelAgency->id
        ]);

        return redirect()->route('client_categories.index')->with(['status' => 'success', 'message' => 'Client category added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client_category = ClientCategory::findOrFail($id);

        return view('client_category.show', compact('client_category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client_category = ClientCategory::findOrFail($id);

        return view('client_category.edit', compact('client_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|string'
        ]);
        $client_category = ClientCategory::findOrFail($id);
        $client_category->update([
            'title' => $request->title
        ]);
        
        return back()->with(['status' => 'success', 'message' => 'Client category updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client_category = ClientCategory::findOrFail($id);
        $client_category->delete();

        return back()->with(['status' => 'success', 'message' => 'Client category deleted.']);
    }
}
