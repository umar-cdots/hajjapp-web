<?php

namespace App\Http\Controllers;

use App\Mail\EventEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{

    public function store(Request $request)
    {

        $details = [
            'greeting' => 'Hi ' . $request->first_name . ' ' . $request->last_name,
            'company_name' => $request->company_name,
            'phone_number' => $request->phone_number,
            'country' => $request->country,
            'email' => $request->email,
            'thanks' => 'Thank you for using Ahlan Guide',
//            'from' => $request->email
            'from' => 'mnabeel992@gmail.com'

        ];
        Mail::to('yasir.naveed@gmail.com')->send(new EventEmail($details));
        return redirect()->route('welcome.index')->with([
            'flash_status' => 'success',
            'flash_message' => 'Your message has been sent.'
        ]);
    }

}
