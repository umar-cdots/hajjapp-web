<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function changePassword(Request $request){
        $this->validate($request, [
            'old_password' => ['required', 'string'],
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        ],
        [
            'password.required' => 'Password is required',
            'password.regex' => 'Password must contain at least one number and both uppercase and lowercase letters.'
        ]);

        $user = User::findOrFail(Auth()->user()->id);

        if(Hash::check($request->old_password, $user->password)){
            $user->update([
                //'name' => $request->name,
                'password' => Hash::make($request->password)
            ]);

            return back()->with([
                'status' => 'success',
                'message' => 'Password has been changed.'
            ]);
        }else{
            return back()->with([
                'status' => 'success',
                'message' => 'Old password is not correct'
            ]);
        }

    }
}
