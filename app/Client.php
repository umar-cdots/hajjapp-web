<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Client extends Model
{
    use SoftDeletes;
	/**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
	protected $guarded = [];
    protected $casts = [
        'dob' => 'date',
    ];

    public function setDobAttribute($value){
        $this->attributes['dob'] = date('Y-m-d', strtotime($value));
    }
    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }

   // protected static function boot()
    //{
      //  parent::boot();

      //  static::addGlobalScope('id', function (Builder $builder) {
       //     $builder->orderBy('id', 'DESC');
       // });
    //}

   
    //public function trips()
    //{
      //  return $this->morphToMany(Trip::class, 'client_trips');
    //}

    //public function clientTrips()
    //{
     //   return $this->hasMany(ClientTrip::class);
    //}

     //public function getPicturePathAttribute($path)
     //{
       //  return asset(str_replace('public/', 'storage/', $path));
     //}

    public function clienttrips()
    {
        return $this->belongsToMany(ClientTrip::class, 'group_client_trips')->withPivot('id');
    }

    public function getFullName(){
        return $this->name;
    }

     public function nationality(){
        return $this->belongsTo(nationality::class, 'nationality_id', 'id');
    }

    public function client_category(){
        return $this->belongsTo(ClientCategory::class, 'client_category_id', 'id');
    }

    public function sub_client_category(){
        return $this->belongsTo(ClientSubCategory::class, 'sub_client_category_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
