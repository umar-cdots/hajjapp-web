<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupClientTrip extends Model
{
    //
    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function clienttrip()
    {
        return $this->belongsTo(ClientTrip::class,'client_trip_id');
    }

    public function flightdetail()
    {
        return $this->belongsToMany(Flight::class, 'client_trip_airlines');
    }

    public function hotelInfo()
    {
        return $this->belongsToMany(TripHotel::class, 'client_trip_hotels');
    }

     public function tripUpdates()
    {
        return $this->hasMany(TripUpdate::class);
    }

     public function feedback()
    {
        return $this->hasMany(Feedback::class);
    }

    public function tripClientMessages(){
        return $this->hasMany(ClientTripMessage::class, 'client_trip_id', 'id');
    }

    public function emergency(){
        return $this->hasMany(EmergencyAlert::class,'client_trip_id','id');
    }

    //public function tripAirlines()
    //{
      //  return $this->belongsToMany(Airline::class, 'client_trip_airlines', 'client_trip_id', 'airline_id')
        //    ->withPivot(['from_airport_id','to_airport_id','departure_datetime', 'arrival_datetime', 'pnr', 'status']);
    //}
}
