<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSubCategory extends Model
{
    protected $table = 'client_sub_categories';
    protected $guarded = [];

    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }
    
    public function clients(){
        return $this->hasMany(Client::class, 'sub_client_category_id', 'id');
    }
    public function client_category(){
    	return $this->belongsTo(ClientCategory::class, 'client_category_id', 'id');
    } 
}
