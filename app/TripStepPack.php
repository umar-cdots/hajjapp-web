<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripStepPack extends Model
{
    //
    public function steppack()
    {
        return $this->belongsTo(StepPack::class,'step_pack_id');
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class,'trip_id');
    }
}
