<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Camroncade\Timezone\Facades\Timezone;
use Carbon\Carbon;
class EmergencyAlert extends Model
{
    protected $guarded = [];
	/**
     * Scope a query to only include logged in users trips.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLoggedTravelAgency($query)
    {
    	if(\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
        	return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }

    public function clientgtouptrip(){
        return $this->belongsTo(GroupClientTrip::class,'client_trip_id');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }
}
