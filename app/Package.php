<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $guarded = [];
    protected $table = "packages";

    public function setStartDateAttribute($value){

        $this->attributes['start_date'] = date('Y-m-d', strtotime($value));
    }

    public function setExpiryDateAttribute($value){

        $this->attributes['expiry_date'] = date('Y-m-d', strtotime($value));
    }
}
