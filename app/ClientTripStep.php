<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientTripStep extends Model
{
    protected $guarded = [];
    public function step()
    {
    	return $this->belongsTo(Step::class);
    }
    public function clientTrip()
    {
        return $this->belongsTo(GroupClientTrip::class);
    }
}
