<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use Illuminate\Support\Facades\Auth;
Use Exception;
Use Auth;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    protected $clientdata;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    // public function __construct($data)
    // {
    //     $this->data=$data;
    // }

    public function __construct($email,$clientdata)
    {
      $this->email =$email;
      $this->clientdata =$clientdata;
   
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try{
            //return $this->from($this->email)->subject('Ahlan Guide')->view('email.email_body')->with(['clientdata'=>$this->clientdata]);
            return $this->from('mnabeel992@gmail.com')->subject('Ahlan Guide')->view('email.email_body')->with(['clientdata'=>$this->clientdata]);
         }
        catch(Exception $e)
        {
            dd("error" +    $e->getMessage());
        }
       
    }
}
