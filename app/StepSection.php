<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


class StepSection extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('id', function (Builder $builder) {
            $builder->orderBy('id', 'ASC');
        });
    }

    public function stepCheckLists()
    {
    	return $this->hasMany(StepSectionCheckList::class);
    }
}
