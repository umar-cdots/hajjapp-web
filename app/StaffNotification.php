<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class StaffNotification extends Model
{
    protected $guarded = [];

    public function staffinfo()
    {
    	return $this->belongsTo(Staff::class,'staff_id');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }
}

