<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StepPack extends Model
{
    protected $guarded = [];
    /**
     * Scope a query to only include logged in users trips.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLoggedTravelAgency($query)
    {
    	if(\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
        	return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }
    
    public function steps()
    {
    	return $this->hasMany(Step::class);
    }

    public function travelAgency()
    {
        return $this->belongsTo(TravelAgency::class);
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    public function tripsteppack()
    {
        return $this->belongsToMany(Trip::class, 'trip_step_packs','trip_id','step_pack_id');
    }
    
}
