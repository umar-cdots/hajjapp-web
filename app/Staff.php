<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
	use SoftDeletes;
    protected $guarded = [];
	protected $table = 'staffs';
    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }
    //public function getImageAttribute($path)
    //{
      //  return asset(str_replace('public/', 'storage/', $path));
    //}
    public function user(){
    	return $this->belongsTo(User::class);
    }

     public function StaffNotificationData()
    {
        return $this->hasMany(StaffNotification::class);
    }
}
