<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ClientTripHotel extends Model
{
    protected $guarded = [];


   public function triphoteldetail()
    {
        return $this->belongsTo(TripHotel::class,'trip_hotel_id');
    }

    public function clientTrip()
    {
        return $this->belongsTo(GroupClientTrip::class);
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }
}
