<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackCategory extends Model
{
    protected $table = 'feedback_categories';
    protected $guarded = [];
    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }
    public function Feedbacks()
    {
    	return $this->hasMany(Feedback::class);
    }
}
