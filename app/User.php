<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_type', 'status', 'fcm_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function client(){
        return $this->hasOne(Client::class);
    }

    public function agent(){
        return $this->hasOne(Agent::class, 'user_id', 'id');
    }

    public function userType()
    {
        if($this->user_type == 'agent')
            return $this->hasOne(Agent::class);
        elseif ($this->user_type == 'staff')
            return $this->hasOne(Staff::class);
        elseif ($this->user_type == 'mobile_agent')
            return $this->hasOne(Staff::class);
        elseif($this->user_type == 'client')
            return $this->hasOne(Client::class);
    }

    public function staff(){
        return $this->hasOne(Staff::class, 'user_id' ,'id');
    }
}
