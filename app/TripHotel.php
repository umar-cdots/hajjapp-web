<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripHotel extends Model
{
	public function setstayfromAttribute($value){
        $this->attributes['stay_from'] = date('Y-m-d', strtotime($value));
    }
    public function setstaytoAttribute($value){
        $this->attributes['stay_to'] = date('Y-m-d', strtotime($value));
    }
    //
    public function hotel()
    {
        return $this->belongsTo(Hotel::class,'hotel_id');
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id');
    }

    public function clientgroup()
    {
        return $this->belongsToMany(GroupClient::class, 'client_trip_hotels');
    }

    public function scopeAllRelationsData()
    {
    	return $this->with(['hotel', 'hotel.city', 'hotel.state', 'hotel.country']);
    }
}
