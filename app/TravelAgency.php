<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelAgency extends Model
{

    public function hotels()
    {
    	return $this->hasMany(Hotel::class);
    }

    public function airlines()
    {
    	return $this->hasMany(Airline::class);
    }

    public function package()
    {
    	return $this->belongsTo(Package::class);
    }

    public function agent()
    {
        return $this->hasOne(Agent::class);
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'travel_agency_client');
    }

    public function stepPack(){
        return $this->hasMany(StepPack::class, 'travel_agency_id', 'id');
    }

    //public function getPathAttribute($path)
    //{
      //  return asset(str_replace('public/', 'storage/', $path));
    //}
}
