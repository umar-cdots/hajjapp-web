<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\SendMail;
use App\GroupClientTrip;
use Mail;
Use Exception;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $getEmail;
    public $authEmail;
    public $clientdata;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($getclientInfo,$authEmail,$clientdata)
    {
        $this->getEmail=$getclientInfo;
        $this->authEmail =$authEmail;
        $this->clientdata=$clientdata;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try{
            
            $when = now()->addSeconds(1);
            $email = new SendMail($this->authEmail,$this->clientdata);
            Mail::to($this->getEmail)->later($when,$email);
        }
        catch(Exception $e)
        {
            dd($e->getMessage());
        }




        // $data    = array('name' => Auth()->user()->name);
        // Mail::to($this->email)->send(new SendMail($data));
    }
}
