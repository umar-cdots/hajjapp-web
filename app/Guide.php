<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
    protected $table = 'guides';
    protected $guarded = [];

    public function travel_agency(){
        return $this->belongsTo(TravelAgency::class, 'travel_agency_id', 'id');
    }
    public function trip(){
        return $this->belongsTo(Trip::class);
    }

    //public function getPathAttribute($path)
    //{
      //  return asset(str_replace('public/', 'storage/', $path));
    //}
}
