<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Airport extends Model
{
    protected $fillable = ['name', 'code', 'city_id', 'country_id'];

    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }
    
    public function city()
    {
    	return $this->belongsTo(City::class);
    }

    public function country()
    {
    	return $this->belongsTo(Country::class);
    }
    public function fromflight()
    {
        return $this->hasMany(Airport::class,'id','from_airport_id');
    }
    public function toflight()
    {
        return $this->hasMany(Airport::class,'id','to_airport_id');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }
}
