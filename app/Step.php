<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('id', function (Builder $builder) {
            $builder->orderBy('id', 'ASC');
        });
    }
    public function stepsection()
    {
    	return $this->hasMany(StepSection::class);
    }

    //public function getLastVideoPathAttribute($last_video_path)
    //{
      //  return asset(str_replace('public/', 'storage/', $last_video_path));
    //}
}
