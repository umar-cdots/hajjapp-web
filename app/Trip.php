<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trip extends Model
{

    use SoftDeletes;

    protected $guarded = [];

    /**
     * Scope a query to only include logged in users trips.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }

    public function travelAgency()
    {
        return $this->belongsTo(TravelAgency::class);
    }

    // public function tripSteps()
    // {
    // 	return $this->belongsToMany(Step::class, 'trip_steps', 'trip_id', 'step_id')
    // 					->withPivot('sort')
    // 						->orderBy('pivot_sort', 'ASC');
    // }

    public function tripGuide()
    {
        return $this->hasMany(Guide::class,'trip_id','id');
    }

   

   // public function stepPack()
   // {
     //   return $this->belongsTo(StepPack::class);
    //}

   // public function clients()
    //{
     //   return $this->morphedByMany(Client::class, 'client_trips');
    //}

    //public function groups()
    //{
      //  return $this->morphedByMany(Group::class, 'client_trips');
    //}
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'client_trips');
    }

    public function steppack()
    {
        return $this->belongsToMany(StepPack::class, 'trip_step_packs','step_pack_id','trip_id');
    }

    //public function getPathAttribute($path)
    //{
     //   return asset(str_replace('public/', 'storage/', $path));
    //}

    public function tripTasks(){
        return $this->hasMany(TripTask::class, 'trip_id', 'id');
    }
}
