<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class TripMessage extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function client(){
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function agent(){
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }

    public function trip(){
        return $this->belongsTo(Trip::class, 'trip_id', 'id');
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }
}
