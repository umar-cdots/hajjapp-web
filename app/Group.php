<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];

    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }
    public function trips()
   {
    	return $this->belongsToMany(Trip::class, 'client_trips');
    }
}
