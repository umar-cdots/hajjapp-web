<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ClientTrip extends Model
{
    protected $guarded = [];
    /**
     * Scope a query to only include logged in users trips.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLoggedInClient($query)
    {
    	if(\Auth::user()->user_type == 'client')
        	return $query->where('client_id', \Auth::user()->userType->id);
        return $query;
    }

    public function tripHotels()
    {
        return $this->belongsToMany(Hotel::class, 'client_trip_hotels', 'client_trip_id', 'hotel_id')
            ->withPivot(['stay_from', 'stay_to']);
    }

    public function tripSteps()
    {
        return $this->belongsToMany(Step::class, 'client_trip_steps', 'client_trip_id', 'step_id')
            ->withPivot('status');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class);
    }

    public function tripUpdates()
    {
        return $this->hasMany(TripUpdate::class);
    }
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'group_client_trips')->withPivot('id');
    }
}
