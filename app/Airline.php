<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Airline extends Model
{
    protected $guarded = [];

    public function scopeLoggedTravelAgency($query)
    {
        if (\Auth::user()->user_type == 'agent' || \Auth::user()->user_type == 'staff' || \Auth::user()->user_type == 'mobile_agent')
            return $query->where('travel_agency_id', \Auth::user()->userType->travel_agency_id);
        return $query;
    }

    public function travelAgency()
    {
    	return $this->belongsTo(TravelAgency::class);
    }
    public function country(){
    	return $this->belongsTo(Country::class);
    }

    public function state(){
    	return $this->belongsTo(State::class);
    }

    public function city(){
    	return $this->belongsTo(City::class);
    }
    public function flight()
    {
        return $this->hasMany(Flight::class,'id','airline_id');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->setTimezone(\Auth::user()->time_zone);
    }

}
