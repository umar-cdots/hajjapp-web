<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* Without Autentication Routes
*/
Route::post('v1/login', "API\V1\Client\LoginController@login")->name('login');

Route::post('v1/resetcheck', "API\V1\Client\LoginController@resetcheck")->name('resetcheck');

Route::post('v1/logout', "API\V1\Client\LoginController@logout")->middleware('auth:api')->name('logout');
Route::post('v1/forgot/password', "Auth\ForgotPasswordController@sendResetLinkEmail")->name('forgot.password');

Route::prefix('v1/client')->name('api.client.')->namespace('API\V1\Client')->group(function(){

});

/*
* With Autentication Routes
*/
Route::middleware('auth:api')->prefix('v1/client')->name('api.client.')->namespace('API\V1\Client')->group(function(){

	Route::get('latest_trip', "TripController@latest")->name('latest_trip');
	Route::get('feedback_categories', "FeedbackCategoryController@index")->name('feedback_categories');
	Route::post('fcm', "LoginController@registerFCM");
	Route::post('update', "LoginController@update");
	Route::post('dp', "LoginController@displayPhoto");

	Route::prefix('trip')->name('trip.')->group(function(){
		Route::get('/', "TripController@index")->name('index');
		Route::get('/{id}', "TripController@show")->name('show');
	});

	Route::prefix('message')->name('message.')->group(function(){
		Route::get('/{trip_id}', "MessageController@index")->name('index');
		Route::post('/', "MessageController@store")->name('store');
		Route::delete('/{id}', "MessageController@destroy")->name('destroy');
	});

	Route::prefix('feedback')->name('feedback.')->group(function(){
		// Route::get('/{client_trip_id}', "FeedbackController@index")->name('index');
		Route::post('/{client_trip_id}', "FeedbackController@store")->name('store');
		// Route::put('/{id}', "FeedbackController@update")->name('update');
		// Route::delete('/{id}', "FeedbackController@destroy")->name('destroy');
	});

	Route::prefix('emergency_alert')->name('emergency_alert.')->group(function(){
		// Route::get('/{client_trip_id}', "EmergencyAlertController@index")->name('index');
		Route::post('/{client_trip_id}', "EmergencyAlertController@store")->name('store');
		// Route::put('/{id}', "EmergencyAlertController@update")->name('update');
		// Route::delete('/{id}', "EmergencyAlertController@destroy")->name('delete');
	});

});




Route::prefix('v1/admin')->name('api.admin.')->namespace('API\V1\Admin')->group(function(){
	Route::post('/login', "LoginController@login")->name('api.admin.login');
});

Route::middleware(['auth:api','web'])->prefix('v1/admin')->name('api.admin.')->namespace('API\V1\Admin')->group(function(){
	Route::post('fcm', "LoginController@registerFCM");
	Route::post('logout', "LoginController@logout")->name('logout');
});