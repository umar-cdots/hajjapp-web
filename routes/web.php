<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('email-test', function(){

//$details ='azeemulrehman71@gmail.com';
// $details['email'] =['azeemulrehman71@gmail.com','jeremiah@creative-dots.com','azeem.devsleague@gmail.com',
//     'yasin@creative-dots.com',
//     'ahmer@creative-dots.com',
//     'adilmughal@creative-dots.com',
//     'nabeel@creative-dots.com',
//     'alihusnain@creative-dots.com'];
// dispatch(new App\Jobs\SendEmailJob($details));
//  dd('done');
//});

Route::get('/', function () {
    return view('welcome');
//    return view('auth.login');
})->name('welcome.index');


Auth::routes();
Route::middleware(['auth', 'disablepreventback'])->group(function () {
    Route::get('/message', 'DashboardController@message');
    Route::get('/LicenseExpriy', 'DashboardController@LicenseExpriy');
    Route::get('/home', 'DashboardController@index')->name('homepage');
    Route::get('/fcm/{id}', "DashboardController@registerFCM");
    Route::post('/SaveImageData', 'DashboardController@SaveImageData')->name('SaveImageData');
    Route::post('/ShowAdminImage', 'DashboardController@ShowAdminImage')->name('ShowAdminImage');
    Route::post('/useForSession', 'DashboardController@useForSession')->name('useForSession');
    Route::post('/usefortimezone', 'DashboardController@usefortimezone')->name('usefortimezone');
    //Route for Task New
    Route::get('staff/tasks', 'StaffTaskController@index')->name('staff.tasks.index');
    Route::post('staff/tasks', 'StaffTaskController@completeTask')->name('staff.task.complete');
});

Route::prefix('admin')->middleware(['auth', 'disablepreventback'])->group(function () {
    Route::resource('trips', 'TripController');
    Route::post('trips/statuscheck', 'TripController@statuscheck')->name('trips.statuscheck');
    Route::post('trips/search', 'TripController@search')->name('trips.search');
    Route::get('trips/staffdetail/{id}', 'TripController@staffdetail')->name('trips.staffdetail');
    Route::get('trips/customertripdetail/{id}', 'TripController@customertripdetail')->name('trips.customertripdetail');

    Route::post('/trips/ChangeTripQibla', 'TripController@ChangeTripQibla')->name('trips.ChangeTripQibla');

    Route::resource('travel_agency', 'TravelAgencyController');
    Route::resource('packages', 'PackagesController');
    Route::resource('step_pack', 'StepPackController');
    Route::resource('client_categories', 'ClientCategoryController');
    Route::resource('clients', 'ClientController');
    Route::post('/clients/search', 'ClientController@search')->name('clients.search');

    Route::get('/clients/showprofile/{id}', 'ClientController@showprofile')->name('clients.showprofile');

    Route::get('/clients/clientalert/{id}', 'ClientController@clientalert')->name('clients.clientalert');

    Route::get('/clients/clientcomplaint/{id}', 'ClientController@clientcomplaint')->name('clients.clientcomplaint');

    Route::post('/clients/ChangeClientStatus', 'ClientController@ChangeClientStatus')->name('clients.ChangeClientStatus');
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    //Routes for user settings
    Route::post('/change/password', 'UserController@changePassword')->name('change.password');

    //Route for feedback categories
    Route::resource('/feedback/categories', 'FeedbackCategoriesController', ['names' => [
        'index' => 'feedback.categories.index',
        'create' => 'feedback.categories.create',
        'store' => 'feedback.categories.store',
        'edit' => 'feedback.categories.edit',
        'update' => 'feedback.categories.update',
        'destroy' => 'feedback.categories.destroy',
    ]
    ]);

    Route::resource('/countries', 'CountriesController', ['names' => [
        'index' => 'countries.index',
        'create' => 'countries.create',
        'store' => 'countries.store',
        'edit' => 'countries.edit',
        'update' => 'countries.update',
        'destroy' => 'countries.destroy',
    ]
    ]);

    Route::resource('/hotels', 'HotelController', ['names' => [
        'index' => 'hotels.index',
        'create' => 'hotels.create',
        'store' => 'hotels.store',
        'edit' => 'hotels.edit',
        'update' => 'hotels.update',
        'destroy' => 'hotels.destroy',
    ]
    ]);

    Route::get('/change_password/changepassword', 'ChangePasswordController@changepassword')->name('changepassword');

    Route::post('change_password/clientpassword', 'ChangePasswordController@clientpassword')->name('clientpassword');

    Route::post('change_password/staffpassword', 'ChangePasswordController@staffpassword')->name('staffpassword');

    //Route for feedback
    Route::get('/feedbacks', 'FeedbackController@index')->name('feedback.index');

    //Route for guide
    Route::get('/guides', 'GuideController@index')->name('guide.index');
    Route::get('/guide/create', 'GuideController@create')->name('guide.create');
    Route::post('/guide', 'GuideController@store')->name('guide.store');
    Route::delete('/guide/{guide}', 'GuideController@destroy')->name('guide.destroy');

    //Route For Client Sub Category
    Route::resource('client_sub_categories', 'ClientSubCategoryController');


    //Routes for Emergency Alerts
    Route::prefix('emergency_alert')->name('emergency_alert.')->group(function () {
        Route::get('/{type?}', 'EmergencyAlertController@index')->name('index');
        Route::put('/{id}', 'EmergencyAlertController@update')->name('update')->where('id', '[0-9]+');
    });

    //Route for Trip step
    Route::resource('steps', 'StepController');

    Route::get('/trip/step_pack/{trip_id}', 'StepPackController@getStepPacks')->name('getStepPacks');


    Route::post('/trip/step_pack', 'TripStepController@assignStepPackToTrip')->name('trip.steppack');
    //assign customer
    Route::get('/assign/customer/{id}', 'TripCustomerController@index')->name('customer.trip.index');
    Route::post('/assign/customer', 'TripCustomerController@store')->name('customer.trip.store');
    Route::post('/assign/updategroup', 'TripCustomerController@updategroup')->name('customer.trip.updategroup');

    Route::post('/assign/customer/deleteclient', 'TripCustomerController@deleteclient')->name('customer.trip.deleteclient');
    Route::post('/assign/customer/deletegroup', 'TripCustomerController@deletegroup')->name('customer.trip.deletegroup');

    Route::post('/assign/customer/addnewgroup', 'TripCustomerController@addnewgroup')->name('addnewgroup');

    Route::post('/assign/customer/getallclient', 'TripCustomerController@getallclient')->name('getallclient');

    Route::post('/client_category/sub_categories', 'TripCustomerController@getSubCategoriesWithCategory')->name('get.subcategories.category');
    Route::post('/sub_categories/clients', 'TripCustomerController@getClientWithSubCategory')->name('get.client.subcategories');

    Route::get('trip/customers/{id}', 'TripCustomerController@viewTripCustomer')->name('view.trip.customer');

    Route::post('trip/customers/customerbyid', 'TripCustomerController@viewTripByCustomerID')->name('view.trip.customer.customerbyid');

    Route::get('/trip/customer/current/step', 'TripCustomerController@changeCurrentStep')->name('change.customer.current.step');
    Route::post('/trip/all_customer/current/step', 'TripCustomerController@changeAllCustomerStep')->name('change.all.customer.current.step');
    Route::get('trip/customer/remove/{trip}', 'TripCustomerController@removeCustomer')->name('remove.customer.index');
    Route::post('trip/customer/remove', 'TripCustomerController@removeTripCustomer')->name('remove.customer.remove');

    //Route for customer hotel
    Route::get('trip/customers/hotel/{trip}', 'TripHotelController@viewTripCustomerHotel')->name('view.trip.customer.hotel');
    Route::post('trip/hotel', 'TripHotelController@assignHotelToTrip')->name('assign.hotel.trip');

    Route::delete('/trip/hotel/hoteldestroy/{id}', 'TripHotelController@hoteldestroy')->name('trip.hotel.hoteldestroy');

    Route::post('/trip/hotel/deletehotelbyid', 'TripHotelController@deletehotelbyid')->name('trip.hotel.deletehotelbyid');

    Route::get('trip/hotel/viewassignhotelclient/{hotelId}', 'TripHotelController@viewassignhotelclient')->name('trip.hotel.viewassignhotelclient');

    Route::post('trip/hotel/viewHotelTripByCustomerID', 'TripHotelController@viewHotelTripByCustomerID')->name('view.trip.hotel.viewHotelTripByCustomerID');

    Route::post('trip/customer/hotel', 'TripHotelController@assignHotelToSingleCustomer')->name('assign.hotel.single.customer');


    //Route::get('trip/test/{trip}', 'TripHotelController@test');

    //send push notification
    Route::get('/push_notification/{trip}', 'TripPushNotificationController@index')->name('trip.notification.index');
    Route::post('/push_notification', 'TripPushNotificationController@store')->name('trip.notification.store');

    //upload client with excel sheet
    Route::get('/excelupload', 'TripPushNotificationController@loadExcelView')->name('load.excel.view');
    Route::post('/excelupload/store', 'TripPushNotificationController@storeExcelSheet')->name('load.excel.store');
    Route::post('/excelupload/storestaff', 'TripPushNotificationController@storeExcelSheetForStaff')->name('load.excel.storestaff');

    Route::get('/excelupload/downloadclient', 'TripPushNotificationController@downloadclient')->name('load.excel.downloadclient');

    Route::get('/excelupload/downloadstaff', 'TripPushNotificationController@downloadstaff')->name('load.excel.downloadstaff');

    //Route for messages
    Route::get('trip/client/messages', 'TripMessageController@index')->name('trip.client.message');

    Route::post('load/client/message', 'TripMessageController@loadClientMessages')->name('load.client.message');

    Route::post('load/client', 'TripMessageController@loadClient')->name('load.client');

    Route::post('trip/client/messages', 'TripMessageController@showmessage')->name('trip.client.showmessage');

    Route::post('/send/messages', 'TripMessageController@sendMessages')->name('send.message');

    //Route for Trip airline
    Route::get('trip/flight/{trip}', 'TripFlightController@index')->name('trip.flight');
    Route::post('/trip/flight/store', 'TripFlightController@addFlightForTrip')->name('trip.flight.store');

    Route::get('trip/flight/viewassignclient/{flightid}', 'TripFlightController@viewassignclient')->name('trip.flight.viewassignclient');

    Route::delete('/trip/flight/destroy/{id}', 'TripFlightController@destroy')->name('trip.flight.destroy');

    Route::post('trip/flight/customerbyid', 'TripFlightController@viewTripByCustomerID')->name('view.trip.flight.customerbyid');
    Route::post('/trip/flight/customer/store', 'TripFlightController@updateFlightForCustomer')->name('trip.flight.customer.store');
    Route::post('/trip/flight/deleteflightbyid', 'TripFlightController@deleteflightbyid')->name('trip.flight.deleteflightbyid');

    //Route for trip tasks
    //Route::get('trip/task/{trip}', 'TripTaskController@index')->name('trip.task');
    Route::get('/trip/task/index', 'TripTaskController@index')->name('trip.task.index');
    Route::get('/trip/task/create', 'TripTaskController@create')->name('trip.task.create');
    Route::post('/trip/task/store', 'TripTaskController@addTasks')->name('trip.task.store');
    Route::delete('/trip/task/destroy/{id}', 'TripTaskController@destroy')->name('trip.task.destroy');
    Route::get('/trip/task/edit/{id}', 'TripTaskController@edit')->name('trip.task.edit');
    Route::post('/trip/task/update', 'TripTaskController@update')->name('trip.task.update');
    Route::get('/trip/task/stafftaskoverduedetail/{id}', 'TripTaskController@stafftaskoverduedetail')->name('trip.task.stafftaskoverduedetail');
    Route::get('/trip/task/customertaskoverduedetail/{id}', 'TripTaskController@customertaskoverduedetail')->name('trip.task.customertaskoverduedetail');


    // Route::post('/trip/custom/step_pack', 'TripStepController@assignCustomStepPackToTrip')->name('trip.customStepPack.store');

    //Route for Step Section
    Route::get('/step_pack/custom/{trip_id}', 'StepPackController@customStepPack')->name('customStepPack');

    Route::get('/step_pack/changestep/{trip_id}', 'StepPackController@changestep')->name('changestep');

    Route::post('/step_pack/changestep/storechangestep', 'StepPackController@storechangestep')->name('changestep.storechangestep');

    Route::get('step_pack/changestep/viewassignclient/{stepid}', 'StepPackController@viewassignclient')->name('changestep.viewassignclient');

    Route::delete('step_pack/changestep/destroy/{id}', 'StepPackController@destroy')->name('changestep.destroy');

    Route::post('step_pack/changestep/customerbyid', 'StepPackController@viewTripByCustomerID')->name('changestep.customerbyid');

    Route::post('/step_pack/custom/GetAllSteps', 'StepPackController@GetAllSteps')->name('GetAllSteps');
    Route::post('/step_pack/custom/SearchAllSteps', 'StepPackController@SearchAllSteps')->name('SearchAllSteps');
    Route::post('/step_pack/custom/AddSteps', 'TripStepController@AddSteps')->name('AddSteps');

    Route::post('/step_pack/custom/create', 'TripStepController@addCustomStepPack')->name('steppack.add');
    Route::post('/step_pack/custom/steps', 'StepPackController@addStepToStepPack')->name('steppack.step.add');

    Route::post('/step_pack/custom/deletechecklist', 'StepPackController@deletechecklist')->name('steppack.step.deletechecklist');

    Route::post('/step_pack/custom/deletesection', 'StepPackController@deletesection')->name('steppack.step.deletesection');

    Route::post('/step_pack/custom/CheckTotalStep', 'StepPackController@CheckTotalStep')->name('steppack.step.CheckTotalStep');

    Route::post('/step_pack/custom/deletestep', 'StepPackController@deletestep')->name('steppack.step.deletestep');
    Route::post('/step_pack/custom/deletestepvideo', 'StepPackController@deletestepVideo')->name('steppack.step.deletestepvideo');
    Route::post('/step/pack/steps', 'StepPackController@loadStepPackSteps')->name('steppack.steps');

    //Rooutes for trip guides
    Route::get('/trip/{trip_id}/guides', 'TripGuideController@index')->name('trip.guide.index');
    //Route::post('/trip/guides', 'TripGuideController@store')->name('trip.guide.store');
    //Route::get('/guides', 'GuideController@index')->name('guide.index');
    Route::post('/trip/guide', 'TripGuideController@store')->name('trip.guide.store');
    Route::delete('/trip/guide/{guide}', 'TripGuideController@destroy')->name('trip.guide.destroy');


    Route::prefix('ajax')->name('ajax.')->group(function () {
        Route::get('loadStepPacks/{travel_agency_id}', 'StepPackController@stepPacksByTravelAgency')->name('loadStepPacks')->middleware('super_admin');
        Route::post('/load/states', 'AjaxController@loadStates')->name('load.states');
        Route::post('/load/cities', 'AjaxController@loadCities')->name('load.cities');

    });

    //Route for Confirm Password
    Route::get('change_password', 'ChangePasswordController@index');
    Route::post('change_password', 'ChangePasswordController@update');

    //Route for Airlines
    Route::resource('airlines', 'AirlineController');

    //Route for Airport
    Route::resource('airports', 'AirportController');

    //Route for Staff
    Route::resource('staffs', 'StaffController');
    Route::get('/staffs/viewprofile/{staff_id}', 'StaffController@viewprofile')->name('staffs.viewprofile');
    Route::post('/staffs/search', 'StaffController@search')->name('staffs.search');
    Route::post('/staffs/ChangeStaffStatus', 'StaffController@ChangeStaffStatus')->name('staffs.ChangeStaffStatus');


    Route::get('/section/create', function () {
        return view('section/create');
    });
    Route::get('/section/index', function () {
        return Auth()->user();
    });
    Route::get('/section/show', function () {
        return view('section/show');
    });
    Route::get('/section/edit', function () {
        return view('section/edit');
    });

});

Route::post('/contact-us', 'ContactUsController@store')->name('contact.us');


